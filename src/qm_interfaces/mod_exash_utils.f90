! Copyright (C) 2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_exash_utils
  !!
  !! - \(N_{chrom}\): Number of different chromophores included.
  !! - \(N_{state}(i)\): Number of *excited* states computed for chromophore \(i\).
  !!
  !! ``list_exc``:
  !!
  !! ```
  !! nchrom
  !! atom_start_chrom1  atom_end_chrom1
  !! atom_start_chrom2  atom_end_chrom2
  !! ...
  !! atom_start_chromN  atom_end_chromN
  !!
  !! LINK_ATOMS
  !!
  !! ```
  !!
  use mod_data_containers, only: &
       & mat2_container_t, mat3_container_t
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config, set_alloc => set_config_with_alloc
  use mod_kinds, only: dp
  use mod_logger, only: &
       & print_conf_ele, nx_log, LOG_DEBUG
  use mod_status_t, only: nx_status_t, NX_ERROR
  use mod_tools, only: to_str
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_exash_t

  type :: nx_exash_t
     private
     integer :: nchrom
     !! Number of chromophores included.
     integer, allocatable :: nstat_array(:)
     !! Array containing the number of *excited* states computed for each chromophore.
     integer, allocatable :: nat_array(:)
     !! Array containing the number of atoms of each chromophore.
     logical :: gs_ = .false.
     !! Toggle to compute only ground state information (default: ``.false.``).
     logical :: dip_ = .true.
     !! Toggle the computation of transition dipole moments (default: ``.true.``).
     integer :: nstat_tot = -1
     !! Total number of *excited* state included in the exciton Hamiltonian (sum of all
     !! elements of ``nstat_array``, included for convenience).
     real(dp), allocatable :: full_diab_ham_(:, :)
     !! Full (diabatic) exciton hamiltonian (including GS).
     real(dp), allocatable :: eigenval_(:)
     !! Eigenvalues of the exciton Hamiltonian.
     real(dp), allocatable :: eigenvectors_(:, :)
     !! Diabatic to adiabatic transformation matrix.
     real(dp), allocatable :: old_eigenvectors_(:, :)
     !! Diabatic to adiabatic transformation matrix for the previous time-step
     real(dp), allocatable :: diab_en_(:)
     !! Diabatic energies.
     real(dp), allocatable :: diab_pop_(:)
     !! Diabatic populations.
     real(dp), allocatable :: adiab_en_(:)
     !! Adiabatic energies.
     real(dp), allocatable :: adiab_grad_(:, :)
     !! Adiabatic gradients.
     real(dp), allocatable :: trdip_(:, :)
     !! Transition dipole moments.
     real(dp), allocatable :: osc_str_(:)
     !! Oscillator strengths.
     
   contains
     private
     procedure, public :: print => exash_print
     procedure, public :: to_str => exash_to_str
     procedure, public :: compute_epot_grad => exash_compute_adiab_epot_grad
     procedure, public :: compute_dipoles => exash_compute_dipoles
     procedure, public :: compute_ovl => exash_compute_overlap_matrix
     procedure, public :: write_ovl => exash_write_ovl_matrix
     procedure, public :: diab_en => get_diab_en
     procedure, public :: adiab_en => get_adiab_en
     procedure, public :: diab_pop => get_diab_pop
     procedure, public :: adiab_grad => get_adiab_grad
     procedure, public :: diab_ham => get_full_diab_ham
     procedure, public :: trdip => get_trdip
     procedure, public :: osc_str => get_osc_str
     procedure, public :: dip => get_dip
     procedure, public :: gs => get_gs

     procedure :: allocate => exash_allocate_memory
  end type nx_exash_t
  interface nx_exash_t
     module procedure :: constructor_from_elements
     module procedure :: constructor_from_parser
  end interface nx_exash_t

  character(len=*), parameter :: MODNAME = 'mod_exash_utils'

contains

  ! BD 2023-10-19: This function is not pure becasue its sister in the interface,
  ! `constructor_from_parser`, cannot be pure due to the current input parser
  ! implementation (see e.g. ``parser_get``, that cannot use pointer assignment if the
  ! procedure is pure).
  function constructor_from_elements(nchrom, nstat_array, nat_array, gs, dip) result(res)
    integer, intent(in) :: nchrom
    integer, intent(in) :: nstat_array(:)
    integer, intent(in) :: nat_array(:)
    logical, intent(in), optional :: gs
    logical, intent(in), optional :: dip

    type(nx_exash_t) :: res

    integer :: nstat_tot

    res%nchrom = nchrom

    allocate( res%nstat_array(nchrom) )
    res%nstat_array(:) = nstat_array(:)
    res%nstat_tot = sum( res%nstat_array(:) )
    call res%allocate()

    allocate( res%nat_array(nchrom) )
    res%nat_array(:) = nat_array(:)

    if (present(gs)) res%gs_ = gs
    if (present(dip)) res%dip_ = dip
  end function constructor_from_elements


  function constructor_from_parser(nchrom, parser, stat) result(res)
    integer, intent(in) :: nchrom
    type(parser_t), intent(in) :: parser
    type(nx_status_t), intent(inout) :: stat

    type(nx_exash_t) :: res

    logical :: exit_func

    res%nchrom = nchrom

    exit_func = .false.
    if (.not. parser%has_key('exc_inp', 'nat_array')) then
       call stat%append(NX_ERROR, &
            & 'Mandatory parameter "nat_array" not found in '//parser%filename// &
            & '. Please add it with the number of atoms for each chromophore !', &
            & mod=MODNAME, func='constructor_from_parser' &
            )
       exit_func = .true.
    end if

    if (.not. parser%has_key('exc_inp', 'nstat')) then
       call stat%append(NX_ERROR, &
            & 'Mandatory parameter "nstat_array" not found in '//parser%filename// &
            & '. Please add it with the number of excited staetes for each chromophore !', &
            & mod=MODNAME, func='constructor_from_parser' &
            )
       exit_func = .true.
    end if

    if (exit_func) return

    call set(parser, 'exc_inp', res%gs_, 'gs')
    call set(parser, 'exc_inp', res%dip_, 'dip')
    call set_alloc(parser, 'exc_inp', res%nat_array, 'nat_array')
    call set_alloc(parser, 'exc_inp', res%nstat_array, 'nstat')

    res%nstat_tot = sum( res%nstat_array(:) )
    call res%allocate()
  end function constructor_from_parser


  function exash_to_str(self) result(res)
    class(nx_exash_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&exc_inp'//nl
    res = res//' gs = '//to_str(self%gs_)//nl
    res = res//' dip = '//to_str(self%dip_)//nl
    res = res//' nat_array = '//to_str(self%nat_array)//nl
    res = res//' nstat_array = '//to_str(self%nstat_array)//nl
    res = res//'/'//nl
  end function exash_to_str
  


  subroutine exash_allocate_memory(self)
    class(nx_exash_t), intent(inout) :: self

    integer :: nstat_tot
    
    nstat_tot = self%nstat_tot
    allocate(self%full_diab_ham_( nstat_tot+1, nstat_tot+1 ))
    allocate(self%eigenvectors_( nstat_tot+1, nstat_tot+1 ))
    allocate(self%old_eigenvectors_( nstat_tot+1, nstat_tot+1 ))
    allocate(self%diab_en_( nstat_tot+1 ))
    allocate(self%diab_pop_( nstat_tot + 1 ))
    allocate(self%adiab_en_( nstat_tot+1 ))
    allocate(self%adiab_grad_( 3,  1))
    allocate(self%eigenval_(nstat_tot))
    allocate(self%trdip_( 3,  1))
    allocate(self%osc_str_(1))
    
    self%full_diab_ham_ = 0.0_dp
    self%eigenval_ = 0.0_dp
    self%eigenvectors_ = 0.0_dp
    self%old_eigenvectors_ = 0.0_dp
    self%diab_en_ = 0.0_dp
    self%diab_pop_ = 0.0_dp
    self%adiab_en_ = 0.0_dp
    self%adiab_grad_ = 0.0_dp
    self%trdip_ = 0.0_dp
    self%osc_str_ = 0.0_dp
  end subroutine exash_allocate_memory


  subroutine exash_print(self, out)
    class(nx_exash_t), intent(inout) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    call print_conf_ele(self%gs_, 'Compute info for GS only', unit=output)
    call print_conf_ele(self%dip_, 'Compute transition dipole', unit=output)
    call print_conf_ele(self%nat_array, 'Number of atoms in chromophores', unit=output)
    call print_conf_ele(self%nstat_array, 'Number of states in chromophores', unit=output)
  end subroutine exash_print


  subroutine exash_compute_adiab_epot_grad(&
       & self, nstatdyn, &
       & omega, array_gs, enMM, &
       & coord, esp, grad_qm, grad_mm, stat &
       & )
    class(nx_exash_t), intent(inout) :: self
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.
    real(dp), intent(in) :: omega(:)
    !! Array of excitation energies, ordered with respect to chromophores order.
    real(dp), intent(in) :: array_gs(:)
    !! Array of GS energies, ordered with respect to chromophores order.
    real(dp), intent(in) :: enMM
    !! Energy of the whole system at the MM level.
    type(mat2_container_t), intent(in) :: coord(:)
    !! Array of containers for the coordinates of each chromophores.
    type(mat2_container_t), intent(in) :: esp(:)
    !! Array of containers for the TrESP charges of each chromophores, for all excited
    !! states computed.
    type(mat3_container_t), intent(in) :: grad_qm(:)
    !! Array of containers for the QM gradients of each chromophore.
    real(dp), intent(in) :: grad_mm(:, :)
    !! Gradient at the MM level.
    type(nx_status_t), intent(inout) :: stat
    !! Status indicator.

    real(dp) :: e_ref
    real(dp), allocatable :: grad_off_dig(:, :), exc_ham(:, :), &
         & grad_GS(:, :), grad_dig(:, :), full_grad(:, :)

    integer :: i, j
    integer :: info

    e_ref = sum(array_gs(:) - enMM) + enMM

    call exash_build_h_dhdr( &
         & coord, esp, self%nstat_array, self%nat_array, omega, &
         & exc_ham, grad_off_dig &
         & )
    ! print *, 'GRAD OFF DIG'
    ! do i=1, size(grad_off_dig, 2)
    !    print '(3F20.12)', (grad_off_dig(j, i), j=1, 3)
    ! end do

    ! Build full Hamiltonian
    self%full_diab_ham_ = exash_build_full_hamiltonian(e_ref, exc_ham)

    ! Save diabatic energies
    self%diab_en_ = exash_build_diab_en(e_ref, exc_ham)

    ! Diagonalize Exciton Hamiltonian
    call exash_diag_hamiltonian(exc_ham, self%eigenval_, info)
    if (info /= 0) then
       call stat%append(NX_ERROR, &
            & 'Error in dsyevd diagonalization: '//to_str(info), &
            & mod=MODNAME, func='exash_compute_adiab_epot_grad'&
            & )
       return
    end if
    ! print *, 'EXC HAM'
    ! do i=1, size(exc_ham, 1)
    !    print '(3F20.12)', (exc_ham(i, j), j=1, size(exc_ham, 2))
    ! end do

    ! Back_up eigenvectors
    self%old_eigenvectors_(:, :) = self%eigenvectors_(:, :)

    ! Build complete set of eigenvectors
    self%eigenvectors_ = 0.0_dp
    self%eigenvectors_(1, 1) = 1.0_dp
    do concurrent(i=2:self%nstat_tot+1, j=2:self%nstat_tot+1)
       self%eigenvectors_(i, j) = exc_ham(i-1, j-1)
    end do
    ! print *, 'EIGEN'
    ! do i=1, size(self%eigenvectors_, 1)
    !    print '(3F20.12)', (self%eigenvectors_(i, j), j=1, size(self%eigenvectors_, 2))
    ! end do

    ! Save adiabatic energies
    self%adiab_en_(1) = e_ref
    self%adiab_en_(2:self%nstat_tot+1) = self%eigenval_(:) + e_ref

    self%diab_pop_ = exash_build_diab_pop(self%eigenvectors_, nstatdyn)
    
    ! Ground state gradient
    grad_GS = exash_build_gs_gradient(grad_qm, grad_mm)
    ! print *, 'GRAD GS'
    ! do i=1, size(grad_GS, 2)
    !    print '(3F20.12)', (grad_GS(j, i), j=1, 3)
    ! end do

    ! Diagonal gradient
    grad_dig = exash_build_diag_exc_gradient(grad_qm, self%nstat_array(:))
    ! print *, 'GRAD DIG'
    ! do i=1, size(grad_dig, 2)
    !    print '(3F20.12)', (grad_dig(j, i), j=1, 3)
    ! end do

    ! Full gradient
    self%adiab_grad_ = exash_build_full_exc_grad_adiab(&
         & exc_ham(:, nstatdyn-1), grad_off_dig, grad_dig, &
         & self%nstat_array, self%nat_array, grad_GS &
         & )
  end subroutine exash_compute_adiab_epot_grad


  subroutine exash_write_ovl_matrix(self, filename, ovl)
    class(nx_exash_t), intent(in) :: self
    character(len=*), intent(in) :: filename
    type(mat2_container_t), intent(in) :: ovl(:)

    integer :: u, i, j

    real(dp), allocatable :: ovl_matrix(:, :)

    ovl_matrix = self%compute_ovl(ovl)
    open(newunit=u, file=filename, action='write')
    write(u, '(a)') 'CI overlap matrix'
    write(u, *) self%nstat_tot + 1, self%nstat_tot + 1
    do i=1, self%nstat_tot + 1
       write(u, *) (ovl_matrix(i, j), j=1, self%nstat_tot+1)
    end do
    close(u)
  end subroutine exash_write_ovl_matrix
  


  function exash_compute_overlap_matrix(self, ovl) result(res)
    class(nx_exash_t), intent(in) :: self
    type(mat2_container_t), intent(in) :: ovl(:)

    real(dp), allocatable :: res(:, :)
    
    real(dp), allocatable :: ovl_matrix(:, :)

    ! Build diabatic overlap matrix
    res = exash_build_ovl_matrix(ovl, self%nstat_tot)

    ! And then transform to adiabatic basis
    res = matmul( transpose( self%old_eigenvectors_ ), res)
    res = matmul( res, self%eigenvectors_ )
  end function exash_compute_overlap_matrix

  
  subroutine exash_build_h_dhdr( &
       & coord, esp, nstat_array, nat_array, omega, ham,&
       & grad_off_dig &
       & )
    !! Construct the exciton Hamiltonian and the off-diagonal term of its gradient.
    !!
    !!
    !! The diagonal blocks are themselves diagonal, and contain the excitation energies
    !! ``omega``.  The off-diagonal blocks are obtained as:
    !!
    !! \( V_{ai,bj} = \sum_{A \in a} \sum_{B \in b} \frac{q_{A,ai} q_{B,bj}}{r_{AB}} \),
    !!
    !! where \(ai\) is the \(i\)-th excitation in chromophore \(a\) , q_
    type(mat2_container_t), intent(in) :: coord(:)
    !! Array of ``nchrom`` containers, where the ``i``-th element corresponds to the
    !! coordinates of the ``i``-th chromophore, with dimenstion ``(3, nat_i)`` (``nat_i``
    !! begin the number of atoms of chromophore ``i``).
    type(mat2_container_t), intent(in) :: esp(:)
    !! Array of ``nchrom`` containers, where the ``i``-th element corresponds to the
    !! transition atomic charges of the ``i``-th chromophore, with dimenstion ``(nstat_i,
    !! nat_i)`` (``nstat_i`` begin the number of *excited* states computed for chromophore
    !! ``i``).
    integer, intent(in) :: nstat_array(:)
    !! Array of size ``nchrom`` containing the number of *excited* states computed for
    !! each chromophore.
    integer, intent(in) :: nat_array(:)
    !! Array of size ``nchrom`` containing the number of atoms of each chromophore.
    real(dp), intent(in) :: omega(:)
    !! Array containing the excitation energies, of dimension \(\sum_i N_{state}(i)\).
    !! The array is built as \( [ (\Delta E_K, K=1..N_{state}(i)), i=1..N_{chrom} ]\).
    real(dp), allocatable, intent(out) :: ham(:, :)
    !! Exciton Hamiltonian, of dimension ``(nstat_tot, nstat_tot)``. 
    real(dp), allocatable, intent(out) :: grad_off_dig(:, :)
    !! Off-diagonal part of the gradient, as it is obtained with ``esp`` elements. Each
    !! element of this array is the \( (x, y, z) \) coordinates of the gradient on the
    !! corresponding atom, for the corresponding state coupling. The gradients are ordered first
    !! with respect to chromophore ordering, then to the state coupling between pairs of
    !! chromophore.  In more formal terms, we have the following order, with
    !! (\(S_N^M\) being the \(N\)-th excited state of chromophore \(M\):
    !!
    !! - (\(S_1^1 / S_1^2 .. S_{N_{state}(2)}^2 \) for all atoms of chromophore 1
    !! (``nat_1 * nstat_2`` elements) ;
    !! - ...
    !! - (\(S_{N_state}(1)^1 / S_1^2 .. S_{N_{state}(2)}^2 \) for all atoms of chromophore 1
    !! (``nat_1 * nstat_2`` elements) ;
    !! - and we go on until we have considered all pairs of chromophores.
    !!
    !! Overall, we will have \(n_{gradients} = \sum_a \sum_{b \neq a} N_{atoms}(a) * N_{states} (b) *
    !! N_{states} (a) \) elements. 

    integer :: ngrad, i, j, ii, jj, w, chrom1, chrom2, stat_1, stat_2, k, l
    integer :: nstat_tot
    real(dp) :: q, tmp(3), eint, r

    ngrad = 0 
    do chrom1=1, size(coord)
       i = nat_array(chrom1) * nstat_array(chrom1)
       do chrom2=1, size(coord)
          if (chrom2 /= chrom1) then
             ngrad = ngrad + i*nstat_array(chrom2)
          end if
       end do
    end do

    nstat_tot = sum( nstat_array(:) )

    allocate(grad_off_dig(3, ngrad))
    grad_off_dig = 0.0_dp

    allocate(ham( nstat_tot, nstat_tot))
    ham = 0.0_dp

    w = 1 
    ! Loop over the chromophores
    do chrom1 = 1, size(coord)
       do chrom2 = 1, size(coord)
          if (chrom1 /= chrom2) then

             ! Loop over the states
             ii = 0
             do i=1, chrom1 - 1
                ii = ii + nstat_array(i)
             end do

             do stat_1=1, nstat_array( chrom1 )

                ii = ii + 1

                jj = 0
                do i=1, chrom2 - 1
                   jj = jj + nstat_array(i)
                end do

                do stat_2=1, nstat_array(chrom2)
                   jj = jj + 1
                   eint = 0._dp
                   ! Loop over the atoms
                   do k = 1, nat_array( chrom1 )
                      tmp = 0.0_dp

                      do l = 1, nat_array( chrom2 )

                         r = sum((coord(chrom1)%m(:, k) - coord(chrom2)%m(:, l))**2)
                         q = esp(chrom1)%m(stat_1, k) * esp(chrom2)%m(stat_2, l)

                         ! Calculate the transition monopole aproximation (TMA) using
                         ! the transition charges (TrESP).       
                         eint = eint + q / sqrt(r) 

                         ! Calculate the dH/dR derivatives
                         tmp(:) = tmp(:) &
                              & - q*(coord(chrom1)%m(:, k) - coord(chrom2)%m(:, l)) / r**(1.5_dp)
                      end do

                      ! print *, 'CHROM1 = ', chrom1, '; CHROM2 = ', chrom2, '; stat_1 = ',&
                      !      & stat_1, '; stat_2 = ', stat_2, '; k = ', k, '; w = ', w
                      grad_off_dig(:, w) = tmp(:)
                      w = w + 1
                   end do
                   ham(ii, jj) = eint
                end do
             end do
          end if
       end do
    end do

    do concurrent (i=1:nstat_tot)
       ham(i, i) = omega(i)
    end do
  end subroutine exash_build_h_dhdr


  pure function exash_build_full_hamiltonian(e_ref, exc_ham) result(res)
    real(dp), intent(in) :: e_ref
    real(dp), intent(in) :: exc_ham(:, :)

    real(dp), allocatable :: res(:, :)

    integer :: nstat_tot, i, j

    nstat_tot = size(exc_ham, 1)
    allocate(res(nstat_tot+1, nstat_tot+1))
    
    res = 0.0_dp
    do concurrent(i=2:nstat_tot+1, j=2:nstat_tot+1)
       res(i, j) = exc_ham(i-1, j-1)
    end do
    do concurrent(i=1:nstat_tot+1)
       res(i, i) = res(i, i) + e_ref
    end do
  end function exash_build_full_hamiltonian


  pure function exash_build_diab_en(e_ref, exc_ham) result(res)
    real(dp), intent(in) :: e_ref
    real(dp), intent(in) :: exc_ham(:, :)

    real(dp), allocatable :: res(:)

    integer :: nstat_tot, i, j

    nstat_tot = size(exc_ham, 1)
    allocate(res(nstat_tot+1))

    res(1) = e_ref
    do concurrent(i=2:nstat_tot+1)
       res(i) = e_ref + exc_ham(i-1, i-1)
    end do
  end function exash_build_diab_en


  pure function exash_build_diab_pop(eigenvectors, nstatdyn) result(res)
    real(dp), intent(in) :: eigenvectors(:, :)
    integer, intent(in) :: nstatdyn

    real(dp), allocatable :: res(:)

    integer :: i
    
    ! Diabatic populations
    allocate( res(size(eigenvectors, 1)) )
    res(:) = 0._dp
    do concurrent(i=1:size(eigenvectors, 1))
       res(i) = ( eigenvectors(i, nstatdyn) )**2
    end do
  end function exash_build_diab_pop


  subroutine exash_diag_hamiltonian(matrix, eigenvalues, info)
    !! Diagonalize the exciton Hamiltonian.
    !!
    !! The routine returns the adiabatic energies as ``eigenvalues``, and the diabatic
    !! to adiabatic transformation matrix as ``matrix``, which *replaces* the input value.
    real(dp), intent(inout) :: matrix(:, :)
    !! On input, the matrix to be diagonalized ; on output, the eigenvectors as returned
    !! by ``dsyevd``.
    real(dp), allocatable, intent(out) :: eigenvalues(:)
    !! Set of eigenvalues.
    integer, intent(out) :: info
    !! Status indicator, as returned by ``dsyevd``.

    real(dp), allocatable :: work(:)
    integer :: d, lwork ,liwork
    integer, allocatable :: iwork(:)

    allocate( eigenvalues( size(matrix, 1)) )

    d = size(matrix, 1)
    allocate(work(1), iwork(1))
    call dsyevd('V', 'U', d, matrix, d, eigenvalues, &
         & work, -1, iwork, -1, info)
    lwork = int( work(1) )
    liwork = iwork(1)

    deallocate(work, iwork)
    allocate( work(lwork), iwork(liwork) )

    call dsyevd('V', 'U', d, matrix, d, eigenvalues, &
         & work, lwork, iwork, liwork, info)
  end subroutine exash_diag_hamiltonian


  function exash_build_gs_gradient(grad_qm, grad_mm) result(res)
    !! Construct the gradient for the ground state.
    !!
    !! The result is a matrix with dimension ``(3, nat_tot)``, with ``nat_tot`` begin the
    !! *total* number of atoms included in the *whole* system.
    type(mat3_container_t), intent(in) :: grad_qm(:)
    !! Array of ``nchrom`` container, each one containing the gradients computed for all
    !! states included in the QM computation. The components for chromophore ``i`` should
    !! have dimensions ``(3, nat_tot, nstat_i)``.
    real(dp), intent(in) :: grad_mm(:, :)
    !! MM gradients as obtained from Tinker, with dimension ``(3, nat_tot)``.

    real(dp), allocatable :: res(:, :)

    integer :: i, j
    integer :: nat_total

    nat_total = size(grad_mm, 2)

    allocate(res(3, nat_total))
    res(:, :) = 0.0_dp

    do i=1, nat_total
       do j=1, size(grad_qm)
          res(:, i) = res(:, i) &
               & + grad_qm(j)%m(:, i, 1) - grad_mm(:, i)
       end do
    end do
    res(:, :) = res(:, :) + grad_mm(:, :)
  end function exash_build_gs_gradient


  function exash_build_diag_exc_gradient(grad_qm, nstat_array) result(res)
    !! Build the diagonal component of the gradient for the exciton Hamiltonian.
    !!
    !! Each atoim in the system will have one gradient for every excited state computed,
    !! so the function returns a matrix of dimension ``(3, nat_total * nstat_total)``,
    !! and is ordered in increasing state for increasing chromophore indices.
    type(mat3_container_t), intent(in) :: grad_qm(:)
    !! Array of ``nchrom`` container, each one containing the gradients computed for all
    !! states included in the QM computation. The components for chromophore ``i`` should
    !! have dimensions ``(3, nat_tot, nstat_i)``.
    integer, intent(in) :: nstat_array(:)
    !! Array containing the number of states computed for each chromophore.

    real(dp), allocatable :: res(:, :)

    integer :: ind, i, j, k
    integer :: nat_total

    nat_total = get_nat_total(grad_qm(1))

    allocate(res( 3, sum( nat_total * nstat_array(:) )))
    res = 0.0_dp
    ind = 1
    do i=1, size(grad_qm)
       do j=1, size( grad_qm(i)%m, 3)-1 ! nstat_array(i)
          do k=1, nat_total
             res(:, ind) = grad_qm(i)%m(:, k, j+1) - grad_qm(i)%m(:, k, 1)
             ind = ind + 1
          end do
       end do
    end do
  end function exash_build_diag_exc_gradient


  function exash_build_full_exc_grad_adiab(&
       & eig_vec, grad_off_dig, grad_dig, nstat_array, nat_array, &
       & grad_GS &
       & ) result(res)
    !! Build the full gradient matrix in the adiabatic basis.
    !!
    !! The result is an array with dimensions ``(3, nat_total)``, and corresponds to the
    !! gradient on the current dynamics surface (``nstatdyn`` surface in Newton-X).
    real(dp), intent(in) :: eig_vec(:)
    !! Eigenvector of the current dynamic state (``nstatdyn``).
    real(dp), intent(in) :: grad_off_dig(:, :)
    !! Off-diagonal component of the gradient, built with ``build_h_dh_dr_old``.
    real(dp), intent(in) :: grad_dig(:, :)
    !! Diagonal component of the gradient.
    integer, intent(in) :: nstat_array(:)
    !! Array containing the number of states computed for each chromophore.
    integer, intent(in) :: nat_array(:)
    !! Array containing the number of atoms in each chromophore.
    real(dp), intent(in) :: grad_GS(:, :)
    !! Ground-state gradient.

    real(dp), allocatable :: res(:, :)

    real(dp), allocatable :: grad_tmp(:, :, :)
    real(dp), allocatable :: full_grd_tmp(:, :)
    integer :: nstat_tot, nchrom, w, z, unz, g, ind_test, nat_tot, numb_atom
    integer :: chrom1, chrom2, nat1, atom_dif, h, ii, j, jj, k, l, m, n, p, q, yy, zz, i

    numb_atom = size(grad_GS, 2)
    nstat_tot = sum(nstat_array)
    nchrom = size(nstat_array)
    nat_tot = sum(nat_array)

    allocate(res(3, numb_atom))
    res = 0.0_dp

    allocate(grad_tmp(3, nstat_tot, nstat_tot))
    grad_tmp = 0.0_dp

    allocate(full_grd_tmp(nstat_tot, 3))
    full_grd_tmp = 0.0_dp

    w = 0
    z = 0
    unz = 0
    g = 0
    do chrom1=1, nchrom
       if (chrom1 > 1) then
          w = w + nat_array(chrom1-1)*nstat_array(chrom1-1)
       else
          w = w
       end if
       do nat1=1, nat_array(chrom1)! nat_array(chrom1)
          unz = unz + 1
          grad_tmp = 0.0_dp
          z = z + 1
          do chrom2=1, nchrom-1

             do l=chrom2+1, nchrom
                ii = 0
                do p = 1, chrom2-1
                   ii = ii + nstat_array( p )
                end do

                do m=1, nstat_array(chrom2)
                   ii = ii + 1
                   jj = 0
                   do q = 1, l-1
                      jj = jj + nstat_array(q)
                   end do
                   h = nat_array(chrom1)*nstat_array(chrom2)*(m-1)

                   do n=1, nstat_array(l)
                      jj = jj + 1
                      g = h + w + nat_array(chrom1)*(n-1) + nat1
                      grad_tmp(:, ii, jj) = grad_off_dig(:, g)
                      grad_tmp(:, jj, ii) = grad_tmp(:, ii, jj)
                   end do
                end do
             end do
          end do
          do ii = 1, nstat_tot
             jj = (ii-1)*numb_atom + unz
             grad_tmp(:, ii, ii) = grad_dig(:, jj)
          end do
          do concurrent(i=1:3)
             full_grd_tmp(:, i) = matmul(grad_tmp(i, :, :), eig_vec)
             res(i, z) = dot_product(eig_vec, full_grd_tmp(:, i))
          end do
       end do
    end do

    atom_dif = numb_atom - nat_tot

    if (nat_tot < numb_atom) then
       do j = 1, atom_dif
          z = z + 1
          unz = unz + 1
          grad_tmp = 0.0_dp
          do k = 1, nstat_tot
             zz = (k-1)*numb_atom + unz
             grad_tmp(:, k, k) = grad_dig(:, zz)
          end do

          do concurrent(i=1:3)
             full_grd_tmp(:, i) = matmul(grad_tmp(i, :, :), eig_vec)
             res(i, z) = dot_product(eig_vec, full_grd_tmp(:, i))
          end do
       end do
    end if

    ! Add GS gradient to make the full result.
    do concurrent(i=1:numb_atom)
       res(:, i) = res(:, i) + grad_GS(:, i)
    end do
  end function exash_build_full_exc_grad_adiab


  pure function exash_compute_trdip(ham, mu) result(res)
    !! Compute the exciton transition dipoles.
    !!
    !! The result is a matrix of dimension ``(3, nstat_tot)``, and contains the ``x, y,
    !! z`` components of the transision dipole for each exciton state in a.u.
    real(dp), intent(in) :: ham(:, :)
    !! Exciton Hamiltonian.
    real(dp), intent(in) :: mu(:, :)
    !! Matrix containing the components of the transition dipoles, with dimension ``(3,
    !! nstat_tot)``, ordered as:
    !!
    !!     X^{chrom1}_{2, 1}    Y^{chrom1}_{2, 1}    Z^{chrom1}_{2, 1}
    !!     ...
    !!     X^{chrom1}_{N_1, 1}  Y^{chrom1}_{N_1, 1}  Z^{chrom1}_{N_1, 1}
    !!     X^{chrom2}_{2, 1}    Y^{chrom2}_{2, 1}    Z^{chrom2}_{2, 1}
    !!     ...
    !!     ...
    !!     X^{chromM}_{N_M, 1}  Y^{chromM}_{N_M, 1}  Z^{chromM}_{N_M, 1}
    !!

    real(dp), allocatable :: res(:, :)

    integer :: ii, k, nstat_tot

    nstat_tot = size(mu, 2)
    allocate(res(3, nstat_tot))

    res = 0.0_dp
    do ii = 1, nstat_tot
       do k = 1, nstat_tot
          res(:, ii) = res(:, ii) + ham(k, ii)*mu(:, k)
       end do
    end do
  end function exash_compute_trdip


  subroutine exash_compute_dipoles(self, dipoles)
    class(nx_exash_t), intent(inout) :: self
    real(dp), intent(in) :: dipoles(:, :, :)

    real(dp), allocatable :: mu(:, :)
    integer :: i, k

    ! Select excitations from state 1 to other states
    allocate(mu(3, self%nstat_tot))
    k = 1
    do i=1, self%nchrom
       mu(:, k:k+self%nstat_array(i)-1) = dipoles(:, 2:size(dipoles, 2), 1)
       k = k + self%nstat_array(i)
    end do

    self%trdip_ = exash_compute_trdip(&
         & self%eigenvectors_(2:self%nstat_tot+1, 2:self%nstat_tot+1), &
         & mu &
         & )

    self%osc_str_ = exash_compute_osc_str(self%trdip_, self%eigenval_(:))
  end subroutine exash_compute_dipoles


  pure function exash_compute_osc_str(trdip, eigenvalues) result(res)
    real(dp), intent(in) :: trdip(:, :)
    !! Transition dipole matrix.
    real(dp), intent(in) :: eigenvalues(:)
    !! Adiabatic enegies of the excitons.

    real(dp), allocatable :: res(:)

    allocate(res(size(trdip, 2)))
    res(:) = eigenvalues(:) * norm2(trdip, dim=1)**2 * 2.0_dp/3.0_dp
  end function exash_compute_osc_str


  pure function exash_build_ovl_matrix(ovl_cont, nstat_tot) result(res)
    !! Construct the exciton cioverlap matrix.
    !!
    !! The overlap matrix is built from the matrices obtained from each individual
    !! chromophores. If we note \(S^a_j\) the \(j\)-th excited state on chromophore
    !! \(a\), and we use \(S\) for components of the exciton overlap matrix and \(s^a\)
    !! for components of the overlap matrix from chromophore \(a\), then:
    !!
    !! - The first element of the matrix is simply the product of all ``<S0|S0>`` couplings
    !! from all chromophores:
    !! \( <S0 | S0> = \prod_{a} <s^a_0|s^a_0> \)
    !!
    !! - The first line is obtained as:
    !! \( <S0 | S^a_i> = <s^a_0|s^a_i> \prod_{b \neq a} <s^b_0|s^b_0> \)
    !!
    !! - The first column is obtained as:
    !! \( <S^a_i | S0> = <s^a_i|s^a_0> \prod_{b \neq a} <s^b_0|s^b_0> \)
    !!
    !! - The diagonal blocks are obtained as:
    !! \( <S^a_i | S^a_j> = <s^a_i|s^a_j> \prod_{b \neq a} <s^b_0|s^b_0>\)
    !!
    !! - Finally, the off-diagonal blocks are constructed as:
    !! \( <S^a_i | S^b_j> = <s^a_i|s^a_0> <s^b_0|s^b_j> \prod_{c \neq a, c \neq b} <s^c_0|s^c_0>\)
    !!
    integer, intent(in) :: nstat_tot
    type(mat2_container_t), intent(in) :: ovl_cont(:)
    !! Array of ``nchrom`` containers, with elements corresponding to the overlap
    !! matrices for each chromophores.

    real(dp), allocatable :: res(:, :)

    integer :: nchrom, nstat
    integer :: i, j, k, l, ind, nstat1, nstat2
    integer :: ii, jj, chrom1, chrom2, p, q
    real(dp) :: tmp, coupl1, coupl2
    real(dp), allocatable :: line(:)

    nchrom = size(ovl_cont)
    allocate(res(nstat_tot+1, nstat_tot+1))
    res = 0.0_dp

    ! First element of the overlap matrix <S0|S0>
    allocate(line(nchrom))
    do concurrent (i=1:nchrom)
       line(i) = ovl_cont(i)%m(1, 1)
    end do
    res(1, 1) = product(line)

    ! First line
    ind = 2
    do i=1, nchrom
       nstat = size(ovl_cont(i)%m, 1)

       tmp = 1.0_dp
       do j=1, nchrom
          if (j /= i) tmp = tmp * ovl_cont(j)%m(1, 1)
       end do

       res(1, ind:ind+nstat-2) = tmp * ovl_cont(i)%m(1, 2:nstat)
       ind = ind + nstat-1
    end do

    ! First column
    ind = 2
    do i=1, nchrom
       nstat = size(ovl_cont(i)%m, 1)

       tmp = 1.0_dp
       do j=1, nchrom
          if (j /= i) tmp = tmp * ovl_cont(j)%m(1, 1)
       end do

       res(ind:ind+nstat-2, 1) = tmp * ovl_cont(i)%m(2:nstat, 1)
       ind = ind + nstat-1
    end do

    ! Diagonal blocks
    ind = 2
    do i=1, nchrom
       nstat = size(ovl_cont(i)%m, 1)

       tmp = 1.0_dp
       do j=1, nchrom
          if (j /= i) tmp = tmp * ovl_cont(j)%m(1, 1)
       end do

       res(ind:ind+nstat-2, ind:ind+nstat-2) = tmp * ovl_cont(i)%m(2:nstat, 2:nstat)
       ind = ind + nstat - 1
    end do

    ! Off-diagonal blocks
    ind = 2
    do chrom1=1, nchrom
       nstat1 = size(ovl_cont(chrom1)%m, 1)

       do chrom2=1, nchrom
          nstat2 = size(ovl_cont(chrom2)%m, 1)

          if (chrom1 /= chrom2) then

             ! Product of GS other than i and j
             tmp = 1.0_dp
             do k=1, nchrom
                if (k /= chrom1 .and. k /= chrom2) &
                     & tmp = tmp * ovl_cont(k)%m(1, 1)
             end do

             ! Loop over the states
             ii = 1
             do p = 1, chrom1-1
                ii = ii + size(ovl_cont(p)%m, 1)-1
             end do

             do i = 2, nstat1
                ii = ii + 1

                jj = 1
                do q = 1, chrom2-1
                   jj = jj + size(ovl_cont(q)%m, 1)-1
                end do

                do j = 2, nstat2
                   jj = jj + 1
                   res(ii,jj) = ovl_cont(chrom1)%m(i,1)*ovl_cont(chrom2)%m(1,j)*tmp
                end do
             end do
          end if
       end do
    end do
  end function exash_build_ovl_matrix


  subroutine exash_save_eigenvectors(filename, eigenvectors)
    !! Write the diabatic to adiabatic transformation matrix.
    !!
    character(len=*), intent(in) :: filename
    !! File to write to.
    real(dp), intent(in) :: eigenvectors(:, :)
    !! Matrix to write.

    integer :: u, i, j

    open(newunit=u, file=filename, action='write')
    write(u, *) size(eigenvectors, 1), size(eigenvectors, 2)
    do i=1, size(eigenvectors, 1)
       write(u, *) (eigenvectors(i, j), j=1, size(eigenvectors, 2))
    end do
    close(u)
  end subroutine exash_save_eigenvectors


  function exash_read_eigenvectors(filename) result(res)
    !! Read the diabatic to adiabatic transformation matrix from ``filename``.
    !!
    character(len=*), intent(in) :: filename
    !! File to read.

    real(dp), allocatable :: res(:, :)

    integer :: u, i, j, d1, d2

    open(newunit=u, file=filename, action='read')
    read(u, *) d1, d2
    allocate(res(d1, d2))
    do i=1, d1
       read(u, *) (res(i, j), j=1, d2)
    end do
    close(u)
  end function exash_read_eigenvectors


  pure function get_diab_en(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:)

    allocate(res, source=self%diab_en_)
  end function get_diab_en

  pure function get_diab_pop(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:)

    allocate(res, source=self%diab_pop_)
  end function get_diab_pop

  pure function get_adiab_en(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:)

    allocate(res, source=self%adiab_en_)
  end function get_adiab_en

  pure function get_adiab_grad(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:, :)

    allocate(res, source=self%adiab_grad_)
  end function get_adiab_grad

  pure function get_full_diab_ham(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:, :)

    allocate(res, source=self%full_diab_ham_)
  end function get_full_diab_ham

  pure function get_trdip(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:, :)

    allocate(res, source=self%trdip_)
  end function get_trdip

  pure function get_osc_str(self) result(res)
    class(nx_exash_t), intent(in) :: self

    real(dp), allocatable :: res(:)

    allocate(res, source=self%osc_str_)
  end function get_osc_str

  pure function get_dip(self) result(res)
    class(nx_exash_t), intent(in) :: self

    logical :: res

    res = self%dip_
  end function get_dip

  pure function get_gs(self) result(res)
    class(nx_exash_t), intent(in) :: self

    logical :: res

    res = self%gs_
  end function get_gs
  

  function get_nat_total(grad_qm) result(res)
    !! Extract total number of atoms in the system from gradient container.
    !!
    type(mat3_container_t), intent(in) :: grad_qm
    !! Array of ``nchrom`` container, each one containing the gradients computed for all
    !! states included in the QM computation. The components for chromophore ``i`` should
    !! have dimensions ``(3, nat_tot, nstat_i)``.

    integer :: res

    res = size( grad_qm%m, 2)
  end function get_nat_total


  function get_nstat_exc(grad_qm) result(res)
    !! Get the number of excited states computed from gradient container.
    !!
    type(mat3_container_t), intent(in) :: grad_qm
    !! Array of ``nchrom`` container, each one containing the gradients computed for all
    !! states included in the QM computation. The components for chromophore ``i`` should
    !! have dimensions ``(3, nat_tot, nstat_i)``.

    integer :: res

    res = size( grad_qm%m, 3 ) - 1
  end function get_nstat_exc
  
end module mod_exash_utils
  
