! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_tinker_mndo_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ang, au2kcalm
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, &
       & call_external, check_error
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_realloc => set_config_realloc
  use mod_interface, only: &
       & copy, gzip
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tinker_utils, only: tinker_get_qmmm_atoms
  use mod_tools, only: &
       & split_pattern, to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_tinker_mndo_t

  type, extends(nx_qm_generic_t) :: nx_tinker_mndo_t
     character(len=:), allocatable :: gradinterf
     !! Absolute path to the ``gradinterf.x`` program.
     character(len=:), allocatable :: tmndo_env
     !! Value of the ``$TINKER_MNDO`` environment.
     character(len=:), allocatable :: mndo_env
     !! Value of the ``$MNDO`` environment.
     logical :: compute_nac_mm = .true.
     !! Indicate if the NACV are computed for the MM atoms.
     logical, allocatable :: qm_filter(:)
     !! Array used to filter QM atoms (value is ``.true.`` for QM atoms, ``.false.`` for
     !! MM atoms.
     logical :: has_osc_str = .false.
     !! Indicate if the job prints oscillator strengths or not.
   contains
     private
     procedure, public :: setup => tinker_mndo_setup
     procedure, public :: print => tinker_mndo_print
     procedure, public :: to_str => tinker_mndo_to_str
     procedure, public :: backup => tinker_mndo_backup
     procedure, public :: update => tinker_mndo_update
     procedure, public :: run => tinker_mndo_run
     procedure, public :: read_output => tinker_mndo_read_output
     procedure, public :: write_geom => tinker_mndo_write_geometry
     procedure, public :: init_overlap => tinker_mndo_init_overlap
     procedure, public :: prepare_overlap => tinker_mndo_prepare_overlap
     procedure, public :: extract_overlap => tinker_mndo_extract_overlap
     procedure, public :: ovl_post => tinker_mndo_ovl_post
     procedure, public :: ovl_run => tinker_mndo_ovl_run
     procedure, public :: get_lcao => tinker_mndo_get_lcao
     procedure, public :: cio_prepare_files => tinker_mndo_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & tinker_mndo_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => tinker_mndo_cio_get_mos_energies
  end type nx_tinker_mndo_t
  interface nx_tinker_mndo_t
     module procedure constructor
  end interface nx_tinker_mndo_t

  character(len=*), parameter :: MODNAME = 'mod_tinker_mndo_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & gradinterf, tmndo_env, mndo_env, compute_nac_mm &
       & ) result(res)
    !! Constructor for the ``nx_tinker_mndo_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    character(len=*), intent(in), optional :: gradinterf
    character(len=*), intent(in), optional :: tmndo_env
    character(len=*), intent(in), optional :: mndo_env
    logical, intent(in), optional :: compute_nac_mm

    type(nx_tinker_mndo_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )
    if (present(gradinterf)) then
       res%gradinterf = trim(gradinterf)
    else
       res%gradinterf = 'gradinterf.x'
    end if

    if (present(tmndo_env)) then
       res%tmndo_env = trim(tmndo_env)
    else
       res%tmndo_env = ''
    end if

    if (present(mndo_env)) then
       res%mndo_env = trim(mndo_env)
    else
       res%mndo_env = ''
    end if
    if (present(compute_nac_mm)) res%compute_nac_mm = compute_nac_mm

    allocate(res%qm_filter(1))
    res%qm_filter(:) = .true.
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine tinker_mndo_setup(self, parser, conf, inp_path, stat)
    class(nx_tinker_mndo_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo
    integer :: ierr
    character(len=MAX_STR_SIZE) :: env

    prt_mo = -1
    call set(parser, 'tinker_mndo', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%tmndo_env == '') then
       call get_environment_variable("TINKER_MNDO", env, status=ierr)
       if (ierr == 1) then
          call stat%append( &
               & NX_ERROR, &
               & 'Setup ABORTED: $TINKER_MNDO environment is not defined', &
               & mod=modname, func='tinker_mndo_setup')
       else
          self%tmndo_env = trim(env)
       end if
    end if

    if (self%mndo_env == '') then
       call get_environment_variable("MNDO", env, status=ierr)
       if (ierr == 1) then
          call stat%append( &
               & NX_ERROR, &
               & 'Setup ABORTED: $MNDO environment is not defined', &
               & mod=modname, func='tinker_mndo_setup')
       else
          self%mndo_env = trim(env)
       end if
    end if

    call set_realloc(parser, 'tinker_mndo', self%gradinterf, 'interface_path')
    call set(parser, 'tinker_mndo', self%compute_nac_mm , 'nac_mm')

    self%qm_filter = tinker_get_qmmm_atoms(trim(conf%init_input), conf%nat)
    self%has_osc_str = has_osc_str_keyword(trim(conf%init_input)//'/template.inp')
  end subroutine tinker_mndo_setup


  subroutine tinker_mndo_print(self, out)
    class(nx_tinker_mndo_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%tmndo_env, 'TINKER_MNDO path', unit=output)
    call print_conf_ele(self%mndo_env, 'MNDO path', unit=output)
    call print_conf_ele(self%gradinterf, 'Interface program', unit=output)
    call print_conf_ele(self%compute_nac_mm, 'Computation of NAC', unit=output)
    call print_conf_ele(self%has_osc_str, 'Print osc. str.', unit=output)
  end subroutine tinker_mndo_print

  function tinker_mndo_to_str(self) result(res)
    class(nx_tinker_mndo_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&tinker_mndo'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' gradinterf = '//trim(self%gradinterf)//nl
    res = res//' compute_nac_mm  = '//to_str(self%compute_nac_mm )//nl
    res = res//'/'//nl
  end function tinker_mndo_to_str

  subroutine tinker_mndo_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo

    integer :: ierr

    ierr = copy(trim(qm_path)//'/molden.dat', chk_path)
    ierr = gzip([trim(chk_path)//'/molden.dat'])

    if (ierr /= 0) then
       call stat%append(NX_ERROR, 'Problem in backing up TMNDO files', &
            & mod=MODNAME, func='tinker_mndo_backup')
    end if
  end subroutine tinker_mndo_backup

  subroutine tinker_mndo_update(self, conf, traj, path, stat)
    class(nx_tinker_mndo_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: env

    logical :: ext
    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    integer :: ierr
    character(len=256), allocatable :: keys(:)
    character(len=256), allocatable :: values(:)

    ierr = rename(trim(path)//'/frame00000.key', trim(path)//'/tinker_mndo.key')

    loaded = tmndo_load_key_file(trim(path)//'/tinker_mndo.key')
    loaded = tmndo_delete_keys(loaded, ['mndocurrentstate', 'mndostates      '])

    if (conf%nstat > 1) then
       allocate(keys(2))
       allocate(values(2))

       keys = [character(len=256) :: 'mndocurrentstate', 'mndostates']
       values = [character(len=256) :: to_str(traj%nstatdyn), to_str(conf%nstat)]
    else
       allocate(keys(1))
       allocate(values(1))
       keys(1) = 'mndocurrentstate'
       values(1) = to_str(traj%nstatdyn)
    end if
    loaded = tmndo_add_key(loaded, keys, values)

    call tmndo_print_key_file(loaded, trim(path)//'/tinker_mndo.key')
    call tmndo_write_coords(&
         & trim(path)//'/frame00000.xyz', trim(path)//'/tinker_mndo.xyz', &
         & au2ang * traj%geom)

    if (traj%step > conf%init_step) then
       ierr = copy(trim(path)//'.old/fort.11', trim(path)//'/fort.11')
       ierr = copy(trim(path)//'.old/imomap.dat', trim(path)//'/imomap.dat')
    end if
  end subroutine tinker_mndo_update

  subroutine tinker_mndo_run(self, stat)
    class(nx_tinker_mndo_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: msg
    character(len=:), allocatable :: cmd
    logical :: ext

    cmd = self%tmndo_env//'/'//self%gradinterf//' tinker_mndo.xyz'
    call call_external(cmd, ierr, outfile='tinker.out', errfile='stdout', cmdmsg=msg)
    if (ierr /= 0) then
       if (msg /= '') msg = ': '//trim(msg)
       call stat%append(NX_ERROR, &
            & 'Problem when running tinker_mndo'//trim(msg)//&
            & '. Please take a look at tinker_mndo.job/ log files.', & 
            & mod=MODNAME, func='tinker_mndo_run'&
            & )
    end if

  end subroutine tinker_mndo_run

  function tinker_mndo_read_output(self, conf, traj, path, stat) result(info)
    class(nx_tinker_mndo_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    real(dp), dimension(:, :), allocatable :: st_components
    real(dp), dimension(:, :), allocatable :: components
    real(dp), dimension(:), allocatable :: osc_str
    ! Temp. array for reading transition properties

    character(len=MAX_STR_SIZE) :: line, err_str, command, msg, cmd
    integer :: imaperr, scferr
    integer :: i, u, j, nat, dum, istate
    logical :: int_ok
    logical :: print_osc, ext

    integer :: sec_id, tmndo_natom_grd, tmndo_nstates, tmndo_istate, &
         tmndo_st_a, tmndo_st_b, nc, &
         nstat, ncoupl, iat, ierr

    character(len=:), allocatable :: outfile, outfile_nac, mndo_out
    

    character(len=*), parameter :: &
         & NAD_H = 'CARTESIAN INTERSTATE COUPLING GRADIENT FOR STATES', &
         & funcname = 'tinker_mndo_read_output'

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    outfile = trim(path)//'/interface.dat'
    outfile_nac = trim(path)//'/interface_nac.dat'
    mndo_out = trim(path)//'/tmp_mndo.out'

    ncoupl = conf%nstat*(conf%nstat - 1) / 2

    inquire(file=outfile, exist=ext)
    if(.not. ext) then
       msg = "ERROR: ACTIVE ORBITAL MAPPING FAILED"
       cmd = "grep '"//trim(msg)//"' tmp_mndo.out > /dev/null 2>&1"
       call execute_command_line(trim(cmd), exitstat=ierr)

       if(ierr == 0) then
          call stat%append(NX_WARNING, &
               & '(MNDO) Error encountered in orbital mapping.', &
               & mod=MODNAME, func=funcname &
               & )
          info%request_adapt_dt = .true.
       end if

       msg = "SCF CONVERGENCE FAILURE"
       cmd = "grep '"//trim(msg)//"' fort.15 > /dev/null 2>&1"
       call execute_command_line(trim(cmd), exitstat=ierr)
       if(ierr == 0) then
          call stat%append(NX_WARNING, &
               & '(MNDO) SCF failed to converge.', &
               & mod=MODNAME, func=funcname &
               & )
          info%request_adapt_dt = .true.
       end if

       if (info%request_adapt_dt) then
          call stat%append(NX_WARNING, &
               & 'I will attempt an adaptive time-step procedure.', &
               & mod=MODNAME, func=funcname &
               & )
       else
          call stat%append(NX_ERROR, &
               & '"interface.dat" file cannot be found ! Please check the Tinker '//&
               & 'and MNDO outputs.', &
               & mod=MODNAME, func=funcname &
               & )
       end if
    end if

    open(newunit=u, file=outfile, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       if (index(line, 'POTENTIAL') /= 0) then
          do i=1, conf%nstat
             read(u, *) info%repot(i)
          end do
       end if

       if (index(line ,'GRADIENT') /= 0) then
          read(line, *) msg, istate, dum
          do i=1, conf%nat
             read(u, *) (info%rgrad(istate, j, i), j=1, 3)
          end do
       end if
    end do
    close(u)

    open(newunit=u, file=outfile_nac, status='old', action='read')
    istate = 0
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       if (index(line, 'CARTESIAN INTERSTATE') /= 0) then
          ! MNDO prints the NACV in a different order than the one expected by NX, so we
          ! have to rearrange a little bit here.

          read(line, *) msg, msg, msg, msg, msg, msg, tmndo_st_a, tmndo_st_b
          istate = (tmndo_st_a - 2)*(tmndo_st_a - 1) / 2 + tmndo_st_b

          do i=1, conf%nat
             read(u, *) dum, (info%rnad(istate, j, i), j=1, 3)
          end do

       end if
    end do
    close(u)
    ! Convert in atomic units (needs to be done now for energy for scaling the NACV (see
    ! below)). For NAD we have them in kcalm / ang, and we divided them by energy in
    ! kcalm, so we just need to multiply by au2ang to get them in a.u.
    info%rnad = info%rnad * au2ang
    info%repot = info%repot / au2kcalm
    info%rgrad = info%rgrad * au2ang / au2kcalm

    ! Correct for MM NAC
    if(.not. self%compute_nac_mm) then
       do i=1, conf%nat
          if( .not. self%qm_filter(i) ) then
             info%rnad(:,:,i) = 0.0_dp
          end if
       end do
    end if

    allocate(st_components(3, conf%nstat))
    st_components(:, :) = 0.0_dp
    allocate(components(3, conf%nstat-1))
    components(:, :) = 0.0_dp

    ! Osc str
    print_osc = .false.

    if (self%has_osc_str) then
       print_osc = .true.
       call tmndo_read_osc_str(mndo_out, conf%nstat, stat, &
            & st_components, components, info%osc_str &
            & )
    end if

    if (print_osc) then
       ! Transition properties from GS
       call info%append_data('Oscillator strengths and transition moments (A.U.)')

       write(msg, '(5a15)') 'Excitation', 'Osc. str.', 'dx', 'dy', 'dz'
       call info%append_data(msg)

       write(msg, '(a)') repeat(' --------------', 5)
       call info%append_data(msg)

       do i=1, conf%nstat-1
          write(msg, '("      1 ->", i3, 4F15.6)') &
               & i+1, info%osc_str(i), (components(j, i), j=1, 3)
          call info%append_data(msg)
       end do
       ! State Dipoles
       msg = ''//NEW_LINE('a')
       msg = trim(msg)//'State Dipoles (A.U.)'
       msg = trim(msg)//NEW_LINE('a')
       call info%append_data(msg)

       write(msg, '(4a15)') 'State', 'dx', 'dy', 'dz'
       call info%append_data(msg)

       write(msg, '(a)') repeat(' --------------', 4)
       call info%append_data(msg)

       do i=1, conf%nstat
          write(msg, '(i15, 3F15.6)') &
               & i, (st_components(j, i), j=1, 3)
          call info%append_data(msg)
       end do
    end if

    deallocate(components)
  end function tinker_mndo_read_output

  subroutine tinker_mndo_write_geometry(self, traj, path, print_merged)
    class(nx_tinker_mndo_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: i, j, u

    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)
       write(u, '(3f13.6)') (traj%geom(j, i), j=1,3)
    end do
    close(u)
  end subroutine tinker_mndo_write_geometry

  subroutine tinker_mndo_init_overlap(self, path_to_qm, stat)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine tinker_mndo_init_overlap

  subroutine tinker_mndo_prepare_overlap(self, path_to_qm, stat)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine tinker_mndo_prepare_overlap

  function tinker_mndo_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_tinker_mndo_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function tinker_mndo_extract_overlap

  function tinker_mndo_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function tinker_mndo_get_lcao

  subroutine tinker_mndo_ovl_run(self, stat)
    class(nx_tinker_mndo_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine tinker_mndo_ovl_run

  subroutine tinker_mndo_ovl_post(self, stat)
    class(nx_tinker_mndo_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine tinker_mndo_ovl_post

  subroutine tinker_mndo_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine tinker_mndo_cio_prepare_files

  subroutine tinker_mndo_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine tinker_mndo_cio_get_singles_amplitudes

  subroutine tinker_mndo_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_tinker_mndo_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine tinker_mndo_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !

  function tmndo_load_key_file(keyfile) result(res)
    character(len=*) :: keyfile

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: u, ierr
    integer :: nlines
    character(len=MAX_STR_SIZE) :: buf

    nlines = 0
    open(newunit=u, file=keyfile, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
    end do

    allocate(res(nlines))
    res(:) = ''
    rewind(u)

    nlines = 0
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
       res(nlines) = trim(buf)

       ! print *, 'BUF ', nlines, ': ', trim(res(nlines))
    end do
    close(u)

    ! do nlines=1, size(res)
    !    print *, 'I ', nlines, ': ', trim(res(nlines))
    ! end do
    ! error stop
  end function tmndo_load_key_file


  function tmndo_delete_keys(loaded, keys) result(res)
    character(len=*) :: loaded(:)
    character(len=*) :: keys(:)

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    character(len=MAX_STR_SIZE) :: tmp(size(loaded))
    integer :: i, j, deleted
    integer :: delete_this(size(keys))

    delete_this(:) = 0

    do i=1, size(loaded)
       tmp(i) = trim(loaded(i))
       do j=1, size(keys)
          if (index(loaded(i), trim(keys(j))) /= 0) then
             tmp(i) = '$$$DELETED$$$'
             delete_this(j) = 1
          end if
       end do
    end do

    deleted = sum(delete_this)
    ! print *, delete_this
    ! print *, 'DELETED =  ', deleted

    allocate(res(size(loaded) - deleted))
    res(:) = ''
    j = 1
    do i=1, size(tmp)
       if (tmp(i) == '$$$DELETED$$$') then
          continue
       else
          res(j) = trim(tmp(i))
          j = j + 1
       end if
    end do
  end function tmndo_delete_keys


  function tmndo_add_key(loaded, keys, values) result(res)
    character(len=*) :: loaded(:)
    character(len=*) :: keys(:)
    character(len=*) :: values(:)

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: add_this(size(keys))
    integer :: i, j, added

    add_this(:) = 0

    do i=1, size(loaded)
       do j=1, size(keys)
          if (index(loaded(i), trim(keys(j))) == 0) then
             add_this(j) = 1
          end if
       end do
    end do

    added = sum(add_this)

    allocate(res(size(loaded) + added))
    res(:) = ''
    do i=1, size(loaded)
       res(i) = trim(loaded(i))
    end do

    if (added /= 0) then
       j = 1
       do i=1, size(keys)
          if (add_this(i) > 0) then
             res(size(loaded)+j) = trim(keys(i))//' '//trim(values(i))
             j = j + 1
          end if
       end do
    end if
  end function tmndo_add_key


  subroutine tmndo_print_key_file(loaded, filename)
    character(len=*), intent(in) :: loaded(:)
    character(len=*), intent(in) :: filename

    integer :: u, i

    open(newunit=u, file=filename, action='write')
    do i=1, size(loaded)
       write(u, '(a)') trim(loaded(i))
    end do
    close(u)
  end subroutine tmndo_print_key_file


  subroutine tmndo_write_coords(origin, newfile, geometry)
    character(len=*), intent(in) :: origin
    !! Original coordinate file.
    character(len=*), intent(in) :: newfile
    !! New coordinate file.
    real(dp), intent(in) :: geometry(:, :)
    !! Geometry in Angstrom

    integer :: u, v, ierr, at, i
    character(len=MAX_STR_SIZE) :: buf, tmp
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    integer :: grpsize

    open(newunit=u, file=origin, action='read')
    open(newunit=v, file=newfile, action='write')

    read(u, '(a)') buf
    write(v, '(a)') trim(buf)

    at = 1
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       group = split_pattern(buf)
       grpsize = size(group)
       tmp = ''
       if (grpsize >= 6) then
          do i=6, grpsize
             tmp = trim(tmp)//' '//trim(group(i))
          end do
       end if

       write(v, '(A6,A,A4,A,3F13.6,A)') &
            & trim(group(1)), ' ', adjustl(group(2)), ' ', &
            & geometry(1, at), geometry(2, at), geometry(3, at), &
            & ' '//trim(tmp)
       at = at + 1
    end do

    close(v)
    close(u)
  end subroutine tmndo_write_coords

  function tmndo_check_trdip(filename) result(res)
    character(len=*), intent(in) :: filename

    logical :: res

    character(len=*), parameter :: &
         & dipole_str = "Properties of transitions   1 -> #:"
    character(len=MAX_STR_SIZE) :: buf

    logical :: ext
    integer :: ierr, id, u

    res = .false.
    inquire(file=filename, exist=ext)
    if(ext) then
       open(newunit=u, file=filename, action='read')
       do
          read(u, '(A)', iostat=ierr) buf
          if (ierr /= 0) exit

          id = index(buf, dipole_str)
          if (id /= 0) then
             res = .true.
             exit
          end if
       end do
       close(u)
    end if
  end function tmndo_check_trdip

  subroutine tmndo_read_osc_str(filename, nstat, stat, st_components, components, osc_str)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nstat
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: st_components(:, :)
    real(dp), intent(out) :: components(:, :)
    real(dp), intent(out) :: osc_str(:)

    character(len=*), parameter :: &
         & HEADER_OSCSTR = " Properties of transitions   1 -> #:", &
         & HEADER_TRNDIP = " Dipole-length electric dipole transition moments   1 -> #:", &
         & HEADER_STDIP = " State dipole moments:", &
         & funcname = 'tmndo_read_osc_str'
    

    character(len=MAX_STR_SIZE) :: line
    integer :: u, sec_id, ierr, i, dumi, j
    real(dp) :: dumr
    character(len=2) :: dumc


    open(newunit=u, file=filename, status='old', action='read')
    i = 0
    sec_id = 0
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       if (index(line, HEADER_STDIP) /= 0) then
          read(u, *)
          read(u, *)
          do i=1, nstat
             read(u, *) dumi, dumc, dumc, dumr, dumr, (st_components(j, i), j=1, 3), &
                  & dumr, dumr, dumr
          end do
       end if

       if (index(line, HEADER_OSCSTR) /= 0) then
          read(u, *)
          read(u, *)
          do i=1, nstat - 1
             read(u, *) dumi, dumc, dumc, dumr, dumr, dumr, osc_str(i), &
                  & dumr, dumr, dumr
          end do
       end if

       if (index(line, HEADER_TRNDIP) /= 0) then
          read(u, *)
          read(u, *)
          do i=1, nstat - 1
             read(u, *) dumi, dumc, dumc, dumr, dumr, (components(j, i), j=1, 3), &
                  & dumr, dumr, dumr
          end do
       end if

       ! if( sec_id == 0 .and. line(1:36) == HEADER_OSCSTR ) then
       !    sec_id = 1

       !    !! Skip two lines
       !    do i = 1, 2
       !       read(u, '(a)', iostat=ierr) line
       !       if(ierr /= 0) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading oscillator strengths: "//&
       !               & trim(line), &
       !               & mod=MODNAME, func=funcname&
       !               & )
       !          ! call nx_log%log(LOG_ERROR, &
       !          !      & "Unexpected error while reading oscillator strenghts.")
       !          ! 
       !          ! error STOP
       !       end if
       !    end do

       !    i = 1
       ! else if( sec_id == 0 .and. line(1:59) == HEADER_TRNDIP ) then
       !    sec_id = 2

       !    !! Skip two lines
       !    do i = 1, 2
       !       read(u, '(a)', iostat=ierr) line
       !       if(ierr /= 0) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading transition dipoles: "//&
       !               & trim(line), &
       !               & mod=MODNAME, func=funcname&
       !               & )
       !          ! call nx_log%log(LOG_ERROR, &
       !          !      & "Unexpected error while reading transition dipoles.")
       !          ! error stop
       !       end if
       !    end do

       !    i = 1
       ! else if( sec_id == 0 .and. line(1:22) == HEADER_STDIP ) then
       !    sec_id = 3

       !    !! Skip two lines
       !    do i = 1, 2
       !       read(u, '(a)', iostat=ierr) line
       !       if(ierr /= 0) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading state dipoles: "//&
       !               & trim(line), &
       !               & mod=MODNAME, func=funcname&
       !               & )
       !          ! call nx_log%log(LOG_ERROR, "Unexpected error while reading state dipoles.")
       !          ! error stop
       !       end if
       !    end do

       !    i = 1
       ! else if(sec_id == 1) then ! Oscillator strength
       !    if( trim(line) == "" ) then
       !       sec_id = 0
       !    else
       !       if(i >= nstat) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading oscillator strengths: "//&
       !               & "state "//to_str(i)//" > NSTAT = "//to_str(nstat), &
       !               & mod=MODNAME, func=funcname &
       !               & )
       !          ! call nx_log%log(LOG_ERROR, &
       !          !      & "Unexpected error while reading oscillator strenghts.")
       !          ! error stop
       !       end if
       !       read(line(52:62), '(f10.6)') osc_str(i)
       !       i = i + 1
       !    end if
       ! else if(sec_id == 2) then ! Transition dipoles
       !    if( trim(line) == "" ) then
       !       sec_id = 0
       !    else
       !       if(i >= nstat) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading transition dipoles: "//&
       !               & "state "//to_str(i)//" > NSTAT = "//to_str(nstat), &
       !               & mod=MODNAME, func=funcname &
       !               & )
       !          ! call nx_log%log(LOG_ERROR, &
       !          !      & "Unexpected error while reading transition dipoles.")
       !          ! error stop
       !       end if
       !       read(line(41:76), '(3f12.6)') components(1,i), &
       !            & components(2,i), components(3, i)
       !       i = i + 1
       !    end if
       ! else if(sec_id == 3) then ! State dipoles
       !    if( trim(line) == "" ) then
       !       sec_id = 0
       !    else
       !       if(i > nstat) then
       !          call stat%append(NX_ERROR, &
       !               & "Unexpected error while reading state dipoles: "//&
       !               & "state "//to_str(i)//" > NSTAT = "//to_str(nstat), &
       !               & mod=MODNAME, func=funcname &
       !               & )
       !          ! call nx_log%log(LOG_ERROR, &
       !          !      &  "Unexpected error while reading state dipoles.")
       !          ! error stop
       !       end if
       !       read(line(41:76), '(3f12.6)') st_components(1,i), &
       !            & st_components(2,i), st_components(3, i)
       !       i = i + 1
       !    end if
       ! end if
    end do
    close(u)

    ! Convert dipoles in D to A.U.
    components(:,:) = components(:,:) * 0.3935
    st_components(:,:) = st_components(:,:) * 0.3935

  end subroutine tmndo_read_osc_str

  function has_osc_str_keyword(filename) result(res)
    character(len=*), intent(in) :: filename

    logical :: res

    integer :: u, ierr, id, test
    character(len=256) :: buf

    res = .false.

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'iuvcd=')
       if (id /= 0) then
          read(buf(id+6:id+6), *) test
          if (test == 2) res = .true.
          exit
       end if
    end do
    close(u)
  end function has_osc_str_keyword
  
  
end module mod_tinker_mndo_t
