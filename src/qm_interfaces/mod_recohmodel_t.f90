! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_recohmodel_t
  !! # Model for studying recoherence
  !!
  !!  Model 1: Flat Adiabats with one avoided crossing
  !!  Model 2: Flat Adiabats with two avoided crossings
  !!
  !! Reference: Subotnik et al. (J. Phys. Chem. A, 123, 5428 (2019)).
  use mod_analytical_generic_1d_t, only: nx_analytical_generic_1d_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: pi
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_recohmodel_t

  type, extends(nx_analytical_generic_1d_t) :: nx_recohmodel_t
     integer :: mnum = 1
     real(dp) :: a = 0.01_dp
     real(dp) :: b = 3.0_dp
     real(dp) :: c = 3.0_dp
   contains
     private
     procedure, public :: setup => recohmodel_setup
     procedure, public :: print => recohmodel_print
     procedure, public :: to_str => recohmodel_to_str
     procedure, public :: backup => recohmodel_backup
     procedure, public :: update => recohmodel_update
     procedure, public :: run => recohmodel_run
     procedure, public :: read_output => recohmodel_read_output
     procedure, public :: write_geom => recohmodel_write_geometry
     procedure, public :: init_overlap => recohmodel_init_overlap
     procedure, public :: prepare_overlap => recohmodel_prepare_overlap
     procedure, public :: extract_overlap => recohmodel_extract_overlap
     procedure, public :: ovl_post => recohmodel_ovl_post
     procedure, public :: ovl_run => recohmodel_ovl_run
     procedure, public :: get_lcao => recohmodel_get_lcao
     procedure, public :: cio_prepare_files => recohmodel_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & recohmodel_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => recohmodel_cio_get_mos_energies
  end type nx_recohmodel_t
  interface nx_recohmodel_t
     module procedure constructor
  end interface nx_recohmodel_t

  character(len=*), parameter :: MODNAME = 'mod_recohmodel_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, &
       ! MANDATORY program- and method-specific parameters
       & nat, nstat, &
       ! OPTIONAL program- and method-specific parameters
       & a, b, c, mnum &
       & ) result(res)
    !! Constructor for the ``nx_recohmodel_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in) :: nat
    integer, intent(in) :: nstat
    real(dp), intent(in), optional :: a
    real(dp), intent(in), optional :: b
    real(dp), intent(in), optional :: c
    integer, intent(in), optional :: mnum

    type(nx_recohmodel_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    call res%set_method(method)
    call res%memalloc(nat, nstat)

    if(present(a)) res%a =  a
    if(present(b)) res%b =  b
    if(present(c)) res%c =  c
    if(present(mnum)) res%mnum = mnum
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine recohmodel_setup(self, parser, conf, inp_path, stat)
    class(nx_recohmodel_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    call set(parser, 'recohmodel', self%mnum, 'modelnum')
    call set(parser, 'recohmodel', self%a, 'A')
    call set(parser, 'recohmodel', self%b, 'B')
    call set(parser, 'recohmodel', self%c, 'C')
  end subroutine recohmodel_setup

  subroutine recohmodel_print(self, out)
    class(nx_recohmodel_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    write(output, *) 'Recoherence Model: Subotnik et al. (J. Phys. Chem. A, 123, 5428 (2019).'
    call print_conf_ele(self%mnum, 'model no. = ', unit=output)
    if (self%mnum == 1) then
       write(output, *) '   Two flat adiabats with one avoided crossing.'
    else if (self%mnum == 2) then
       write(output, *) '   Two flat adiabats with two  avoided crossing.'
    end if
    call print_conf_ele(self%a, 'A', unit=output)
    call print_conf_ele(self%b, 'B', unit=output)
    call print_conf_ele(self%c, 'C', unit=output)
  end subroutine recohmodel_print


  function recohmodel_to_str(self) result(res)
    class(nx_recohmodel_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&recohmodel'//nl
    res = res//' mnum = '//to_str(self%mnum)//nl
    res = res//' a = '//to_str(self%a)//nl
    res = res//' b = '//to_str(self%b)//nl
    res = res//' c = '//to_str(self%c)//nl
    res = res//'/'//nl
  end function recohmodel_to_str

  subroutine recohmodel_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine recohmodel_backup

  subroutine recohmodel_update(self, conf, traj, path, stat)
    class(nx_recohmodel_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    ! Transfer new geometry and velocities to the object.
    call self%set_from_traj(traj%geom, traj%veloc)
  end subroutine recohmodel_update

  subroutine recohmodel_run(self, stat)
    class(nx_recohmodel_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: theta, dtheta, h(2, 2), dh(2, 2)

    call compute_theta(self%mnum, self%b, self%c, self%geom(), theta, dtheta)

    h = compute_diab_hamiltonian(self%a, theta)
    dh = compute_diab_deriv_hamiltonian(self%a, theta, dtheta)

    call self%set_epot( compute_epot(h, dh) )
    call self%set_grad( compute_grad(h, dh) )
    call self%set_nad( compute_nad(h, dh) )
  end subroutine recohmodel_run

  function recohmodel_read_output(self, conf, traj, path, stat) result(info)
    class(nx_recohmodel_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    info%has_osc_str = .false.
    info%has_data_qm = .false.

    info%repot(:) = self%epot()

    ! Transfer gradients
    info%rgrad(:, 1, :) = self%grad()

    ! Transfer NAD
    info%rnad(:, 1, :) = self%nad()

    ! CS-FSSH
    if (conf%run_complex) then
       info%rnad_i(:, 1, :) = self%nad_i()
       info%rgamma(:) = self%gamma()
    end if
  end function recohmodel_read_output

  subroutine recohmodel_write_geometry(self, traj, path, print_merged)
    class(nx_recohmodel_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged
  end subroutine recohmodel_write_geometry

  subroutine recohmodel_init_overlap(self, path_to_qm, stat)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine recohmodel_init_overlap

  subroutine recohmodel_prepare_overlap(self, path_to_qm, stat)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine recohmodel_prepare_overlap

  function recohmodel_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_recohmodel_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function recohmodel_extract_overlap

  function recohmodel_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function recohmodel_get_lcao

  subroutine recohmodel_ovl_run(self, stat)
    class(nx_recohmodel_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine recohmodel_ovl_run

  subroutine recohmodel_ovl_post(self, stat)
    class(nx_recohmodel_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine recohmodel_ovl_post

  subroutine recohmodel_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine recohmodel_cio_prepare_files

  subroutine recohmodel_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine recohmodel_cio_get_singles_amplitudes

  subroutine recohmodel_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_recohmodel_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine recohmodel_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  pure subroutine compute_theta(mnum, b, c, r, theta, dtheta)
    integer, intent(in) :: mnum
    real(dp), intent(in) :: b
    real(dp), intent(in) :: c
    real(dp), intent(in) :: r(:)
    real(dp), intent(out) :: theta
    real(dp), intent(out) :: dtheta

    real(dp) :: erfunc, erfunc1, erfunc2

    select case(mnum)
    case(1)
       erfunc = erf(b * r(1))
       theta  = (pi/2.0_dp) * (erfunc + 1.0_dp)
       dtheta = b * sqrt(pi) * exp(-(b * r(1))**2)
    case(2)
       erfunc1 = erf(b * (r(1) - c))
       erfunc2 = erf(b * (r(1) + c))
       theta = (pi/2.0_dp) * (erfunc1 - erfunc2)
       dtheta = b * sqrt(pi) &
            & * ( exp(-(b*(r(1) - c))**2) - exp(-(b*(r(1) + c))**2) )
    end select
  end subroutine compute_theta

  pure function compute_diab_hamiltonian(a, theta) result(h)
    real(dp), intent(in) :: a
    real(dp), intent(in) :: theta

    real(dp) :: h(2, 2)

    h(1,1) = a * cos(theta)
    h(2,2) = a * cos(theta)
    h(1,2) = a * sin(theta)
    h(2,1) = h(1,2)
  end function compute_diab_hamiltonian

  pure function compute_diab_deriv_hamiltonian(a, theta, dtheta) result(dh)
    real(dp), intent(in) :: a
    real(dp), intent(in) :: theta
    real(dp), intent(in) :: dtheta
    
    real(dp) :: dh(2, 2)

    dh(1,1) = a * sin(theta) * dtheta
    dh(2,2) =-a * sin(theta) * dtheta
    dh(1,2) = a * cos(theta) * dtheta
    dh(2,1) = dh(1,2)
  end function compute_diab_deriv_hamiltonian

  pure function compute_epot(h, dh) result(epot)
    real(dp), intent(in) :: h(2, 2)
    real(dp), intent(in) :: dh(2, 2)

    real(dp) :: epot(2)

    real(dp) :: tmp

    tmp = (h(2,2)-h(1,1))**2 + 4.0_dp*h(1,2)*h(1,2)
    epot(1) = ( (h(1,1)+h(2,2)) - sqrt(tmp) ) * 0.5_dp
    epot(2) = ( (h(1,1)+h(2,2)) + sqrt(tmp) ) * 0.5_dp
  end function compute_epot

  pure function compute_grad(h, dh) result(grad)
    real(dp), intent(in) :: h(2, 2)
    real(dp), intent(in) :: dh(2, 2)

    real(dp) :: grad(2, 1)

    real(dp) :: tmp1, tmp2, tmp3, tmp4

    tmp1 = (dh(1,1) + dh(2,2)) * 0.5_dp
    tmp2 = 0.5_dp * (h(2,2)-h(1,1)) * (dh(2,2)-dh(1,1))
    tmp3 = 2.0_dp * h(1,2) * dh(1,2)
    tmp4 = 0.25_dp * (h(2,2)-h(1,1))**2 + h(1,2)*h(1,2)
    grad(1, 1) = tmp1 - (tmp2 + tmp3) * (1.0_dp/sqrt(tmp4))
    grad(2, 1) = tmp1 + (tmp2 + tmp3) * (1.0_dp/sqrt(tmp4))
  end function compute_grad

  pure function compute_nad(h, dh) result(nad)
    real(dp), intent(in) :: h(2, 2)
    real(dp), intent(in) :: dh(2, 2)

    real(dp) :: nad(1, 1)

    real(dp) :: tm1, tm2, tm3

    tm1 = 1.0_dp + (2.0_dp * h(1,2) / (h(2,2)-h(1,1)) )**2
    tm2 = 1.0D0/(h(2,2)-h(1,1)) * dh(1,2)
    tm3 = h(1,2)/(h(2,2)-h(1,1))**2 * (dh(2,2)-dh(1,1))
    nad(1, 1) = -1.0_dp/tm1 * (tm2 - tm3)
  end function compute_nad
  
end module mod_recohmodel_t
