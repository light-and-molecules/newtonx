! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_gaussian_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ang, au2ev
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, call_external
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_interface, only: &
       & copy, mkdir, gzip
  use mod_orbspace, only: nx_orbspace_t
  use mod_print_utils, only: &
       & to_lower
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: &
       & split_pattern, to_str, split_blanks, &
       & remove_blanks
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_gaussian_t
  public :: gau_read_orb
  ! For use in exc_gaussian

  public :: gau_parse_input

  type, extends(nx_qm_generic_t) :: nx_gaussian_t
     private
     integer :: mocoef = 1
     !!  Molecular orbitals usage:
     !!
     !! - ``0``: Compute the initial guess at every time step ;
     !! - ``1``: Use the mocoef from the previous time step ;
     !! - ``2``: Use the same checkpoint file for all time steps.
     integer :: td_st = 0
     !! Reading of excited states from the previous steps:
     !!
     !! - ``0``: Compute the excited states without previous reference ;
     !! - ``1``: Read states from the checkpoint file, what file is controlled by mocoef
     !!   parameter.
     integer :: type = 0
     !! Wavefunction occupation:
     !!
     !! - ``0``: closed shell RHF wavefunction (in GS)
     !! - ``1``: open shell, restricted or unrestricted.
     integer :: ld_thr = 14
     !! Linear dependence threshold.
     logical :: no_fc = .false.
     !! If ``.true.`` do a full correlation computation (don't freeze any core orbitals).  This
     !! amounts to having an instruction ``TD(Full,Nstates=...)``.
     character(len=:), allocatable :: g16root
     !! Gaussian ``g16root`` environment.
     integer :: coptda = 1
     !! Linear response vectors to be used for the evaluation of NAD
     !! (``cioverlap``-related). (Default: 1).
     !!
     !! - ``0``: Use ``|X>`` ;
     !! - ``1``: Use ``|X+Y>`` ;
     integer :: typedft = -1
     !! Type of DFT computation (automatically set during dynamics, see the documentation
     !! for ``nx_traj_t`` for more details).
   contains
     private
     procedure, public :: setup => gaussian_setup
     procedure, public :: print => gaussian_print
     procedure, public :: to_str => gaussian_to_str
     procedure, public :: backup => gaussian_backup
     procedure, public :: update => gaussian_update
     procedure, public :: run => gaussian_run
     procedure, public :: read_output => gaussian_read_output
     procedure, public :: write_geom => gaussian_write_geometry
     procedure, public :: init_overlap => gaussian_init_overlap
     procedure, public :: prepare_overlap => gaussian_prepare_overlap
     procedure, public :: extract_overlap => gaussian_extract_overlap
     procedure, public :: ovl_post => gaussian_ovl_post
     procedure, public :: ovl_run => gaussian_ovl_run
     procedure, public :: get_lcao => gaussian_get_lcao
     procedure, public :: cio_prepare_files => gaussian_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & gaussian_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => gaussian_cio_get_mos_energies
     procedure :: generate_route => gau_generate_route
  end type nx_gaussian_t
  interface nx_gaussian_t
     module procedure constructor
  end interface nx_gaussian_t

  character(len=*), parameter :: MODNAME = 'mod_gaussian_t'

contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & mocoef, td_st, type, ld_thr, no_fc, &
       & coptda, g16root) result(res)
    !! Constructor for the ``nx_gaussian_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    integer, intent(in), optional :: mocoef
    integer, intent(in), optional :: td_st
    integer, intent(in), optional :: type
    integer, intent(in), optional :: ld_thr
    logical, intent(in), optional :: no_fc
    integer, intent(in), optional :: coptda
    character(len=*), intent(in), optional :: g16root

    type(nx_gaussian_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(mocoef)) res%mocoef = mocoef
    if (present(td_st)) res%td_st = td_st
    if (present(type)) res%type = type
    if (present(ld_thr)) res%ld_thr = ld_thr
    if (present(no_fc)) res%no_fc = no_fc
    if (present(coptda)) res%coptda = coptda
    if (present(g16root)) then
       res%g16root = g16root
    else
       res%g16root = ''
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine gaussian_setup(self, parser, conf, inp_path, stat)
    class(nx_gaussian_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    character(len=*), parameter :: funcname = 'gaussian_setup'
    character(len=MAX_STR_SIZE) :: env
    integer :: prt_mo, ierr

    prt_mo = -1
    call set(parser, 'gaussian', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%g16root == '') then
       call get_environment_variable("g16root", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $g16root environment is not defined', &
               & mod=modname, func=funcname)
          return
       else
          self%g16root = trim(env)
       end if
    end if
    call set(parser, 'gaussian', self%mocoef, 'mocoef')
    call set(parser, 'gaussian', self%td_st, 'td_st')
    call set(parser, 'gaussian', self%type, 'kind_g09')
    call set(parser, 'gaussian', self%ld_thr, 'ld_thr')
    call set(parser, 'gaussian', self%no_fc, 'no_fc')
    call set(parser, 'gaussian', self%coptda, 'coptda')
  end subroutine gaussian_setup

  subroutine gaussian_print(self, out)
    class(nx_gaussian_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%g16root, '$g16root path', unit=output)
    call print_conf_ele(self%mocoef, 'mocoef', unit=output)
    call print_conf_ele(self%td_st, 'td_st', unit=output)
    call print_conf_ele(self%type, 'type', unit=output)
    call print_conf_ele(self%ld_thr, 'ld_thr', unit=output)
    call print_conf_ele(self%coptda, 'coptda', unit=output)
  end subroutine gaussian_print


  function gaussian_to_str(self) result(res)
    class(nx_gaussian_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&gaussian'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' mocoef = '//to_str(self%mocoef)//nl
    res = res//' td_st = '//to_str(self%td_st)//nl
    res = res//' type = '//to_str(self%type)//nl
    res = res//' ld_thr = '//to_str(self%ld_thr)//nl
    res = res//' no_fc = '//to_str(self%no_fc)//nl
    res = res//' coptda = '//to_str(self%coptda)//nl
    res = res//'/'//nl
  end function gaussian_to_str


  subroutine gaussian_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo

    logical :: copy_only_mo
    integer :: ierr
    character(len=256) :: files_to_copy(2)

    copy_only_mo = .false.
    if (present(only_mo)) copy_only_mo = only_mo

    files_to_copy = [ character(len=256) :: &
         & trim(qm_path)//'/gaussian.chk', &
         & trim(qm_path)//'/gaussian.log'&
         & ]

    ierr = copy(files_to_copy, chk_path//'/')
    if (ierr /= 0) &
         call stat%append(&
         & NX_ERROR, 'Error in copying files to '//trim(chk_path), &
         & mod=MODNAME, func='gaussian_backup' &
         & )

    files_to_copy = [ character(len=256) :: &
         & trim(chk_path)//'/gaussian.chk', &
         & trim(chk_path)//'/gaussian.log'&
         & ]
    ierr = gzip(files_to_copy)
    if (ierr /= 0) &
         call stat%append(&
         & NX_ERROR, 'Error in gzipping files in '//trim(chk_path), &
         & mod=MODNAME, func='gaussian_backup' &
         & )
  end subroutine gaussian_backup


  subroutine gaussian_update(self, conf, traj, path, stat)
    class(nx_gaussian_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    character(len=1), parameter :: nl = NEW_LINE('c')

    integer :: u, v, ierr, i
    character(len=MAX_STR_SIZE) :: link0, route, title, other
    character(len=MAX_STR_SIZE) :: buf, tmp
    character(len=MAX_STR_SIZE), allocatable :: group(:)

    integer :: charge, mult
    logical :: ext, is_first_step

    is_first_step = traj%step == conf%init_step

    ierr = 0

    ! Some checks first
    if (is_first_step) then
       inquire(file=trim(path)//'/basis', exist=ext)
       if (.not. ext) then
          call stat%append(NX_ERROR, &
               & 'No "basis" file provided. Please provide a "basis" file with '// &
               & 'the name of the chosen basis set in '//trim(conf%init_input), &
               & mod=MODNAME, func='gaussian_setup')
       end if
    end if
       
    if ((is_first_step .and. self%mocoef /= 0) .or. self%mocoef == 2) then
       ! Check if an initial guess with the orbitals is present in the input folder. This
       ! is relevant only in the case ``mocoef = 2`` (always use the same initial guess,
       ! provided by the user) and ``mocoef = 1`` only at the first step.
       inquire(file=trim(path)//'/gaussian.chk.ini', exist=ext)
       if (.not. ext) then
          call stat%append(NX_ERROR, &
               & 'No gaussian.chk.ini file provided with mocoef='//&
               & to_str(self%mocoef), &
               & mod=MODNAME, func='gaussian_setup')
       end if
    end if

    if (traj%nstatdyn > 1) then
       self%typedft = 1
    else if ((traj%nstatdyn == 1) .and. (conf%nstat > 1)) then
       self%typedft = 2
    else
       self%typedft = 3
    end if

    call gau_parse_input(trim(path)//'/gaussian.com', &
         & link0, route, title, charge, mult, other)

    route = self%generate_route(&
         & route, conf%nstat, traj%nstatdyn, (traj%step == conf%init_step)&
         & )

    if (index(link0, '%chk') == 0) then
       link0 = trim(link0)//nl//'%chk=gaussian.chk'
    end if

    if (index(link0, '%rwf') == 0) then
       link0 = trim(link0)//nl//'%rwf=gaussian.rwf'
    end if

    open(newunit=u, file=trim(path)//'/gaussian.com', action='write')
    write(u, '(a)') trim(link0)
    write(u, '(a)') trim(route)
    write(u, '(a)') ''
    write(u, '(a)') trim(title)
    write(u, '(a)') ''
    write(u, '(I5,I5)') charge, mult

    open(newunit=v, file=trim(path)//'/geom', action='read')
    do
       read(v, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(u, '(a)') trim(buf)
    end do
    write(u, '(a)') ' '

    write(u, '(a)') trim(other)
    write(u, '(a)') ' '

    if (self%typedft == 2) then
       write(u, '(a)') '--link1--'
       write(u, '(a)') trim(link0)

       group = split_pattern(route)
       tmp = ''
       do i=1, size(group)
          if (group(i)(1:2) == 'td' .or. group(i)(1:3) == 'cis') then
             continue
          else
             write(tmp, '(a)') trim(tmp)//' '//trim(group(i))
          end if
       end do
       write(u, '(a)') trim(tmp)//' force Geom=Allcheck'
       write(u, '(a)') ''
       write(u, '(a)') trim(other)
    end if

    close(v)
    close(u)
  end subroutine gaussian_update


  subroutine gaussian_run(self, stat)
    class(nx_gaussian_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg

    call call_external(&
         & self%g16root//'/g16/g16 gaussian.com', ierr, &
         & outfile='gaussian.log', cmdmsg=cmdmsg &
         & )
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running g16'//trim(cmdmsg)//&
            & '. Please take a look at gaussian.job/gaussian.log', & 
            & mod=MODNAME, func='gaussian_run'&
            & )
    end if
  end subroutine gaussian_run


  function gaussian_read_output(self, conf, traj, path, stat) result(info)
    class(nx_gaussian_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    integer :: u, id, state_id, id1, id2, i
    real(dp) :: en
    integer :: ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    character(len=64) :: osc_str

    character(len=*), parameter :: funcname = 'gaussian_read_output'
    character(len=:), allocatable :: logfile

    logfile = trim(path)//'/gaussian.log'

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    state_id = 1

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       info%orb = gau_read_orb( logfile )
    end if

    ! info%dim_dataread = 1
    open(newunit=u, file=logfile, action='read')
    do
       read(u, '(A)', iostat=ierr) buf

       if (ierr /= 0) exit

       id = index(buf, 'Convergence criterion not met')
       if (id /= 0) then
          call stat%append(NX_ERROR, &
               & 'Convergence criterion not met (see gaussian.log)', &
               & mod=MODNAME, func=funcname&
               & )
          ! ierr = 1
          ! call nx_log%log(LOG_ERROR, 'Convergence criterion not met (see gaussian.log)')
          ! call check_error(ierr, GAU_ERR_CONV)
       end if

       id = index(buf, 'SCF Done:')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(5), *) info%repot(1)
       end if

       id = index(buf, 'Excited State ') ! Capital letters required here !!!
       if (id /= 0) then
          state_id = state_id + 1
          call split_blanks(buf, split)
          read(split(5), *) en
          info%repot(state_id) = info%repot(1) + en / au2ev

          read(split(9), *) osc_str
          read(osc_str(3:), *) info%osc_str(state_id - 1)

          ! call info%append_data('Dominant contributions and oscillator strengths', unique=.true.)
          call info%append_data(buf)

          FIND_CONTRIB: do
             read(u, '(A)') buf

             id1 = index(buf, '->')
             id2 = index(buf, '<-')
             if ((id1 /= 0) .or. (id2 /= 0)) then
                call info%append_data(buf)
             else
                exit FIND_CONTRIB
             end if
          end do FIND_CONTRIB
       end if

       id = index(buf, 'Center     Atomic                   Forces')
       if (id /= 0) then
          ! Read two lines (no information in those)
          read(u, *) buf
          read(u, *) buf
          do i=1, conf%nat
             read(u, '(a)') buf
             call split_blanks(buf, split)
             read(split(3), *) info%rgrad(traj%nstatdyn, 1, i)
             read(split(4), *) info%rgrad(traj%nstatdyn, 2, i)
             read(split(5), *) info%rgrad(traj%nstatdyn, 3, i)
          end do
       end if
    end do

    ! Gaussian prints the force, and we want the gradient.
    info%rgrad(:, :, :) = -info%rgrad(:, :, :)
    close(u)
  end function gaussian_read_output


  subroutine gaussian_write_geometry(self, traj, path, print_merged)
    class(nx_gaussian_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: i, u, j
    logical :: merge_coords

    merge_coords = .false.
    if (present(print_merged)) merge_coords = print_merged

    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)
       write(u, '(A4, 3F12.8)') traj%atoms(i), (traj%geom(j, i)*au2ang, j=1, 3)
    end do
    if (merge_coords) then
       do i=1, size(traj%geom, 2)
          write(u, '(A4, 3F12.8)') traj%atoms(i), (traj%old_geom(j, i)*au2ang, j=1, 3)
       end do
    end if
    close(u)
  end subroutine gaussian_write_geometry

  subroutine gaussian_init_overlap(self, path_to_qm, stat)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: link0
    character(len=MAX_STR_SIZE) :: route
    character(len=MAX_STR_SIZE) :: title
    integer :: charge
    integer :: mult
    character(len=MAX_STR_SIZE) :: other
    character(len=MAX_STR_SIZE) :: basis
    logical :: ext1, ext2
    integer :: ierr, u

    ierr = mkdir('double_molecule_input')

    call gau_parse_input(&
         & trim(path_to_qm)//'/gaussian.com', &
         & link0, route, title, charge, mult, other&
         & )

    route = "#p nosymm Geom=NoTest IOp(3/33=1)"

    inquire(file=trim(path_to_qm)//'/basis', exist=ext1)
    inquire(file=trim(path_to_qm)//'/basis2', exist=ext2)
    if (ext1) then
       open(newunit=u, file=trim(path_to_qm)//'/basis', action='read')
       read(u, '(a)') basis
       close(u)

       route = trim(route)//' '//trim(basis)
    else if (ext2) then
       route = trim(route)//' '//'GEN'
       ierr = copy(trim(path_to_qm)//'/basis2', 'double_molecule_input/basis2')
    end if

    open(newunit=u, file='double_molecule_input/sgaussian.com', action='write')
    write(u, '(a)') "%kjob l302"
    write(u, '(a)') "%rwf=sgaussian"
    write(u, '(a)') "%chk=sgaussian"
    write(u, '(a)') trim(route)
    write(u, '(a)') ""
    write(u, '(a)') "Overlap run"
    write(u, '(a)') ""
    write(u, '(a)') "0 1"
    close(u)
  end subroutine gaussian_init_overlap

  subroutine gaussian_prepare_overlap(self, path_to_qm, stat)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: u, v, ierr, i
    integer :: nat, at
    character(len=MAX_STR_SIZE) :: buf, gen, gen_ele, orig, added
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    character(len=:), allocatable :: tmp
    logical :: ext
    character(len=1) :: nl

    nl = NEW_LINE('c')

    open(newunit=v, file='overlap/sgaussian.com', action='write')

    ! Header part
    open(newunit=u, file='double_molecule_input/sgaussian.com', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do
    close(u)

    ! Geometry
    nat = 0
    open(newunit=u, file='overlap/geom', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
       nat = nat + 1
    end do
    close(u)

    nat = nat / 2

    ! Generated basis
    inquire(file='double_molecule_input/basis2', exist=ext)
    if (ext) then
       gen = ''
       open(newunit=u, file='double_molecule_input/basis2', action='read')
       do
          read(u, '(a)', iostat=ierr) buf
          if (ierr /= 0) exit

          tmp = remove_blanks(buf)

          gen_ele = ''

          group = split_pattern(tmp)
          read(group(1), *, iostat=ierr) at
          if (ierr /= 0) then
             gen_ele = trim(tmp)
          else
             orig = tmp(1:len_trim(tmp)-1)
             added = ''
             do i=1, size(group)-1
                at = at + nat
                added = trim(added)//' '//to_str(at)
             end do
             added = trim(added)//' 0'
             gen_ele = trim(orig)//trim(added)
          end if

          gen = trim(gen)//trim(gen_ele)//nl
       end do
       close(u)

       write(v, '(a)') ''
       write(v, '(a)') trim(gen)
    end if

    write(v, '(a)') ''

    close(v)
  end subroutine gaussian_prepare_overlap


  function gaussian_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_gaussian_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)

    integer :: u, id, ierr, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: rem, nlines, nele, ele

    ! We first need to 
    
    open(newunit=u, file='overlap/S_matrix', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nlines = int(nele / 5)
          rem = mod(nele, nlines)

          if (allocated(split)) deallocate(split)
          allocate(split(5))

          ele = 1
          do i=1, nlines
             read(u, *) split
             do j=1, 5
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end do

          if (rem /= 0) then
             deallocate(split)
             allocate(split(rem))
             read(u, *) split
             do j=1, rem
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end if

       end if

    end do
    close(u)
  end function gaussian_extract_overlap

  function gaussian_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)

    integer :: u, ierr, id, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer :: nele, rem, nbLines, ele

    real(dp), dimension(:), allocatable :: temp

    allocate(temp(nao*nao))
    ele = 1

    ! First read the matrix as a vector
    open(newunit=u, file=trim(dir_path)//'/MO_coefs_a', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nbLines = int(nele / 5)
          rem = mod(nele, nbLines)

          do i=1, nbLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                read(split(j), *) temp(ele)
                ele = ele + 1
             end do

          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) temp(ele)
                ele = ele + 1
             end do
          end if

       end if
    end do
    close(u)

    do i=1, nao
       ele = i
       do j=1, nao
          lcao(i, j) = temp(ele)
          ele = ele + nao
       end do
    end do
  end function gaussian_get_lcao

  subroutine gaussian_ovl_run(self, stat)
    class(nx_gaussian_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg

    call call_external(&
         & self%g16root//'/g16/g16 sgaussian.com', ierr, &
         & outfile='sgaussian.log', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running g16'//trim(cmdmsg)//&
            & '. Please see overlap/sgaussian.log for details.', &
            & mod=MODNAME, func='gaussian_ovl_post'&
            & )
    end if
  end subroutine gaussian_ovl_run

  subroutine gaussian_ovl_post(self, stat)
    class(nx_gaussian_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg

    call execute_command_line(&
         & self%g16root//'/g16/rwfdump overlap/sgaussian.rwf overlap/S_matrix 514R',&
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running rwfdump'//trim(cmdmsg), &
            & mod=MODNAME, func='gaussian_ovl_post'&
            & )
    end if
  end subroutine gaussian_ovl_post


  subroutine gaussian_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: cmdmsg
    integer :: ierr
    character(len=:), allocatable :: rwfdump
    character(len=*), parameter :: funcname = 'gaussian_cio_prepare_files'

    rwfdump = self%g16root//'/g16/rwfdump '//trim(path_to_qm)//'/gaussian.rwf'

    ! Extract the molecular orbitals and single amplitudes now
    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/eigenvalues 522R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf eigenvalues 522R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/MO_coefs_a 524R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf MO_coefs_a 524R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    if (self%type == 1) then
       call execute_command_line(&
            & rwfdump//' '//trim(path_to_qm)//'/MO_coefs_b 526R', &
            & exitstat=ierr, cmdmsg=cmdmsg)
       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running "rwfdump gaussian.rwf MO_coefs_b 526R"'//trim(cmdmsg), &
               & mod=MODNAME, func=funcname&
               & )
       end if
    end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/TDDFT 635R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf TDDFT 635R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if
  end subroutine gaussian_cio_prepare_files

  subroutine gaussian_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)

    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: u, ierr, i, j, k, l, m, id, ic
    integer :: mit, leer
    integer :: nLines, rem
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b, mseek
    real(dp) :: cNorm
    real(dp), dimension(:), allocatable :: coef

    nvirt_a = orb%nvirt
    nvirt_b = orb%nvirt_b
    nocc_b = orb%nocc_b
    nocc_a = orb%nocc - orb%nfrozen
    mseek = orb%mseek

    mit = mseek * (nocc_b*nvirt_b + nocc_a*nvirt_a)
    leer = mseek + 2*mit + 12 ! Number of elements
    nLines = int(leer / 5)
    rem = mod(leer, nLines)
    allocate( coef(leer) )

    ! Read all coefficients into ``coeff`` array. The elements up to ``mit``
    ! correspond to | X + Y >, an thos from ``mit+1`` to ``2*mit`` correspond
    ! to | X - Y >.
    ic = 1
    open(newunit=u, file=trim(path_to_qm)//'/TDDFT', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          do i=1, nLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                if ((ic > 12) .and. (ic <= leer - mseek )) then
                   read(split(j), *) coef(ic-12)
                end if
                ic = ic + 1
             end do
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, rem
                read(split(j), *) coef(ic-12)
                ic = ic + 1
             end do
          end if

       end if
    end do
    close(u)

    ! In ``coef`` we have the list of CI coefficient, ordered as nstate lists
    ! of amplitudes, each composed of two lists : amplitudes for alpha and
    ! beta electrons. Each of these lists is in turn composed of the
    ! amplitudes ordered as the lines of the Cia matrix.
    ! Normalization
    cNorm = sqrt(2.0_dp)
    if (present(tib)) then
       cNorm = 1
    end if
    ! open(newunit=u, file='coef_norm', action='write')
    ! write(u, *) 'cnorm = ', cnorm
    if (self%coptda == 0) then
       ! Use | X > coefficients
       coef(:) = cNorm * coef(:)
    else if (self%coptda == 1) then
       ! Use | X + Y > coeffieicnts
       do i=1, mit
          coef(i) = cNorm*( coef(i) + coef(i+mit) ) / 2
       end do
    end if
    close(u)

    ! Now we reorder the single list into the array tia (and tia_b if required).
    l = 1
    m = 0
    do i=1, size(tia, 3)
       ! Populate the alpha matrix for each state
       do j=1, nocc_a
          do k=1, nvirt_a
             tia(k, j, i) = coef(l+m)
             l = l+1
          end do
       end do

       if (present(tib)) then
          do j=1, nocc_b
             do k=1, nvirt_b
                tib(k, j, i) = coef(l + m)
                m = m+1
             end do
          end do
       else
          m = m + nocc_b*nvirt_b
       end if
    end do

  end subroutine gaussian_cio_get_singles_amplitudes

  subroutine gaussian_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)

    integer :: u, id, mo, ierr, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split
    integer :: nFields, nLines, rem

    mo = 1
    open(newunit=u, file=trim(path_to_qm)//'/eigenvalues', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)

          read(split(6), *) nFields

          ! We consider only the RHF case. In 'eigenvalues' we have both
          ! alpha and beta MOs energies. We only need one of those in RHF.
          if (self%type == 0) then
             nFields = nFields / 2
          end if

          nLines = int(nFields / 5)
          rem = mod(nFields, nLines)

          do i=1, nLines
             read(u, *) mos(mo:mo+4)
             mo = mo + 5
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) mos(mo)
                mo = mo + 1
             end do
          end if

       end if

    end do
    close(u)

  end subroutine gaussian_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  function gau_generate_route(self, init_route, nstat, nstatdyn, is_first_step)  result(res)
    !!
    class(nx_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: init_route
    integer, intent(in) :: nstat
    integer, intent(in) :: nstatdyn
    logical, intent(in) :: is_first_step

    character(len=:), allocatable :: res

    character(len=MAX_STR_SIZE) :: tmp
    character(len=MAX_STR_SIZE) :: method_input
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    integer :: i
    logical :: nosym_found

    tmp = '#p'
    if (&
         & ((self%mocoef == 1) .and. (.not. is_first_step))&
         & .or. (self%mocoef == 2)&
         & ) then
       tmp = trim(tmp)//' Guess=Read'
    end if

    method_input = ''
    group = split_pattern(init_route)
    nosym_found = .false.
    do i=1, size(group)
       if ((group(i)(1:2) == 'td') .or. (group(i)(1:3) == 'cis')) then
          method_input = trim(group(i))

          if (self%no_fc) then
             method_input = trim(method_input)//'(full,'
          else
             method_input = trim(method_input)//'('
          end if

          if ((self%mocoef /= 0) .and. (self%td_st /= 0)) then
             method_input = trim(method_input)//'read,root='
          else
             method_input = trim(method_input)//'root='
          end if

          method_input = trim(method_input)//to_str(nstatdyn-1)//&
               & ',nstates='//to_str(nstat-1)//')'

       else
          tmp = trim(tmp)//' '//trim(group(i))
       end if

       if (group(i) == 'nosymm') then
          nosym_found = .true.
       end if
    end do

    if (nstat > 1) then
       if (method_input == '') then
          if (self%no_fc) then
             method_input = 'td(full,root='//to_str(nstatdyn-1)//',nstates='&
                  &//to_str(nstat-1)//')'
          else
             method_input = 'td(root='//to_str(nstatdyn-1)//',nstates='&
                  &//to_str(nstat-1)//')'
          end if
       end if
    end if

    tmp = trim(tmp)//' '//trim(method_input)//' '//'IOP(3/59='//to_str(self%ld_thr)//')'

    if (.not. nosym_found) tmp = trim(tmp)//' '//'nosymm'
    if (self%typedft /= 2) tmp = trim(tmp)//' '//'force'
    res = trim(tmp)
  end function gau_generate_route


  ! ===============
  ! HELPER ROUTINES
  ! ===============
  subroutine gau_parse_input(filename, link0, route, title, charge, mult, other)
    !! Break a Gaussian input file into its components.
    !!
    !! The following input:
    !!
    !! ```
    !! %chk=gaussian
    !! %rwf=gaussian
    !! %mem=200mw
    !! #td(Nstates=3,Root=2) 3-21g BHandHLYP nosymm
    !!
    !! comment
    !!
    !! 0   1
    !! 1.0 1.0 1.0
    !! ...
    !! 
    !! some_other_directives
    !! ```
    !!
    !! will be parsed as (``\n`` stands for new line character):
    !!
    !! - ``link0 = %chk=gaussian\n%rwf=gaussian\n%mem=200mw`` ;
    !! - ``route = #td(Nstates=3,Root=2) 3-21g BHandHLYP nosymm`` ;
    !! - ``title = comment`` ;
    !! - ``charge = 0`` ;
    !! - ``mult = 1`` ;
    !! - ``other = some_other_directives``.
    character(len=*), intent(in) :: filename
    character(len=*), intent(out) :: link0
    character(len=*), intent(out) :: route
    character(len=*), intent(out) :: title
    integer, intent(out) :: charge
    integer, intent(out) :: mult
    character(len=*), intent(out) :: other

    integer :: u, ierr, i
    integer :: blank
    character(len=MAX_STR_SIZE) :: buf
    character(len=1) :: nl
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    logical :: read_charge

    link0 = ''
    title = ''
    route = ''
    charge = -1
    mult = -1
    other = ''

    nl = NEW_LINE('c')

    read_charge = .true.
    blank = 0
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (buf(1:1) == '%') then
          buf = to_lower(buf)
          link0 = trim(link0)//trim(buf)//nl
       else if (buf(1:1) == '#') then
          buf = to_lower(buf)
          if (buf(2:2) == 'p') then
             group = split_pattern(buf(3:len_trim(buf)))
          else
             group = split_pattern(buf(2:len_trim(buf)))
          end if

          do i=1, size(group)
             if (group(i)(1:2) == 'td') then
                group(i) = 'td'
             else if (group(i)(1:3) == 'cis') then
                group(i) = 'cis'
             end if

             route = trim(route)//' '//trim(group(i))
          end do
       end if

       if (buf == ' ') then
          blank = blank + 1
       end if

       if (blank == 1) then
          read(u, '(a)') title
       end if

       if ((blank == 2) .and. read_charge) then
          read(u, '(a)') buf
          read(buf, *) charge, mult
          read_charge = .false.
       end if

       if ((blank == 2) .and. .not. read_charge) then
          continue
       end if

       if (blank == 3) then
          other = trim(other)//trim(buf)//nl
       end if
    end do
    close(u)

    link0 = link0(1:len_trim(link0)-1)
    other = other(2:len_trim(other))
  end subroutine gau_parse_input
  
  function gau_read_orb(filename) result(orb)
    character(len=*), intent(in) :: filename

    type(nx_orbspace_t) :: orb

    integer :: u, id1, id2, id3, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b

    open(newunit=u, file=filename, action='read')
    ! check = .false.
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id1 = index(buf, 'NBasis')
       id2 = index(buf, 'NAE')
       id3 = index(buf, 'NBE')
       if ((id1 /= 0) .and. (id2 /= 0) .and. (id3 /= 0)) then
          call split_blanks(buf, split)
          read(split(2), *) orb%nbas
       end if

       id1 = index(buf, 'NROrb')
       id2 = index(buf, 'NOA')
       id3 = index(buf, 'NOB')
       if ((id1 /= 0) .and. (id2 /= 0) .and. (id3 /= 0)) then
          call split_blanks(buf, split)
          read(split(4), *) nocc_a
          read(split(6), *) nocc_b
          read(split(8), *) nvirt_a
          read(split(10), *) nvirt_b

          orb%nfrozen = orb%nbas - nocc_a - nvirt_a
          orb%nocc = orb%nfrozen + nocc_a
          orb%nocc_b = nocc_b
          orb%nvirt = nvirt_a
          orb%nvirt_b = nvirt_b
          ! check = .true.
       end if

       id1 = index(buf, 'roots to seek:')
       if (id1 /= 0) then
          call split_blanks(buf, split)
          read(split(7), *) orb%mseek
       end if

    end do
    close(u)

    orb%nelec = nocc_a + nocc_b + 2*orb%nfrozen

    ! if (nx_qm%qmcode .eq. 'exc_gaussian') then
    !    open(newunit=u, file=trim(adjustl(namefile))//'.orbinf', &
    !         status='unknown', form='formatted')
    !    rewind u
    ! 
    !    if (check) then
    !       write(u,*) nx_qm%orb%nfrozen
    !       write(u,*) nx_qm%orb%nocc
    !       write(u,*) nx_qm%orb%nocc_b
    !       write(u,*) nx_qm%orb%nvirt
    !       write(u,*) nx_qm%orb%nvirt_b
    !       write(u,*) nx_qm%orb%nelec
    !    else
    !       write(u,*) nx_qm%orb%nelec
    !    end if
    !    close(u)
    ! 
    ! end if

  end function gau_read_orb

  
end module mod_gaussian_t
