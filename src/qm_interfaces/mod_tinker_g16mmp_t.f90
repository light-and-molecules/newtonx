! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_tinker_g16mmp_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ang, au2kcalm
  use mod_gaussian_t, only: gau_read_orb, gau_parse_input
  use mod_interface, only: &
       & mkdir, copy
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, call_external
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: &
       & to_str, split_blanks, remove_blanks, split_pattern
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_tinker_g16mmp_t

  type, extends(nx_qm_generic_t) :: nx_tinker_g16mmp_t
     character(len=:), allocatable :: g16root
     character(len=:), allocatable :: tinker_env
   contains
     private
     procedure, public :: setup => tinker_g16mmp_setup
     procedure, public :: print => tinker_g16mmp_print
     procedure, public :: to_str => tinker_g16mmp_to_str
     procedure, public :: backup => tinker_g16mmp_backup
     procedure, public :: update => tinker_g16mmp_update
     procedure, public :: run => tinker_g16mmp_run
     procedure, public :: read_output => tinker_g16mmp_read_output
     procedure, public :: write_geom => tinker_g16mmp_write_geometry
     procedure, public :: init_overlap => tinker_g16mmp_init_overlap
     procedure, public :: prepare_overlap => tinker_g16mmp_prepare_overlap
     procedure, public :: extract_overlap => tinker_g16mmp_extract_overlap
     procedure, public :: ovl_post => tinker_g16mmp_ovl_post
     procedure, public :: ovl_run => tinker_g16mmp_ovl_run
     procedure, public :: get_lcao => tinker_g16mmp_get_lcao
     procedure, public :: cio_prepare_files => tinker_g16mmp_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & tinker_g16mmp_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => tinker_g16mmp_cio_get_mos_energies
  end type nx_tinker_g16mmp_t
  interface nx_tinker_g16mmp_t
     module procedure constructor
  end interface nx_tinker_g16mmp_t

  character(len=*), parameter :: MODNAME = 'mod_tinker_g16mmp_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & g16root, tinker_env &
       & ) result(res)
    !! Constructor for the ``nx_tinker_g16mmp_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    character(len=*), intent(in), optional :: g16root
    character(len=*), intent(in), optional :: tinker_env

    type(nx_tinker_g16mmp_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(g16root)) then
       res%g16root = trim(g16root)
    else
       res%g16root = ''
    end if

    if (present(tinker_env)) then
       res%tinker_env = trim(tinker_env)
    else
       res%tinker_env = ''
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine tinker_g16mmp_setup(self, parser, conf, inp_path, stat)
    class(nx_tinker_g16mmp_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo

    character(len=MAX_STR_SIZE) :: env
    integer :: ierr
    character(len=*), parameter :: funcname = 'tinker_g16mmp_setup'

    prt_mo = -1
    call set(parser, 'tinker_g16mmp', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%g16root == '') then
       call get_environment_variable("g16root", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $g16root environment is not defined', &
               & mod=modname, func=funcname)
          return
       else
          self%g16root = trim(env)
       end if
    end if

    if (self%tinker_env == '') then
       call get_environment_variable("TINKER_G16", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $TINKER_G16 environment is not defined', &
               & mod=modname, func=funcname)
          return
       else
          self%tinker_env = trim(env)
       end if
    end if
  end subroutine tinker_g16mmp_setup

  subroutine tinker_g16mmp_print(self, out)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%g16root, '$g16root path', unit=output)
    call print_conf_ele(self%tinker_env, '$TINKER_G16 path', unit=output)
  end subroutine tinker_g16mmp_print


  function tinker_g16mmp_to_str(self) result(res)
    class(nx_tinker_g16mmp_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&tinker_g16mmp'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//'/'//nl
  end function tinker_g16mmp_to_str
  

  subroutine tinker_g16mmp_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine tinker_g16mmp_backup

  subroutine tinker_g16mmp_update(self, conf, traj, path, stat)
    class(nx_tinker_g16mmp_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    character(len=:), allocatable :: cmd
    character(len=256), allocatable :: group(:)
    character(len=MAX_STR_SIZE) :: buf
    integer :: ierr, u, v, iline, i

    ierr = rename(trim(path)//'/frame00000.key', trim(path)//'/tg16mmp.key')
    ierr = rename(trim(path)//'/frame00000.mmp', trim(path)//'/tg16mmp.mmp')

    cmd = "sed -i -e '/^\\s*qmmm_root.*/Id' tg16mmp.key"
    call execute_command_line(cmd)

    cmd = "echo qmmm_root "//to_str(traj%nstatdyn - 1)//" >> tg16mmp.key"
    call execute_command_line(cmd)

    cmd = "sed -i -e '/^\\s*qmmm_nstates.*/Id' tg16mmp.key"
    call execute_command_line(cmd)

    cmd = "echo qmmm_nstates "//to_str(conf%nstat - 1)//" >> tg16mmp.key"
    call execute_command_line(cmd)

    open(newunit=u, file=trim(path)//'/tg16mmp.xyz', action='write')
    open(newunit=v, file=trim(path)//'/frame00000.xyz', action='read')

    iline = 0
    do
       read(v, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (iline == 0) then
          write(u, '(a)') trim(buf)
       else
          call split_blanks(buf, group)
          write(u, '(I5, 4A, 3F12.6, A)') &
               & group(1), trim(group(2)), (traj%geom(i, iline+1), i=1, 3), trim(group(6))
       end if

       iline = iline + 1
    end do
    
    close(v)
    close(u)
  end subroutine tinker_g16mmp_update

  subroutine tinker_g16mmp_run(self, stat)
    class(nx_tinker_g16mmp_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=256) :: cmdmsg

    call call_external(&
         & self%tinker_env//'/gradinterf.x tg16mmp.xyz', ierr, &
         & outfile='gaussian.log', cmdmsg=cmdmsg &
         & )
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running g16'//trim(cmdmsg)//&
            & '. Please take a look at gaussian.job/gaussian.log', & 
            & mod=MODNAME, func='gaussian_run'&
            & )
    end if
  end subroutine tinker_g16mmp_run

  function tinker_g16mmp_read_output(self, conf, traj, path, stat) result(info)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    integer :: step, init_step
    integer :: kt
    integer :: nat
    integer :: nstatdyn

    character(len=MAX_STR_SIZE) :: line, namefile
    integer :: i, u, ierr
    
    integer :: sec_id, tg16_natom_grd, tg16_nstates, tg16_istate

    kt = conf%kt
    nat = conf%nat
    step = traj%step
    nstatdyn = traj%nstatdyn
    init_step = conf%init_step

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       ! For instance, use something like:
       info%orb = gau_read_orb('gaussian.log')
       ! info%orb = self%read_orb( path )
    end if

    open(newunit=u, file=trim(path)//'/interface.dat', status='old', action='read')

    sec_id = 1
    i = 0
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit
       if (sec_id == 1) then
          ! POTENTIAL section
          if( i < 1 ) then
             read(line(10:), '(i10)') tg16_nstates
          else
             read(line, '(f20.8)') info%repot(i)
          end if
          if( i .eq. tg16_nstates) then
             sec_id = sec_id + 1
             i = 0
          else
             i = i + 1
          end if
       else if(sec_id == 2) then
          ! GRADIENT section
          if(i < 1) then
             read(line(9:), "(2i10)") tg16_istate, tg16_natom_grd
             ! TODO if tg16_natom_grd /= nat => ERROR
             ! TODO if tg16_istate /= nstatedyn => ERROR
          else
             read(line, '(3f20.8)') info%rgrad(nstatdyn,:,i)
          end if

          if(i == tg16_natom_grd) then
             sec_id = sec_id + 1
          else
             i = i + 1
          end if
       else
          exit
       end if
    end do

    ! Convert potential in atomic units
    info%repot = info%repot / au2kcalm
    ! Convert gradients in atomic units
    info%rgrad = info%rgrad * au2ang / au2kcalm
  end function tinker_g16mmp_read_output

  subroutine tinker_g16mmp_write_geometry(self, traj, path, print_merged)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: i, j, u

    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)
       write(u, '(3f13.6)') (traj%geom(j, i), j=1,3)
    end do
    close(u)
  end subroutine tinker_g16mmp_write_geometry

  subroutine tinker_g16mmp_init_overlap(self, path_to_qm, stat)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: link0
    character(len=MAX_STR_SIZE) :: route
    character(len=MAX_STR_SIZE) :: title
    integer :: charge
    integer :: mult
    character(len=MAX_STR_SIZE) :: other
    character(len=MAX_STR_SIZE) :: basis
    logical :: ext1, ext2
    integer :: ierr, u

    ierr = mkdir('double_molecule_input')

    call gau_parse_input(&
         & trim(path_to_qm)//'/gaussian.com', &
         & link0, route, title, charge, mult, other&
         & )

    route = "#p nosymm Geom=NoTest IOp(3/33=1)"

    inquire(file=trim(path_to_qm)//'/basis', exist=ext1)
    inquire(file=trim(path_to_qm)//'/basis2', exist=ext2)
    if (ext1) then
       open(newunit=u, file=trim(path_to_qm)//'/basis', action='read')
       read(u, '(a)') basis
       close(u)

       route = trim(route)//' '//trim(basis)
    else if (ext2) then
       route = trim(route)//' '//'GEN'
       ierr = copy(trim(path_to_qm)//'/basis2', 'double_molecule_input/basis2')
    end if

    open(newunit=u, file='double_molecule_input/sgaussian.com', action='write')
    write(u, '(a)') "%kjob l302"
    write(u, '(a)') "%rwf=sgaussian"
    write(u, '(a)') "%chk=sgaussian"
    write(u, '(a)') trim(route)
    write(u, '(a)') ""
    write(u, '(a)') "Overlap run"
    write(u, '(a)') ""
    write(u, '(a)') "0 1"
    close(u)
  end subroutine tinker_g16mmp_init_overlap

  subroutine tinker_g16mmp_prepare_overlap(self, path_to_qm, stat)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: u, v, ierr, i
    integer :: nat, at
    character(len=MAX_STR_SIZE) :: buf, gen, gen_ele, orig, added
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    character(len=:), allocatable :: tmp
    logical :: ext
    character(len=1) :: nl

    nl = NEW_LINE('c')

    open(newunit=v, file='overlap/sgaussian.com', action='write')

    ! Header part
    open(newunit=u, file='double_molecule_input/sgaussian.com', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do
    close(u)

    ! Geometry
    nat = 0
    open(newunit=u, file='overlap/geom', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
       nat = nat + 1
    end do
    close(u)

    nat = nat / 2

    ! Generated basis
    inquire(file='double_molecule_input/basis2', exist=ext)
    if (ext) then
       gen = ''
       open(newunit=u, file='double_molecule_input/basis2', action='read')
       do
          read(u, '(a)', iostat=ierr) buf
          if (ierr /= 0) exit

          tmp = remove_blanks(buf)

          gen_ele = ''

          group = split_pattern(tmp)
          read(group(1), *, iostat=ierr) at
          if (ierr /= 0) then
             gen_ele = trim(tmp)
          else
             orig = tmp(1:len_trim(tmp)-1)
             added = ''
             do i=1, size(group)-1
                at = at + nat
                added = trim(added)//' '//to_str(at)
             end do
             added = trim(added)//' 0'
             gen_ele = trim(orig)//trim(added)
          end if

          gen = trim(gen)//trim(gen_ele)//nl
       end do
       close(u)

       write(v, '(a)') ''
       write(v, '(a)') trim(gen)
    end if

    write(v, '(a)') ''

    close(v)
  end subroutine tinker_g16mmp_prepare_overlap

  function tinker_g16mmp_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)

    integer :: u, id, ierr, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: rem, nlines, nele, ele

    ! We first need to 

    open(newunit=u, file='overlap/S_matrix', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nlines = int(nele / 5)
          rem = mod(nele, nlines)

          if (allocated(split)) deallocate(split)
          allocate(split(5))

          ele = 1
          do i=1, nlines
             read(u, *) split
             do j=1, 5
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end do

          if (rem /= 0) then
             deallocate(split)
             allocate(split(rem))
             read(u, *) split
             do j=1, rem
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end if

       end if

    end do
    close(u)
  end function tinker_g16mmp_extract_overlap

  function tinker_g16mmp_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)

    integer :: u, ierr, id, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer :: nele, rem, nbLines, ele

    real(dp), dimension(:), allocatable :: temp

    allocate(temp(nao*nao))
    ele = 1

    ! First read the matrix as a vector
    open(newunit=u, file=trim(dir_path)//'/MO_coefs_a', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nbLines = int(nele / 5)
          rem = mod(nele, nbLines)

          do i=1, nbLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                read(split(j), *) temp(ele)
                ele = ele + 1
             end do

          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) temp(ele)
                ele = ele + 1
             end do
          end if

       end if
    end do
    close(u)

    do i=1, nao
       ele = i
       do j=1, nao
          lcao(i, j) = temp(ele)
          ele = ele + nao
       end do
    end do
  end function tinker_g16mmp_get_lcao

  subroutine tinker_g16mmp_ovl_run(self, stat)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg

    call call_external(&
         & self%g16root//'/g16/g16 sgaussian.com', ierr, &
         & outfile='sgaussian.log', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running g16'//trim(cmdmsg)//&
            & '. Please see overlap/sgaussian.log for details.', &
            & mod=MODNAME, func='gaussian_ovl_post'&
            & )
    end if
  end subroutine tinker_g16mmp_ovl_run

  subroutine tinker_g16mmp_ovl_post(self, stat)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg

    call execute_command_line(&
         & self%g16root//'/g16/rwfdump overlap/sgaussian.rwf overlap/S_matrix 514R',&
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running rwfdump'//trim(cmdmsg), &
            & mod=MODNAME, func='gaussian_ovl_post'&
            & )
    end if
  end subroutine tinker_g16mmp_ovl_post

  subroutine tinker_g16mmp_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: cmdmsg
    integer :: ierr
    character(len=:), allocatable :: rwfdump
    character(len=*), parameter :: funcname = 'gaussian_cio_prepare_files'

    rwfdump = self%g16root//'/g16/rwfdump '//trim(path_to_qm)//'/gaussian.rwf'

    ! Extract the molecular orbitals and single amplitudes now
    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/eigenvalues 522R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf eigenvalues 522R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/MO_coefs_a 524R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf MO_coefs_a 524R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    ! if (self%type == 1) then
    !    call execute_command_line(&
    !         & rwfdump//' '//trim(path_to_qm)//'/MO_coefs_b 526R', &
    !         & exitstat=ierr, cmdmsg=cmdmsg)
    !    if (ierr /= 0) then
    !       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
    !       call stat%append(NX_ERROR, &
    !            & 'Problem when running "rwfdump gaussian.rwf MO_coefs_b 526R"'//trim(cmdmsg), &
    !            & mod=MODNAME, func=funcname&
    !            & )
    !    end if
    ! end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/TDDFT 635R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf TDDFT 635R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if
  end subroutine tinker_g16mmp_cio_prepare_files

  subroutine tinker_g16mmp_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)

    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: u, ierr, i, j, k, l, m, id, ic
    integer :: mit, leer
    integer :: nLines, rem
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b, mseek
    real(dp) :: cNorm
    real(dp), dimension(:), allocatable :: coef

    nvirt_a = orb%nvirt
    nvirt_b = orb%nvirt_b
    nocc_b = orb%nocc_b
    nocc_a = orb%nocc - orb%nfrozen
    mseek = orb%mseek

    mit = mseek * (nocc_b*nvirt_b + nocc_a*nvirt_a)
    leer = mseek + 2*mit + 12 ! Number of elements
    nLines = int(leer / 5)
    rem = mod(leer, nLines)
    allocate( coef(leer) )

    ! Read all coefficients into ``coeff`` array. The elements up to ``mit``
    ! correspond to | X + Y >, an thos from ``mit+1`` to ``2*mit`` correspond
    ! to | X - Y >.
    ic = 1
    open(newunit=u, file=trim(path_to_qm)//'/TDDFT', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          do i=1, nLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                if ((ic > 12) .and. (ic <= leer - mseek )) then
                   read(split(j), *) coef(ic-12)
                end if
                ic = ic + 1
             end do
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, rem
                read(split(j), *) coef(ic-12)
                ic = ic + 1
             end do
          end if

       end if
    end do
    close(u)

    ! In ``coef`` we have the list of CI coefficient, ordered as nstate lists
    ! of amplitudes, each composed of two lists : amplitudes for alpha and
    ! beta electrons. Each of these lists is in turn composed of the
    ! amplitudes ordered as the lines of the Cia matrix.
    ! Normalization
    cNorm = sqrt(2.0_dp)
    if (present(tib)) then
       cNorm = 1
    end if
    ! open(newunit=u, file='coef_norm', action='write')
    ! write(u, *) 'cnorm = ', cnorm
    ! if (self%coptda == 0) then
       ! Use | X > coefficients
       ! coef(:) = cNorm * coef(:)
    ! else if (self%coptda == 1) then
       ! Use | X + Y > coeffieicnts
       do i=1, mit
          coef(i) = cNorm*( coef(i) + coef(i+mit) ) / 2
       end do
    ! end if
    close(u)

    ! Now we reorder the single list into the array tia (and tia_b if required).
    l = 1
    m = 0
    do i=1, size(tia, 3)
       ! Populate the alpha matrix for each state
       do j=1, nocc_a
          do k=1, nvirt_a
             tia(k, j, i) = coef(l+m)
             l = l+1
          end do
       end do

       if (present(tib)) then
          do j=1, nocc_b
             do k=1, nvirt_b
                tib(k, j, i) = coef(l + m)
                m = m+1
             end do
          end do
       else
          m = m + nocc_b*nvirt_b
       end if
    end do

  end subroutine tinker_g16mmp_cio_get_singles_amplitudes

  subroutine tinker_g16mmp_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_tinker_g16mmp_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)

    integer :: u, id, mo, ierr, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split
    integer :: nFields, nLines, rem

    mo = 1
    open(newunit=u, file=trim(path_to_qm)//'/eigenvalues', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)

          read(split(6), *) nFields

          ! We consider only the RHF case. In 'eigenvalues' we have both
          ! alpha and beta MOs energies. We only need one of those in RHF.
          ! if (self%type == 0) then
             nFields = nFields / 2
          ! end if

          nLines = int(nFields / 5)
          rem = mod(nFields, nLines)

          do i=1, nLines
             read(u, *) mos(mo:mo+4)
             mo = mo + 5
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) mos(mo)
                mo = mo + 1
             end do
          end if

       end if

    end do
    close(u)

  end subroutine tinker_g16mmp_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  
end module mod_tinker_g16mmp_t
