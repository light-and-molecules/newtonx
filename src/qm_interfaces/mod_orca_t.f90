! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_orca_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE, au2ang
  use mod_interface, only: &
       & copy, rm, to_c_str
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, call_external
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: split_pattern, to_str, split_blanks
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_orca_t

  type, extends(nx_qm_generic_t) :: nx_orca_t
     logical :: is_tda = .true.
     !! Indicate if the computation is TDA (``.true.``) or TD-DFT (``.false.``) (Default: ``.true.``).
     integer :: mocoef = 1
     !! Decide which initial guess to use:
     !!
     !! - 0: compute the guess at every time step ;
     !! - 1: use the guess from previous step ;
     !! - 2: use the same guess for all steps.
     character(len=:), allocatable :: orca_path
     !! Path to Orca executables, usually the ``$ORCA`` environment variable.
     !! Number of atomic orbitals. This is useful for extracting information about
     !! overlap, as the interface only has information about the number of unique
     !! elements in the overlap matrix (symmetric). To avoid computing it at each
     !! time-step, we read it the first time from the output files !
   contains
     private
     procedure, public :: setup => orca_setup
     procedure, public :: print => orca_print
     procedure, public :: to_str => orca_to_str
     procedure, public :: backup => orca_backup
     procedure, public :: update => orca_update
     procedure, public :: run => orca_run
     procedure, public :: read_output => orca_read_output
     procedure, public :: write_geom => orca_write_geometry
     procedure, public :: init_overlap => orca_init_overlap
     procedure, public :: prepare_overlap => orca_prepare_overlap
     procedure, public :: extract_overlap => orca_extract_overlap
     procedure, public :: ovl_post => orca_ovl_post
     procedure, public :: ovl_run => orca_ovl_run
     procedure, public :: get_lcao => orca_get_lcao
     procedure, public :: cio_prepare_files => orca_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & orca_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => orca_cio_get_mos_energies
  end type nx_orca_t
  interface nx_orca_t
     module procedure constructor
  end interface nx_orca_t

  character(len=*), parameter :: MODNAME = 'mod_orca_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & is_tda, mocoef, orca_path &
       & ) result(res)
    !! Constructor for the ``nx_orca_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    logical, intent(in), optional :: is_tda
    integer, intent(in), optional :: mocoef
    character(len=*), intent(in), optional :: orca_path

    type(nx_orca_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(is_tda)) res%is_tda = is_tda
    if (present(mocoef)) res%mocoef = mocoef
    if (present(orca_path)) then
       res%orca_path = orca_path
    else
       res%orca_path = ''
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine orca_setup(self, parser, conf, inp_path, stat)
    class(nx_orca_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo, ierr
    character(len=MAX_STR_SIZE) :: env
    logical :: ext

    prt_mo = -1
    call set(parser, 'orca', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%orca_path == '') then
       call get_environment_variable("ORCA", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $ORCA environment is not defined', &
               & mod=MODNAME, func='orca_setup')
          return
       else
          self%orca_path = trim(env)
       end if
    end if

    call set(parser, 'orca', self%is_tda, 'is_tda')
    call set(parser, 'orca', self%mocoef, 'mocoef')

    if (self%mocoef == 2) then
       inquire(file=trim(inp_path)//'/orca.gbw', exist=ext)
       if (.not. ext) then
          call stat%append(NX_ERROR, &
               & 'MOCOEF = 2 but orca.gbw not found in '//trim(inp_path)//' !', &
               & mod=MODNAME, func='orca_setup' &
               & )
       end if
    end if
  end subroutine orca_setup

  subroutine orca_print(self, out)
    class(nx_orca_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%orca_path, 'ORCA path', unit=output)
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%is_tda, 'TDA enabled ?', unit=output)
    call print_conf_ele(self%mocoef, 'MOCOEF', unit=output)
  end subroutine orca_print


  function orca_to_str(self) result(res)
    class(nx_orca_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&orca'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' is_tda = '//to_str(self%is_tda)//nl
    res = res//' mocoef = '//to_str(self%mocoef)//nl
    res = res//'/'//nl
  end function orca_to_str


  subroutine orca_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo


  end subroutine orca_backup

  subroutine orca_update(self, conf, traj, path, stat)
    class(nx_orca_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr, finp

    if (self%mocoef == 1 .and. traj%step /= conf%init_step) then
       ierr = copy(trim(path)//'.old/orca.gbw', trim(path)//'/')
    end if

    open(newunit=finp, file=trim(path)//'/orca.inp', position='append', action='write')
    write(finp, '(A)') '%tddft'
    write(finp, '(A)') ' iroot '//to_str(traj%nstatdyn - 1)
    write(finp, '(A)') ' nroots '//to_str(conf%nstat - 1)
    write(finp, '(A)') 'end'
    write(finp, '(A)') ''
    close(finp)
  end subroutine orca_update

  subroutine orca_run(self, stat)
    class(nx_orca_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg
    character(len=:), allocatable :: orca_fragovl

    call call_external(&
         & self%orca_path//'/orca orca.inp', ierr, &
         & outfile='orca.out', cmdmsg=cmdmsg &
         & )
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running orca'//trim(cmdmsg)//&
            & '. Please take a look at orca.job/orca.out', & 
            & mod=MODNAME, func='orca_run'&
            & )
    end if

    orca_fragovl = self%orca_path//'/orca_fragovl '//'orca.gbw orca.gbw'
    call call_external(orca_fragovl, ierr, outfile='orca.ovl.dat', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running orca_fragovl'//trim(cmdmsg), &
            & mod=MODNAME, func='orca_prepare_overlap'&
            & )
    end if

    call orca_parse_cis('orca.cis', 0)
  end subroutine orca_run

  function orca_read_output(self, conf, traj, path, stat) result(info)
    class(nx_orca_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info
    character(len=256) :: dum, buf
    character(len=256), allocatable :: split(:)
    integer :: ierr, u, istate, idum, i, j
    real(dp) :: refen

    character(len=:), allocatable :: in_enstr, out_enstr, osc_strstr

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! call orca_parse_cis(trim(path)//'/orca.cis', 0, trim(path)//'/energies.)
    open(newunit=u, file=trim(path)//'/energies.dat', action='read')
    do i=2, size(info%repot)
       read(u, *) dum, dum, info%repot(i)
    end do
    close(u)

    in_enstr = 'TD-DFT EXCITED STATES (SINGLETS)'
    if (self%is_tda) in_enstr = 'TD-DFT/TDA EXCITED STATES (SINGLETS)'
    out_enstr = 'TD-DFT-EXCITATION SPECTRA'
    if (self%is_tda) out_enstr = 'TD-DFT/TDA-EXCITATION SPECTRA'
    osc_strstr = 'ABSORPTION SPECTRUM VIA TRANSITION ELECTRIC DIPOLE MOMENTS'

    open(newunit=u, file=trim(path)//'/orca.out', action='read')
    READ_ORCA_OUT: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       ENERGIES: if (index(buf, in_enstr) /= 0) then
          call info%append_data('Excitation informations read from orca.out:')
          istate = 1
          do
             read(u, '(A)') buf
             if (index(buf, 'STATE') /= 0) then
                ! call split_blanks(buf, split)
                call info%append_data(buf)

                ! Fill qminfo%repot(2:) with excitation energies
                ! read(split(4), *) qminfo%repot(istate+1)
                CONTRIB: do
                   read(u, '(A)') buf
                   if (index(buf, '->') /= 0) then
                      call info%append_data(buf)
                   else
                      exit CONTRIB
                   end if
                end do CONTRIB
                istate = istate + 1
             else if (index(buf, out_enstr) /= 0) then
                exit ENERGIES
             end if
          end do
       end if ENERGIES

       ! Save the energy of the state `nstatdyn` as `refen`
       REFENERGY: if (index(buf, 'FINAL SINGLE POINT ENERGY') /=0) then
          call info%append_data(buf)
          read(buf, *) dum, dum, dum, dum, refen
       end if REFENERGY

       ! Read oscillator strength
       OSCSTR: if (index(buf, osc_strstr) /= 0) then
          read(u, *)
          read(u, '(A)') buf
          split = split_pattern(buf)

          ! ORCA 5
          if (index(split(4), 'fosc') /= 0) then
            read(u, *)
            read(u, *)
            do i=1, conf%nstat - 1
               read(u, '(A)') buf
               split = split_pattern(buf)
               read(split(4), *) info%osc_str(i)
            end do
          
          ! ORCA 6
          else if (index(split(5), 'fosc') /= 0) then
            read(u, *)
            read(u, *)
            do i=1, conf%nstat - 1
               read(u, '(A)') buf
               split = split_pattern(buf)
               read(split(7), *) info%osc_str(i)
            end do
          end if
       end if OSCSTR
    end do READ_ORCA_OUT

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       call orca_read_orb(info%orb, u)
    end if

    close(u)

    open(newunit=u, file=trim(path)//'/orca.engrad', action='read')
      GRADIENTS: do
         read(u, '(A)',  iostat=ierr) buf
         if (ierr /= 0) exit

         READ_GRAD: if (index(buf, 'The current gradient in Eh/bohr') /= 0) then
            do i=1, 1
               read(u, *)
            end do

            do i=1, conf%nat
               do j=1, 3
                  read(u, *) info%rgrad(traj%nstatdyn, j, i)
               end do
            end do
            exit READ_GRAD
         end if READ_GRAD
      end do GRADIENTS
    close(u)

    ! Finally, derive the GS energy, and the energies of all states.
    info%repot(1) = refen - info%repot( traj%nstatdyn )
    info%repot( traj%nstatdyn ) = refen
    do i=2, conf%nstat
       if (i /= traj%nstatdyn) then
          info%repot(i) = info%repot(1) + info%repot(i)
       end if
    end do
  end function orca_read_output

  subroutine orca_write_geometry(self, traj, path, print_merged)
    class(nx_orca_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: i, u, j

    open(newunit=u, file=trim(path)//'/geom.xyz', action='write', status='unknown')
    write(u, '(I0)') size(traj%geom, 2)
    write(u, '(A)') ''
    do i=1, size(traj%geom, 2)
       write(u, '(A4, 3F19.14)') traj%atoms(i), (traj%geom(j, i)*au2ang, j=1, 3)
    end do
    close(u)
  end subroutine orca_write_geometry

  subroutine orca_init_overlap(self, path_to_qm, stat)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr

    ! Create self-overlap (for LCAO)
    ierr = copy(trim(path_to_qm)//'/orca.ovl.dat', 'overlap/')
  end subroutine orca_init_overlap

  subroutine orca_prepare_overlap(self, path_to_qm, stat)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    logical :: ext
    character(len=MAX_STR_SIZE) :: cmdmsg
    character(len=:), allocatable :: orca_fragovl

    ierr = copy(trim(path_to_qm)//'/orca.ovl.dat', 'overlap/')
    ierr = copy(trim(path_to_qm)//'.old/orca.ovl.dat', 'overlap/orca.ovl.dat.old')

    ! Create overlap with previous step
    orca_fragovl = self%orca_path//'/orca_fragovl '//trim(path_to_qm)//'/orca.gbw '//trim(path_to_qm)//'.old/orca.gbw'
    call call_external(orca_fragovl, ierr, outfile='overlap/orca.ovl_double.dat', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running orca_fragovl'//trim(cmdmsg), &
            & mod=MODNAME, func='orca_prepare_overlap'&
            & )
    end if
  end subroutine orca_prepare_overlap

  function orca_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_orca_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)

    integer :: u, ierr, i, j, id, nao
    character(len=MAX_STR_SIZE) :: buf
    integer :: dum, start_index, end_index, nele
    real(dp), allocatable :: tmp(:, :), tmp2(:, :)

    nao = int((1 + sqrt(1.0_dp+8*dim_ovl)) / 4)

    allocate(tmp(nao, nao), tmp2(2*nao, 2*nao))

    tmp2 = 0.0_dp
    call orca_parse_fragovl('overlap/orca.ovl.dat', 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao, tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i, j) = tmp(i, j)
       end do
    end do

    call orca_parse_fragovl('overlap/orca.ovl_double.dat', 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao, tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i+nao, j) = tmp(i, j)
          tmp2(i, j+nao) = tmp(i, j)
       end do
    end do

    call orca_parse_fragovl('overlap/orca.ovl.dat.old', 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao,&
         & tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i+nao, j+nao) = tmp(i, j)
       end do
    end do

    id = 1
    do i=1, 2*nao
       do j=1, i
          ovl(id) = tmp2(i, j)
          id = id + 1
       end do
    end do
  end function orca_extract_overlap

  function orca_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)

    call orca_parse_fragovl( &
         & trim(dir_path)//'/orca.ovl.dat', &
         & 'FRAGMENT A MOs MATRIX', nao, lcao &
         & )
  end function orca_get_lcao

  subroutine orca_ovl_run(self, stat)
    class(nx_orca_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine orca_ovl_run

  subroutine orca_ovl_post(self, stat)
    class(nx_orca_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine orca_ovl_post

  subroutine orca_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    logical :: ext
    character(len=MAX_STR_SIZE) :: cmdmsg
    character(len=MAX_STR_SIZE) :: curdir

    ierr = getcwd( curdir )
    ierr = chdir( path_to_qm )
    call call_external(self%orca_path//'/orca_2mkl orca -molden', ierr, &
         & cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running orca_2mkl'//trim(cmdmsg), &
            & mod=MODNAME, func='orca_cio_prepare_files'&
            & )
    end if
    ierr = chdir( curdir )
  end subroutine orca_cio_prepare_files

  subroutine orca_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)

    logical :: cis_already_dumped
    character(len=:), allocatable :: fname
    integer :: istate, i, u, ivirt, iocc

    do istate=1, size(tia, 3)
       if (self%is_tda) then
          i = istate
       else
          i = istate*2 - 1
       end if

       fname = trim(path_to_qm)//'/orca.cis_r'//to_str(i)//'.dat'
       open(newunit=u, file=fname, action='read')
       do iocc=1, size(tia, 2)
          do ivirt=1, size(tia, 1)
             read(u, *) tia(ivirt, iocc, istate)
          end do
       end do
    end do
  end subroutine orca_cio_get_singles_amplitudes

  subroutine orca_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_orca_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)

    integer :: u, ierr, norb
    CHARACTER(LEN=MAX_STR_SIZE) :: buf
    character(len=12) :: dum
    character(len=:), allocatable :: filename

    filename = trim(path_to_qm)//'/orca.molden.input'
    
    norb = 0
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '[MO]') /= 0) then
          do
             read(u, '(A)', iostat=ierr) buf
             if (ierr /= 0) exit
             if (index(buf, 'Ene=') /= 0) then
                norb = norb + 1
                read(buf, *) dum, mos(norb)
             end if
          end do
       end if
    end do
    close(u)
  end subroutine orca_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  subroutine orca_read_orb(orb, unit)
    type(nx_orbspace_t), intent(inout) :: orb
    integer, intent(in) :: unit

    integer :: ierr, tmp, orb_start, orb_end
    character(len=MAX_STR_SIZE) :: buf
    character(len=256), allocatable :: split(:), split2(:)

    rewind(unit)
    do
       read(unit, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Orbital ranges used for CIS calculation') /= 0) then
          read(unit, '(A)') buf
          split = split_pattern(buf, pattern='Orbitals')
          split = split_pattern(split(2), pattern='to')
          split2 = split_pattern(split(1), pattern='...')
          read(split2(1), *) orb_start
          read(split2(2), *) orb_end
          orb%nfrozen = orb_start
          orb%nocc = orb_end + 1
          orb%nocc_b = orb%nocc

          split2 = split_pattern(split(2), pattern='...')
          read(split2(1), *) orb_start
          read(split2(2), *) orb_end
          orb%nvirt = orb_end - orb_start + 1
       end if

       if (index(buf, 'Number of basis functions') /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) orb%nbas
       end if

       if (index(buf, ' Number of Electrons') /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) orb%nelec
       end if
    end do
  end subroutine orca_read_orb

  subroutine orca_parse_cis(cisfile, debug)
    use iso_c_binding, only: c_int
    character(len=*), intent(in) :: cisfile
    !! Name of the file to parse.
    integer, intent(in) :: debug

    character(len=len(cisfile)+1) :: tmp1
    integer(c_int) :: tda_c, res, debug_c

    interface
       function c_orca_parse_cis(sfile, db) bind(C, name='orca_parse_cis')
         use iso_c_binding, only: c_int, c_char
         implicit none
         integer(c_int) :: c_orca_parse_cis
         character(kind=c_char), intent(in) :: sfile(*)
         integer(c_int), value, intent(in) :: db
       end function c_orca_parse_cis
    end interface

    tmp1 = to_c_str(cisfile)

    debug_c = int(debug, c_int)

    res = c_orca_parse_cis(tmp1, debug_c)
  end subroutine orca_parse_cis

  subroutine orca_parse_fragovl(filename, label, nao, res)
    character(len=*), intent(in) :: filename
    !! File to read.
    character(len=*), intent(in) :: label
    !! Label to look for in the file.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(out) :: res
    !! Resulting matrix

    integer :: u, ierr, iblock, jline, dum, j
    integer :: nblocks, remainder
    integer :: start_index, end_index
    character(len=MAX_STR_SIZE) :: buf

    nblocks = int(nao / 6)
    remainder = mod(nao, 6)

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, label) /= 0) then
          read(u, '(A)') buf
          read(u, '(A)') buf
          do iblock=1, nblocks
             start_index = (iblock-1) * 6 + 1
             end_index = iblock * 6
             do jline=1, nao
                read(u, '(A)') buf
                call add_space_before_minus(buf)
                read(buf, *) dum, (res(jline, j), j=start_index, end_index)
             end do
             read(u, '(A)') buf
          end do

          if (remainder /= 0) then
             start_index = nblocks*6+1
             end_index = start_index + remainder - 1
             do jline=1, nao
                read(u, '(A)') buf
                read(buf, *) dum, (res(jline, j), j=start_index, end_index)
             end do
          end if
       end if
    end do
    close(u)
  end subroutine orca_parse_fragovl

  
  subroutine add_space_before_minus(line)
   character(len=*), intent(inout) :: line
   character(len=len(line)) :: temp_line
   integer :: i, j
 
   temp_line = line
   j = 1
 
   do i = 1, len_trim(line)
     if (line(i:i) == '-') then
       temp_line(j:j) = ' '
       j = j + 1
     end if
     temp_line(j:j) = line(i:i)
     j = j + 1
   end do
 
   line = trim(temp_line)
 end subroutine add_space_before_minus

  
end module mod_orca_t
