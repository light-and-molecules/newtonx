! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_columbus_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, &
       & proton
  use mod_kinds, only: dp
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config
  use mod_interface, only: &
       & copy, mkdir, rm, gzip
  use mod_logger, only: &
       & nx_log, &
       & LOG_ERROR, LOG_WARN, LOG_DEBUG, &
       & print_conf_ele, check_error, call_external
  use mod_orbspace, only: nx_orbspace_t
  use mod_print_utils, only: &
       & to_lower
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: &
       & split_pattern, split_blanks, &
       & to_str, remove_blanks, append_prefix
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_columbus_t

  type, extends(nx_qm_generic_t) :: nx_columbus_t
     ! All components are private, as they should not be used outside this module. When
     ! needed, setter and getter functions and routines will be implemented.
     private
     character(len=:), allocatable :: colenv
     !! Path to Columbus executables (``COLUMBUS`` environment).
     integer :: mem = 200
     !! Memory used by the program
     integer :: cirestart = 0
     integer :: reduce_tol =  1
     real(dp) :: citol = 1E-4_dp
     integer :: quad_conv = 60
     integer :: mc_conv = 0
     integer :: ci_conv = 0
     integer :: ivmode = 8
     integer :: mocoef = 1
     integer :: ci_iter = 30
     character(len=:), allocatable :: mode
     !! Columbus mode: ms (multi-state), ss (single-state), mc (multi-configuration) or sc
     !! (single configuration)
     integer :: mel = 0
     !! Maximum excitation level
     integer :: ci_type = -1
     !! Type of CI computation:
     !!
     !! - 0: No CI (typically MCSCF only jobs);
     !! - 1: Serial version of CI programs will be called;
     !! - 2: Parallel version of CI programs.
     integer :: all_grads = 1
     integer :: grad_lvl = -1
     ! integer :: prt_mo = -1
     integer :: ci_consolidate = 1
     logical :: read_nac = .false.
     logical :: compute_nac = .false.
     character(len=4) :: geomfile = 'geom'
   contains
     private
     procedure, public :: setup => columbus_setup
     procedure, public :: print => columbus_print
     procedure, public :: to_str => columbus_to_str
     procedure, public :: backup => columbus_backup
     procedure, public :: update => columbus_update
     procedure, public :: run => columbus_run
     procedure, public :: read_output => columbus_read_output
     procedure, public :: write_geom => columbus_write_geometry
     procedure, public :: init_overlap => columbus_init_overlap
     procedure, public :: prepare_overlap => columbus_prepare_overlap
     procedure, public :: extract_overlap => columbus_extract_overlap
     procedure, public :: ovl_post => columbus_ovl_post
     procedure, public :: ovl_run => columbus_ovl_run
     procedure, public :: get_lcao => columbus_get_lcao
     procedure, public :: cio_prepare_files => columbus_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes =>&
          & columbus_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => columbus_cio_get_mos_energies
     procedure :: write_transmomin => col_write_transmomin
     procedure :: get_ninact => col_get_ninact
     procedure :: read_orb => col_read_orb
     procedure :: complete_setup => col_complete_setup
  end type nx_columbus_t
  interface nx_columbus_t
     module procedure constructor
  end interface nx_columbus_t

  character(len=128), parameter :: MOFILES(*) = [ character(len=128) :: &
       & "MOCOEFS/mocoef_mc.sp"&
       & ]
  !! Files containing molecular orbitals, backed_up every ``prt_mo`` steps.
  
  character(len=128), parameter :: BACKUPFILES(*) = [ character(len=128) :: &
       & "runls", "runc.error", "LISTINGS/", "GRADIENTS/"&
       & ]
  !! Main backup files, used for inpecting a specific step, saved every ``save_cwd``
  !! steps.

  integer, parameter :: COL_ERR_MISC = 101
  integer, parameter :: COL_ERR_MAIN = 102
  integer, parameter :: CIO_COL_CIPC = 103
  integer, parameter :: CIO_COL_MCPC = 104
  integer, parameter :: CIO_COL_MISC = 105
  integer, parameter :: CIO_COL_CONSOLIDATE = 106

  character(len=*), parameter :: MODNAME = 'mod_columbus_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & colenv, mem, cirestart, reduce_tol, citol, quad_conv, &
       & mc_conv, ci_conv, ivmode, mocoef, ci_iter, all_grads, &
       & grad_lvl, read_nac, compute_nac &
       & ) result(res)
    !! Constructor for the ``nx_columbus_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    !! Name of the method (can be empty if not applicable).
    integer, intent(in), optional :: prt_mo
    character(len=*), intent(in), optional :: colenv
    integer, intent(in), optional :: mem
    integer, intent(in), optional :: cirestart
    integer, intent(in), optional :: reduce_tol
    integer, intent(in), optional :: citol
    integer, intent(in), optional :: quad_conv
    integer, intent(in), optional :: mc_conv
    integer, intent(in), optional :: ci_conv
    integer, intent(in), optional :: ivmode
    integer, intent(in), optional :: mocoef
    integer, intent(in), optional :: ci_iter
    integer, intent(in), optional :: all_grads
    integer, intent(in), optional :: grad_lvl
    logical, intent(in), optional :: read_nac
    logical, intent(in), optional :: compute_nac

    type(nx_columbus_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(mem)) res%mem = mem
    if (present(cirestart)) res%cirestart = cirestart
    if (present(reduce_tol)) res%reduce_tol = reduce_tol
    if (present(citol)) res%citol = citol
    if (present(quad_conv)) res%quad_conv = quad_conv
    if (present(mc_conv)) res%mc_conv = mc_conv
    if (present(ci_conv)) res%ci_conv = ci_conv
    if (present(ivmode)) res%ivmode = ivmode
    if (present(mocoef)) res%mocoef = mocoef
    if (present(ci_iter)) res%ci_iter = ci_iter
    if (present(all_grads)) res%all_grads = all_grads
    if (present(grad_lvl)) res%grad_lvl = grad_lvl
    if (present(read_nac)) res%read_nac = read_nac
    if (present(compute_nac)) res%compute_nac = compute_nac
    if (present(colenv)) then
       res%colenv = colenv
    else
       res%colenv = ''
    end if

    call col_complete_setup(res)
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine columbus_setup(self, parser, conf, inp_path, stat)
    !! Setup a Columbus job with the content of parsed input file.
    !!
    !! The setup will not go through if ``colenv`` cannot be defined, either because the
    !! ``$COLUMBUS`` environment is not defined, or if the ``colenv`` argument was not
    !! set before hand (in the object creation).  In this case, the routine sets
    !! ``self%stat`` to ``-1``.
    !!
    !! The routine will issue a warning (and set ``self%stat`` to 1) when ``prt_mo > 0``
    !! (so the MOs should be saved), while ``mocoef = 0``, as that means the same set of
    !! MOs is used for all steps. In that case, ``prt_mo`` is reset to -1, and the MOs
    !! will not be saved.
    class(nx_columbus_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    !! Input parser result.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    character(len=*), intent(in) :: inp_path
    !! Path to the input folder, where the file ``control.run`` can be found.
    type(nx_status_t), intent(inout) :: stat

    character(len=*), parameter :: funcname = 'columbus_setup'

    integer :: ierr, u, prt_mo
    character(len=MAX_STR_SIZE) :: buf, env
    character(len=:), allocatable :: str, control

    prt_mo = -1
    call set(parser, 'columbus', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%colenv == '') then
       call get_environment_variable("COLUMBUS", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $COLUMBUS environment is not defined', &
               & mod=modname, func=funcname)
          return
       else
          self%colenv = trim(env)
       end if
    end if

    call set(parser, 'columbus', self%mem, 'mem')
    call set(parser, 'columbus', self%cirestart, 'cirestart')
    call set(parser, 'columbus', self%reduce_tol, 'reduce_tol')
    call set(parser, 'columbus', self%citol, 'citol')
    call set(parser, 'columbus', self%quad_conv, 'quad_conv')
    call set(parser, 'columbus', self%mc_conv, 'mc_conv')
    call set(parser, 'columbus', self%ci_conv, 'ci_conv')
    call set(parser, 'columbus', self%ivmode, 'ivmode')
    call set(parser, 'columbus', self%mocoef, 'mocoef')
    call set(parser, 'columbus', self%ci_iter, 'ci_iter')
    call set(parser, 'columbus', self%all_grads, 'all_grads')
    call set(parser, 'columbus', self%grad_lvl, 'grad_lvl')
    call col_complete_setup(self)

    ! Internal variables setup
    control = trim( inp_path )//'/control.run'

    self%ci_type = col_ci_type(control)
    self%mode = col_get_mode(control)

    if (conf%dc_method == 1) then
       self%compute_nac = .true. 
       self%read_nac = .true. 
    end if

    ! Maximume excitation level
    if (self%mode == 'ms' .or. self%mode == 'ss') then
       if (self%mode == 'ms') str = trim( inp_path )//'/cidrtmsin'
       if (self%mode == 'ss') str = trim( inp_path )//'/cidrtin'

       self%mel = col_get_maximum_excitation_level(str)
    end if

    ! Adjust compute_nac if required
    open(newunit=u, file=control, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'nadcoupl') /= 0) then
          self%compute_nac = .true.
       end if
    end do
    close(u)

    if (self%prt_mo() > 0 .and. self%mocoef == 0) then
       str = 'prt_mo = '//to_str(self%prt_mo())//' and mocoef = 0 : &
            & the orbitals will not be saved (using original orbitals for all steps)'
       call stat%append(NX_WARNING, str, mod=modname, func=funcname)
       call self%set_prt_mo( -1 )
    end if
  end subroutine columbus_setup


  subroutine columbus_print(self, out)
    !! Print the content of the QM object.
    !!
    class(nx_columbus_t), intent(in) :: self
    integer, intent(in), optional :: out
    !! Optional output unit. (Default: ``stdout``).

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''

    call print_conf_ele(self%colenv, 'COLUMBUS path', unit=output)
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%mem, 'mem', unit=output)
    call print_conf_ele(self%reduce_tol, 'reduce_tol', unit=output)
    call print_conf_ele(self%ci_type, 'type of CI', unit=output)
    call print_conf_ele(self%grad_lvl, 'grad_lvl', unit=output)
    call print_conf_ele(self%all_grads, 'all_grads', unit=output)
    call print_conf_ele(self%cirestart, 'cirestart', unit=output)
    call print_conf_ele(self%citol, 'citol', elefmt='(e10.2)', unit=output)
    call print_conf_ele(self%quad_conv, 'quad_conv', unit=output)
    call print_conf_ele(self%mc_conv, 'mc_conv', unit=output)
    call print_conf_ele(self%ci_conv, 'ci_conv', unit=output)
    call print_conf_ele(self%ivmode, 'ivmode', unit=output)
    call print_conf_ele(self%mocoef, 'mocoef', unit=output)
    call print_conf_ele(self%ci_iter, 'ci_iter', unit=output)
    call print_conf_ele(self%mode, 'mode', unit=output)
    call print_conf_ele(self%mel, 'mel', unit=output)
  end subroutine columbus_print

  function columbus_to_str(self) result(res)
    class(nx_columbus_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&columbus'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' mem = '//to_str(self%mem)//nl
    res = res//' reduce_tol = '//to_str(self%reduce_tol)//nl
    res = res//' grad_lvl = '//to_str(self%grad_lvl)//nl
    res = res//' all_grads = '//to_str(self%all_grads)//nl
    res = res//' cirestart = '//to_str(self%cirestart)//nl
    res = res//' citol = '//to_str(self%citol)//nl
    res = res//' quad_conv = '//to_str(self%quad_conv)//nl
    res = res//' mc_conv = '//to_str(self%mc_conv)//nl
    res = res//' ci_conv = '//to_str(self%ci_conv)//nl
    res = res//' ivmode = '//to_str(self%ivmode)//nl
    res = res//' mocoef = '//to_str(self%mocoef)//nl
    res = res//' ci_iter = '//to_str(self%ci_iter)//nl
    res = res//'/'//nl
  end function columbus_to_str

  subroutine columbus_backup(self, qm_path, chk_path, stat, only_mo)
    !! Backup Columbus MOs and files.
    !!
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    !! Path to the directory where the QM job was performed.
    character(len=*), intent(in) :: chk_path
    !! Path to the backup destination.
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
    !! Indicate whether to backup only MOs, or whether to perform a full backup.

    integer :: ierr, i
    character(len=256) :: mo_with_path(size(MOFILES)), files_with_paths(size(BACKUPFILES))
    logical :: copy_mo_only

    character(len=*), parameter :: funcname = 'columbus_backup'

    copy_mo_only = .false.
    if (present(only_mo)) copy_mo_only = only_mo

    do concurrent(i=1:size(mo_with_path))
       mo_with_path(i) = trim(qm_path)//'/'//trim(mofiles(i))
    end do

    do concurrent(i=1:size(files_with_paths))
       files_with_paths(i) = trim(qm_path)//'/'//trim(BACKUPFILES(i))
    end do
    
    ierr = copy(mo_with_path, chk_path)
    if (ierr /= 0) then
       call stat%append(&
            & NX_ERROR, 'Cannot copy MO files to '//chk_path, &
            & mod=modname, func=funcname)
    end if
    
    ierr = gzip([trim(chk_path)//'/mocoef_mc.sp'])
    if (ierr /= 0) then
       call stat%append(&
            & NX_ERROR, 'Cannot compress '//chk_path//'/mocoef_mc.sp', &
            & mod=modname, func=funcname)
    end if

    if (.not. copy_mo_only) then
       ierr = copy(files_with_paths, chk_path)
       if (ierr /= 0) then
          call stat%append(&
               & NX_ERROR, 'Cannot copy backup files to '//chk_path, &
               & mod=modname, func=funcname)
       end if
    end if

  end subroutine columbus_backup


  subroutine columbus_update(self, conf, traj, path, stat)
    !! Update the Columbus input.
    !!
    !! The function prepares the files required to run Columbus at the current step, by
    !! performing the following tasks:
    !!
    !! 1. The ``redtol`` parameter is computed as a string, depending on the value of the
    !! ``col_reduce_tol`` and ``col_citol`` parameters (see [[col_wrt_redtol]] documentation).
    !! 2. The ``mcscfin``  file is updated, with the ``col_quad_conv`` parameter being
    !! used to reset ``ncoupl``.
    !! 3. For multi-state computations, in the ``ciudgin.drt1`` file, the ``ivmode``,
    !! ``nroot``, ``rtolbk`` and ``rtolci`` parameters are then modified with the content
    !! of ``col_ivmode``, ``nstat``, ``redtol`` and ``redtol`` respectively.
    !! 4. For single-state computations, the ``lroot`` parameter from file ``cidenin`` is
    !! set to ``nstatdyn``, and the following parameters are set in ``ciudgin``:
    !!     - ``nvkbmn = nstat``
    !!     - ``nvkbmx = nstat + 5``
    !!     - ``niter  = nstat * col_ci_iter``
    !!     - ``nvcimn = nstat + 2``
    !!     - ``nvcimx = nstat + 5``
    !!     - ``ivmode = col_ivmode``
    !!     - ``rtolbk = redtol``
    !!     - ``rtolci = redtol``
    !!     - ``nroot  = nstat``
    !! 5. After the first step of the dynamics, the ``scf`` keyword is deleted from
    !! ``control.run``.
    !! 6. If required (*i.e.* if ``col_cirestart = 1``, the we prepare the restart of the
    !! CI computation by calling the ``col_ci_restart_setup`` routine (see
    !! [[col_ci_restart_setup]] documentation).
    !! 7. If required (/i.e/ if ``col_mocoef = 1``), the ``mocoef`` file from ``path.old``
    !! (previous run) is copied to ``path`` to be used as starting orbitals.
    !! 8. Finally, the ``transmomin`` file is written by the ``col_write_transmomin``
    !! routine (see [[col_write_transmomin]] documentation).
    class(nx_columbus_t), intent(inout) :: self
    !! QM configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Main trajectory object.
    type(nx_config_t), intent(in) :: conf
    !! General NX configuration.
    character(len=*), intent(in) :: path
    !! Path to the files to modify.
    type(nx_status_t), intent(inout) :: stat

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt
    integer :: typeofdyn

    integer :: ierr
    logical :: ext
    character(len=:), allocatable :: redtol

    character(len=*), parameter :: funcname = 'columbus_update'

    ierr = 0

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt
    typeofdyn = traj%typeofdyn

    ! Now we start updating the different input files.
    redtol = col_write_redtol(&
         & self%citol, typeofdyn, nstat, nstatdyn, self%reduce_tol&
         & )

    ierr = copy('transmomin', path//'/')

    call col_change_keyword_nml(&
         & path//'/mcscfin', path//'/mcscfin', &
         & ['ncoupl'], [to_str(self%quad_conv)]&
         & )

    if (self%mode == 'ms') then
       block
         character(len=128) :: ms_conf_change(4), ms_conf_keys(4)
         ms_conf_keys   = [ character(len=128) :: 'ivmode', 'nroot', 'rtolbk', 'rtolci' ]
         ms_conf_change = [ character(len=128) :: &
              & to_str(self%ivmode), to_str(nstat), redtol, redtol&
              & ]
         call col_change_keyword_nml(&
              & path//'/ciudgin.drt1', path//'/ciudgin.drt1', &
              & ms_conf_keys, ms_conf_change&
              & )
       end block
    else if (self%mode == 'ss') then
       block
         character(len=128) :: ss_conf_change(9), ss_conf_keys(9)
         ss_conf_keys = [ character(len=128) :: &
              & 'nvbkmn', 'nvbkmx', 'niter ', &
              & 'nvcimn', 'nvcimx', 'ivmode', &
              & 'rtolbk', 'rtolci', 'nroot' &
              & ]
         ss_conf_change = [ character(len=128) :: &
              to_str(nstat), to_str(nstat+5), to_str(nstat * self%ci_iter), &
              to_str(nstat+2), to_str(nstat+5), to_str(self%ivmode), &
              redtol, redtol, to_str(nstat) &
              & ]
         call col_change_keyword_nml(&
              & path//'/ciudgin', path//'/ciudgin', &
              & ss_conf_keys, ss_conf_change)
         call col_change_keyword_nml(&
              & path//'/cidenin', path//'/cidenin', &
              & ['lroot'], [to_str(nstatdyn)])
       end block
    end if

    ! Delete the scf keyword after the first step
    if (traj%step > conf%init_step) then
       call col_delete_keyword(&
            & path//'/control.run', path//'/control.run', ['scf'])
    end if

    ! If required, prepare the CI restart computation
    if (self%ci_type >= 1) then
       if (self%cirestart == 1) then
          if (traj%step > conf%init_step) then
             call col_ci_restart_setup(self%mode, path)
          end if
       end if
    end if

    ! If required, copy the previous mocoef file as the starting coefficients for the new
    ! computation.
    if (self%mocoef == 1) then
       if (traj%step /= conf%init_step) then
          inquire(file=path//'.old/MOCOEFS/mocoef_mc.sp', exist=ext)
          if (ext) then
             ierr = copy(path//'.old/MOCOEFS/mocoef_mc.sp', path//'/mocoef')
             if (ierr /= 0) then
                call stat%append(&
                     NX_ERROR, &
                     'Cannot copy "MOCOEFS/mocoef_mc.sp" from '//trim(path)//'.old', &
                     mod=MODNAME, func=funcname &
                     ) 
             end if
          else
             call stat%append(&
                  NX_ERROR, &
                  'mocoef is set to 1, but no mocoef file can be found.', &
                  mod=MODNAME, func=funcname &
                  ) 
          end if
       end if
    end if

    if (conf%same_mo) then
       ! This file only exists AFTER reunning the reference calc.
       inquire(file='mocoef_ref', exist=ext)
       if (ext) then
          ierr = copy('mocoef_ref', trim(path)//'/mocoef')
          call check_error(ierr, COL_ERR_MAIN, &
               & 'COLUMBUS: Cannot copy ref_calc/mocoef as mocoef', system=.true.)
       end if
    end if

    ! Finally, rewrite the transmomin file for usage with Columbus.
    call self%write_transmomin(conf%dc_method, nstatdyn, nstat, path)
  end subroutine columbus_update


  subroutine columbus_run(self, stat)
    !! Run the Columbus job.
    !!
    class(nx_columbus_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: cmd
    integer :: ierr

    character(len=*), parameter :: funcname = 'columbus_run'
    character(len=256) :: cmdmsg
    
    write(cmd, '(a, i10)') &
         & self%colenv//'/runc -m ', self%mem
    call call_external(cmd, ierr, outfile='runls', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       call stat%append(NX_ERROR, 'Problem when running Columbus: '//cmdmsg, &
            & mod=MODNAME, func=funcname)
    end if
  end subroutine columbus_run


  function columbus_read_output(self, conf, traj, path, stat) result(info)
    !! Read the Columbus output.
    !!
    class(nx_columbus_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    !! NX main configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Main trajectory.
    character(len=*), intent(in) :: path
    !! Path to the directory where the Columbus job has been run.
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    character(len=:), allocatable :: enfile
    character(len=:), allocatable :: gradfile
    character(len=:), allocatable :: nadfile
    character(len=:), allocatable :: oscfile

    integer :: step, init_step
    integer :: nstatdyn

    integer :: i, j, nc
    logical :: iext
    integer :: niter, converged
    integer :: nstat, ncoupl
    integer, dimension(:), allocatable :: couplings
    logical :: print_osc
    logical :: ext

    character(len=1024) :: msg
    integer :: nread

    ! A little hack for running CS-FSSH with reference computations. Basically, if we
    ! detect that ``nstat`` is 1, then we switch off reading and computing nac in the
    ! reading process.
    logical :: dont_read_nac

    character(len=*), parameter :: funcname = 'columbus_read_output'

    step = traj%step
    nstatdyn = traj%nstatdyn
    init_step = conf%init_step

    nstat = conf%nstat
    ncoupl = nstat*(nstat - 1) / 2
    allocate(couplings(ncoupl))

    dont_read_nac = .false.
    if (nstat == 1) dont_read_nac = .true.

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! FIRST STEP ONLY: populate orbital space
    ! if (step == init_step .and. .not. qminfo%is_reference) then
    if (step == init_step) then
       info%orb = self%read_orb( path )
    end if

    ! Start by reading MCSCF information
    enfile = path//'/WORK/mcscfsm'
    call col_read_mcscf_energies(enfile, info%repot, niter, converged)
    if (converged == 0) then
       call stat%append(NX_WARNING, 'MCSCF did not converge', &
            & mod=MODNAME, func=funcname)
    end if

    if (self%grad_lvl == 2) then
       ! In this case we have a non-zero maximum excitation level, so we have
       ! to read the CI-related files (MRCI computation).

       enfile = path//'/WORK/ciudgsm'

       ! Read energies
       call col_read_mrci_energies(enfile, info%repot, niter, converged)
       if (converged == 0) then
          call stat%append(NX_WARNING, 'MCSCF did not converge', &
               & mod=MODNAME, func=funcname)
       end if

       ! Read contributions
       enfile = path//'/WORK/cipcls'
       call col_read_mrci_contributions(enfile, info)

       enfile = path//'/WORK/ciudgls'
       call col_read_mrci_ciudg(enfile, 0.85_dp, stat)

    else
       ! MCSCF only
       ! The contributions can be found either in WORK/mcpcls or in a series
       ! of LISTINGS/mcpclc.drt1.stateI.sp. We test for the presence of the
       ! first, and if it is not there then we use the series of files under
       ! LISTINGS.
       enfile = path//'/WORK/mcpcls'
       inquire(file=enfile, exist=ext)
       nread = 0
       if (.not. ext) then
          do i=1, nstat
             nread = nread + 1
             enfile = path//'/LISTINGS/mcpcls.drt1.state'//to_str(i)//'.sp'
             call info%append_data('Dominant contributions for state '//to_str(nread))
             call col_read_mcscf_contributions(enfile, info)
          end do
       else
          call col_read_mcscf_contributions(enfile, info)
       end if
    end if

    ! Gradients
    if (self%compute_nac) then
       if (.not. dont_read_nac) then
          do i=1, size(info%rgrad, 1)
             gradfile = path//'/GRADIENTS/cartgrd.drt1.state'//to_str(i)//'.sp'
             call col_read_gradients(gradfile, info%rgrad, i)
          end do
       end if
    else
       gradfile = path//'/GRADIENTS/cartgrd.all'
       call col_read_gradients(gradfile, info%rgrad, nstatdyn)
    end if

    ! NAD
    if (self%read_nac) then
       nc = 1
       do i=2, nstat
          do j=1, i-1
             if (traj%couplings(i, j) == 1) then
                nadfile = path//'/GRADIENTS/cartgrd.nad.drt1.state'//&
                     & to_str(i)//'.drt1.state'//to_str(j)//'.sp'
                call col_read_nad(nadfile, info%rnad, nc)
                nc = nc + 1
             end if
          end do
       end do
    end if

    nc = 1
    do i=2, nstat
       do j=1, i-1

          ! A little bit cumbersome, but necessary (Columbus does not
          ! necessarily prints oscillator strengths).
          oscfile = path//'/LISTINGS/trncils.FROMdrt1.state'// &
               & to_str(i)//'TOdrt1.state'//to_str(j)
          inquire(file=oscfile, exist=iext)
          if (iext) then
             info%has_osc_str = .true.
             info%has_osc_comp = .true.
             call col_read_osc_str(oscfile, info%osc_str_comp(:, nc), info%osc_str(nc))
          else
             info%has_osc_str = .false.
          end if

          nc = nc + 1
       end do
    end do

    ! if (print_osc) then
    !    call info%append_data('')
    !    call info%append_data('Oscillator strengths and transition moments')
    !    call info%append_data('')
    !    write(msg, '(5a15)') 'Excitation', 'Osc. str.', 'dx', 'dy', 'dz'
    !    call info%append_data(msg)
    !    call info%append_data(repeat(' --------------', 5))
    !    do i=1, ncoupl
    !       write(msg, '(i15, 4F15.6)') &
    !            & i, info%osc_str(i), (info%osc_str_comp(j, i), j=1, 3)
    !       call info%append_data(msg)
    !    end do
    ! end if
  end function columbus_read_output


  subroutine columbus_write_geometry(self, traj, path, print_merged)
    !! Write a coordinate file in the ``geom`` format for Columbus.
    !!
    class(nx_columbus_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object containing the geometry to print.
    character(len=*), intent(in) :: path
    !! Path where to save the resulting file.
    logical, intent(in), optional :: print_merged
    !! Indicate if the routine should print the merged (double molecule) coordinates (not
    !! used).

    real(dp) :: mass
    integer :: i, j, u
    
    open(newunit=u, file=trim(path)//'/'//self%geomfile, action='write')
    do i=1, size(traj%geom, 2)

       ! Columbus limit in mass format
       mass = traj%masses(i) / proton
       if (mass >= 100.0_dp) then
          mass = 99.9_dp
       end if

       write(u, '(1x,a2,2x,f5.1,4f14.8)') &
            & traj%atoms(i), traj%Z(i), (traj%geom(j, i), j=1, 3), &
            & mass
    end do
    close(u)
  end subroutine columbus_write_geometry
  


  subroutine columbus_init_overlap(self, path_to_qm, stat)
    !! Initialize an overlap computation.
    !!
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    !! Path to the directory running the QM job.
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    logical :: ext

    inquire(file=path_to_qm//'/WORK/daltaoin', exist=ext)
    if (ext) then
       ierr = copy(path_to_qm//'/WORK/daltaoin', 'overlap.old/daltaoin_single.old')
       if (ierr /= 0) then
          call stat%append(&
               & NX_ERROR, 'Cannot copy WORK/daltaoin as overlap.old/daltaoin_single.old', &
               & mod=MODNAME, func='columbus_init_overlap')
       end if
    end if
  end subroutine columbus_init_overlap


  subroutine columbus_prepare_overlap(self, path_to_qm, stat)
    !! Prepare an overlap computation.
    !!
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    !! Path to the directory running the QM job.
    type(nx_status_t), intent(inout) :: stat

    logical :: ext
    integer :: u, v, w, ierr, i
    character(len=MAX_STR_SIZE) :: buf, dum
    character(len=1) :: contraction
    integer :: nat, maxpri
    character(len=*), parameter :: funcname = 'columbus_prepare_overlap'

    character(len=:), allocatable :: origin, oldinp, newinp, &
         & daltcom_single, daltcom_new

    origin = path_to_qm//'/WORK/daltaoin'
    oldinp = 'overlap.old/daltaoin_single.old' 
    newinp = 'overlap/daltaoin'
    daltcom_single = path_to_qm//'/WORK/daltcomm' 
    daltcom_new = 'overlap/daltcomm'

    inquire(file=origin, exist=ext)
    if (.not. ext) then
       call stat%append( &
            & NX_ERROR, &
            & 'COLUMBUS: Cannot find '//trim(origin)//', required for double molecule !', &
            & mod=MODNAME, func=funcname)
    end if

    open(newunit=u, file=origin, action='read')
    open(newunit=v, file=newinp, action='write')
    ! Copy first three lines without modifications
    do i=1, 3
       read(u, '(a)') buf
       write(v, '(a)') trim(buf)
    end do

    ! For the fourth line, we need to update the second number
    read(u, '(a)') buf
    contraction = buf(1:1)
    read(buf(2:5), *) nat
    read(buf(6:), '(A)') dum
    write(v, '(a,I4,a)') contraction, 2*nat, trim(dum)

    open(newunit=w, file=oldinp, action='read')
    do i=1, 4
       read(w, '(a)') buf
    end do
    do
       read(w, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do

    ! We already read the first 4 lines of this file before
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do

    close(u)
    close(w)
    close(v)

    maxpri = -100
    open(newunit=u, file=daltcom_single, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '.MAXPRI') /= 0) then
          read(u, *) maxpri
          exit
       end if
    end do
    close(u)

    open(newunit=u, file=daltcom_new, action='write')
    write(u, '(a)') "**DALTONINPUT"
    write(u, '(a)') ".INTEGRALS"
    write(u, '(a)') ".PRINT"
    write(u, '(a)') "   2"
    write(u, '(a)') "**INTEGRALS"
    write(u, '(a)') ".PRINT"
    write(u, '(a)') "   2"
    write(u, '(a)') ".NOSUP"
    write(u, '(a)') ".NOTWO"
    write(u, '(a)') "*READIN"
    if (maxpri /= -100) then
       write(u, '(a)') ".MAXPRI"
       write(u, '(I4)') maxpri
    end if
    write(u, '(a)') "**END OF INPUTS"
    close(u)

    ierr = copy(path_to_qm//'/WORK/daltaoin', 'overlap/daltaoin_single.old')
    if (ierr /= 0) then
       call stat%append( NX_ERROR, &
            & 'Cannot copy WORK/daltaoin as overlap/daltaoin_single.old', &
            & mod=MODNAME, func=funcname)
    end if
  end subroutine columbus_prepare_overlap


  function columbus_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    !! Extract the AO overlap from a double-molecule computation.
    !!
    !! The ``aoints`` file is first parsed with the ``readsifs`` utility that should be
    !! found in the directory defined by the ``$CIOVERLAP`` environment.
    !!
    !! The the resulting binary file ``aoints1S`` is read to populate the ``ovl`` array.
    class(nx_columbus_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    !! Dimension of the resulting array.
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    !! Path to the ``readsifs`` program (usually ``$CIOVERLAP``).

    real(dp) :: ovl(dim_ovl)

    character(len=:), allocatable :: ovlfile, cmd
    character(len=256) :: mydir, cmdmsg
    logical :: ext
    integer :: status, ierr, irec, i, u, v

    ovlfile = './overlap/aoints1S'
    if (.not. ext) then
       cmd = trim(script_path)//'/readsifs -1 aoints'
       status = getcwd(mydir)
       status = chdir('./overlap/')
       call call_external(cmd, ierr, outfile='readsifsls', cmdmsg=cmdmsg)
       if (ierr /= 0) then
          call stat%append( &
               & NX_ERROR, 'Problem with readsifs: '//trim(cmdmsg), &
               & mod=MODNAME, func='columbus_extract_overlap'&
               & )
       end if
       status = chdir(mydir)
    end if

    irec = 0
    open(newunit=u, file=ovlfile, form='unformatted', access='direct', recl=8)
    ! open(newunit=v, file='temp')
    do i=1, dim_ovl
       read(u, rec=i) ovl(i)
       irec = irec + 1
       ! write(v, '(a,i5,a,F25.15)') 'irec = ', irec, ': ', ovl(i)
    end do
    close(u)
    ! close(v)
  end function columbus_extract_overlap


  function columbus_get_lcao(self, dir_path, nao, stat) result(lcao)
    !! Extract the LCAO matrix from a Columbus computation.
    !!
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    !! Path to QM execution folder.
    integer, intent(in) :: nao
    !! Number of atomic orbitals (dimension of the LCAO matrix).
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: lcao(nao, nao)

    integer :: u, i, j, k, l
    integer :: ntitle
    integer :: nele, remainder
    character(len=:), allocatable :: mosfile
    character(len=MAX_STR_SIZE) :: buf

    mosfile = trim(dir_path)//'/MOCOEFS/mocoef_mc.sp'

    ! nele = aint(float(nao/3)) ! Number of elements in full lines
    nele = int(nao/3)
    remainder = nao - 3*nele  ! Number of elements in partial lines

    ! write(*, *) 'nele = ', nele, '; remainder = ', remainder

    open(newunit=u, file=mosfile, action='read')
    ! Header lines are discarded
    do i=1, 2
       read(u, *)
    end do

    ! Number of title lines
    read(u, *) ntitle
    do i=1, ntitle+5
       read(u, *)
    end do

    ! Now read the coefficient
    do i=1, nao
       k = 1
       do j=1, nele
          read(u, '(a)') buf
          read(buf, *) (lcao(i, l), l=k, k+2)
          k = k+3
       end do
       if (remainder > 0) then
          k = 3*nele
          read(u, *) (lcao(i, l), l=k+1, k+remainder)
       end if
    end do
    close(u)

    ! Now we finished reading, we have to transpose the result
    lcao = transpose(lcao)
  end function columbus_get_lcao


  subroutine columbus_ovl_post(self, stat)
    !! NOT USED.
    class(nx_columbus_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    ! Not needed for Columbus
  end subroutine columbus_ovl_post


  subroutine columbus_ovl_run(self, stat)
    !! Run the overlap computation.
    !!
    !! This computation just needs to call ``dalton.x`` to print the AO overlap.
    class(nx_columbus_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=256) :: msg

    call call_external( &
         & self%colenv//'/dalton.x -m '//to_str(8 * self%mem), ierr, &
         & outfile='daltonls', cmdmsg=msg&
         & )
    if (ierr /= 0) then
       call stat%append(NX_ERROR, 'Problem when running dalton.x: '//trim(msg), &
            & mod=MODNAME, func='columbus_ovl_run')
    end if
  end subroutine columbus_ovl_run


  subroutine columbus_cio_prepare_files(self, path_to_qm, orb, stat)
    !! Prepare the necessary files for running ``cioverlap``.
    !!
    !! This routine will be done in case we need the state-overlap matrix (for instance
    !! when running with local diabatization algorithm).
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    !! Path to QM files to read.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space
    type(nx_status_t), intent(inout) :: stat

    logical :: ext
    integer :: u, status, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=256) :: msg
    character(len=:), allocatable :: cmd, mydir, cioenv
    character(len=*), parameter :: funcname = 'columbus_cio_prepare_files'

    ! First, go into the WORK directory. All files are there, so it is more
    ! simple than copying everything.
    status = getcwd(buf)
    mydir = trim(buf)
    status = chdir(trim(path_to_qm)//'/WORK')

    ierr = 0
    ! Prepare list of slater determinants with cipc
    ! call get_environment_variable('COLUMBUS', buf)
    ! colenv = trim(buf)
    ! write(msg, '(a30, a5)') 'Preparing list of Slater det', ' ... '
    ! res = message(msg, advance='no')
    ! call nx_log%log(LOG_DEBUG, 'Preparing list of Slater determinants')
    if (self%ci_type >= 1) then
       inquire(file=self%colenv//'/mycipc.x', exist=ext)
       if (ext) then
          cmd = self%colenv//'/mycipc.x < cipcin > mycipcls'
       else
          cmd = 'echo 3 | '//self%colenv//'/cipc.x > mycipcls'
       end if
       call execute_command_line(trim(cmd), exitstat=ierr, cmdmsg=msg)
       if (ierr /= 0) then
          call stat%append(NX_ERROR, 'Problem when running mycipc.x: '//trim(msg), &
               & mod=MODNAME, func=funcname &
               & )
          return
       end if
       
       inquire(file='eivectors', exist=ext)
       if (.not. ext) then
          call stat%append(NX_ERROR, "Eivectors does not exist !! Check cipc program.", &
               & mod=MODNAME, func=funcname &
               & )
          return
       end if

    else
       open(newunit=u, file='mcpcin', action='write')
       write(u, '(a)') "1"
       write(u, '(a)') "1"
       write(u, '(a)') "9"
       write(u, '(a)') ''
       close(u)
       cmd = self%colenv//'/mcpc.x < mcpcin > mcpcls.eivec'
       call execute_command_line(trim(cmd), exitstat=ierr, cmdmsg=msg)
       if (ierr /= 0) then
          call stat%append(NX_ERROR, 'Problem when running mcpc.x: '//trim(msg), &
               & mod=MODNAME, func=funcname &
               & )
          return
       end if
    end if
    ! call nx_log%log(LOG_DEBUG, 'Done preparing Slater determinants')

    ierr = rename('eivectors', 'eivectors.columbus')
    if (ierr /= 0) then
       call stat%append( &
            & NX_ERROR, "Couldn't rename 'eivectors' as 'eivectors.columbus'.", &
            & mod=MODNAME, func=funcname &
            & )
       return
    end if
    
    call execute_command_line(&
         & 'cat eivectorshead eivectors.columbus > eivectors2', &
         & exitstat=ierr)
    if (ierr /= 0) then
       call stat%append( &
            & NX_ERROR, "Couldn't create 'eivectors2'", &
            & mod=MODNAME, func=funcname &
            & )
       return
    end if

    ! Next consolidate the file if required
    if (self%ci_consolidate == 1) then
       ! call nx_log%log(LOG_DEBUG, 'Consolidating CI wavefunction')

       ierr = copy('eivectors2', 'eivectors2.org')
       ierr = copy('slaterfile', 'slaterfile.org')

       call get_environment_variable('CIOVERLAP', buf)
       cioenv = trim(buf)
       
       cmd = 'echo '//to_str(orb%nelec)//' 0 | '//cioenv//&
            & '/civecconsolidate eivectors2.org slaterfile.org'//&
            & ' eivectors2 slaterfile consolidatefile'//&
            & ' > civecconsolidate.out'

       call execute_command_line(cmd, exitstat=ierr, cmdmsg=msg)
       ! call check_error(ierr, CIO_COL_CONSOLIDATE)
       if (ierr /= 0) then
          call stat%append(NX_ERROR, 'Problem in consolidating WF: '//trim(msg), &
               & mod=MODNAME, func=funcname &
               & )
          return
       end if
    end if
    status = chdir(mydir)

    ! In the first step cioverlap/ contains nothing (and does not
    ! exist yet), so let's test !
    inquire(file='./cioverlap/eivectors2', exist=ext)
    if (.not. ext) then
       ierr = copy(trim(path_to_qm)//'/WORK/eivectors2', 'cioverlap/')
       ierr = copy(trim(path_to_qm)//'/WORK/slaterfile', 'cioverlap/')
       if (self%ci_consolidate == 1) then
          ierr = copy(trim(path_to_qm)//'/WORK/consolidatefile', 'cioverlap/')
       end if
    end if
  end subroutine columbus_cio_prepare_files

  subroutine columbus_cio_get_mos_energies(self, path_to_qm, stat, mos)
    !! NOT NEEDED IN COLUMBUS
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    real(dp), intent(out) :: mos(:)
    type(nx_status_t), intent(inout) :: stat

    ! Not needed for Columbus
  end subroutine columbus_cio_get_mos_energies

  subroutine columbus_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    !! NOT NEEDED IN COLUMBUS
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    real(dp), intent(out) :: tia(:, :, :)
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out), optional :: tib(:, :, :)

    ! Not needed for Columbus
  end subroutine columbus_cio_get_singles_amplitudes


  ! ================================= !
  ! Private functions and routines
  ! ================================= !
  ! ======================
  ! 1. Input file parsers.
  ! ======================
  subroutine col_load_file(filename, loaded)
    !! Load a file in memory.
    !!
    !! The file is loaded as an array ``loaded`` of strings of size ``MAX_STR_SIZE``.
    !! The size of the ``loaded`` array is determined by a first go over the file, so the
    !! routine parses the file twice.
    character(len=*), intent(in) :: filename
    !! File to read.
    character(len=MAX_STR_SIZE), allocatable, intent(inout) :: loaded(:)
    !! Loaded file.

    integer :: u, ierr, nlines, i
    character(len=MAX_STR_SIZE) :: buf

    open(newunit=u, file=filename, action='read')
    nlines = 0
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
    end do

    rewind(u)
    allocate(loaded(nlines))

    do i=1, nlines
       read(u, '(a)') buf
       loaded(i) = to_lower(trim(buf))
    end do
    close(u)
  end subroutine col_load_file

  function col_ci_type(infile) result(res)
    !! Find the "CI type" based on the content of infile ``control.run``.
    !!
    !! The test is based on the presence of either keywords:
    !!
    !! - ``pciudg``: ``ci_type = 2`` ;
    !! - ``ciudg`` or ``cigrad``: ``ci_type = 1`` ;
    !! - ``nadcoupl`` and no ``mcscf``: ``ci_type = 1``;
    !! - default: ``ci_type = 0``.
    character(len=*), intent(in) :: infile
    !! Path to the ``control.run`` file.

    integer :: res

    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    integer :: i
    logical :: nadcoupl, mcscf, pciudgav, ciudg

    nadcoupl = .false.
    mcscf = .false.
    pciudgav = .false.
    ciudg = .false.

    call col_load_file(infile, loaded)
    do i=1, size(loaded)
       if (loaded(i) == 'pciudg') then
          pciudgav = .true.
       end if

       if (loaded(i) == 'ciudg' .or. loaded(i) == 'cigrad') then
          ciudg = .true.
       end if

       if (loaded(i) == 'nadcoupl') then
          nadcoupl = .true.
       end if

       if (loaded(i) == 'mcscf') then
          mcscf = .true.
       end if
    end do

    if (pciudgav) then
       res = 2
       return
    else if (ciudg) then
       res = 1
       return
    else if (nadcoupl .and. .not. mcscf) then
       res = 1
       return
    else
       res = 0
       return
    end if
  end function col_ci_type

  function col_get_mode(infile) result(res)
    !! Get the mode for the Columbus run.
    !!
    !! The mode is determined by inspecting the ``control.run`` file, and is equal to:
    !!
    !! - ``ms`` (multi-state) when ``ciudgav`` is found ;
    !! - ``ss`` (single-state) when ``ciudg`` is found ;
    !! - ``mcs`` (multi-configuration gradients) when ``mcscfgrad`` is found ;
    !! - ``mc`` in other cases (default value).
    !!
    !! *NOTE (BD 2023-08-30)*: it seems that ``mcs`` and ``mc`` seem equivalent in the
    !! code, the distinction was taken from NX-CS. The real distinction here is done with
    !! a default case (``mc``), and specific ``ms`` or ``ss`` cases.
    character(len=*), intent(in) :: infile
    !! Path to ``control.run``.

    character(len=:), allocatable :: res

    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: buf

    res = "mc"
    open(newunit=u, file=infile, action='read')
    LOOP: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'ciudgav') /= 0) then
          res = 'ms'
          exit LOOP
       else if (index(buf, 'ciudg') /= 0) then
          res = 'ss'
          exit LOOP
       else if (index(buf, 'mcscfgrad') /= 0) then
          res = 'mcs'
          exit LOOP
       end if
    end do LOOP
    close(u)
  end function col_get_mode

  function col_get_maximum_excitation_level(infile) result(res)
    !! Get the maximum excitation level.
    !!
    !! Inspect ``infile`` for the ``maximum excitation level`` parameter value.  This is
    !! mode-dependent, and ``infile`` should be:
    !!
    !! - ``cidrtmsin`` in ``ms``-mode ;
    !! - ``cidrtin`` in ``ss``-mode.
    !!
    !! In other modes, the maximum excitation-level is kept to 0.
    character(len=*), intent(in) :: infile

    integer :: res

    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: group(:)

    res = -1
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'maximum excitation level') /= 0) then
          group = split_pattern(buf, pattern='/')
          read(group(1), *) res
       end if
    end do
    close(u)
  end function col_get_maximum_excitation_level


  function col_write_redtol(&
       & citol, type_of_dyn, nstat, nstatdyn, reduce_tol &
       & ) &
       & result(res)
    !! Write a string of reduced tolerances.
    !!
    !! The string consists in a number of ``nstat`` statements, giving the
    !! tolerance in the CI iteration for the corresponding state. The basis is
    !! given by the ``citol`` parameter. This value is always set for the state of
    !! interest (i.e. ``nstatdyn``). The value for the other states depend on
    !! ``reduce_tol``, ``type_of_dyn``, and on the content of the transmomin
    !! file.
    !!
    !! With ``reduce_tol = 1``, all tolerances will be set to ``citol * 10``. With
    !! ``reduce_tol = 2``, the tolerance is set to ``citol * 10`` if ``type_of_dyn = 1``
    !! (*i.e.* the state of interest is not "too far" from the surrounding
    !! states) or if a coupling between this state and the state of interest is
    !! to be computed. In all other cases, the tolerance is set to ``citol * 1000``.
    !!
    !! Usage:
    !! ```fortran
    !! character(len=:, allocatable) :: redtol
    !! redtol = col_wrt_redtol(citol, type_of_dyn, nstat, nstatdyn, reduce_tol)
    !! ```
    !!
    !! Example:
    !!
    !! ```fortran
    !! character(len=:, allocatable) :: redtol1, redtol2, redtol3, redtol4, redtol5
    !! redtol1 = col_wrt_redtol(1E-4, 1, 3, 2, 1)
    !! redtol2 = col_wrt_redtol(1E-4, 1, 3, 2, 0)
    !! redtol3 = col_wrt_redtol(1E-4, 1, 3, 3, 1)
    !! redtol4 = col_wrt_redtol(1E-4, 2, 4, 2, 2) ! (assuming kross = 0)
    !! redtol5 = col_wrt_redtol(1E-4, 2, 4, 2, 2) ! (assuming kross = 1)
    !!
    !! print *, 'REDTOL1: ', redtol1
    !! print *, 'REDTOL2: ', redtol2
    !! print *, 'REDTOL3: ', redtol3
    !! print *, 'REDTOL4: ', redtol4
    !! print *, 'REDTOL5: ', redtol5
    !! ```
    !!
    !! The results will be:
    !!
    !!     REDTOL1: 1E-3, 1E-4, 1E-3
    !!     REDTOL2: 1E-4, 1E-4, 1E-4
    !!     REDTOL3: 1E-3, 1E-3, 1E-4
    !!     REDTOL4: 1E-3, 1E-4, 1E-3, 1E-1
    !!     REDTOL5: 1E-3, 1E-4, 1E-3, 1E-3
    !!
    real(dp), intent(in) :: citol
    !! CI tolerance (``col_citol``).
    integer, intent(in) :: type_of_dyn
    !! Current type of dynamics, as given by ``type_of_dynamics`` routine from ``mod_md_utils``.
    integer, intent(in) :: nstat
    !! Number of states.
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.
    integer, intent(in) :: reduce_tol
    !! How to reduce the tolerance (``col_redtol``).

    character(len=:), allocatable :: res
    character(len=MAX_STR_SIZE) :: tmp

    integer :: istring, ele
    real(dp) :: tol0
    integer, allocatable :: pairs(:)

    tmp = ''
    do istring=1, nstat
       if (istring == nstatdyn) then
          tol0 = citol
       else
          if (reduce_tol == 0) then
             tol0 = citol
          else if (reduce_tol == 1) then
             tol0 = citol * 10_dp
          else if (reduce_tol == 2) then

             if (type_of_dyn == 1) then
                tol0 = citol * 10_dp
             else
                tol0 = citol * 1000_dp
             end if

             call col_read_transmomin('transmomin', 'CI', pairs)
             do ele=1, size(pairs)
                if (istring == pairs(ele)) then
                   tol0 = citol * 10_dp
                end if
             end do
          end if
       end if

       write(tmp, '(A,E7.1,A)') trim(tmp)//' ', tol0, ','
    end do

    res = trim(tmp)
  end function col_write_redtol


  subroutine col_change_keyword_nml(origin, newfile, keys, values)
    !! Change the value of one or several keywords in namefile.
    !!
    !! The routine starts by loading ``origin`` with ``col_load_file`` (see
    !! [[col_load_file]] documentation), and parses the content of the loaded array.
    !! Whenever a line contains any of the keywords from array ``keys``, with index ``j``,
    !! it replaces the corresponding value with ``values(j)``.  The resulting array is
    !! then written as file ``newfile``.
    !!
    !! Usage:
    !!
    !! ```fortran
    !! call col_change_keyword_nml('file1.nml', 'file2.nml', ['param1'], ['val1'])
    !! ```
    !!
    !! After the call:
    !!
    !! ```shell
    !! $ cat file1.nml
    !!  &namelist
    !!    param1 = 2
    !!    param2 = 4
    !!    param3 = 'temp'
    !!  /
    !! $ cat file2.nml
    !!  &namelist
    !!    param1 = val1
    !!    param2 = 4
    !!    param3 = 'temp'
    !!  /
    !! ```
    !!
    character(len=*), intent(in) :: origin
    !! File to be modified.
    character(len=*), intent(in) :: newfile
    !! File written with modified content (usually the same name as ``origin``).
    character(len=*), intent(in) :: keys(:)
    !! List of keys to modify.
    character(len=*), intent(in) :: values(:)
    !! For each key, the corresponding modified value.

    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    character(len=:), allocatable :: buf
    integer :: i, j, id, u

    call col_load_file(origin, loaded)

    do i=1, size(loaded)
       id = index(loaded(i), '=')
       if (id /= 0) then
          ! We found a line with a keyword on it, we are only interested in the part
          ! before the equal sign
          buf = remove_blanks( loaded(i)(1:id-1) )
          do j=1, size(keys)
             if (trim(buf) == trim(keys(j))) then
                loaded(i) = trim(keys(j))//' = '//trim(values(j))//','
             end if
          end do
          if (loaded(i)(len_trim(loaded(i)):len_trim(loaded(i))) == ',') &
               & loaded(i)(len_trim(loaded(i)):len_trim(loaded(i))) = ''
          deallocate(buf)
       end if
    end do

    open(newunit=u, file=newfile, action='write')
    do i=1, size(loaded)
       write(u, '(a)') trim(loaded(i))
    end do
    close(u)
  end subroutine col_change_keyword_nml


  subroutine col_delete_keyword(oldfile, newfile, key)
    !! Delete a keyword from the given file.
    !!
    !! The routine deletes a keyword from a namelist, or from a file containing simply a
    !! list of keywords (in particular ``control.run``).
    !!
    !! Usage:
    !!
    !! ```fortran
    !! call col_delete_keyword('myfile.nml', 'modfile.nml', ['key1', 'key2'])
    !! ```
    !!
    !! The result is:
    !! ```shell
    !! $ cat myfile.nml
    !!  &namelist
    !!    key1 = 2
    !!    key2 = 4
    !!    key3 = 'temp'
    !!  /
    !! $ cat modfile.nml
    !!  &namelist
    !!    key3 = 'temp'
    !!  /
    !! ```
    !!
    character(len=*), intent(in) :: oldfile
    !! File to read.
    character(len=*), intent(in) :: newfile
    !! Modified file (usually the same as ``oldfile``).
    character(len=*), intent(in) :: key(:)
    !! List of keys to delete.

    integer :: u, id, i, j
    character(len=MAX_STR_SIZE), allocatable :: loaded(:), new_array(:)
    character(len=MAX_STR_SIZE) :: buf

    call col_load_file(oldfile, loaded)

    allocate( new_array(size(loaded)) )
    new_array(:) = ''

    id = 1
    do i=1, size(loaded)
       if (index('=', loaded(i)) /= 0) then
          ! We have a namelist with 'key = value'
          buf = trim(loaded(i)(1:id))
       else
          ! We have simply one key per line, as in control.run
          buf = loaded(i)
       end if

       buf = remove_blanks(buf)

       do j=1, size(key)
          if (trim(buf) /= key(j)) then
             new_array(id) = trim(buf)
             id = id + 1
          end if
       end do
    end do

    open(newunit=u, file=newfile, action='write')
    do i=1, size(new_array)
       write(u, '(a)') trim(new_array(i))
    end do
    close(u)
  end subroutine col_delete_keyword


  subroutine col_ci_restart_setup(mode, run_dir)
    !! Set up up the CI restart procedure.
    !!
    !! If the backup folder ``WORK1`` exists (*i.e.* we are after step 0), then the
    !! keyword ``ivmode`` is set to 4 in ``ciudgin`` (``ciudgin.drt1`` for ``mode =
    !! ms``).   Then the ``drt1`` suffix is either added or removed from the files in
    !! ``WORK1``, depending on the ``mode`` value.  Finally, the ``WORK1`` directory is
    !! renamed as ``WORK`` to be read by Columbus.
    character(len=*), intent(in) :: mode
    !! Columbus mode of the computation.
    character(len=*), intent(in) :: run_dir
    !! Name of the directory where to look for ``WORK/``.

    character(len=:), allocatable :: ciu, cif, cio, ciubad, cifbad, ciobad
    logical :: ext
    integer :: ierr

    ciu = ''
    cif = ''
    cio = ''
    ciubad = ''
    cifbad = ''
    ciobad = ''

    if (mode == 'ss') then
       ciu = 'ciudgin'
       cif = 'civfl_restart'
       cio = 'civout_restart'
       cifbad = 'civout_restart.drt1'
       ciobad = 'civout_restart.drt1'
    else if (mode == 'ms') then
       ciu = 'ciudgin.drt1'
       cif = 'civfl_restart.drt1'
       cio = 'civout_restart.drt1'
       cifbad = 'civfl_restart'
       ciobad = 'civout_restart'
    end if

    call col_change_keyword_nml( &
         & run_dir//'/'//ciu, run_dir//'/'//ciu, ['ivmode'], ['4']&
         & )

    inquire(file=run_dir//'/WORK/', exist=ext)
    if (.not. ext) then
       ierr = mkdir(run_dir//'/WORK/')
    end if

    ierr = copy(run_dir//'.old/WORK/'//cifbad, run_dir//'/WORK/'//cif)
    ierr = copy(run_dir//'.old/WORK/'//ciobad, run_dir//'/WORK/'//cio)
  end subroutine col_ci_restart_setup


  subroutine col_read_transmomin(filename, level, pairs)
    !! Find the couplings to be computed from a ``transmomin`` file.
    !!
    !! The routine parses ``transmomin`` and returns an array ``pairs`` with elements
    !! grouped by 2, giving the couplings to be computed.
    character(len=*), intent(in) :: filename
    !! File to parse.
    character(len=*), intent(in) :: level
    !! Level at which the gradients are computed.
    integer, intent(inout) :: pairs(:)
    !! Resulting pairs array.

    integer :: u, ierr, id
    integer :: d1, d2, st1, st2
    character(len=MAX_STR_SIZE) :: buf
    logical :: start_read

    start_read = .false.
    pairs(:) = -1
    id = 1
    open(newunit=u, file=filename, action='read')
    READ_TRANSMOMIN: do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit READ_TRANSMOMIN

       if ( index(level, buf) /= 0 ) start_read = .true.

       if (start_read) then
          do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_TRANSMOMIN

             read(buf, *) d1, st1, d2, st2
             if (d1 /= 1) exit READ_TRANSMOMIN

             pairs(id) = st1
             pairs(id+1) = st2

             id = id+2
          end do
       end if
    end do READ_TRANSMOMIN
    close(u)
  end subroutine col_read_transmomin


  subroutine col_write_transmomin(self, dc, nstatdyn, nstat, path)
    !! Write the ``transmomin`` file.
    !!
    !! This routine writes a ``transmomin`` file that can be parsed by Columbus.  It
    !! starts by backing up the existing ``transmomin`` file as ``transmomin.nx``, then
    !! it parses ``transmomin.nx``.
    !!
    !! First, we check if the right gradient level is set (MCSCF or CI, depending on the
    !! value of ``col_grad_lvl``).  If needed, we modify the value found in
    !! ``transmomin.nx``.  Then the lines containing the couplings to be computed are
    !! copied from ``transmomin.nx``, adding a ``T`` label at the end if the coupling
    !! should not be computed (for ``derivative_coupling`` different than one, so for
    !! dynamics where the couplings are either not needed (local diabatization) or
    !! computed by different means).
    !!
    !! Finally, depending on the value of ``col_all_grads``, the lines for the gradient
    !! computation are added.
    class(nx_columbus_t), intent(in) :: self
    !! QM configuration.
    integer, intent(in) :: dc
    !! Derivative couplings mode (``conf%dc_coupling`` parameter).
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.
    integer, intent(in) :: nstat
    !! Total number of states.
    character(len=*), intent(in) :: path

    integer :: u, v, ierr, i
    integer :: drt1, st1, drt2, st2
    character(len=MAX_STR_SIZE) :: buf
    logical :: ext

    inquire(file=path//'/transmomin', exist=ext)
    if (.not. ext) then
       open(newunit=v, file=path//'/transmomin', action='write')
       if (self%grad_lvl == 2) then
          write(v, '(a)') 'CI'
       else if (self%grad_lvl == 1) then
          write(v, '(a)') 'MCSCF'
       end if
       write(v, '(a)') ' 1 1 1 1'
       close(v)
       return
    end if

    ierr = copy(path//'/transmomin', path//'/transmomin.nx')
    call check_error(ierr, COL_ERR_MISC, &
         & 'COLUMBUS: Cannot copy transmomin !')

    open(newunit=v, file=path//'/transmomin', action='write')
    open(newunit=u, file=path//'/transmomin.nx', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if ( index(buf, 'MCSCF') /= 0 ) then
          if (self%grad_lvl == 2) then
             ! CI gradients
             write(v, '(a)') 'CI'
          else
             write(v, '(a)') trim(buf)
          end if
       else if ( index(buf, 'CI') /= 0 ) then
          if (self%grad_lvl == 1) then
             ! MCSCF gradients
             write(v, '(a)') 'MCSCF'
          else
             write(v, '(a)') trim(buf)
          end if
       else
          read(buf, *) drt1, st1, drt2, st2

          if (dc /= 1) then
             if (st1 /= st2) then
                write(v, '(4I3,A)') drt1, st1, drt2, st2, ' T'
             end if
          else
             if (st1 /= st2) then
                write(v, '(4I3)') drt1, st1, drt2, st2
             end if
          end if
       end if
    end do
    close(u)

    if (self%all_grads == 0) then
       write(v, '(4I3)') 1, nstatdyn, 1, nstatdyn
    else if (self%all_grads == 1) then
       do i=1, nstat
          write(v, '(4I3)') 1, i, 1, i
       end do
    end if
    close(v)
  end subroutine col_write_transmomin


  function col_read_orb(self, path) result(orb)
    !! Populate the ``orb`` member of ``nx_qm``.
    !!
    class(nx_columbus_t), intent(in) :: self
    character(len=*), intent(in) :: path

    type(nx_orbspace_t) :: orb

    integer :: u, io, id
    integer :: nbas, nelec, ncore, ndisc
    character(len=MAX_STR_SIZE) :: infile
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    nbas = 0
    nelec = -1
    ncore = 0
    ndisc = 0

    ! First, find the number of inactive orbitals
    orb%ninact = self%get_ninact(path)

    infile = path//'/WORK/mcscfls'
    if (self%ci_type == 0) then
       infile = path//'/LISTINGS/mcscfls.sp'
    end if

    nelec = -1
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'Total number of basis functions:')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split( size(split) ), *) nbas
       end if

       id = index(buf, 'Total number of electrons:')
       if ((id /= 0) .and. (nelec == -1)) then
          call split_blanks(buf, split)
          read(split( size(split) ), *) nelec
       end if

       id = index(buf, 'number of frozen core orbitals')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split( size(split) ), *) ncore
       end if

       id = index(buf, 'number of frozen virtual orbitals')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split( size(split) ), *) ndisc
       end if
    end do
    close(u)

    if (self%ci_type == 0) then
       ncore = orb%ninact
       nelec = nelec - 2 * orb%ninact
       ndisc = 0
    end if

    orb%nbas = nbas
    orb%nfrozen = ncore
    orb%nelec = nelec
    orb%ndisc = ndisc
  end function col_read_orb


  function col_get_ninact(self, path) result(res)
    !! Find the number of inactive (doubly occupied) orbitals.
    !!
    !! The routine will read either the ``CIDRT`` input file (for MR-CI
    !! computation) or the ``MCDRT`` output file (MCSCF) to find the number
    !! of inactive orbitals.  This information will be useful for running
    !! ``cioverlap``.  This routine is intended to be run once, just after
    !! the first run of Columbus.
    class(nx_columbus_t), intent(in) :: self
    !! ``nx_qm`` object where ``ninact`` will be set.
    character(len=*), intent(in) :: path

    integer :: res

    character(len=:), allocatable :: infile

    ! 1. Find the number of inactive orbitals
    if (self%ci_type == 0) then
       infile = path//'/LISTINGS/mcdrtls.drt1.sp'
       res = col_read_ninact_from_mcdrtls(infile)
    else
       ! 0. Find the file to read. By default, use cidrtin
       infile = col_find_file_for_ninact_citype1(path)
       if (infile == 'NOT_FOUND') then
          call nx_log%log(LOG_ERROR, &
               & 'COLUMBUS: impossible to read either cidrtin or cidrtmsin'//&
               & ', while ciudg or ciudgav is used !!'&
               &)

       end if
       res = col_read_ninact_for_citype1(infile)
    end if
  end function col_get_ninact


  function col_read_ninact_from_mcdrtls(infile) result(res)
    character(len=*), intent(in) :: infile

    integer :: res

    integer :: u, io, id1
    character(len=256) :: buf
    character(len=256), allocatable :: split(:)

    open(newunit=u, file=infile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id1 = index(buf, 'number of doubly-occupied orbitals:')
       if (id1 /= 0) then
          call split_blanks(buf, split)
          read(split( size(split) ), *) res
       end if
    end do
    close(u)
  end function col_read_ninact_from_mcdrtls


  function col_find_file_for_ninact_citype1(directory) result(res)
    character(len=*), intent(in) :: directory

    character(len=:), allocatable :: res

    logical :: ext
    character(len=128) :: test_file

    test_file = directory//'/cidrtin'
    inquire(file=test_file, exist=ext)

    if (.not. ext) then
       ! If not found, us cidrtmsin
       test_file = directory//'/cidrtmsin'
       inquire(file=test_file, exist=ext)

       if (.not. ext) then
          ! If not found, use cidrtmsin
          test_file = directory//'/mcdrtin.1'
          inquire(file=test_file, exist=ext)

          if (.not. ext) then
             test_file = 'NOT_FOUND'
          end if
       end if
    end if
    res = trim(test_file)
  end function col_find_file_for_ninact_citype1


  function col_read_ninact_for_citype1(infile) result(res)
    !! Find the number of inactive orbitals.
    !!
    !! This number is read from the ``step masks`` parameter from either ``cidrtin``,
    !! ``cidrtmsin`` or ``mcdrtin.1`` files.  The block read looks like:
    !!
    !!     1000 1000 1000 1000 1111 1111 1111
    !!     1000 1000 1111 1111 1111 1111 1111 / step masks
    !!
    !! So we need to load the file until ``step masks``, then extract upward from this
    !! initial load only the lines corresponding to this mask. We take all lines from
    !! ``/ step masks`` until we find another ``/`` sign.
    !!
    !! Inactive orbitals are indicated by the ``1000`` label.
    character(len=*), intent(in) :: infile
    !! File to parse.

    integer :: res

    integer :: u, id1, id2, id3, io, i, j, istep
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: mainbuf(:)
    character(len=MAX_STR_SIZE), allocatable :: mask(:), temp(:)
    ! The file should be smaller than 512 lines...

    res = 0

    ! First we load the file up to the line 'step masks' into ``mainbuf``
    i = 1
    allocate(mainbuf(512))
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       mainbuf(i) = buf

       id1 = index(buf, 'step masks r')
       id3 = index(buf, 'step masksr')
       id2 = index(buf, 'step masks')

       if ((id2 /= 0) .and. (id1 == 0) .and. (id3 == 0)) then
          istep = i
          exit
       end if

       i = i + 1
    end do
    close(u)

    ! 2. Now keep only the lines with the relevant masks
    allocate(mask(istep))
    mask(:) = ''
    mask(istep) = mainbuf(istep)

    do i=1, istep+1
       j = istep - i
       id1 = index(mainbuf(j), '/')
       if (id1 == 0) then
          mask(j) = mainbuf(j)
       else
          exit
       end if
    end do

    ! 3. Among the lines kept, find the '1000'
    do i=1, size(mask)
       if (mask(i) /= '') then
          ! Count the number of blanks (i.e. the number of elements in the line)
          call split_blanks(mask(i), temp)

          do j=1, size(temp)
             id1 = index(temp(j), '1000')
             if (id1 /= 0) then
                res = res + 1
             end if
          end do

       end if
    end do
  end function col_read_ninact_for_citype1
  
  
  subroutine col_read_mcscf_energies(mcscffile, repot, niter, converged)
    !! Read the MCSCF energies from a Columbus computation.
    !!
    !! The routine parses the file ``mscsffile``, and populates the ``repot`` array with
    !! the obtained MCSCF energies.  It also returns the number of iterations as
    !! ``niter``, and reports if the computation has converged or not (``converged = 1 or
    !! 0`` respectively).
    character(len=*), intent(in) :: mcscffile
    !! Output of the MCSCF job.
    real(dp), dimension(:), intent(out) :: repot
    !! MCSCF potential energies.
    integer, intent(out) :: niter
    !! Number of iterations done.
    integer, intent(out) :: converged
    !! If 1, the job has converged. Else, no convergence.

    integer :: u, io, id
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(8) :: split

    converged = 0
    istate = 1

    open(newunit=u, file=mcscffile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'final mcscf')
       if (id /= 0) then
          read(u, *) split
          read(split(1), *) niter
          id = index(split(8), 'converged')
          if (id /= 0) converged = 1
       end if

       id = index(buf, 'total energy=')
       if (id /= 0) then
          read(buf(id+13:id+13+18), '(F19.9)') repot(istate)
          ! write(*, *) trim(buf)
          istate = istate + 1

          ! Exit if we read enough energies already
          if (istate > size(repot)) exit
       end if
    end do
    close(u)
  end subroutine col_read_mcscf_energies


  subroutine col_read_mcscf_contributions(mcscffile, qminfo)
    !! Read the contributions to the MCSCF wavefunction.
    !!
    !! The routine parses the file ``mcscffile``, and populates ``qminfo%datread`` with
    !! the contributions to the wavefunction.
    character(len=*), intent(in) :: mcscffile
    !! Output of the MCSCF job.
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    integer :: u, io, id, i
    character(len=MAX_STR_SIZE) :: buf

    call qminfo%append_data('Dominant contributions to the MCSCF wavefunction', unique=.true.)
    open(newunit=u, file=mcscffile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'csf       coeff       coeff')
       if (id /= 0) then
          call qminfo%append_data(buf)
          do i=1, 4
             read(u, '(a)') buf
             call qminfo%append_data(buf)
          end do
       end if
    end do
    call qminfo%append_data('')
    close(u)
  end subroutine col_read_mcscf_contributions


  subroutine col_read_mrci_energies(mrcifile, repot, niter, converged)
    !! Read MRCI energies from Columbus output file.
    !!
    !! The routine parses ``mrcifile`` and populates the ``repot`` array with the
    !! electronic MRCI energies found.  It also reports the total number of iteration in
    !! the computation as ``niter``, and if the computation has converged or not
    !! (``converged = 1 or 0`` respectively).
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    real(dp), dimension(:), intent(out) :: repot
    !! MCSCF potential energies.
    integer, intent(out) :: niter
    !! Number of iterations done.
    integer, intent(out) :: converged
    !! If 1, the job has converged. Else, no convergence.

    integer :: u, io, id
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(7) :: split

    converged = 0
    istate = 1

    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'convergence criteria satisfied')
       if (id /= 0) then
          read(buf, *) split
          read(split(6), *) niter
          converged = 1
       end if

       id = index(buf, 'convergence information')
       if (id /= 0) then

          do istate=1, size(repot)
             read(u, *) split
             read(split(5), *) repot(istate)
          end do
       end if
    end do
    close(u)
  end subroutine col_read_mrci_energies


  subroutine col_read_mrci_contributions(mrcifile, qminfo)
    !! Read the contributions to the MRCI wavefunction.
    !!
    !! The routine parses the file ``mrcifile``, and populates ``qminfo%datread`` with
    !! the contributions to the wavefunction.
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    integer :: u, io, id, i
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=1024) :: msg

    istate = 1
    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'indcsf')
       if (id /= 0) then
          write(msg, '(a,i4)') 'Dominant contributions for state ', istate
          call qminfo%append_data(msg)
          call qminfo%append_data(buf)

          do i=1, 4
             read(u, '(a)') buf
             call qminfo%append_data(buf)
          end do
          call qminfo%append_data('')
          istate = istate + 1
       end if
    end do
    close(u)
  end subroutine col_read_mrci_contributions


  subroutine col_read_mrci_ciudg(mrcifile, refwgt, stat)
    !! Read an MRCI result file.
    !!
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    real(dp), intent(in) :: refwgt
    !! Reference weight: if the weight found is smaller than this
    !! reference a warning is issued
    type(nx_status_t), intent(inout) :: stat

    integer :: u, io, id
    integer :: istate
    real(dp) :: weight
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(7) :: split
    character(len=1), parameter :: nl = NEW_LINE('c')

    istate = 1
    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'relaxed')
       if (id /= 0) then
          read(buf, *) split
          read(split(7), *) weight
          if (weight < refwgt) then
             buf = &
                  & 'LOW WEIGHT OF REFERENCE WAVE FUNCTION for state '//to_str(istate)//&
                  & ': cnot**2 = '//to_str(weight, fmt='(F20.12)')
             call stat%append(NX_WARNING, trim(buf), &
                  & mod=MODNAME, func='col_read_mrci_ciudg')
          end if
       end if
    end do
    close(u)
  end subroutine col_read_mrci_ciudg


  subroutine col_read_gradients(gradfile, rgrad, state)
    !! Read Columbus gradient files.
    !!
    !! In Columbus gradient files, the first three lines have to be
    !! skipped.
    character(len=*), intent(in) :: gradfile
    real(dp), dimension(:, :, :), intent(inout) :: rgrad
    integer, intent(in):: state

    integer :: u, i, j, str_len
    character(len=MAX_STR_SIZE) :: line

    str_len = len_trim(gradfile)
    open(newunit=u, file=gradfile, action='read')
    
    if ( gradfile(str_len - 3:str_len) == '.all' ) then
      do i=1, 3
         read(u, '(a)') line
      end do
    end if

    do i=1, size(rgrad, 3)
       read(u, *) (rgrad(state, j, i), j=1, 3)
    end do
    close(u)

  end subroutine col_read_gradients


  subroutine col_read_nad(nadfile, rnad, coupl)
    !! Read Columbus NAD files.
    !!
    character(len=*), intent(in) :: nadfile
    real(dp), dimension(:, :, :), intent(inout) :: rnad
    integer, intent(in):: coupl

    integer :: u, i, j

    open(newunit=u, file=nadfile, action='read')
    do i=1, size(rnad, 3)
       read(u, *) (rnad(coupl, j, i), j=1, 3)
    end do
    close(u)
  end subroutine col_read_nad


  subroutine col_read_osc_str(oscfile, components, osc_str)
    !! Read oscillator strength and transition components.
    !!
    character(len=*), intent(in) :: oscfile
    !! File to read.
    real(dp), dimension(3), intent(out) :: components
    !! x, y and z components of the transition moment.
    real(dp), intent(out) :: osc_str
    !! OScillator strength

    integer :: u, id, io, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(5) :: split
    character(len=MAX_STR_SIZE), dimension(4) :: split2

    open(newunit=u, file=oscfile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'Transition moment components')
       if (id /= 0) then
          do i=1, 3
             read(u, '(a)') buf
          end do

          read(u, *) split
          ! read(buf, *) split
          do i=1, 3
             read(split(i+1), *) components(i)
          end do

       end if

       id = index(buf, 'Oscillator strength')
       if (id /=0) then
          read(buf, *) split2
          read(split2(4), *) osc_str
       end if
    end do

    close(u)

  end subroutine col_read_osc_str


  pure subroutine col_complete_setup(self)
    class(nx_columbus_t), intent(inout) :: self

    if (self%method() == 'mrci') then
       self%grad_lvl = 2
    else
       self%grad_lvl = 1
    end if
  end subroutine col_complete_setup
  
end module mod_columbus_t
