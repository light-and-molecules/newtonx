! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_exc_gaussian_t
  use mod_async, only: run_async
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ang, au2ev, proton
  use mod_data_containers, only: &
       & mat2_container_t, mat3_container_t, &
       & geom_to_container
  use mod_exash_utils, only: nx_exash_t
  use mod_gaussian_t, only: gau_read_orb
  use mod_interface, only: copy, rm
  use mod_kinds, only: dp
  use mod_logger, only: &
       & print_conf_ele, call_external
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_with_alloc
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: &
       & to_str, split_blanks
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_exc_gaussian_t

  type, extends(nx_qm_generic_t) :: nx_exc_gaussian_t
     integer :: nchrom = 1
     !! Number of chromophores.
     integer :: nproc = 1
     !! Number of cpus to split the QM/MM calculations
     integer :: nstattd = 1
     !! Number of excited states in TDDFT calculations (the same number for all
     !! chromophores).
     integer :: nproc_gau = 1
     !! Number of cpu for each gaussian job.
     character(len=:), allocatable :: functional
     !! Functional to be used, with name according to Gaussian.
     character(len=:), allocatable :: basis
     !! Name of the basis set.
     character(len=:), allocatable :: force_field
     !! Name of the force field (UFF or Amber or Dreiding).
     character(len=:), allocatable :: g16root
     !! Value of the ``$g16root`` environment variable.
     integer, allocatable :: nat_array(:)
     !! Array with the number of atom belonging to each chromophore.
     integer, allocatable :: nstat_array(:)
     !! Array with the number of states belonging to compute in each chromophore.
     real(dp), allocatable :: epot_chrom(:, :)
     !! Array containing the electronic potential energies computed for each chromophore.
   contains
     private
     procedure, public :: setup => exc_gaussian_setup
     procedure, public :: print => exc_gaussian_print
     procedure, public :: to_str => exc_gaussian_to_str
     procedure, public :: backup => exc_gaussian_backup
     procedure, public :: update => exc_gaussian_update
     procedure, public :: run => exc_gaussian_run
     procedure, public :: read_output => exc_gaussian_read_output
     procedure, public :: write_geom => exc_gaussian_write_geometry
     procedure, public :: init_overlap => exc_gaussian_init_overlap
     procedure, public :: prepare_overlap => exc_gaussian_prepare_overlap
     procedure, public :: extract_overlap => exc_gaussian_extract_overlap
     procedure, public :: ovl_post => exc_gaussian_ovl_post
     procedure, public :: ovl_run => exc_gaussian_ovl_run
     procedure, public :: get_lcao => exc_gaussian_get_lcao
     procedure, public :: cio_prepare_files => exc_gaussian_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & exc_gaussian_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => exc_gaussian_cio_get_mos_energies

     ! cioverlap
     procedure, public :: cio_prepare_chrom => exc_gaussian_cio_prepare_chrom
     procedure, public :: cio_restore_chrom => exc_gaussian_cio_restore_chrom
     procedure, public :: cio_save_chrom => exc_gaussian_cio_save_chrom
     procedure, public :: cio_process_ovl => exc_gaussian_cio_process_ovl

     procedure :: create_input => inp_exc_gau
     procedure :: get_site_energies => exc_gau_get_site_energies
  end type nx_exc_gaussian_t
  interface nx_exc_gaussian_t
     module procedure constructor
  end interface nx_exc_gaussian_t

  character(len=*), parameter :: MODNAME = 'mod_exc_gaussian_t'

  type(nx_exash_t) :: exash_main

contains

  function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & nchrom, nproc, nstattd, nproc_gau, functional, &
       & basis, force_field, g16root, &
       & nat_array, nstat_array &
       & ) result(res)
    !! Constructor for the ``nx_exc_gaussian_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    integer, intent(in), optional :: nchrom
    integer, intent(in), optional :: nproc
    integer, intent(in), optional :: nstattd
    integer, intent(in), optional :: nproc_gau
    character(len=*), intent(in), optional :: functional
    character(len=*), intent(in), optional :: basis
    character(len=*), intent(in), optional :: force_field
    character(len=*), intent(in), optional :: g16root
    integer, intent(in), optional :: nat_array(:)
    integer, intent(in), optional :: nstat_array(:)

    type(nx_exc_gaussian_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(nchrom)) res%nchrom = nchrom
    if (present(nproc)) res%nproc = nproc
    if (present(nstattd)) res%nstattd = nstattd

    allocate( res%epot_chrom( res%nchrom, res%nstattd + 1))
    res%epot_chrom = 0.0_dp
    
    if (present(nproc_gau)) res%nproc_gau = nproc_gau
    if (present(functional)) then
       res%functional = trim(functional)
    else
       res%functional = 'b3lyp'
    end if
    
    if (present(basis)) then
       res%basis = trim(basis)
    else
       res%basis = '631-G(d)'
    end if
    
    if (present(force_field)) then
       res%force_field = trim(force_field)
    else
       res%force_field = 'UFF'
    end if
    
    if (present(g16root)) then
       res%g16root = g16root
    else
       res%g16root = ''
    end if

    if (present(nat_array)) then
       allocate(res%nat_array(size(nat_array)))
       res%nat_array(:) = nat_array(:)
    else
       allocate(res%nat_array(1))
       res%nat_array(1) = 1
    end if

    if (present(nstat_array)) then
       allocate(res%nstat_array(size(nstat_array)))
       res%nstat_array(:) = nstat_array(:)
    else
       allocate(res%nstat_array(1))
       res%nstat_array(1) = 1
    end if

  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine exc_gaussian_setup(self, parser, conf, inp_path, stat)
    class(nx_exc_gaussian_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo
    character(len=MAX_STR_SIZE) :: env
    integer :: ierr

    prt_mo = -1
    call set(parser, 'exc_gaussian', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%g16root == '') then
       call get_environment_variable("g16root", env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $g16root environment is not defined', &
               & mod=modname, func='exc_gaussian_setup')
          return
       else
          self%g16root = trim(env)
       end if
    end if

    call set(parser, 'exc_gaussian', self%nchrom, 'nchrom')
    call set(parser, 'exc_gaussian', self%nproc, 'nproc')
    call set(parser, 'exc_gaussian', self%nstattd, 'nstattd')
    call set(parser, 'exc_gaussian', self%nproc_gau, 'nproc_gau')
    call set(parser, 'exc_gaussian', self%functional, 'functional')
    call set(parser, 'exc_gaussian', self%basis, 'basis')
    call set(parser, 'exc_gaussian', self%force_field, 'force_field')
    call set_alloc(parser, 'exc_inp', self%nat_array, 'nat_array')
    call set_alloc(parser, 'exc_inp', self%nstat_array, 'nstat')


    if (allocated(self%epot_chrom)) deallocate(self%epot_chrom)
    allocate(self%epot_chrom(self%nchrom, self%nstattd+1))
    self%epot_chrom = 0.0_dp

    exash_main = nx_exash_t( self%nchrom, parser, stat )
  end subroutine exc_gaussian_setup

  subroutine exc_gaussian_print(self, out)
    class(nx_exc_gaussian_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%nchrom, 'nchrom', unit=output)
    call print_conf_ele(self%nproc, 'nproc', unit=output)
    call print_conf_ele(self%nstattd, 'nstattd', unit=output)
    call print_conf_ele(self%nproc_gau, 'nproc_gau', unit=output)
    call print_conf_ele(self%functional, 'functional', unit=output)
    call print_conf_ele(self%basis, 'basis', unit=output)
    call print_conf_ele(self%force_field, 'force_field', unit=output)
  end subroutine exc_gaussian_print

  function exc_gaussian_to_str(self) result(res)
    class(nx_exc_gaussian_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&exc_gaussian'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' nchrom = '//to_str(self%nchrom)//nl
    res = res//' nproc = '//to_str(self%nproc)//nl
    res = res//' nstattd = '//to_str(self%nstattd)//nl
    res = res//' nproc_gau = '//to_str(self%nproc_gau)//nl
    res = res//' functional = '//trim(self%functional)//nl
    res = res//' basis = '//trim(self%basis)//nl
    res = res//' force_field = '//trim(self%force_field)//nl
    res = res//'/'//nl//nl

    res = res//exash_main%to_str()
  end function exc_gaussian_to_str

  subroutine exc_gaussian_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine exc_gaussian_backup

  subroutine exc_gaussian_update(self, conf, traj, path, stat)
    class(nx_exc_gaussian_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    call self%create_input(exash_main, path, conf%nat, traj%step)
  end subroutine exc_gaussian_update

  subroutine exc_gaussian_run(self, stat)
    class(nx_exc_gaussian_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: env, cmdmsg
    character(len=MAX_STR_SIZE) :: job_list(self%nchrom)
    integer :: i, ierr

    do i=1, size(job_list)
       job_list(i) = self%g16root//'/g16/g16 exc_gau'//to_str(i)
    end do

    ierr = 0
    call run_async(job_list, self%nproc, ierr)
    if (ierr /= 0) then
       call stat%append(NX_ERROR, &
            & 'Problem when running Gaussian (excitons)'//&
            & '. Please take a look at exc_gaussian.job/ log files.', & 
            & mod=MODNAME, func='exc_gaussian_run' &
            & )
    end if

    call call_external(&
         & self%g16root//'/g16/g16 exc_gau_MM.com', ierr, &
         & outfile='exc_gau_MM.log', cmdmsg=cmdmsg &
         & )
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running g16 (MM job)'//trim(cmdmsg)//&
            & '. Please take a look at gaussian.job/gaussian.log', & 
            & mod=MODNAME, func='gaussian_run'&
            & )
    end if
  end subroutine exc_gaussian_run

  function exc_gaussian_read_output(self, conf, traj, path, stat) result(info)
    class(nx_exc_gaussian_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info
    real(dp), allocatable :: grad_mm(:, :), array_gs(:), omega(:), &
         & dipoles(:, :, :), osc_str(:)
    real(dp) :: enMM
    integer :: i, nstat_tot, j, k, st
    character(len=64), allocatable :: basename(:)
    character(len=MAX_STR_SIZE) :: buf
    integer :: ierr, u, ind_start

    type(mat2_container_t), allocatable :: esp(:), coord(:), ovl(:)
    type(mat3_container_t), allocatable :: grad_qm(:)

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex, &
         & use_exciton=.true.&
         & )

    allocate(info%epot_chrom( self%nchrom, self%nstattd + 1))

    ! Create list of filenames
    allocate(basename(self%nchrom))
    do concurrent (i=1:self%nchrom)
       basename(i) = trim(path)//'/exc_gau'//to_str(i)
    end do

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       do i=1, self%nchrom
          info%orb = gau_read_orb(trim(basename(i))//'.log')
          open(newunit=u, file=trim(basename(i))//'.orbinf', action='write') 
          write(u,*) info%orb%nfrozen
          write(u,*) info%orb%nocc
          write(u,*) info%orb%nocc_b
          write(u,*) info%orb%nvirt
          write(u,*) info%orb%nvirt_b
          write(u,*) info%orb%nelec
          close(u)
       end do
    end if

    ! ===========
    ! COORDINATES
    ! ===========
    allocate(coord(self%nchrom))
    coord = geom_to_container(trim(path)//'/geom', self%nat_array)

    ! ========
    ! ENERGIES
    ! ========
    ! Extract site energies
    call self%get_site_energies(basename, omega, array_gs)
    k = 1
    do i=1, self%nchrom
       info%epot_chrom(i, 1) = array_gs(i)
       info%epot_chrom(i, 2:self%nstattd+1) = &
            & omega(k:k+self%nstat_array(i)-1) + array_gs(i)
       k = k+self%nstat_array(i)
    end do

    ! Read MM energies
    open(newunit=u, file=trim(path)//"/exc_gau_MM.log", status="old", form="formatted")
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit
       if (index(buf, 'Energy=') /= 0) then
          read(buf, *) buf, enMM
       end if
    end do
    close(u)
    
    ! =========
    ! GRADIENTS
    ! =========
    ! MM gradient
    ! allocate(grad_mm(3, conf%nat))
    grad_mm = exc_gau_read_mm_grad(trim(path)//'/exc_gau_MM.log', conf%nat)
    ! print *, 'GRAD MM'
    ! do j=1, size(grad_mm, 2)
    !    print '(20F20.12)', (grad_mm(k, j), k=1, size(grad_mm, 1))
    ! end do

    ! QM gradients
    allocate(grad_qm( self%nchrom ))
    do i=1, self%nchrom
       allocate( grad_qm(i)%m(3, conf%nat, self%nstat_array(i)+1) )
       grad_qm(i)%m = exc_gau_extract_gradient(&
            & trim(basename(i))//'.log', &
            & conf%nat, self%nstat_array(i)+1 &
            & )

       ! do st=1, self%nstat_array(i) + 1
       !    print *, 'GRAD QM ', i, ' STATE ', st
       !    do j=1, size(grad_qm(i)%m, 2)
       !       print '(20F20.12)', (grad_qm(i)%m(k, j, st), k=1, size(grad_qm(i)%m, 1))
       !    end do
       ! end do
    end do
     
    ! ===========
    ! ESP CHARGES
    ! ===========
    ind_start = 0
    allocate(esp(self%nchrom))
    do i=1, self%nchrom
       esp(i)%m = gau_get_electro_charges( &
            & trim(basename(i))//'.log', self%nat_array(i), self%nstat_array(i), &
            & ind_start=ind_start)
       ind_start = ind_start + self%nat_array(i)

       ! esp(i)%m(1, :) = esp(i)%m(2, :)
       ! esp(i)%m(2, :) = 0.0_dp
    end do

    ! =================
    ! EXASH COMPUTATION
    ! =================
    
    ! Energies and gradients
    call exash_main%compute_epot_grad( &
         & traj%nstatdyn, omega, array_gs, enMM, coord, esp, grad_qm, &
         & grad_mm, stat &
         & )
    info%diab_ham = exash_main%diab_ham()
    info%diab_en = exash_main%diab_en()
    info%repot = exash_main%adiab_en()
    info%rgrad(traj%nstatdyn, :, :) = exash_main%adiab_grad()
    info%diab_pop = exash_main%diab_pop()

    ! Transition dipole and oscillator strengths
    if (.not. exash_main%gs())  then
       if (exash_main%dip()) then
          info%has_osc_str = .true.

          do i=1, self%nchrom
             dipoles = gaussian_get_dipoles(trim(basename(i))//'.log', self%nstat_array(i)+1)
          end do

          nstat_tot = sum(self%nstat_array(:))
          call exash_main%compute_dipoles(dipoles)
          info%trdip = exash_main%trdip()

          info%osc_str(1:nstat_tot+1) = exash_main%osc_str()
       end if
    end if
  end function exc_gaussian_read_output


  subroutine exc_gaussian_write_geometry(self, traj, path, print_merged)
    class(nx_exc_gaussian_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: u, i, j
    real(dp) :: mass

    ! Now write the updated 'geom' file
    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)

       ! Columbus limit in mass format
       mass = traj%masses(i) / proton
       if (mass >= 100.0_dp) then
          mass = 99.9_dp
       end if

       write(u, '(1x,a2,2x,f5.1,4f14.8)') &
            & traj%atoms(i), traj%Z(i), (traj%geom(j, i), j=1, 3), &
            & mass
    end do
    close(u)
  end subroutine exc_gaussian_write_geometry

  subroutine exc_gaussian_init_overlap(self, path_to_qm, stat)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_gaussian_init_overlap

  subroutine exc_gaussian_prepare_overlap(self, path_to_qm, stat)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: i, ierr
    character(len=256) :: gaussian_jobs( self%nchrom )

    do i=1, self%nchrom
       gaussian_jobs(i) = trim(path_to_qm)//'/exc_gau'//to_str(i)//'_ovl.com'
    end do

    ierr = copy(gaussian_jobs, 'overlap/')
  end subroutine exc_gaussian_prepare_overlap

  function exc_gaussian_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_exc_gaussian_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)

    integer :: u, id, ierr, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: rem, nlines, nele, ele

    ! We first need to 

    open(newunit=u, file='overlap/S_matrix', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nlines = int(nele / 5)
          rem = mod(nele, nlines)

          if (allocated(split)) deallocate(split)
          allocate(split(5))

          ele = 1
          do i=1, nlines
             read(u, *) split
             do j=1, 5
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end do

          if (rem /= 0) then
             deallocate(split)
             allocate(split(rem))
             read(u, *) split
             do j=1, rem
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end if

       end if

    end do
    close(u)
  end function exc_gaussian_extract_overlap

  function exc_gaussian_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)

    integer :: u, ierr, id, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer :: nele, rem, nbLines, ele

    real(dp), dimension(:), allocatable :: temp

    allocate(temp(nao*nao))
    ele = 1

    ! First read the matrix as a vector
    open(newunit=u, file=trim(dir_path)//'/MO_coefs_a', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nbLines = int(nele / 5)
          rem = mod(nele, nbLines)

          do i=1, nbLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                read(split(j), *) temp(ele)
                ele = ele + 1
             end do

          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) temp(ele)
                ele = ele + 1
             end do
          end if

       end if
    end do
    close(u)

    do i=1, nao
       ele = i
       do j=1, nao
          lcao(i, j) = temp(ele)
          ele = ele + nao
       end do
    end do
  end function exc_gaussian_get_lcao

  subroutine exc_gaussian_ovl_run(self, stat)
    class(nx_exc_gaussian_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: i, ierr
    character(len=256) :: gaussian_jobs( self%nchrom )
    character(len=:), allocatable :: command

    do i=1, self%nchrom
       command = self%g16root//'/g16/g16 exc_gau'//to_str(i)//'_ovl'
       call call_external(command, ierr, outfile='exc_gau'//to_str(i)//'_ovl.log')
    end do
  end subroutine exc_gaussian_ovl_run

  subroutine exc_gaussian_ovl_post(self, stat)
    class(nx_exc_gaussian_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: i, ierr
    character(len=:), allocatable :: ovlf, smatf, command

    do i=1, self%nchrom
       ovlf = 'exc_gau'//to_str(i)//'_ovl.rwf'
       smatf = 'S_matrix'//to_str(i)
       command = self%g16root//'/g16/rwfdump overlap/'//ovlf//&
            &' overlap/'//smatf//' 514R'
       call execute_command_line(command, exitstat=ierr)
    end do
  end subroutine exc_gaussian_ovl_post

  subroutine exc_gaussian_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: cmdmsg
    integer :: ierr
    character(len=:), allocatable :: rwfdump
    character(len=*), parameter :: funcname = 'gaussian_cio_prepare_files'

    rwfdump = self%g16root//'/g16/rwfdump '//trim(path_to_qm)//'/gaussian.rwf'

    ! Extract the molecular orbitals and single amplitudes now
    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/eigenvalues 522R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf eigenvalues 522R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/MO_coefs_a 524R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf MO_coefs_a 524R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if

    call execute_command_line(&
         & rwfdump//' '//trim(path_to_qm)//'/TDDFT 635R', &
         & exitstat=ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running "rwfdump gaussian.rwf TDDFT 635R"'//trim(cmdmsg), &
            & mod=MODNAME, func=funcname&
            & )
    end if
  end subroutine exc_gaussian_cio_prepare_files


  subroutine exc_gaussian_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)

    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: u, ierr, i, j, k, l, m, id, ic
    integer :: mit, leer
    integer :: nLines, rem
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b, mseek
    real(dp) :: cNorm
    real(dp), dimension(:), allocatable :: coef

    nvirt_a = orb%nvirt
    nvirt_b = orb%nvirt_b
    nocc_b = orb%nocc_b
    nocc_a = orb%nocc - orb%nfrozen
    mseek = orb%mseek

    mit = mseek * (nocc_b*nvirt_b + nocc_a*nvirt_a)
    leer = mseek + 2*mit + 12 ! Number of elements
    nLines = int(leer / 5)
    rem = mod(leer, nLines)
    allocate( coef(leer) )

    ! Read all coefficients into ``coeff`` array. The elements up to ``mit``
    ! correspond to | X + Y >, an thos from ``mit+1`` to ``2*mit`` correspond
    ! to | X - Y >.
    ic = 1
    open(newunit=u, file=trim(path_to_qm)//'/TDDFT', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          do i=1, nLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                if ((ic > 12) .and. (ic <= leer - mseek )) then
                   read(split(j), *) coef(ic-12)
                end if
                ic = ic + 1
             end do
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, rem
                read(split(j), *) coef(ic-12)
                ic = ic + 1
             end do
          end if

       end if
    end do
    close(u)

    ! In ``coef`` we have the list of CI coefficient, ordered as nstate lists
    ! of amplitudes, each composed of two lists : amplitudes for alpha and
    ! beta electrons. Each of these lists is in turn composed of the
    ! amplitudes ordered as the lines of the Cia matrix.
    ! Normalization
    cNorm = sqrt(2.0_dp)
    if (present(tib)) then
       cNorm = 1
    end if
    ! open(newunit=u, file='coef_norm', action='write')
    ! write(u, *) 'cnorm = ', cnorm
     !if (self%coptda == 0) then
     !   ! Use | X > coefficients
     !   coef(:) = cNorm * coef(:)
     !else if (self%coptda == 1) then
       ! Use | X + Y > coeffieicnts
       do i=1, mit
          coef(i) = cNorm*( coef(i) + coef(i+mit) ) / 2
       end do
    ! end if
    close(u)

    ! Now we reorder the single list into the array tia (and tia_b if required).
    l = 1
    m = 0
    do i=1, size(tia, 3)
       ! Populate the alpha matrix for each state
       do j=1, nocc_a
          do k=1, nvirt_a
             tia(k, j, i) = coef(l+m)
             l = l+1
          end do
       end do

       if (present(tib)) then
          do j=1, nocc_b
             do k=1, nvirt_b
                tib(k, j, i) = coef(l + m)
                m = m+1
             end do
          end do
       else
          m = m + nocc_b*nvirt_b
       end if
    end do
  end subroutine exc_gaussian_cio_get_singles_amplitudes

  subroutine exc_gaussian_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)

    integer :: u, id, mo, ierr, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split
    integer :: nFields, nLines, rem

    mo = 1
    open(newunit=u, file=trim(path_to_qm)//'/eigenvalues', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)

          read(split(6), *) nFields

          ! We consider only the RHF case. In 'eigenvalues' we have both
          ! alpha and beta MOs energies. We only need one of those in RHF.
          ! if (self%type == 0) then
             nFields = nFields / 2
          ! end if

          nLines = int(nFields / 5)
          rem = mod(nFields, nLines)

          do i=1, nLines
             read(u, *) mos(mo:mo+4)
             mo = mo + 5
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) mos(mo)
                mo = mo + 1
             end do
          end if

       end if

    end do
    close(u)
  end subroutine exc_gaussian_cio_get_mos_energies


  subroutine exc_gaussian_cio_process_ovl(self, path)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path

    type(mat2_container_t), allocatable :: ovl(:)
    integer :: i, j, k

    allocate(ovl(self%nchrom))
    do i=1, self%nchrom
       ovl(i)%m = exc_gaussian_get_ciovl_from_file( &
            & 'cio_chrom_'//to_str(i)//'/cioverlap/cioverlap.out' &
            & )
       print *, 'OVL ', i
       do j=1, size(ovl(i)%m, 1)
          print '(20F20.12)', (ovl(i)%m(j, k), k=1, size(ovl(i)%m, 2))
       end do
    end do

    call exash_main%write_ovl(trim(path)//'/cioverlap.out', ovl)
  end subroutine exc_gaussian_cio_process_ovl


  subroutine exc_gaussian_cio_prepare_chrom(self, path_to_qm, index, ierr)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    integer, intent(in) :: index
    integer, intent(inout) :: ierr

    character(len=256) :: copy_from(3), copy_to(3)

    copy_from = [ character(len=256) :: &
         & trim(path_to_qm)//'/exc_gau'//to_str(index)//'.rwf', &
         & trim(path_to_qm)//'/exc_gau'//to_str(index)//'.log', &
         & trim(path_to_qm)//'/exc_gau'//to_str(index)//'.orbinf' &
         & ]

    copy_to = [ character(len=256) :: &
         & trim(path_to_qm)//'/gaussian.rwf', &
         & trim(path_to_qm)//'/gaussian.log', &
         & './cio_chrom_'//to_str(index)//'/' &
         & ]

    ierr = copy(copy_from, copy_to)
  end subroutine exc_gaussian_cio_prepare_chrom
  

  subroutine exc_gaussian_cio_restore_chrom(self, path_to_qm, index, ierr)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    integer, intent(in) :: index
    integer, intent(inout) :: ierr

    character(len=256) :: copy_from(5), copy_to(5)

    copy_from = [ character(len=256) :: &
         & './cio_chrom_'//to_str(index)//'/cioverlap/', &
         & './cio_chrom_'//to_str(index)//'/MO_coefs_a', &
         & './cio_chrom_'//to_str(index)//'/exc_gau'//to_str(index)//'.orbinf', &
         & './overlap/S_matrix'//to_str(index), &
         & trim(path_to_qm)//'/exc_gau'//to_str(index)//'.rwf' &
         & ]

    copy_to = [ character(len=256) :: &
         & './cioverlap.old', &
         & trim(path_to_qm)//'.old/MO_coefs_a', &
         & trim(path_to_qm)//'/exc_gau'//to_str(index)//'.orbinf', &
         & './overlap/S_matrix', &
         & trim(path_to_qm)//'/gaussian.rwf' &
         & ]

    ierr = copy(copy_from, copy_to)
  end subroutine exc_gaussian_cio_restore_chrom


  subroutine exc_gaussian_cio_save_chrom(self, path_to_qm, index, ierr)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    integer, intent(in) :: index
    integer, intent(inout) :: ierr

    character(len=256) :: to_delete(5), backup(2)

    to_delete = [ character(len=256) :: &
         & trim(path_to_qm)//".old/MO_coefs_a.old", &
         & trim(path_to_qm)//"/TDDFT", &
         & trim(path_to_qm)//"/eigenvalues", &
         & './cis_slatergen.input', &
         & './cio_chrom_'//to_str(index)//'/cioverlap/' &
         & ]
    ierr = rm(to_delete)
    backup(1) = trim(path_to_qm)//'/MO_coefs_a'
    backup(2) = 'cioverlap/'
    ierr = copy(backup, 'cio_chrom_'//to_str(index)//'/')
  end subroutine exc_gaussian_cio_save_chrom

  

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  subroutine inp_exc_gau(self, exash, path_to_inp, nat, step)
    !! It generates the Gaussian inputs
    class(nx_exc_gaussian_t), intent(in) :: self
    type(nx_exash_t), intent(in) :: exash
    character(len=*), intent(in) :: path_to_inp
    !
    integer :: nchrom
    ! Numb of chromophores
    integer :: nat
    ! Total number of atoms
    integer :: step
    ! Current step
    integer :: nstattd
    ! Number of excited states in TDDFT calculations
    integer :: nproc_gau
    ! Number of cpu for gaussian calculations (double paralalization)
    character(len=:), allocatable :: functional
    ! The functional, for example, B3LYP...
    character(len=:), allocatable :: basis
    ! Basis set
    character(len=:), allocatable :: force_field
    ! Force field (UFF or Amber or Dreiding)

    !! Other variables
    integer :: i, j, k, l
    integer :: u
    integer :: ibuf
    integer :: nlink
    integer :: npar
    integer :: nl
    integer :: io
    integer :: link_atoms
    integer :: par
    integer :: ierr

    integer, allocatable, dimension(:,:) :: atom_group
    integer, allocatable, dimension(:,:) :: at_position

    ! character(len=150) :: name_ind
    character(len=:), allocatable :: first_part
    character(len=:), allocatable :: firstline
    character(len=150) :: cbuff
    character(len=150) :: charge_mult
    character(len=150) :: name_ind2
    character(len=150) :: name_ind3
    character(len=150) :: coup_file
    character(len=:), allocatable :: secondline
    character(len=:), allocatable :: line3
    character(len=:), allocatable :: file_chk
    character(len=150) :: file_esp
    character(len=:), allocatable :: file_nproc
    character(len=:), allocatable :: nfile
    character(len=150) :: card
    character(len=:), allocatable :: file_rwf

    character(len=120), allocatable, dimension(:) :: namefiles_qmmm
    character(len=120), allocatable, dimension(:) :: label
    character(len=120), allocatable, dimension(:) :: string_par
    character(len=120), allocatable, dimension(:) :: atom_label

    character(len=256), allocatable :: basenames(:), inputs(:)

    real(dp) :: rbuff

    real(dp), allocatable, dimension(:,:) :: coord
    real(dp), allocatable, dimension(:,:) :: coord_old

    logical :: print1, print2, print3
    logical :: conferma
    !!
    !
    nchrom = self%nchrom
    nstattd = self%nstattd
    nproc_gau = self%nproc_gau
    functional = self%functional
    basis = self%basis
    force_field = self%force_field
    !

    ! ! Default values /exc_inp/:
    ! tresp = 1    ! Off dig terms of the H will be performed using TMA aproximation.
    ! gs = 0       ! Compute only the ground state inf.
    ! dip = 0      ! Compute the tra. dip. moments. if .ne. 1
    ! verbose = 0  ! verbose = 0 meas that the exash outputs will not be generated
    ! coup_file = ""

    ! allocate ( nat_array(nchrom), nstat(nchrom) )
    ! nat_array(:) = 0
    ! nstat(:) = 0
    ! call exc_read_input(nx_qm, parser)

    !! Array which contains the name of the qmmm calculations for all the
    ! chromophores
    ! allocate( namefiles_qmmm(nchrom) )
    ! namefiles_qmmm(:) = ""
    allocate(basenames(nchrom))
    allocate(inputs(nchrom))

    do i=1, nchrom
       basenames(i) = 'exc_gau'//to_str(i)
       inputs(i) = trim(path_to_inp)//'/'//trim(basenames(i))
    end do

    !! Read the current geometry
    allocate( coord(nat,3) )
    coord(:,:) = 0._dp
    open(newunit=u, file=trim(path_to_inp)//'/geom', action='read')
    do i = 1, nat
       read(u,*) cbuff, rbuff, (coord(i,j), j = 1,3), rbuff
    end do
    close(u)
    coord = coord * au2ang

    !! Read the model for gaussian coordinates
    allocate( label(nat) )
    label(:) = ""
    open(newunit=u, file=trim(path_to_inp)//'/geom_model', action='read')
    read(u,'(a)') charge_mult
    do i = 1, nat
       read(u,*) label(i)
    end do
    close(u)

    !! Checks the number of lines in the "list_exc" file
    nl = 0
    open(newunit=u, file=trim(path_to_inp)//'/list_exc', action='read')
    do
       read(u,*,iostat=io)
       if (io /= 0) exit
       nl = nl + 1
    end do
    close(u)

    ! write (name_ind, "(I10)") nstattd

    !! Read the informations from "list_exc"
    open(newunit=u, file=trim(path_to_inp)//'/list_exc', status='old', form='formatted')
    rewind u
    read(u,*) nchrom
    allocate( atom_group(nchrom,2) )
    do i = 1, nchrom
       read(u,*) (atom_group(i,j), j = 1, 2)
    end do
    ! Parameters
    npar = 0
    rewind u

    par = 0
    do
       read(u, '(a)', iostat=ierr) cbuff
       if (ierr /= 0) exit

       if (index(cbuff, "PARAMETERS") /= 0) then
          par = 1
          read(u,*) npar
          allocate( string_par(npar) )
          do i = 1, npar
             read(u,'(a)') string_par(i)
          end do
       end if
    end do
    close(u)

    !! Generate the inputs
    do j = 1, nchrom
       open(newunit=u, file=trim(path_to_inp)//'/list_exc', action='read')
       link_atoms = 0
       nlink = 0

       if (allocated(at_position)) deallocate(at_position)

       do
          read(u, '(a)', iostat=ierr) cbuff
          if (ierr /= 0) exit

          if (index(cbuff, 'LINK ATOMS') /= 0) then
             if (j .eq. 1) then
                read(u,*) ibuf, nlink
                if (nlink .gt. 0) then
                   link_atoms = 1
                   allocate(at_position(nlink,2))
                   at_position(:,:) = 0
                   allocate(atom_label(nlink))
                   atom_label(:) = ""
                   do i = 1, nlink
                      read(u,*) (at_position(i,k), k = 1, 2), atom_label(i)
                   end do
                end if
             else
                do i = 1, j-1
                   read(u,*) ibuf, nlink
                   if (nlink .ne. 0) then
                      do k = 1, nlink
                         read(u,*)
                      end do
                   end if
                end do
                read(u,*) ibuf, nlink
                allocate( at_position(nlink,2), atom_label(nlink) )
                if (nlink .gt. 0) then
                   link_atoms = 1
                   do k = 1, nlink
                      read(u,*) (at_position(k,l), l = 1, 2), atom_label(k)
                   end do
                end if
             end if
          end if
       end do
       close(u)
       
       if (.not. allocated(at_position)) then
          allocate(at_position(1, 1))
          at_position(:, :) = 0
       endif

       !! Ground state
       open(newunit=u, file=trim(inputs(j))//'.com', action='write')

       first_part = '# ONIOM('
       firstline = first_part//functional//"/"&
            &//basis//" :"//force_field//")=EmbedCharge force"

       nfile = basenames(j)
       file_chk = '%chk='//trim(nfile)//'.chk'
       file_rwf = '%rwf='//trim(nfile)//'.rwf'

       ! write (name_ind2, "(I10)") nproc_gau

       file_nproc = '%nproc='//to_str(nproc_gau)
       ! file_nproc = adjustl(file_nproc)

       write(u,*) file_nproc
       write(u,*) file_chk
       write(u,*) file_rwf
       write(u,*) firstline
       write(u,*)
       write(u,*)"Ground state exc_gaussian"
       write(u,*)
       write(u,*) trim(charge_mult)
       do i = 1, nat
          print2 = .false.
          print3 = .false.
          do k = atom_group(j,1), atom_group(j,2)
             if (i .eq. k) then
                print1 = .false.
                print2 = .true.
                if (nlink .ne. 0) then
                   do l = 1, nlink
                      if ( i .eq. at_position(l,1) ) then
                         write(u,100) label(i), coord(i,1), coord(i,2), coord(i,3), "H", &
                              atom_label(l), at_position(l,2)
                         print1 = .true.
                      end if
                   end do
                end if
                if ( print1 ) then
                   continue
                else
                   write(u,101) label(i), coord(i,1), coord(i,2), coord(i,3), "H"
                end if
             end if
          end do
          if ( print2 ) then
             continue
          else
             if (nlink .ne. 0) then
                do l = 1, nlink
                   if ( i .eq. at_position(l,1) ) then
                      write(u,100) label(i), coord(i,1), coord(i,2), coord(i,3), "L", &
                           atom_label(l), at_position(l,2)
                      print3= .true.
                   end if
                end do
             end if
             if ( print3 ) then
                continue
             else
                write(u,101) label(i), coord(i,1), coord(i,2), coord(i,3), "L"
             end if
          end if
       end do

       if ( par .eq. 1 ) then
          write(u,*)
          do i = 1, npar
             write(u,*) string_par(i)
          end do
       end if

       write(u,*)

       ! Excited states for the individual chromophores
       do i = 1, self%nstat_array(j)
          write(u,'(a9)') "--Link1--"
          write(u,*) file_nproc
          write(u,*) file_chk
          write(u,*) file_rwf
          ! write (name_ind3, "(I10)") i

          ! secondline = trim(adjustl(first_part))//trim(adjustl(functional))//"/"&
          !      &//trim(adjustl(basis))//" TD=(NStates="//trim(adjustl(name_ind))//&
          !      &",root="//trim(adjustl(name_ind3))//") :"//trim(adjustl(force_field))&
          !      &//")=EmbedCharge force guess=read geom=check"

          secondline = first_part//functional//"/"&
               &//basis//" TD=(NStates="//to_str(nstattd)//&
               &",root="//to_str(i)//") :"//force_field&
               &//")=EmbedCharge force guess=read geom=check"

          write(u,*) secondline
          line3 = "IOp(6/22=-4) IOp(6/29="//to_str(i)//") pop=mk IOp(6/17=2)"
          write(u,*) line3

          write(u,*)
          write(u,*)"Excited state exc_gaussian"
          write(u,*)
          write(u,*) charge_mult
          write(u,*)
          write(u,*)
       end do

       !! Write the input just of the QM part in order to exctract the
       ! wave function informations
       firstline = "#p "//functional//"/"&
            &//basis//" TD=(NStates="//to_str(nstattd)//&
            &",root=0) nosymm IOP(3/59=14) "
       write(u,'(a9)') "--Link1--"
       write(u,*) file_nproc
       write(u,*) file_chk
       write(u,*) file_rwf
       write(u,*) firstline
       write(u,*)
       write(u,*) "exctract the wavefunction iformations"
       write(u,*)
       write(u,*) charge_mult
       do i = atom_group(j,1), atom_group(j,2)
          write(u,102) label(i), (coord(i,k), k = 1, 3)
       end do
       write(u,*)

       close(u)

       if (link_atoms .eq. 1) then
          deallocate(at_position, atom_label)
       end if

    end do

    !! Generate the MM input
    secondline = "#"//force_field//" force"
    open(newunit=u, file=trim(path_to_inp)//"/exc_gau_MM.com", status='unknown', form='formatted')
    write(u,*) secondline
    write(u,*)
    write(u,*) "MM energy and gradients exc_gaussian"
    write(u,*)
    write(u,*) charge_mult
    do i = 1, nat
       write(u,102) label(i), (coord(i,j), j = 1, 3)
    end do
    write(u,*)
    if ( par .eq. 1 ) then
       write(u,*)
       do i = 1, npar
          write(u,*) string_par(i)
       end do
    end if
    write(u,*)
    write(u,*)
    close(u)

    !! Generate the overlap inputs
    ! Read the coordinates of the previous time step
    allocate(coord_old(nat,3))
    coord_old(:,:) = 0._dp
    if (step == 0) then
       coord_old = coord
    else
       open(newunit=u, file=trim(path_to_inp)//'.old/geom', status='old', form='formatted')
       rewind u
       do i = 1, nat
          read(u,*) cbuff, rbuff, (coord_old(i,j), j = 1,3), rbuff
       end do
       close(u)
       coord_old = coord_old * au2ang
    end if

    secondline = "#p nosymm Geom=NoTest IOp(3/33=1) "//basis
    do i = 1, nchrom
       nfile = basenames(i)
       file_chk = '%chk='//trim(nfile)//'_ovl.chk'
       file_rwf = '%rwf='//trim(nfile)//'_ovl.rwf'
       open(newunit=u, file=trim(inputs(i))//"_ovl.com", action='write')
       write(u,*) '%kjob l302'
       write(u,*) file_rwf
       write(u,*) file_chk
       write(u,*) secondline
       write(u,*)
       write(u,*) "Overlap run, chromophore:", i
       write(u,*)
       write(u,*) charge_mult
       do j = atom_group(i,1), atom_group(i,2)
          write(u,102) label(j), (coord_old(j,k), k = 1, 3)
       end do
       do j = atom_group(i,1), atom_group(i,2)
          write(u,102) label(j), (coord(j,k), k = 1, 3)
       end do
       write(u,*)
       write(u,*)
       close(u)
    end do

    ! deallocate( nat_array, nstat )
    ! deallocate( namefiles_qmmm )
    deallocate( coord )
    deallocate( coord_old )
    deallocate( label )
    deallocate( atom_group )
    if (par == 1) then
       deallocate( string_par )
    end if

100 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6, 2x, a8, 2x, a6, 1x, i6)
101 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6, 2x, a8)
102 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6)

  end subroutine inp_exc_gau


  subroutine exc_gau_get_site_energies(self, basename, omega, array_gs)
    class(nx_exc_gaussian_t), intent(in) :: self
    character(len=*), intent(in) :: basename(:)
    real(dp), allocatable, intent(out) :: omega(:)
    real(dp), allocatable, intent(out) :: array_gs(:)

    integer :: i, j, k, u, ierr, chrom, nstat_tot, st_read
    character(len=MAX_STR_SIZE) :: buf
    character(len=256), allocatable :: splitted(:)
    logical :: read_gs, read_es

    nstat_tot = sum( self%nstat_array(:) )
    allocate(array_gs(self%nchrom))
    allocate(omega( nstat_tot ))
    array_gs = 0.0_dp
    omega = 0.0_dp

    j = 1 ! Indexes array_gs
    k = 1 ! Indexes omega
    MAIN_LOOP: do chrom=1, self%nchrom
       st_read = 0
       open(newunit=u, file=trim(basename(chrom))//'.log', action='read')
       read_gs = .true.
       read_es = .true.
       do
          read (u, '(a)', iostat=ierr) buf
          if (ierr /= 0) exit

          if (index(buf, 'ONIOM: extrapolated energy =') /= 0) then
             if (read_gs) then
                call split_blanks(buf, splitted)
                read(splitted(5), *) array_gs(j)
                j = j+1
                read_gs = .false.
             end if
          end if

          if (index(buf, 'Excited State ') /= 0) then
             if (read_es) then
                ! Increment the number of ES read.
                st_read = st_read + 1

                call split_blanks(buf, splitted)
                read(splitted(5), *) omega(k)
                k = k+1

                ! We have more than one job in each input, so we have to stop after the
                ! first time we read ES information.
                if (st_read == self%nstat_array(chrom)) read_es = .false.
             end if
          end if
          
       end do 
    end do MAIN_LOOP
    close(u)
    omega = omega / au2ev
  end subroutine exc_gau_get_site_energies


  function exc_gau_read_mm_grad(filename, nat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat

    real(dp), allocatable :: res(:, :)

    integer :: u, ierr, j, z, i
    character(len=MAX_STR_SIZE) :: buf

    allocate(res(3, nat))
    res = 0.0_dp

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Forces (Hartrees/Bohr)') /= 0) then
          read(u, *)
          read(u, *)
          do j=1, nat
             read(u, *) z, z, (res(i, j), i=1, 3)
          end do
       end if
    end do
    close(u)
    res = -res
  end function exc_gau_read_mm_grad


  function exc_gau_extract_gradient(filename, nat, nstat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat
    integer, intent(in) :: nstat

    real(dp), allocatable :: res(:, :, :)

    integer :: u, ierr, i, dum, j, state
    character(len=MAX_STR_SIZE) :: buf

    allocate(res(3, nat, nstat))
    res = 0.0_dp

    state = 1
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Integrated Forces') /= 0) then
          read(u, *)
          read(u, *)
          do i=1, nat
             read(u, *) dum, dum, (res(j, i, state), j=1, 3)
          end do
          state = state + 1
       end if
       if (state > nstat) exit
    end do
    close(u)

    res = -res
  end function exc_gau_extract_gradient


  function gau_get_electro_charges(filename, nat, nstat, ind_start) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat
    integer, intent(in) :: nstat
    integer, intent(in) :: ind_start

    real(dp), allocatable :: res(:, :)

    integer :: u, i, ierr, state, dum
    character(len=MAX_STR_SIZE) :: buf
    character(len=2) :: c

    allocate(res(nstat, nat))

    state = 1
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'ESP charges:') /= 0) then
          read(u, *)
          do i=1, ind_start
             read(u, *)
          end do
          
          do i=1, nat
             read(u, *) dum, c, res(state, i)
          end do
          state = state + 1
       end if
       if (state > nstat) exit
    end do
    close(u)
  end function gau_get_electro_charges


  function gaussian_get_dipoles(filename, nstat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nstat

    real(dp), allocatable :: res(:, :, :)

    integer :: u, i, j, ierr, state, dum
    character(len=MAX_STR_SIZE) :: buf
    character(len=2) :: c

    character(len=*), parameter :: str = &
         & 'Ground to excited state transition electric dipole moments (Au)'

    allocate( res(3, nstat, nstat))
    res = 0.0_dp

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, str) /= 0) then
          read(u, *)
          do i=1, nstat-1
             read(u, *) state, (res(j, state+1, 1), j=1, 3)
          end do

          ! We need to do this only once !!
          exit
       end if
    end do
    close(u)
  end function gaussian_get_dipoles


  function exc_gaussian_get_ciovl_from_file(filename) result(res)
    character(len=*), intent(in) :: filename

    real(dp), allocatable :: res(:, :)

    integer :: u, i, j, ierr, nstat
    character(len=MAX_STR_SIZE) :: buf
    
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'CI overlap matrix') /=0 ) then
          read(u, *) nstat, nstat
          allocate(res(nstat, nstat))
          do i=1, nstat
             read(u, *) (res(i, j), j=1, nstat)
          end do
       end if
    end do
    close(u)
  end function exc_gaussian_get_ciovl_from_file
  
  
end module mod_exc_gaussian_t
