! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_onedim_t
  use mod_analytical_generic_1d_t, only: nx_analytical_generic_1d_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: pi
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, &
       & nx_log, LOG_DEBUG
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_onedim_t

  type, extends(nx_analytical_generic_1d_t) :: nx_onedim_t
     integer :: onedim_mod = 1
     real(dp), allocatable :: parm(:)
   contains
     private
     procedure, public :: setup => onedim_setup
     procedure, public :: print => onedim_print
     procedure, public :: to_str => onedim_to_str
     procedure, public :: backup => onedim_backup
     procedure, public :: update => onedim_update
     procedure, public :: run => onedim_run
     procedure, public :: read_output => onedim_read_output
     procedure, public :: write_geom => onedim_write_geometry
     procedure, public :: init_overlap => onedim_init_overlap
     procedure, public :: prepare_overlap => onedim_prepare_overlap
     procedure, public :: extract_overlap => onedim_extract_overlap
     procedure, public :: ovl_post => onedim_ovl_post
     procedure, public :: ovl_run => onedim_ovl_run
     procedure, public :: get_lcao => onedim_get_lcao
     procedure, public :: cio_prepare_files => onedim_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & onedim_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => onedim_cio_get_mos_energies

     procedure :: get_v_dv
  end type nx_onedim_t
  interface nx_onedim_t
     module procedure constructor
  end interface nx_onedim_t

  integer, parameter :: ONED_SAC = 1
  !! Simple avoided crossing model
  integer, parameter :: ONED_DAC = 2
  !! Dual avoided crossing model
  integer, parameter :: ONED_EXT_REFL = 3
  !! Extended coupling with reflection
  integer, parameter :: ONED_DOUBLE_ARCH = 4
  !! Double arch
  integer, parameter :: ONED_NIKITIN = 5
  !! Nikitin Hamiltonian

  integer, parameter :: NMODELS = 5

  integer, parameter :: PARAM_SIZE(*) = [ &
       & 4, 5, 3, 4, 5 &
       & ]
  character(len=64), parameter :: MODEL_NAMES(*) = [ character(len=64) :: &
       & 'Simple avoided crossing model', &
       & 'Dual avoided crossing model', &
       & 'Extended coupling with reflection', &
       & 'Double arch', &
       & 'Nikitin Hamiltonian' &
       & ]

  character(len=*), parameter :: MODNAME = 'mod_onedim_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, &
       ! MANDATORY program- and method-specific parameters
       nat, nstat, &
       ! OPTIONAL program- and method-specific parameters
       & onedim_mod, parm &
       & ) result(res)
    !! Constructor for the ``nx_onedim_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in) :: nat
    integer, intent(in) :: nstat
    integer, intent(in), optional :: onedim_mod
    real(dp), intent(in), optional :: parm(:)

    type(nx_onedim_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    call res%set_method(method)
    call res%memalloc(nat, nstat)

    if (present(onedim_mod)) res%onedim_mod = onedim_mod
    if (present(parm)) then
       allocate(res%parm(size(parm)))
       res%parm(:) = parm(:)
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine onedim_setup(self, parser, conf, inp_path, stat)
    class(nx_onedim_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: model, ierr, u
    character(len=:), allocatable :: control

    real(dp), allocatable :: parm(:)
    namelist /onedim_parameters/ parm

    control = trim(inp_path)//'/onedim_parameters.inp'

    call set(parser, 'onedim_model', self%onedim_mod, 'onedim_mod')

    if (self%onedim_mod > NMODELS) then
       call stat%append(NX_ERROR, &
            & 'onedim_mod should be inferior or equal to '//to_str(NMODELS)//&
            & ' (setup value is '//to_str(self%onedim_mod)//')', &
            & mod=MODNAME, func='onedim_setup'&
            & )
    end if

    if (allocated(self%parm)) then
       if (size(self%parm) /= PARAM_SIZE( self%onedim_mod )) then
          call stat%append(NX_ERROR, &
               & 'Number of parameters is inconsistent with model '//&
               & to_str(self%onedim_mod)//': it should have '//&
               & to_str(PARAM_SIZE( self%onedim_mod ))//', while '//&
               & to_str(size(self%parm))//' have been set)', &
               & mod=MODNAME, func='onedim_setup' &
               & )
       else
          allocate( parm( size(self%parm) ) )
          open(newunit=u, file=control, status='old', action='read')
          read(u, nml=onedim_parameters)
          close(u)
       end if
    else
       call allocate_parameters(self, stat)
       if (stat%has_error()) return
       allocate( parm( size(self%parm) ) )
       open(newunit=u, file=control, status='old', action='read')
       read(u, nml=onedim_parameters)
       close(u)
    end if

    if (.not. stat%has_error() ) self%parm(:) = parm(:)
  end subroutine onedim_setup

  subroutine onedim_print(self, out)
    class(nx_onedim_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output, i
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''

    write(output, '(A)') 'Model: '//trim(MODEL_NAMES(self%onedim_mod))
    do i=1, size(self%parm)
       call print_conf_ele(self%parm(i), 'Parameter '//to_str(i), unit=output)
    end do
  end subroutine onedim_print

  function onedim_to_str(self) result(res)
    class(nx_onedim_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&onedim_model'//nl
    res = res//' onedim_mod = '//to_str(self%onedim_mod)//nl
    res = res//'/'//nl
  end function onedim_to_str

  subroutine onedim_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine onedim_backup

  subroutine onedim_update(self, conf, traj, path, stat)
    class(nx_onedim_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    ! Transfer new geometry and velocities to the object.
    call self%set_from_traj(traj%geom, traj%veloc)
  end subroutine onedim_update

  subroutine onedim_run(self, stat)
    !! Compute the energies, gradients and NAC.
    !!
    !! The computation is carried out according to:
    !!
    !! \[ E_{1,2} = \frac{1}{2} (V_{11} + V_{22}) \mp \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2 \right )^{1/2} \]
    !!
    !! \[ G_{1,2}(x) = \frac{1}{2} \left (
    !!   \frac{dV_{11}}{dx} + \frac{dV_{22}}{dx} \right )
    !! \mp \left ( \frac{1}{4} (V_{22}-V_{11}) \left (
    !!   \frac{dV_{22}}{dx} - \frac{dV_{11}}{dx} \right ) + V_{12} \frac{dV_{12}}{dx}\right ) \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2) \right )^{-1/2} \]
    !!
    !! \[ F_{12} = \frac{1}{
    !!               1 + \left ( \frac{2V_{12}}{V_{22}-V_{11}} \right )^2 }
    !!            \left (
    !!                \frac{1}{(V_{22}-V_{11})} \frac{dV_{12}}{dx}
    !!                - \frac{V_{12}}{(V_{22}-V_{11})^2}
    !!                  \left (
    !!                     \frac{dV_{22}}{dx}
    !!                     - \frac{dV_{11}}{dx}
    !!                  \right )
    !!           \right )
    !! \]
    class(nx_onedim_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: v(3), dv(3)

    call self%get_v_dv(self%geom(), v, dv)
    call nx_log%log(LOG_DEBUG, v, &
         & labels=['V11', 'V12', 'V22'])
    call nx_log%log(LOG_DEBUG, dv, &
         & labels=['dV11', 'dV12', 'dV22'])


    call self%set_epot( compute_energies(v, dv) )
    call self%set_grad( compute_gradients(v, dv) )
    call self%set_nad( compute_nad(v, dv) )
  end subroutine onedim_run

  function onedim_read_output(self, conf, traj, path, stat) result(info)
    class(nx_onedim_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    info%has_osc_str = .false.
    info%has_data_qm = .false.

    info%repot(:) = self%epot()

    ! Transfer gradients
    info%rgrad(:, 1, :) = self%grad()

    ! Transfer NAD
    info%rnad(:, 1, :) = self%nad()

    ! CS-FSSH
    if (conf%run_complex) then
       info%rnad_i(:, 1, :) = self%nad_i()
       info%rgamma(:) = self%gamma()
    end if
  end function onedim_read_output

  subroutine onedim_write_geometry(self, traj, path, print_merged)
    class(nx_onedim_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged
  end subroutine onedim_write_geometry

  subroutine onedim_init_overlap(self, path_to_qm, stat)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine onedim_init_overlap

  subroutine onedim_prepare_overlap(self, path_to_qm, stat)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine onedim_prepare_overlap

  function onedim_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_onedim_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function onedim_extract_overlap

  function onedim_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function onedim_get_lcao

  subroutine onedim_ovl_run(self, stat)
    class(nx_onedim_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine onedim_ovl_run

  subroutine onedim_ovl_post(self, stat)
    class(nx_onedim_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine onedim_ovl_post

  subroutine onedim_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine onedim_cio_prepare_files

  subroutine onedim_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine onedim_cio_get_singles_amplitudes

  subroutine onedim_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_onedim_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine onedim_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  subroutine allocate_parameters(self, stat)
    !! Allocate the memory for the ``parm`` array.
    !!
    !! The allocation is based on the value of ``nx_qm%onedim_mod``.
    !! If no model has been defined (i.e. ``onedim_mod`` does not
    !! correspond to any defined ``ONED_`` models), the computation
    !! is aborted.
    class(nx_onedim_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    if (self%onedim_mod == ONED_SAC) then
       allocate(self%parm(4))
    else if (self%onedim_mod == ONED_DAC) then
       allocate(self%parm(5))
    else if (self%onedim_mod == ONED_EXT_REFL) then
       allocate(self%parm(3))
    else if (self%onedim_mod == ONED_DOUBLE_ARCH) then
       allocate(self%parm(4))
    else if (self%onedim_mod == ONED_NIKITIN) then
       allocate(self%parm(5))
    else
       allocate(self%parm(0))
       call stat%append(NX_ERROR, &
            & 'ONEDIM: Cannot allocate parameters (unknowm model '//&
            & to_str( self%onedim_mod )//')', &
            & mod=MODNAME, func='allocate_parameters'&
            & )
    end if
    self%parm(:) = 0.0_dp
  end subroutine allocate_parameters

  subroutine get_v_dv(self, coord, v, dv)
    !! Compute the potential and its derivative.
    !!
    !! As different models will have different number of parameters
    !! with different names, a ``block`` is assigned to each method
    !! for clarity.
    class(nx_onedim_t), intent(in) :: self
    real(dp), intent(in) :: coord(:)
    !! QM setup.
    real(dp), intent(out) :: v(3)
    !! Potential.
    real(dp), intent(out) :: dv(3)
    !! Potential derivative.

    real(dp) :: r

    r = coord(1)

    if (self%onedim_mod == ONED_SAC) then
       block
         real(dp) :: a, b, c, d
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         d = self%parm(4)
         if (r >= 0) then
            v(1) = a * (1.0_dp - exp(-b * r))
            dv(1) = a * b * exp(-b * r)
         else
            v(1) = -a * (1.0_dp - exp(b * r))
            dv(1) = a * b * exp(b * r)
         end if
         v(3) = -v(1)
         dv(3) = -dv(1)
         v(2) = c * exp(-d * r**2)
         dv(2) = -2.0_dp * d * r * v(2)
       end block

    else if (self%onedim_mod == ONED_DAC) then
       block
         real(dp) :: a, b, c, d, e0
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         d = self%parm(4)
         e0 = self%parm(5)

         v(1) = 0.0_dp
         v(3) = -a * exp(-b * r**2) + e0
         v(2) = c * exp(-d * r**2)

         dv(1) = 0.0_dp
         dv(3) = -2.0_dp * b * r * (v(3)-e0)
         dv(2) = -2.0_dp * d * r * v(2)
       end block

    else if (self%onedim_mod == ONED_EXT_REFL) then
       block
         real(dp) :: a, b, c
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)

         v(1) = a
         v(3) = -a
         dv(1) = 0.0_dp
         dv(3) = 0.0_dp

         if (r <= 0) then
            v(2) = b*exp(c*r)
            dv(2) = c*v(2)
         else
            v(2) = b*(2.0_dp - exp(-c*r))
            dv(2) = b*c*exp(-c*r)
         end if
       end block

    else if (self%onedim_mod == ONED_DOUBLE_ARCH) then
       block
         real(dp) :: a, b, c, z
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         z = self%parm(4)

         v(1) = -a
         v(3) = a
         dv(1) = 0.0_dp
         dv(3) = 0.0_dp

         if (r <= -z) then
            v(2) = b * (-exp( c*(r-z) ) + exp( c*(r+z) ))
            dv(2) = b*c*(-exp(c*(r-z)) + exp(c*(r+z)))
         else if ((r > -z) .and. (r <= z)) then
            v(2) = b * (-exp( c*(r-z) ) - exp( -c*(r+z) )) + 2*b
            dv(2) = b*c*(-exp(c*(r-z)) + exp(-c*(r+z)))
         else
            v(2) = b * (exp( -c*(r-z) ) - exp( -c*(r+z) ))
            dv(2) = b*c*(-exp(-c*(r-z)) + exp(-c*(r+z)))
         end if
       end block

    else if (self%onedim_mod == ONED_NIKITIN) then
       block
         real(dp) :: a, b, N, alpha, de, theta
         a = self%parm(1)
         b = self%parm(2)
         N = self%parm(3)
         alpha = self%parm(4)
         de = self%parm(5)

         theta = pi / N

         v(1) = b*exp(-alpha*r) &
              & + de*0.5_dp &
              & - a*0.5_dp*cos(theta)*exp(-alpha*r)
         v(3) = b*exp(-alpha*r) &
              & - de*0.5_dp &
              & + a*0.5_dp*cos(theta)*exp(-alpha*r)
         v(2) = -a*0.5_dp * sin(theta) * exp(-alpha*r)

         dv(1) = (-b + a*0.5_dp * cos(theta)) &
              & * alpha * exp(-alpha*r)
         dv(3) = (-b - a*0.5_dp * cos(theta)) &
              & * alpha * exp(-alpha*r)
         dv(2) = -alpha*v(2)
       end block
    end if
  end subroutine get_v_dv

  subroutine onedim_compute(self, v, dv, epot, grad, nad)
    !! Compute the energies, gradients and NAC.
    !!
    !! The computation is carried out according to:
    !!
    !! \[ E_{1,2} = \frac{1}{2} (V_{11} + V_{22}) \mp \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2 \right )^{1/2} \]
    !!
    !! \[ G_{1,2}(x) = \frac{1}{2} \left (
    !!   \frac{dV_{11}}{dx} + \frac{dV_{22}}{dx} \right )
    !! \mp \left ( \frac{1}{4} (V_{22}-V_{11}) \left (
    !!   \frac{dV_{22}}{dx} - \frac{dV_{11}}{dx} \right ) + V_{12} \frac{dV_{12}}{dx}\right ) \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2) \right )^{-1/2} \]
    !!
    !! \[ F_{12} = \frac{1}{
    !!               1 + \left ( \frac{2V_{12}}{V_{22}-V_{11}} \right )^2 }
    !!            \left (
    !!                \frac{1}{(V_{22}-V_{11})} \frac{dV_{12}}{dx}
    !!                - \frac{V_{12}}{(V_{22}-V_{11})^2}
    !!                  \left (
    !!                     \frac{dV_{22}}{dx}
    !!                     - \frac{dV_{11}}{dx}
    !!                  \right )
    !!           \right )
    !! \]
    class(nx_onedim_t), intent(inout) :: self
    !! QM setup.
    real(dp), intent(in) :: v(:)
    !! Potential.
    real(dp), intent(in) :: dv(:)
    !! Potential derivative.
    real(dp), intent(inout) :: epot(:)
    !! Potential derivative.
    real(dp), intent(inout) :: grad(:)
    !! Potential derivative.
    real(dp), intent(inout) :: nad(:)
    !! Potential derivative.

    real(dp) :: temp

    temp = v(2)**2 + 0.25_dp * (v(3) - v(1))**2
    temp = sqrt(temp)
    epot(1) = 0.5_dp * (v(1) + v(3)) - temp
    epot(2) = 0.5_dp * (v(1) + v(3)) + temp

    temp = 1.0_dp / temp
    temp = temp &
         & * ( (v(2)*dv(2)) + 0.25_dp*(v(3)-v(1))*(dv(3)-dv(1)) )
    grad(1) = 0.5_dp * (dv(1) + dv(3)) - temp
    grad(2) = 0.5_dp * (dv(1) + dv(3)) + temp

    ! Here we compute the coupling 1/2, and we store 2/1
    temp = 1.0_dp / (1 + (2*v(2) / (v(3)-v(1)))**2) &
         & * (&
         & 1.0_dp / (v(3)-v(1)) * dv(2) &
         & - (v(2) / (v(3)-v(1))**2) * (dv(3)-dv(1))&
         & )
    nad(1) = -temp
  end subroutine onedim_compute


  pure function compute_energies(v, dv) result(epot)
    real(dp), intent(in) :: v(3)
    real(dp), intent(in) :: dv(3)

    real(dp) :: epot(2)

    real(dp) :: temp

    temp = sqrt(v(2)**2 + 0.25_dp * (v(3) - v(1))**2)
    epot(1) = 0.5_dp * (v(1) + v(3)) - temp
    epot(2) = 0.5_dp * (v(1) + v(3)) + temp
  end function compute_energies

  pure function compute_gradients(v, dv) result(grad)
    real(dp), intent(in) :: v(3)
    real(dp), intent(in) :: dv(3)

    real(dp) :: grad(2, 1)

    real(dp) :: temp

    temp = 1.0_dp / sqrt(v(2)**2 + 0.25_dp * (v(3) - v(1))**2)
    temp = temp &
         & * ( (v(2)*dv(2)) + 0.25_dp*(v(3)-v(1))*(dv(3)-dv(1)) )
    grad(1, 1) = 0.5_dp * (dv(1) + dv(3)) - temp
    grad(2, 1) = 0.5_dp * (dv(1) + dv(3)) + temp
  end function compute_gradients

  pure function compute_nad(v, dv) result(nad)
    real(dp), intent(in) :: v(3)
    real(dp), intent(in) :: dv(3)

    real(dp) :: nad(1, 1)

    real(dp) :: temp

    ! Here we compute the coupling 1/2, and we store 2/1
    temp = 1.0_dp / (1 + (2*v(2) / (v(3)-v(1)))**2) &
         & * (&
         & 1.0_dp / (v(3)-v(1)) * dv(2) &
         & - (v(2) / (v(3)-v(1))**2) * (dv(3)-dv(1))&
         & )
    nad(1, 1) = -temp
  end function compute_nad
  
  
end module mod_onedim_t
