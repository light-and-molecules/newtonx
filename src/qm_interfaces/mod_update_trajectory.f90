! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_update_trajectory
  use mod_qm_item_t, only: nx_qm_item_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_trajectory, only: nx_traj_t
  implicit none

  private

contains

  subroutine qm_update_trajectory(qmitem, qminfo, traj, report, is_test)
    type(nx_qm_item_t), intent(inout) :: qmitem
    type(nx_qminfo_t), intent(in) :: qminfo
    type(nx_traj_t), intent(inout) :: traj
    logical, intent(in), optional :: report
    logical, intent(in), optional :: is_test

    logical :: print_info

    integer, allocatable :: phase(:)
    real(dp), allocatable :: cossine(:)

    print_info = .true.
    if (present(report)) print_info = report

    if (.not. qminfo%request_adapt_dt) then
       if (qminfo%update_nad) then
          if (qminfo%correct_phase) then

             allocate(phase(size(qminfo%rnad, 1)))
             allocate(cossine(size(qminfo%rnad, 1)))
             call phase_adjust_h_vec(traj%old_nad, qminfo%rnad, phase, cossine)
             deallocate(phase, cossine)
          end if
       end if

       
    end if

    
    
  end subroutine qm_update_trajectory
  

  subroutine qm_correct_phase(qm, step, init_step, nstat, rnad, old_nad)
    type(nx_qm_t), intent(in) :: qm
    integer, intent(in) :: nstat
    integer, intent(in) :: step
    integer, intent(in) :: init_step
    real(dp), intent(inout) :: rnad(:, :, :)
    real(dp), intent(in) :: old_nad(:, :, :)

    integer, allocatable :: phase(:)
    real(dp), allocatable :: cossine(:)

    integer :: i

    ! if (step == init_step) then
    !    select case(qm%qmcode)
    !    case ("columbus")
    !       if (ci_phase) then
    !          call col_initial_phase(rnad, nstat)
    !       end if
    !    end select
    ! end if

    allocate(phase(size(rnad, 1)))
    allocate(cossine(size(rnad, 1)))
    call phase_adjust_h_vec(old_nad, rnad, phase, cossine)
    if (qm%is_test .and. step == init_step) then
       do i=1, size(rnad, 1)
          if (rnad(i, 1, 1) < 0) then
             rnad(i, :, :) = -1.0_dp * rnad(i, :, :)
          end if
       end do
    end if
    ! print *, 'GLOBAL PHASE: ', phase
    deallocate(phase)
    deallocate(cossine)
  end subroutine qm_correct_phase


  subroutine qm_update_traj_components(qminfo, config, traj)
    !! Update the trajectory with the content read from output.
    !!
    type(nx_qminfo_t), intent(in) :: qminfo
    type(nx_config_t), intent(in) :: config
    type(nx_traj_t), intent(inout) :: traj

    integer :: i

    ! If surface hopping occurs, we do not need to read the energies
    ! again: we are only interested in the gradients !!
    if (nx_qm%update_epot == 1) then
       traj%old_epot(3, :) = traj%old_epot(2, :)
       traj%old_epot(2, :) = traj%old_epot(1, :)
       traj%old_epot(1, :) = traj%epot
       traj%epot(:) = qminfo%repot(:)

       if (traj%run_complex) then
          ! The other cases are already dealt with in `mod_csfssh`, in routine
          ! `csfssh_compute_gamma`.
          if (config%gamma_model == 0) then
             traj%old_gamma(3, :) = traj%old_gamma(2, :)
             traj%old_gamma(2, :) = traj%old_gamma(1, :)
             traj%old_gamma(1, :) = traj%gamma
             traj%gamma(:) = qminfo%rgamma(:)
          end if
       end if
    end if

    traj%old_acc(:, :) = traj%acc(:, :)
    traj%old_grad(:, :, :) = traj%grad(:, :, :)

    traj%grad(:, :, :) = qminfo%rgrad(:, :, :)

    do i=1, size(traj%acc, 2)
       traj%acc(:, i) = traj%grad(traj%nstatdyn, :, i)
       traj%acc(:, i) = (-1.0_dp / traj%masses(i)) * traj%acc(:, i)
    end do

    if (qminfo%update_nad) then
       traj%old_nad = traj%nad
       traj%nad(:, :, :) = qminfo%rnad(:, :, :)

       if (traj%run_complex) then
          traj%old_nad_i = traj%nad_i
          traj%nad_i(:, :, :) = qminfo%rnad_i(:, :, :)
       end if
    end if

    if (size(qminfo%repot) > 1) then
       traj%osc(:) = nx_qm%osc_str(:)
    end if

    if (config%progname == "exc_mopac" .or. &
         & config%progname == "exc_gaussian") then
       traj%diaham(:, :) = nx_qm%diaham(:, :)
       traj%diapop(:) = nx_qm%diapop(:)
       traj%diaen(:) = nx_qm%diaen(:)
    end if

    call traj%print_epot_all()
  end subroutine qm_update_traj

end module mod_update_trajectory
