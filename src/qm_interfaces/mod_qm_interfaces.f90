! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_qm_interfaces
  use mod_constants, only: MAX_STR_SIZE
  use mod_configuration, only: nx_config_t
  use mod_kinds, only: dp
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qm_item_t, only: nx_qm_item_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_sbh_t, only: nx_sbh_t
  use mod_recohmodel_t, only: nx_recohmodel_t
  use mod_onedim_t, only: nx_onedim_t
  use mod_conint_t, only: nx_conint_t
  use mod_cs_analytical_t, only: nx_cs_analytical_t
  use mod_columbus_t, only: nx_columbus_t
  use mod_gaussian_t, only: nx_gaussian_t
  use mod_orca_t, only: nx_orca_t
  use mod_mopac_t, only: nx_mopac_t
  use mod_openmolcas_t, only: nx_openmolcas_t
  use mod_tinker_mndo_t, only: nx_tinker_mndo_t
  use mod_tinker_g16mmp_t, only: nx_tinker_g16mmp_t
  use mod_exc_mopac_t, only: nx_exc_mopac_t
  use mod_exc_gaussian_t, only: nx_exc_gaussian_t
  use mod_turbomole_t, only: nx_turbomole_t
  use mod_external_t, only: nx_external_t
  use mod_tools, only: to_str
  implicit none

  public :: nx_qm_create_item !, nx_qm_create_list_exash

contains

  function nx_qm_create_item(conf) result(qm)
    type(nx_config_t), intent(in) :: conf

    type(nx_qm_item_t) :: qm

    character(len=MAX_STR_SIZE) :: env, env2
    integer :: ierr
    logical :: is_test
    character(len=:), allocatable :: debug_path

    debug_path = '../'//trim(conf%debug_path)

    is_test = .false.
    call get_environment_variable("NX_TEST_ENV", status=ierr)
    if (ierr /= 1) then
       if (ierr >= 0) then
          is_test = .true.
       end if
    end if

    select case(conf%progname)
    case('analytical')
       select case(conf%methodname)
       case('sbh')
          qm = nx_qm_item_t( &
               nx_sbh_t( trim(conf%methodname), conf%nat, conf%nstat ), &
               progname=trim(conf%progname), &
               run_dir='./', &
               inp_dir=trim(conf%init_input), &
               chk_dir=trim(debug_path), &
               is_test=is_test, &
               ovl_required=.false., &
               read_nac = .true., &
               need_external_call = .false., &
               need_copy_inputs = .false. &
               )
       case('recohmodel')
          qm = nx_qm_item_t( &
               nx_recohmodel_t( trim(conf%methodname), conf%nat, conf%nstat ), &
               progname=trim(conf%progname), &
               run_dir='./', &
               inp_dir=trim(conf%init_input), &
               chk_dir=trim(debug_path), &
               is_test=is_test, &
               ovl_required=.false., &
               read_nac = .true., &
               need_external_call = .false., &
               need_copy_inputs = .false. &
               )
       case('onedim_model')
          qm = nx_qm_item_t( &
               nx_onedim_t( trim(conf%methodname), conf%nat, conf%nstat ), &
               progname=trim(conf%progname), &
               run_dir='./', &
               inp_dir=trim(conf%init_input), &
               chk_dir=trim(debug_path), &
               is_test=is_test, &
               ovl_required=.false., &
               read_nac = .true., &
               need_external_call = .false., &
               need_copy_inputs = .false. &
               )
       case('con_int')
          qm = nx_qm_item_t( &
               nx_conint_t( trim(conf%methodname), conf%nat, conf%nstat ), &
               progname=trim(conf%progname), &
               run_dir='./', &
               inp_dir=trim(conf%init_input), &
               chk_dir=trim(debug_path), &
               is_test=is_test, &
               ovl_required=.false., &
               read_nac = .true., &
               need_external_call = .false., &
               need_copy_inputs = .false. &
               )
       case('cs_fssh')
          qm = nx_qm_item_t( &
               nx_cs_analytical_t( trim(conf%methodname), conf%nat, conf%nstat ), &
               progname=trim(conf%progname), &
               run_dir='./', &
               inp_dir=trim(conf%init_input), &
               chk_dir=trim(debug_path), &
               is_test=is_test, &
               ovl_required=.false., &
               read_nac = .true., &
               need_external_call = .false., &
               need_copy_inputs = .false. &
               )
       end select
       
    case ('columbus')
       call get_environment_variable('COLUMBUS', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_columbus_t( trim(conf%methodname), colenv=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./columbus.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required=.true., &
            read_nac = conf%dc_method == 1 &
            )
    case ('turbomole')
       call get_environment_variable('TURBODIR', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_turbomole_t( trim(conf%methodname), turbodir=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./turbomole.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= conf%thres > 0, &
            read_nac = .false. &
            )
    case ('gaussian')
       call get_environment_variable('g16root', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_gaussian_t( trim(conf%methodname), g16root=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./gaussian.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= conf%thres > 0, &
            read_nac = .false. &
            )
    case ('orca')
       call get_environment_variable('ORCA', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_orca_t( trim(conf%methodname), orca_path=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./orca.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= conf%thres > 0, &
            read_nac = .false. &
            )
    case ('openmolcas')
       call get_environment_variable('MOLCAS', env, status=ierr)
       qm = nx_qm_item_t( &
           nx_openmolcas_t( trim(conf%methodname), openmolcas_path=trim(env) ), &
           progname=trim(conf%progname), &
           run_dir='./openmolcas.job', &
           inp_dir=trim(conf%init_input), &
           chk_dir=trim(debug_path), &
           is_test=is_test, &
           ovl_required= .false., &
           !ovl_required= conf%thres > 0, &
           read_nac = .true., &
           produce_cio = .false. &
           )      
    case ('mopac')
       call get_environment_variable('MOPAC', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_mopac_t( trim(conf%methodname), mopac_env=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./mopac.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= .false., &
            read_nac = .false., &
            produce_cio = .true. &
            )
    case ('tinker_mndo')
       call get_environment_variable('TINKER_MNDO', env, status=ierr)
       call get_environment_variable('MNDO', env2, status=ierr)
       qm = nx_qm_item_t( &
            nx_tinker_mndo_t( &
            &   trim(conf%methodname), tmndo_env=trim(env), mndo_env=trim(env2) ), &
            progname=trim(conf%progname), &
            run_dir='./tinker_mndo.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= conf%thres > 0, &
            read_nac = .true. &
            )

    case ('tinker_g16mmp')
       call get_environment_variable('TINKER_G16', env, status=ierr)
       call get_environment_variable('g16root', env2, status=ierr)
       qm = nx_qm_item_t( &
            nx_tinker_g16mmp_t( &
            &   trim(conf%methodname), tinker_env=trim(env), g16root=trim(env2) ), &
            progname=trim(conf%progname), &
            run_dir='./tinker_g16mmp.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= .true., &
            read_nac = .false. &
            )

    case('exc_mopac')
       call get_environment_variable('TINKER', env2, status=ierr)
       call get_environment_variable('MOPAC', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_exc_mopac_t( trim(conf%methodname), mopac_env=trim(env), tinker_env=trim(env2) ), &
            progname=trim(conf%progname), &
            run_dir='./exc_mopac.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= .false., &
            read_nac = .false., &
            produce_cio = .true. &
            )

    case('exc_gaussian')
       call get_environment_variable('g16root', env, status=ierr)
       qm = nx_qm_item_t( &
            nx_exc_gaussian_t( trim(conf%methodname), g16root=trim(env) ), &
            progname=trim(conf%progname), &
            run_dir='./exc_gaussian.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= .true., &
            read_nac = .false., &
            produce_cio = .false. &
            )

    case('external')
       qm = nx_qm_item_t( &
            nx_external_t( trim(conf%methodname) ), &
            progname=trim(conf%progname), &
            run_dir='./external.job', &
            inp_dir=trim(conf%init_input), &
            chk_dir=trim(debug_path), &
            is_test=is_test, &
            ovl_required= .false., &
            read_nac = .true., &
            produce_cio = .false. &
            )
    end select
  end function nx_qm_create_item


  ! function nx_qm_create_list_exash(conf, masses) result(res)
  !   type(nx_config_t), intent(in) :: conf
  !   real(dp), intent(in), optional :: masses(:)
  ! 
  !   type(nx_qm_item_t), allocatable :: res(:)
  ! 
  !   integer :: i, ierr
  !   character(len=:), allocatable :: debug_path
  !   logical :: is_test
  !   character(len=MAX_STR_SIZE) :: env
  ! 
  !   debug_path = '../'//trim(conf%debug_path)
  ! 
  !   is_test = .false.
  !   call get_environment_variable("NX_TEST_ENV", status=ierr)
  !   if (ierr /= 1) then
  !      if (ierr >= 0) then
  !         is_test = .true.
  !      end if
  !   end if
  ! 
  !   allocate(res( conf%n_qm_jobs) )
  ! 
  !   select case( conf%progname )
  !   case('exc_mopac')
  !      do i=1, conf%n_qm_jobs
  !         res(i) = &
  !              & nx_qm_item_t(&
  !              &     nx_mopac_t(conf%methodname, mopac_env=env), &
  !              &     conf%progname, &
  !              &     run_dir='./mopac_'//to_str(i)//'.job', &
  !              &     inp_dir=trim(conf%init_input)//'_'//to_str(i), &
  !              &     chk_dir=trim(debug_path), &
  !              &     read_nac=.false., &
  !              &     is_test=is_test, &
  !              &     need_external_call=.true., &
  !              &     need_copy_inputs=.true., &
  !              &     ovl_required=.false., &
  !              &     is_qmmm=.false., &
  !              &     produce_cio=.true.&
  !              & )
  !      end do
  !   case('exc_gaussian')
  !   end select
  !   
  ! end function nx_qm_create_list_exash
  
end module mod_qm_interfaces
