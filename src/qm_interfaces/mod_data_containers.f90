! Copyright (C) 2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_data_containers
  use mod_kinds, only: dp
  implicit none

  private

  public :: geom_to_container

  type, public :: mat2_container_t
     real(dp), allocatable :: m(:, :)
  end type mat2_container_t

  type, public :: mat3_container_t
     real(dp), allocatable :: m(:, :, :)
  end type mat3_container_t

contains

  function geom_to_container(filename, nat_array) result(res)
    !! Read a geometry file in NX format into a container.
    !!
    !! The function can be used to obtain separate components for different chromophores
    !! in a system, in particular for using EXASH.
    !!
    !! The file read must contain first the atoms belonging to each subsystem !
    character(len=*), intent(in) :: filename
    !! Name of the file to read, with NX (or Columbus) format.
    integer, intent(in) :: nat_array(:)
    !! Array containing the number of atoms in each sub-system.

    type(mat2_container_t) :: res( size(nat_array) )

    integer :: i, j, u, nchrom
    character(len=2) :: dumc
    real(dp) :: dumr

    open(newunit=u, file=filename, action='read')
    do nchrom=1, size(nat_array)
       allocate(res(nchrom)%m (3, nat_array(nchrom)))
       do i=1, nat_array(nchrom)
          read(u, *) dumc, dumr, (res(nchrom)%m(j, i), j=1, 3), dumr
       end do
    end do
    close(u)
  end function geom_to_container
end module mod_data_containers
