! Copyright (C) 2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_status_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_status_t
  public :: NX_ERROR, NX_WARNING

  type :: nx_status_t
     private
     integer :: ierr = 0
     !! Level of the status code:
     !!
     !! - ``1``: warning level
     !! - ``2``: fatal level
     character(len=:), allocatable :: msg
     !! Message corresponding to the status.
     character(len=:), allocatable :: mod
     !! Module where the exception was reported.
     character(len=:), allocatable :: func
     !! Function (or routine) where the exception was reported.
     type(nx_status_t), pointer :: next => null()
     !! Pointer to the next error in the chain.
     integer :: nerrors = 0
     integer :: nwarnings = 0
   contains
     private
     procedure, public :: append
     procedure, public :: raise
     procedure, public :: reset
     procedure, public :: has_error
     procedure, public :: has_warning
     procedure, public :: filter_errors
     procedure, public :: filter_warnings
     procedure :: init
     procedure :: format
  end type nx_status_t
  interface nx_status_t
     module procedure constructor
  end interface nx_status_t

  integer, parameter :: NX_ERROR = 2
  integer, parameter :: NX_WARNING = 1

contains

  pure function constructor(ierr, msg, mod, func) result(stat)
    type(nx_status_t) :: stat
    integer, intent(in) :: ierr
    character(len=*), intent(in) :: msg
    character(len=*), intent(in), optional :: mod
    character(len=*), intent(in), optional :: func

    stat%ierr = ierr
    stat%msg = msg

    if (present(mod)) then
       stat%mod = mod
    else
       stat%mod = ''
    end if

    if (present(func)) then
       stat%func = func
    else
       stat%func = ''
    end if
  end function constructor


  subroutine init(self, ierr, msg, mod, func)
    class(nx_status_t) :: self
    integer, intent(in) :: ierr
    character(len=*), intent(in) :: msg
    character(len=*), intent(in), optional :: mod
    character(len=*), intent(in), optional :: func

    self%ierr = ierr
    self%msg = msg

    if (ierr == 1) then
       self%nwarnings = self%nwarnings + 1
    else if (ierr == 2) then
       self%nerrors = self%nerrors + 1
    end if

    if (present(mod)) then
       self%mod = mod
    else
       self%mod = ''
    end if

    if (present(func)) then
       self%func = func
    else
       self%func = ''
    end if
  end subroutine init


  recursive subroutine append(self, ierr, msg, mod, func)
    class(nx_status_t), intent(inout) :: self
    integer, intent(in) :: ierr
    character(len=*), intent(in) :: msg
    character(len=*), intent(in), optional :: mod
    character(len=*), intent(in), optional :: func

    if (self%ierr == 0) then
       ! The current item contains no exception yet
       call self%init(ierr, msg, mod=mod, func=func)
    else
       if (.not. associated(self%next)) allocate(self%next)
       call self%next%append(ierr, msg, mod=mod, func=func)
    end if
 
  end subroutine append


  recursive subroutine raise(self, output)
    class(nx_status_t), intent(in) :: self
    integer, intent(in), optional :: output

    integer :: out

    out = stdout
    if (present(output)) out = output

    if (allocated(self%msg)) then
       if (self%msg /= '') then
          write(out, '(A)') self%format()
       end if
       
       if (associated(self%next)) call self%next%raise()
    end if
  end subroutine raise


  recursive subroutine reset(self)
    class(nx_status_t), intent(inout) :: self

    if (allocated(self%msg)) then
       self%msg = ''
       self%ierr = 0
       if (associated(self%next)) call self%next%reset()
    else
       self%msg = ''
       self%ierr = 0
    end if
  end subroutine reset


  function has_error(self) result(res)
    class(nx_status_t), intent(in) :: self

    logical :: res

    res = self%nerrors > 0
  end function has_error


  function has_warning(self) result(res)
    class(nx_status_t), intent(in) :: self

    logical :: res

    res = self%nwarnings > 0
  end function has_warning


  recursive function filter_errors(self) result(res)
    class(nx_status_t), target, intent(in) :: self

    character(len=:), allocatable :: res

    type(nx_status_t), pointer :: stat

    integer :: i

    res = ''
    stat => self
    LOOP: do while(associated(stat))
       if (stat%ierr == 2) then
          res = res//stat%format()//new_line('a')
       end if
       stat => stat%next
    end do LOOP
  end function filter_errors


  recursive function filter_warnings(self) result(res)
    class(nx_status_t), target, intent(in) :: self

    character(len=:), allocatable :: res

    type(nx_status_t), pointer :: stat

    integer :: i

    res = ''
    stat => self
    LOOP: do while(associated(stat))
       if (stat%ierr == 1) then
          res = res//stat%format()//new_line('a')
       end if
       stat => stat%next
    end do LOOP
  end function filter_warnings


  pure function format(self) result(res)
    class(nx_status_t), intent(in) :: self

    character(len=:), allocatable :: res

    character(len=:), allocatable :: prefix, modstr, funcstr

    prefix = ''
    if (self%ierr == 1) then
       prefix = 'WARNING'
    else if (self%ierr == 2) then
       prefix = 'ERROR'
    else
       prefix = 'UNKNOWN'
    end if

    if (self%mod /= '') then
       if (self%func /= '') then
          prefix = prefix//' ('//self%mod//'::'//self%func//')'
       else
          prefix = prefix//' ('//self%mod//')'
       end if
    else
       if (self%func /= '') then
          prefix = prefix//' ('//self%func//')'
       end if
    end if

    res = prefix//' -> '//self%msg
  end function format
end module mod_status_t
