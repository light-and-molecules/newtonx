! Copyright (C) 2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_tinker_utils
  use mod_constants, only: au2kcalm, au2ang
  use mod_kinds, only: dp
  use mod_tools, only: split_blanks
  use mod_print_utils, only: to_upper
  implicit none

  private

  public :: tinker_get_qmmm_atoms
  public :: tinker_get_mm_energy
  public :: tinker_get_gradient

contains

  function tinker_get_qmmm_atoms(path_to_file, nat) result(res)
    character(len=*), intent(in) :: path_to_file
    integer, intent(in) :: nat

    logical :: res(nat)

    character(len=:), allocatable :: filename
    character(len=256) :: line
    character(len=256), allocatable :: group(:)
    integer :: ierr

    integer :: qmatoms(nat), nqmatoms
    integer :: i, u

    qmatoms(:) = 0
    nqmatoms = 0

    filename = trim(path_to_file)//'/frame00000.key'
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       line = to_upper(line)
       
       if(line(1:8) == 'QMATOMS ') then
          print *, line
          call split_blanks(line(8:), group)
          do i = 1, size(group)
             read(group(i), *) qmatoms(nqmatoms+i)
          end do

          do while( qmatoms(nqmatoms+1) > 0 )
             nqmatoms = nqmatoms + 1
             if( nqmatoms == nat ) exit
          end do
          print *, nqmatoms, nat
       end if
    end do

    res = qmatoms > 0
  end function tinker_get_qmmm_atoms


  function tinker_get_mm_energy(filename) result(en)
    character(len=*), intent(in) :: filename

    real(dp) :: en

    character(len=256) :: buf
    character(len=256), allocatable :: split(:)
    integer :: u, ierr

    open(newunit=u, file=filename, action='read')
    PARSE: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Total Potential Energy :') /= 0) then
          call split_blanks(buf, split)
          read(split(5), *) en
          exit PARSE
       end if
    end do PARSE
    close(u)
    en = en / au2kcalm 
  end function tinker_get_mm_energy


  function tinker_get_gradient(filename, nat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat

    real(dp) :: res(3, nat)

    integer :: u, ierr, i, j, k
    character(len=256) :: buf

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Cartesian Gradient Breakdown over Individual Atoms :') /= 0) then
          read(u, *)
          read(u, *)
          read(u, *)
          do i=1, nat
             read(u, *) buf, j, (res(k, i), k = 1, 3)
          end do
          exit
       end if
    end do
    close(u)

    res = res / au2kcalm * au2ang
  end function tinker_get_gradient
  
  

end module mod_tinker_utils
