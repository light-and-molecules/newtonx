! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_sbh_t
  use mod_analytical_generic_1d_t, only: nx_analytical_generic_1d_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: cm2au, hbar, pi
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_sbh_t

  type, extends(nx_analytical_generic_1d_t) :: nx_sbh_t
     private
     real(dp) :: e0 = 12000.0_dp
     !! Energy separation between the 2 electronic states (cm-1).
     !! (Default: ``12000 cm-1``).
     real(dp) :: v0 = 800.0_dp
     !! Coupling between the 2 electronic states (cm-1).
     !! (Default: ``800 cm-1``).
     character(len=:), allocatable :: jw
     !! Type of spectral density: ``user``, ``debye`` or ``ohmic``.
     !! (Default: ``user``).
     real(dp) :: wc = 0.0_dp
     !! If ``jw`` is ``debye`` or ``ohmic``: resonance energy.
     !! (Default: ``0``).
     real(dp) :: wmax = 0.0_dp
     !! If ``jw`` is ``debye`` or ``ohmic``: Maximum energy.
     !! (Default: ``0``).
     real(dp) :: xi = 0.0_dp
     !! If ``jw`` is ``ohmic``: \(xi\) parameter.
     !! (Default: ``0``).
     real(dp) :: er = 0.0_dp
     !! If ``jw`` is ``debye``: \(E_R\) parameter.
     !! (Default: ``0``).
     real(dp), allocatable :: w(:)
     !! Vibrational levels.
     real(dp), allocatable :: g(:)
     !! Oscillator strengths.
   contains
     private
     procedure, public :: setup => sbh_setup
     procedure, public :: print => sbh_print
     procedure, public :: to_str => sbh_to_str
     procedure, public :: backup => sbh_backup
     procedure, public :: update => sbh_update
     procedure, public :: run => sbh_run
     procedure, public :: read_output => sbh_read_output
     procedure, public :: write_geom => sbh_write_geometry
     procedure, public :: init_overlap => sbh_init_overlap
     procedure, public :: prepare_overlap => sbh_prepare_overlap
     procedure, public :: extract_overlap => sbh_extract_overlap
     procedure, public :: ovl_post => sbh_ovl_post
     procedure, public :: ovl_run => sbh_ovl_run
     procedure, public :: get_lcao => sbh_get_lcao
     procedure, public :: cio_prepare_files => sbh_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & sbh_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => sbh_cio_get_mos_energies

     procedure :: read_g_w
     procedure :: debye_g_w
     procedure :: ohmic_g_w
     procedure :: repr_g_w
  end type nx_sbh_t
  interface nx_sbh_t
     module procedure constructor
  end interface nx_sbh_t

  character(len=*), parameter :: MODNAME = 'mod_sbh_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, &
       ! MANDATORY program- and method-specific parameters
       & nat, nstat, &
       ! OPTIONAL program- and method-specific parameters
       & e0, v0, jw, wc, wmax, xi, er, &
       & w, g &       
       & ) result(res)
    !! Constructor for the ``nx_sbh_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in) :: nat
    integer, intent(in) :: nstat
    real(dp), intent(in), optional :: e0 
    real(dp), intent(in), optional :: v0 
    character(len=*), intent(in), optional :: jw
    real(dp), intent(in), optional :: wc 
    real(dp), intent(in), optional :: wmax 
    real(dp), intent(in), optional :: xi 
    real(dp), intent(in), optional :: er 
    real(dp), intent(in), optional :: w(:)
    real(dp), intent(in), optional :: g(:)

    type(nx_sbh_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    call res%set_method(method)
    call res%memalloc(nat, nstat)

    ! Default to ``user``
    res%jw = 'user'

    if (present(e0)) res%e0 = e0
    if (present(v0)) res%v0 = v0
    if (present(jw)) res%jw = jw
    if (present(wc)) res%wc = wc
    if (present(wmax)) res%wmax = wmax
    if (present(xi)) res%xi = xi
    if (present(er)) res%er = er

    if (present(w)) then
       allocate(res%w(size(w)))
       res%w(:) = w(:)
    else
       allocate(res%w(nat))
       res%w = 0.0_dp
    end if

    if (present(g)) then
       allocate(res%g(size(g)))
       res%g(:) = g(:)
    else
       allocate(res%g(nat))
       res%g = 0.0_dp
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine sbh_setup(self, parser, conf, inp_path, stat)
    class(nx_sbh_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    call set(parser, 'sbh', self%e0, 'e0')
    call set(parser, 'sbh', self%v0, 'v0')
    call set(parser, 'sbh', self%jw, 'jw')

    call set(parser, 'sbh', self%wc, 'wc')
    call set(parser, 'sbh', self%wmax, 'wmax')
    call set(parser, 'sbh', self%xi, 'xi')
    call set(parser, 'sbh', self%er, 'er')

    self%e0 = self%e0 * cm2au
    self%v0 = self%v0 * cm2au
    self%wc = self%wc * cm2au
    self%wmax = self%wmax * cm2au
    self%er = self%er * cm2au

    ! We initialize g and w here, as the input file is available. For other cases, it will
    ! be done later, in the ``update`` routine, as ``masses`` will be available from
    ! ``traj`` there.
    if (self%jw == 'user') then
       call self%read_g_w(trim(inp_path)//'/user_sd_sbh.inp')
    end if
  end subroutine sbh_setup

  subroutine sbh_print(self, out)
    class(nx_sbh_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%e0, 'e0', unit=output)
    call print_conf_ele(self%v0, 'v0', unit=output)
    call print_conf_ele(self%jw, 'jw', unit=output)

    if (self%jw /= 'user') then
       call print_conf_ele(self%wc, 'wc',  unit=output)
       call print_conf_ele(self%wmax, 'wmax', unit=output)
    else if (self%jw == 'ohmic') then
       call print_conf_ele(self%xi, 'xi', unit=output)
    else if (self%jw == 'debye') then
       call print_conf_ele(self%er, 'er', unit=output)
    end if

    ! write(output, *) ''
    ! write(output, '(A)') self%repr_g_w()
  end subroutine sbh_print


  function sbh_to_str(self) result(res)
    class(nx_sbh_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&sbh'//nl
    res = res//' e0 = '//to_str(self%e0)//nl
    res = res//' v0 = '//to_str(self%v0)//nl
    res = res//' jw = '//trim(self%jw)//nl
    res = res//' wc = '//to_str(self%wc)//nl
    res = res//' wmax = '//to_str(self%wmax)//nl
    res = res//' xi = '//to_str(self%xi)//nl
    res = res//' er = '//to_str(self%er)//nl
    res = res//'/'//nl
  end function sbh_to_str
  

  subroutine sbh_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine sbh_backup

  subroutine sbh_update(self, conf, traj, path, stat)
    class(nx_sbh_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    if (traj%step == conf%init_step) then
       call self%set_masses(traj%masses)

       ! Populate g and w, only if ``jw = user`` (we don't have access to masses yet, so the
       ! initialization of the other parts will be done in the ``update_input`` routine).
       if (self%jw == 'debye') then
          call self%debye_g_w(self%masses())
       else if (self%jw == 'ohmic') then
          call self%ohmic_g_w(self%masses())
       end if

       write(*, *) ''
       write(*, '(A)') self%repr_g_w()
       
    end if
    
    call self%set_from_traj(traj%geom, traj%veloc)

  end subroutine sbh_update

  subroutine sbh_run(self, stat)
    class(nx_sbh_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: eta, fac
    real(dp) :: epot(2), grad(2, size(self%w)), nad(1, size(self%w))

    eta = sbh_compute_eta( self%e0, self%g, self%geom() )
    fac = sbh_compute_fac( eta, self%v0 )

    call self%set_epot( &
         & sbh_compute_energies(self%w, self%masses(), self%geom(), fac) &
         & )
    call self%set_grad( &
         & sbh_compute_gradients(&
         &    self%w, self%g, self%masses(), self%geom(), fac, eta &
         & ) &
         )
    call self%set_nad( sbh_compute_nac(self%g, self%v0, fac) )
  end subroutine sbh_run

  function sbh_read_output(self, conf, traj, path, stat) result(info)
    class(nx_sbh_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    real(dp) :: eta, fac
    real(dp) :: epot(2), grad(2, size(self%w)), nad(1, size(self%w))

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )
    info%has_osc_str = .false.
    info%has_data_qm = .false.

    info%repot(:) = self%epot()

    ! Transfer gradients
    info%rgrad(:, 1, :) = self%grad()

    ! Transfer NAD
    info%rnad(:, 1, :) = self%nad()

    ! CS-FSSH
    if (conf%run_complex) then
       info%rnad_i(:, 1, :) = self%nad_i()
       info%rgamma(:) = self%gamma()
    end if
  end function sbh_read_output

  subroutine sbh_write_geometry(self, traj, path, print_merged)
    class(nx_sbh_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged
  end subroutine sbh_write_geometry

  subroutine sbh_init_overlap(self, path_to_qm, stat)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine sbh_init_overlap

  subroutine sbh_prepare_overlap(self, path_to_qm, stat)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine sbh_prepare_overlap

  function sbh_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_sbh_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function sbh_extract_overlap

  function sbh_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function sbh_get_lcao

  subroutine sbh_ovl_run(self, stat)
    class(nx_sbh_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine sbh_ovl_run

  subroutine sbh_ovl_post(self, stat)
    class(nx_sbh_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine sbh_ovl_post

  subroutine sbh_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine sbh_cio_prepare_files

  subroutine sbh_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine sbh_cio_get_singles_amplitudes

  subroutine sbh_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_sbh_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine sbh_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  subroutine read_g_w(self, control)
    !! Read user provided harmonic oscillator energies and intensity.
    class(nx_sbh_t), intent(inout) :: self
    !! Nx_Qm object.
    character(len=*), intent(in) :: control
    !! File to read.

    integer :: u, i

    open(newunit=u, file=control, action='read')
    do i=1, size(self%w)
       read(u, *) self%w(i), self%g(i)
    end do
    close(u)

    self%w(:) = self%w(:) * cm2au
  end subroutine read_g_w

  subroutine debye_g_w(self, m)
    !! Generate a Debye spectral density.
    !!
    !! The spectral density is generated according to:
    !! \[ \omega_j = \mathrm{tan} \left ( \frac{j}{N}
    !! \mathrm{tan^{-1}} \left ( \frac{\omega_{max}}{\omega_c}
    !! \right ) \right ) \omega_c \]
    !! \[ g_j = \left [ \frac{M_j E_r}{\pi N} \mathrm{tan^{-1}} \left (
    !! \frac{\omega_{max}}{\omega_c} \right )  \right ]^{1/2}
    !! \omega_j \]
    class(nx_sbh_t), intent(inout) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: m(:)

    integer :: i, n
    real(dp) :: temp

    temp = atan(self%wmax / self%wc)

    n = size(self%w)

    do i=1, n
       ! First construct the energy \omega
       self%w(i) = tan(i*temp / n) * self%wc

       ! And then the strength g
       self%g(i) = sqrt( m(i) * self%er * temp / (pi * n) ) * self%w(i)
    end do
  end subroutine debye_g_w

  subroutine ohmic_g_w(self, m)
    !! Generate an ohmic spectral density.
    !!
    !! The spectral density is generated according to:
    !! \[ \omega_0 = \frac{\omega_c}{N} \left ( 1 -
    !! \mathrm{e}^{-\omega_{max}/\omega_c} \right ) \]
    !! \[ \omega_j = -\omega_c \mathrm{ln} \left ( 1 -
    !! j\frac{\omega_0}{\omega_c} \right ) \]
    !! \[ g_j = \left ( \xi \hbar \omega_0 M_j \right )^{1/2}
    !! \omega_c \]
    class(nx_sbh_t), intent(inout) :: self
    real(dp), intent(in) :: m(:)

    integer :: i, n
    real(dp) :: w0

    n = size(self%w)
    w0 = self%wc / n
    w0 = w0 * (1 - exp( -self%wmax / self%wc ))

    do i=1, n
       ! First construct the energy \omega
       self%w(i) = log( 1 - i * w0 / self%wc)
       self%w(i) = - self%w(i) * self%wc

       ! And then the strength g
       self%g(i) = sqrt(hbar * w0 * self%xi * m(i)) * self%w(i)
    end do
  end subroutine ohmic_g_w

  function repr_g_w(self) result(res)
    class(nx_sbh_t), intent(in) :: self

    character(len=:), allocatable :: res

    character(len=70) :: tmp
    integer :: i
    
    write(tmp, '(A10, 2A30)') 'i', 'w[i] (cm-1)', 'g[i] (hartree/bohr)'
    res = tmp//NEW_LINE('c')
    
    do i=1, size(self%w)
       write(tmp, '(I10, 2F30.12)') i, self%w(i) / cm2au, self%g(i)
       res = res//tmp//NEW_LINE('c')
    end do
  end function repr_g_w

  pure function sbh_compute_eta(e0, g, r) result(eta)
    !! Compute the \(\eta\) factor.
    !!
    !! \[ \eta = \left ( \sum_N g_j R_j \right ) + \epsilon_0 \]
    real(dp), intent(in) :: e0
    real(dp), intent(in) :: g(:)
    real(dp), intent(in) :: r(:)

    real(dp) :: eta

    integer :: j

    eta = e0 + sum( g(:) * r(:) )
  end function sbh_compute_eta

  pure function sbh_compute_fac(eta, v0) result(fac)
    !! Compute a useful factor in SBH.
    !!
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    real(dp), intent(in) :: eta
    real(dp), intent(in) :: v0

    real(dp) :: fac

    fac = sqrt(eta**2 + v0**2)
  end function sbh_compute_fac


  function sbh_compute_energies(w, m, r, fac) result(epot)
    !! Compute the SBH energies with the precomputed factor.
    !!
    !! We have:
    !! \[ E_i = \frac{1}{2} \sum_j^N M_j \omega_j^2 R_j^2 + (-1)^i f
    !!  (i=1, 2)\]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    real(dp), intent(in) :: w(:)
    real(dp), intent(in) :: m(:)
    real(dp), intent(in) :: r(:)
    real(dp), intent(in) :: fac

    real(dp) :: epot(2)
    !! Resulting potential energies.

    integer :: i, j
    real(dp) :: temp

    temp = sum( m(:) * w(:)**2 * r(:)**2 )
    do concurrent (i=1:2)
       epot(i) = 0.5_dp * temp + (-1)**i * fac
    end do
  end function sbh_compute_energies

  function sbh_compute_gradients(w, g, m, r, fac, eta) result(grad)
    !! Compute the SBH energies with the precomputed factor.
    !!
    !! We have:
    !! \[ \frac{\partial E_i}{\partial Q_k} = M_k \omega_k^2 R_k +
    !! (-1)^i g_k \left [ \frac{\eta}{f} \right ] (k=1, ..., N) \]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    real(dp), intent(in) :: w(:)
    real(dp), intent(in) :: g(:)
    real(dp), intent(in) :: m(:)
    real(dp), intent(in) :: r(:)
    real(dp), intent(in) :: fac
    real(dp), intent(in) :: eta

    real(dp) :: grad( 2, size(w) )

    integer :: i

    do concurrent(i=1:2)
       grad(i, :) = &
            & m(:) * w(:)**2 * r(:) &
            & + (-1)**i * g(:) * eta / fac
    end do
  end function sbh_compute_gradients

  function sbh_compute_nac(g, v0, fac) result(nac)
    !! Compute the SBH non-adiabatic couplings.
    !!
    !! We have:
    !! \[ F_{12}^k = -\frac{1}{2} g_k \left [ \frac{\nu_0}{f^2}
    !! \right] \]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    real(dp), intent(in) :: g(:)
    real(dp), intent(in) :: v0
    real(dp), intent(in) :: fac

    real(dp) :: nac(1, size(g) )

    ! We actually need F21 and not F12, so the minus sign cancels
    ! out from the expressions.
    nac(1, :) = 0.5_dp * g(:) * v0 / fac**2
  end function sbh_compute_nac
end module mod_sbh_t
