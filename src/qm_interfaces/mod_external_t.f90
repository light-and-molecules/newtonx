! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_external_t
  !! authors: Max Pinheiro Jr <max.pinheiro-jr@univ-amu.fr>
  !!          Eduarda Sangiogo <dudasangiogogil@hotmail.com>
  !!          Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-12-13
  !!1      2023-10-24
  !!
  !! This module contains routines used to communicate Newton-x with
  !! an external script. The goal is to give more flexibility to perform
  !! dynamics simulations with any QM program or a Machine Learning model.
  !! To this end, the path to an external script should be given in the
  !! configuration.inp file, and this script must provide the information
  !! required by Newton-X (energies, gradients, and non-adiabatic couplings)
  !! at every time step to run the dynamics.
  !!
  !! The script has to work with the following call:
  !!
  !!     $ script_name nat nstat nstatdyn
  !!
  !! and should write the files ``epot``, ``grad`` and ``nad_vectors``.
  !!
  !! These files should be organized, respectively, as:
  !!
  !! - ``epot``:
  !!
  !!         epot( state 1 )
  !!         epot( state 2 )
  !!         ...
  !!
  !! - ``grad``:
  !!
  !!         grad_x( atom 1 )   grad_y( atom 1 )  grad_z( atom 1 )
  !!         grad_x( atom 2 )   grad_y( atom 2 )  grad_z( atom 2 )
  !!         ...
  !!
  !! - ``nad_vectors``:
  !!
  !!         nad_x^{2 / 1} (atom 1)  nad_z^{2 / 1} (atom 1)  nad_z^{2 / 1} (atom 1)
  !!         nad_x^{2 / 1} (atom 2)  nad_z^{2 / 1} (atom 2)  nad_z^{2 / 1} (atom 2)
  !!         ...
  !!         nad_x^{3 / 2} (atom 2)  nad_z^{3 / 2} (atom 2)  nad_z^{3 / 2} (atom 2)
  !!         ...
  !!         nad_x^{3 / 1} (atom 2)  nad_z^{3 / 1} (atom 2)  nad_z^{3 / 1} (atom 2)
  !!         ...
  !!
  !!
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE, au2ang
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, call_external
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config, set_realloc => set_config_realloc
  use mod_print_utils, only: &
       & to_lower
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_external_t

  type, extends(nx_qm_generic_t) :: nx_external_t
     character(len=:), allocatable :: script_path
     character(len=:), allocatable :: geom_unit
     logical :: compute_nac = .true.
     integer :: nat = -1
     integer :: nstat = -1
     integer :: nstatdyn = -1
   contains
     private
     procedure, public :: setup => external_setup
     procedure, public :: print => external_print
     procedure, public :: to_str => external_to_str
     procedure, public :: backup => external_backup
     procedure, public :: update => external_update
     procedure, public :: run => external_run
     procedure, public :: read_output => external_read_output
     procedure, public :: write_geom => external_write_geometry
     procedure, public :: init_overlap => external_init_overlap
     procedure, public :: prepare_overlap => external_prepare_overlap
     procedure, public :: extract_overlap => external_extract_overlap
     procedure, public :: ovl_post => external_ovl_post
     procedure, public :: ovl_run => external_ovl_run
     procedure, public :: get_lcao => external_get_lcao
     procedure, public :: cio_prepare_files => external_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & external_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => external_cio_get_mos_energies
  end type nx_external_t
  interface nx_external_t
     module procedure constructor
  end interface nx_external_t

  character(len=*), parameter :: MODNAME = 'mod_external_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & script_path, compute_nac, &
       & geom_unit ) result(res)
    !! Constructor for the ``nx_external_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    character(len=:), allocatable, intent(in), optional :: script_path
    character(len=:), allocatable, intent(in), optional :: geom_unit
    logical, intent(in), optional :: compute_nac

    type(nx_external_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(script_path)) then
       res%script_path = trim(script_path)
    else
       res%script_path = ''
    end if

    if (present(geom_unit)) then
      res%geom_unit = trim(geom_unit)
    else
      res%geom_unit = 'bohr'
    end if

    if (present(compute_nac)) res%compute_nac = compute_nac
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine external_setup(self, parser, conf, inp_path, stat)
    class(nx_external_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo

    prt_mo = -1
    call set(parser, 'external', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    call set_realloc(parser, 'external', self%script_path, 'script_path')
    call set_realloc(parser, 'external', self%geom_unit, 'geom_unit')
    call set(parser, 'external', self%compute_nac, 'compute_nac')
  end subroutine external_setup

  subroutine external_print(self, out)
    class(nx_external_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%compute_nac, 'compute NAC', unit=output)
    call print_conf_ele(self%script_path, 'script path', unit=output)
    call print_conf_ele(self%geom_unit, 'geom unit', unit=output)
  end subroutine external_print


  function external_to_str(self) result(res)
    class(nx_external_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&external'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' script_path = '//trim(self%script_path)//nl
    res = res//' geom_unit = '//trim(self%geom_unit)//nl
    res = res//' compute_nac = '//to_str(self%compute_nac)//nl
    res = res//'/'//nl
  end function external_to_str

  subroutine external_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine external_backup

  subroutine external_update(self, conf, traj, path, stat)
    class(nx_external_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    self%nat = conf%nat
    self%nstat = conf%nstat
    self%nstatdyn = traj%nstatdyn
    
  end subroutine external_update

  subroutine external_run(self, stat)
    class(nx_external_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    character(len=:), allocatable :: cmd
    integer :: ierr
    character(len=256) :: cmdmsg
    
    cmd = self%script_path//' '//to_str(self%nat)//' '//to_str(self%nstat)//' '//to_str(self%nstatdyn)

    ierr = 0
    call call_external(cmd, ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running external script'//trim(cmdmsg)//&
            & '. Please take a look at external.job/ files.', & 
            & mod=MODNAME, func='external_run'&
            & )
    end if
  end subroutine external_run

  function external_read_output(self, conf, traj, path, stat) result(info)
    class(nx_external_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    integer :: ncoup
    integer :: u, i, j

    real(dp) :: gx,gy,gz

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       ! For instance, use something like:
       ! info%orb = self%read_orb( path )
    end if

    open(newunit=u, file=trim(path)//'/epot', action='read', status='old')
    do i = 1, conf%nstat
       read(u,*) info%repot(i)
    end do
    close(u)

    open(newunit=u, file=trim(path)//'/grad', action='read', status='old')
    do i = 1, conf%nat
       read(u,*) gx,gy,gz
       info%rgrad(traj%nstatdyn,1,i) = gx
       info%rgrad(traj%nstatdyn,2,i) = gy
       info%rgrad(traj%nstatdyn,3,i) = gz
    end do
    close(u)

    ! NAD
    if (self%compute_nac) then
       ncoup = conf%nstat * (conf%nstat - 1)/2
       open(newunit=u, file=trim(path)//'/nad_vectors', action='read', status='old')
       do i = 1, ncoup
          do j = 1, conf%nat
             read(u,*) gx,gy,gz
             info%rnad(i,1,j) = gx
             info%rnad(i,2,j) = gy
             info%rnad(i,3,j) = gz
          end do
       end do
       close(u)
    end if

    info%has_osc_comp = .false.
    info%has_osc_str = .false.
  end function external_read_output


  subroutine external_write_geometry(self, traj, path, print_merged)
    class(nx_external_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    real(dp) :: unit_conv
    integer :: i, u, j, n_atoms

    n_atoms = size(traj%geom, 2)

    if (to_lower(self%geom_unit(1:1)) == 'a') then
      unit_conv = au2ang
    else
      unit_conv = 1.0_dp
    end if

    open(newunit=u, file=trim(path)//'/geom.xyz', action='write')
    rewind u
    write(u, *) n_atoms
    write(u, *)
    do i=1, n_atoms
      write(u, '(A4, 3(F14.8, 2X))') traj%atoms(i), &
      &                (traj%geom(j, i) * unit_conv, j=1, 3)
    end do
    close(u)
  end subroutine external_write_geometry


  subroutine external_init_overlap(self, path_to_qm, stat)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine external_init_overlap

  subroutine external_prepare_overlap(self, path_to_qm, stat)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine external_prepare_overlap

  function external_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_external_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function external_extract_overlap

  function external_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function external_get_lcao

  subroutine external_ovl_run(self, stat)
    class(nx_external_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine external_ovl_run

  subroutine external_ovl_post(self, stat)
    class(nx_external_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine external_ovl_post

  subroutine external_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine external_cio_prepare_files

  subroutine external_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine external_cio_get_singles_amplitudes

  subroutine external_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_external_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine external_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  
end module mod_external_t
