! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_qm_item_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE
  use mod_exc_gaussian_t, only: nx_exc_gaussian_t
  use mod_input_parser, only: parser_t
  use mod_interface, only: copy, mkdir
  use mod_kinds, only: dp
  use mod_logger, only: &
       & check_error, &
       & nx_log, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_TRIVIA, LOG_DEBUG
  use mod_orbspace, only: nx_orbspace_t
  use mod_print_utils, only: to_upper
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_status_t, only: nx_status_t
  use mod_tools, only: &
       & phase_adjust_h_vec, to_str
  use mod_trajectory, only: nx_traj_t
  use iso_fortran_env, only: stdout => output_unit

  ! Individual QM interfaces
  use mod_tinker_mndo_t, only: nx_tinker_mndo_t
  implicit none

  private

  public :: nx_qm_item_t

  type :: nx_qm_item_t
     private
     class(nx_qm_generic_t), allocatable, public :: qm
     !! QM-object, containing a description of the QM job (see [[mod_qm_generic_t.f90]]
     !! for the details).
     character(len=:), allocatable :: progname_
     ! character(len=:), allocatable :: methodname_
     character(len=:), allocatable :: run_dir_
     !! Path to the folder where the QM code is running (relative to ``TEMP``).     
     character(len=:), allocatable :: chk_dir_
     !! Path to the directory where to save checkpoint files (relative to ``TEMP``).
     character(len=:), allocatable :: inp_dir_
     !! Path where to find the set of input files for this QM job (relative to ``TEMP``).
     type(nx_orbspace_t) :: orb_
     !! Orbital space.
     type(nx_status_t) :: stat
     !! Status indicator
     logical, public :: request_adapt_dt = .false.

     ! Flags for running QM
     logical :: read_nac_ = .false.
     !! Indicate if the non-adiabatic coupling vectors should be read from the QM
     !! program. This is used for Columbus for instance, where NACV will be computed
     !! (MRSCF), but are not to be read for some reason (when using local-diabatization).
     logical :: is_test_ = .false.
     !! Indicate if the current job is part of the testsuite.
     logical :: need_external_call_ = .true.
     !! Indicate if the job needs calling an external program. Typically it is set to
     !! ``.false.`` only for analytical models.
     logical :: need_copy_inputs_ = .true.
     !! Indicate if we need to copy inputs from ``inp_dir`` to ``run_dir`` at every step.
     logical :: ovl_required_ = .true.
     !! Indicate if an overlap ("double-molecule") computation should be carried out.
     logical :: is_qmmm_ = .false.
     !! Indicate if the comptation is done in QM/MM (for Tinker interfaces).
     logical :: produce_cio_ = .false.
     !! Indicate if the run produces the state overlap matrix by itself. For NX to work
     !! properly, the state overlap file should be named ``overlap.out``, in the working
     !! directory, and have the following format:
     !!
     !! ```
     !! CI overlap matrix
     !! ele(1, 1) ele(1, 2) ... ele(1, N)
     !! ...
     !! ele(N, 1) ele(N, 2) ... ele(N, N)
     !!```

     ! Specific things for some interface
     real(dp), public, allocatable :: epot_chrom(:, :)
     !! EXCITON MODEL: electronic potential energies for individual chromophores. The
     !! array should have dimensions ``(nchromophores, max( nstat_array ))``.
     integer, public, allocatable :: nstat_array(:)
     !! EXCITON MODEL: number of states computed for each chromophore.

   contains
     private
     ! Getters for members of ``item``
     procedure, public :: progname => qm_progname
     procedure, public :: run_dir => qm_run_dir
     procedure, public :: chk_dir => qm_chk_dir
     procedure, public :: inp_dir => qm_inp_dir
     procedure, public :: read_nac => qm_read_nac
     procedure, public :: is_test => qm_is_test
     procedure, public :: need_external_call => qm_need_external_call
     procedure, public :: need_copy_inputs => qm_need_copy_inputs
     procedure, public :: ovl_required => qm_ovl_required
     procedure, public :: orb => qm_orb
     procedure, public :: is_qmmm => qm_is_qmmm
     procedure, public :: produce_cio => qm_produce_cio

     ! Processing routines
     procedure, public :: check_error => qm_check_error
     
     ! Setters
     procedure, public :: set_orbspace => qm_set_orbspace

     ! Interface to ``qm_generic`` procedures
     procedure, public :: setup => qm_item_setup
     procedure, public :: print => qm_item_print
     procedure, public :: backup => qm_backup
     procedure, public :: update => qm_update
     procedure, public :: run => qm_run
     procedure, public :: read_output => qm_read_output
     procedure, public :: write_geom => qm_write_geom

     ! Overlap routines
     procedure, public :: ovl_init => qm_ovl_init
     procedure, public :: ovl_prepare => qm_ovl_prepare
     procedure, public :: ovl_run => qm_ovl_run
     procedure, public :: ovl_post => qm_ovl_post
     procedure, public :: get_ovl => qm_get_ovl
     procedure, public :: get_lcao => qm_get_lcao

     ! State overlap matrix routines
     procedure, public :: cio_prepare_files => qm_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => qm_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => qm_cio_get_mos_energies

     ! General interface with other objects
     procedure, public :: set_qmmm_in_traj => qm_set_qmmm_in_traj
     procedure, public :: transfer_to_traj => qm_update_trajectory
     procedure, public :: import_orb => qm_import_orb
  end type nx_qm_item_t
  interface nx_qm_item_t
     module procedure constructor
  end interface nx_qm_item_t
  

contains

  pure function constructor(&
       & qm, progname, run_dir, inp_dir, chk_dir, &
       & orb, &
       & read_nac, is_test, need_external_call, need_copy_inputs, &
       & ovl_required, is_qmmm, produce_cio &
       & ) result(qm_item)
    class(nx_qm_generic_t), intent(in) :: qm
    character(len=*), intent(in) :: progname
    character(len=*), intent(in) :: run_dir
    character(len=*), intent(in) :: inp_dir
    character(len=*), intent(in) :: chk_dir
    type(nx_orbspace_t), intent(in), optional :: orb
    logical, intent(in), optional :: read_nac
    logical, intent(in), optional :: is_test
    logical, intent(in), optional :: need_external_call
    logical, intent(in), optional :: need_copy_inputs
    logical, intent(in), optional :: ovl_required
    logical, intent(in), optional :: is_qmmm
    logical, intent(in), optional :: produce_cio

    type(nx_qm_item_t) :: qm_item

    allocate(qm_item%qm, source=qm)

    qm_item%progname_ = progname
    qm_item%run_dir_ = run_dir
    qm_item%inp_dir_ = inp_dir
    qm_item%chk_dir_ = chk_dir

    if (present(orb)) qm_item%orb_ = orb
    if (present(read_nac)) qm_item%read_nac_ = read_nac
    if (present(is_test)) qm_item%is_test_ = is_test
    if (present(need_copy_inputs)) qm_item%need_copy_inputs_ = need_copy_inputs
    if (present(need_external_call)) qm_item%need_external_call_ = need_external_call
    if (present(ovl_required)) qm_item%ovl_required_ = ovl_required
    if (present(is_qmmm)) qm_item%is_qmmm_ = is_qmmm
    if (present(produce_cio)) qm_item%produce_cio_ = produce_cio
  end function constructor

  ! =====================
  ! Getters for QM object
  ! =====================

  ! =======================
  ! Getters for self object
  ! =======================
  pure function qm_progname(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    character(len=:), allocatable :: res

    res = self%progname_
  end function qm_progname

  pure function qm_run_dir(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    character(len=:), allocatable :: res

    res = self%run_dir_
  end function qm_run_dir

  pure function qm_inp_dir(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    character(len=:), allocatable :: res

    res = self%inp_dir_
  end function qm_inp_dir

  pure function qm_chk_dir(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    character(len=:), allocatable :: res

    res = self%chk_dir_
  end function qm_chk_dir

  pure function qm_read_nac(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%read_nac_
  end function qm_read_nac

  pure function qm_is_test(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%is_test_
  end function qm_is_test

  pure function qm_need_copy_inputs(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%need_copy_inputs_
  end function qm_need_copy_inputs

  pure function qm_need_external_call(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%need_external_call_
  end function qm_need_external_call

  pure function qm_ovl_required(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%ovl_required_
  end function qm_ovl_required

  pure function qm_orb(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    type(nx_orbspace_t) :: res

    res = self%orb_
  end function qm_orb

  pure function qm_is_qmmm(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%is_qmmm_
  end function qm_is_qmmm

  pure function qm_produce_cio(self) result(res)
    class(nx_qm_item_t), intent(in) :: self

    logical :: res

    res = self%produce_cio_
  end function qm_produce_cio

  ! =======================
  ! Setters for self object
  ! =======================
  subroutine qm_set_orbspace(self, orb)
    class(nx_qm_item_t), intent(inout) :: self
    type(nx_orbspace_t), intent(in) :: orb

    self%orb_ = orb
  end subroutine qm_set_orbspace


  ! ===================
  ! Processing routines
  ! ===================
  function qm_check_error(self) result(res)
    class(nx_qm_item_t), intent(inout) :: self

    integer :: res

    character(len=:), allocatable :: tmp

    res = 0
    if (self%stat%has_warning()) then
       call nx_log%log(LOG_WARN, 'Warnings encountered: ')
       print *, self%stat%filter_warnings()
    end if
    
    if (self%stat%has_error()) then
       call nx_log%log(LOG_ERROR, 'Fatal error encountered: ')
       print *, self%stat%filter_errors()
       res = 1
    end if
  end function qm_check_error

  subroutine qm_set_qmmm_in_traj(self, traj)
    class(nx_qm_item_t), target, intent(in) :: self
    type(nx_traj_t), intent(inout) :: traj

    class(nx_qm_generic_t), pointer :: qm

    qm => self%qm
    select type(qm)
    type is (nx_tinker_mndo_t)
       if (allocated(traj%is_qm_atom)) deallocate(traj%is_qm_atom)
       allocate(traj%is_qm_atom, source=qm%qm_filter)
       traj%is_qmmm = .true.
    end select
  end subroutine qm_set_qmmm_in_traj
  


  ! =======================================
  ! Interfaces to ``qm_generic`` procedures
  ! =======================================
  subroutine qm_item_setup(self, parser, conf)
    class(nx_qm_item_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf

    call self%stat%reset()
    call self%qm%setup(parser, conf, self%inp_dir_, self%stat)
  end subroutine qm_item_setup
  

  subroutine qm_item_print(self, out)
    class(nx_qm_item_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, '(A)') repeat('*', 80)
    write(output, '(A)') &
         & '  Electronic structure configuration using '//to_upper(self%progname())
    call self%qm%print(out=output)
    write(output, '(A)') repeat('*', 80)
  end subroutine qm_item_print


  subroutine qm_backup(self, conf, traj)
    class(nx_qm_item_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    logical :: only_mo, all_files, ext
    character(len=:), allocatable :: chk_dir
    integer :: ierr

    all_files = conf%save_cwd > 0 .and. mod(traj%step, conf%save_cwd) == 0
    only_mo = (self%qm%prt_mo() > 0 .and. mod(traj%step, self%qm%prt_mo()) == 0) &
         & .and. .not. all_files

    if (only_mo .or. all_files) then
       
       if (traj%is_virtual) then
          chk_dir = 'debug/'//self%progname()//'.'//to_str(traj%step)
       else
          chk_dir = self%chk_dir()//'/'//self%progname()//'.'//to_str(traj%step)   
       end if

       inquire(file=chk_dir, exist=ext)
       ! If the directory already exists, it means that we are at the second run, so update
       ! the name accordingly (for instance, a surface hopping occurs)
       if (ext) then
          chk_dir = chk_dir//'.2'
       end if

       ierr = mkdir(chk_dir)
       call check_error(ierr, 101, &
            & 'Cannot create '//chk_dir, system=.true.)

       call self%stat%reset()
       call self%qm%backup( self%run_dir(), chk_dir, self%stat, only_mo)
    end if
  end subroutine qm_backup


  subroutine qm_update(self, conf, traj)
    class(nx_qm_item_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    logical :: ext
    integer :: ierr

    if (self%need_external_call()) then
       inquire(file=self%run_dir(), exist=ext)
       if (ext) then
          inquire(file=self%run_dir()//'.old', exist=ext)
          if (ext) then
             call execute_command_line('rm -rf '//self%run_dir()//'.old')
          end if

          ierr = rename(self%run_dir(), self%run_dir()//'.old')
       end if

       ierr = mkdir( self%run_dir() )
    end if

    if (self%need_copy_inputs()) then
       call execute_command_line('cp -r '//self%inp_dir_//'/* '//self%run_dir())
    end if
    call self%write_geom(traj, self%run_dir())

    call self%stat%reset()
    call self%qm%update(conf, traj, self%run_dir(), self%stat)
  end subroutine qm_update


  subroutine qm_run(self)
    class(nx_qm_item_t), intent(inout) :: self

    character(len=:), allocatable :: dir
    logical :: ext
    integer :: ierr
    character(len=1024) :: curdir

    ierr = getcwd( curdir )
    dir = self%run_dir()

    call self%stat%reset()

    if (self%need_external_call()) ierr = chdir(dir)
    call self%qm%run( self%stat )
    if (self%need_external_call()) ierr = chdir(curdir)

    ! inquire(file=dir, exist=ext)
    ! if (.not. ext) then
    !    print *, 'DIR '//dir//' DOES NOT EXIST !'
    !    error stop
    ! else

    ! end if
  end subroutine qm_run


  function qm_read_output(self, conf, traj) result(info)
    class(nx_qm_item_t), target, intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    type(nx_qminfo_t) :: info

    real(dp), allocatable :: cossine(:)
    integer, allocatable :: phase(:)
    integer :: i

    class(nx_qm_generic_t), pointer :: qm

    call self%stat%reset()
    info = self%qm%read_output(conf, traj, self%run_dir(), self%stat)
    if (self%progname() == 'analytical') info%correct_phase = .false.
    qm => self%qm
    select type( qm )
    type is (nx_exc_gaussian_t)
       
       if (allocated(self%epot_chrom)) deallocate(self%epot_chrom)
       allocate(self%epot_chrom, source=info%epot_chrom)

       if (traj%step == conf%init_step) then
          allocate(self%nstat_array, source=qm%nstat_array)
       end if
    end select

    ! Correct NAD values
    if (self%read_nac() .and. info%correct_phase) then
       allocate(phase(size(traj%nad, 1)))
       allocate(cossine(size(traj%nad, 1)))
       call phase_adjust_h_vec(traj%nad, info%rnad, phase, cossine)
       deallocate(phase, cossine)
    end if
    if (self%is_test()) then
       if (traj%step == conf%init_step) then
          do i=1, size(info%rnad, 1)
             if (info%rnad(i, 1, 1) < 0) then
                info%rnad(i, :, :) = -1.0_dp * info%rnad(i, :, :)
             end if
          end do
       end if
    end if
  end function qm_read_output


  subroutine qm_write_geom(self, traj, path, print_merged)
    class(nx_qm_item_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    logical :: merge_coord
    
    merge_coord = .false.
    if (present(print_merged)) merge_coord = print_merged

    call self%qm%write_geom(traj, path, print_merged=merge_coord)
  end subroutine qm_write_geom


  subroutine qm_ovl_prepare(self)
    class(nx_qm_item_t), intent(inout) :: self

    call self%stat%reset()
    call self%qm%prepare_overlap( self%run_dir(), self%stat )
  end subroutine qm_ovl_prepare


  subroutine qm_ovl_init(self, path_to_qm)
    class(nx_qm_item_t), intent(inout) :: self
    character(len=*), intent(in) :: path_to_qm

    call self%stat%reset()
    call self%qm%init_overlap(path_to_qm, self%stat)
  end subroutine qm_ovl_init


  subroutine qm_ovl_run(self)
    class(nx_qm_item_t), intent(inout) :: self

    call self%stat%reset()
    call self%qm%ovl_run( self%stat )
  end subroutine qm_ovl_run
  
  
  subroutine qm_ovl_post(self)
    class(nx_qm_item_t), intent(inout) :: self

    call self%stat%reset()
    call self%qm%ovl_post( self%stat )
  end subroutine qm_ovl_post
  

  function qm_get_ovl(self, dim_ovl, script_path) result(res)
    class(nx_qm_item_t), intent(inout) :: self
    integer, intent(in) :: dim_ovl
    character(len=*), intent(in), optional :: script_path
    real(dp) :: res(dim_ovl)

    call self%stat%reset()
    res = self%qm%extract_overlap(dim_ovl, self%stat, script_path=script_path)
  end function qm_get_ovl


  function qm_get_lcao(self, dir_path, nao) result(res)
    class(nx_qm_item_t), intent(inout) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    real(dp) :: res(nao, nao)

    call self%stat%reset()
    res = self%qm%get_lcao(dir_path, nao, self%stat)
  end function qm_get_lcao

  subroutine qm_cio_prepare_files(self)
    class(nx_qm_item_t), intent(inout) :: self
    ! character(len=*), intent(in) :: path_to_qm
    ! type(nx_orbspace_t), intent(in) :: orb

    call self%stat%reset()
    call self%qm%cio_prepare_files( self%run_dir(), self%orb(), self%stat)
  end subroutine qm_cio_prepare_files

  subroutine qm_cio_get_singles_amplitudes(self, tia, tib)
    class(nx_qm_item_t), intent(inout) :: self
    ! character(len=*), intent(in) :: path_to_qm
    ! type(nx_orbspace_t), intent(in) :: orb
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)

    call self%stat%reset()
    call self%qm%cio_get_singles_amplitudes(&
         & self%run_dir(), self%orb(), tia, self%stat, tib=tib)
  end subroutine qm_cio_get_singles_amplitudes

  subroutine qm_cio_get_mos_energies(self, mos)
    class(nx_qm_item_t), intent(inout) :: self
    ! character(len=*), intent(in) :: path_to_qm
    real(dp), intent(out) :: mos(:)

    call self%stat%reset()
    call self%qm%cio_get_mos_energies( self%run_dir(), self%stat, mos)
  end subroutine qm_cio_get_mos_energies


  ! ====================================
  ! General interface with other objects
  ! ====================================
  subroutine qm_update_trajectory(self, qminfo, traj, config, update_epot, report)
    class(nx_qm_item_t), intent(inout) :: self
    type(nx_qminfo_t), intent(in) :: qminfo
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(in) :: config
    logical, intent(in), optional :: update_epot
    logical, intent(in), optional :: report

    logical :: print_info, update_epot_

    integer :: i, nc

    print_info = .true.
    if (present(report)) print_info = report

    update_epot_ = .true.
    if (present(update_epot)) update_epot_ = update_epot

    if (traj%step == config%init_step) then
       call self%set_orbspace( qminfo%orb )
    end if

    ! Backup trajectory components
    if (update_epot_) then
       traj%old_epot(3, :) = traj%old_epot(2, :)
       traj%old_epot(2, :) = traj%old_epot(1, :)
       traj%old_epot(1, :) = traj%epot
       traj%epot(:) = qminfo%repot(:)

       if (traj%run_complex) then
          ! The other cases are already dealt with in `mod_csfssh`, in routine
          ! `csfssh_compute_gamma`.
          if (config%gamma_model == 0) then
             traj%old_gamma(3, :) = traj%old_gamma(2, :)
             traj%old_gamma(2, :) = traj%old_gamma(1, :)
             traj%old_gamma(1, :) = traj%gamma
             traj%gamma(:) = qminfo%rgamma(:)
          end if
       end if
    end if

    traj%old_acc(:, :) = traj%acc(:, :)
    traj%old_grad(:, :, :) = traj%grad(:, :, :)
    traj%grad(:, :, :) = qminfo%rgrad(:, :, :)

    do i=1, size(traj%acc, 2)
       traj%acc(:, i) = traj%grad(traj%nstatdyn, :, i)
       traj%acc(:, i) = (-1.0_dp / traj%masses(i)) * traj%acc(:, i)
    end do

    if (qminfo%update_nad) then
       traj%old_nad = traj%nad
       traj%nad(:, :, :) = qminfo%rnad(:, :, :)

       if (traj%run_complex) then
          traj%old_nad_i = traj%nad_i
          traj%nad_i(:, :, :) = qminfo%rnad_i(:, :, :)
       end if
    end if

    nc = 1
    do i=1, config%nstat - 1
       traj%osc(i) = qminfo%osc_str(nc)
       nc = nc + 1
    end do

    if (qminfo%use_exciton) then
       traj%diapop = qminfo%diab_pop
       traj%diaham = qminfo%diab_ham
       traj%diaen = qminfo%diab_en
    end if

    self%request_adapt_dt = qminfo%request_adapt_dt
    
    if (print_info) then
       call traj%print_epot_all()

       if (qminfo%has_data_qm) &
            call nx_log%log(LOG_INFO, qminfo%dataread)

       if (qminfo%has_osc_str) &
            call nx_log%log(LOG_INFO, qminfo%format_osc_str())
    end if
  end subroutine qm_update_trajectory


  subroutine qm_import_orb(self, filename)
    class(nx_qm_item_t), intent(inout) :: self
    character(len=*), intent(in) :: filename

    call self%orb_%import_orb(filename)
  end subroutine qm_import_orb
  
end module mod_qm_item_t
