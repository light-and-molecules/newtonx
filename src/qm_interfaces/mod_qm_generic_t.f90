! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_qm_generic_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2023-09-07
  !!
  !! # Generic implementation of QM interfaces
  !!
  !! This module defines an abstract type for defining arbitrary interfaces with various
  !! QM codes in Newton-X. The goal is to simplify and streamline as much as possible the
  !! use of QM interfaces to:
  !!
  !! - Setup, run and extract information (energies, gradients, ...) from QM jobs ;
  !! - Provide routines to compute overlaps between successive steps (e.g. for state-overlap
  !!   matrix computation) ;
  !! - Provide routines to extract valuable supplementary information from QM outputs
  !!   (amplitudes of single excitations, energies of MOs).
  !!
  !! ## Usage
  !!
  !! ```f90
  !! use mod_nx_qm_generic_t, only: nx_qm_generic_t
  !!
  !! type, extends(nx_qm_generic_t) :: nx_myinterface_t
  !!     integer :: my_param_1
  !!     character(len=:), allocatable :: my_param_2
  !!  contains
  !!    private
  !!    ! Implementation of deferred procedures
  !!    procedure, public :: setup
  !!    ...
  !!    ! Other type-bound specific procedures
  !!    procedure :: my_helper_private_proc
  !! end type
  !! ```
  !!
  !! ## Implementation
  !!
  !! The QM objects in Newton-X should always assume that they exist in the top directory
  !! (``TEMP``) when it comes to file manipulation. They should also avoid handling
  !! directories, as it is better suited for the higher level ``nx_qm_item_t`` object
  !! (see the corresponding documentation).
  !!
  !! The generic QM object has the following (private) attributes defined:
  !!
  !! - ``method_``: a label for the method being used (see ``mod_implemented.f90`` for the
  !!   list of method implemented for each QM program) ;
  !! - ``prt_mo_``: frequency at which the MOs should be saved (the exact files will vary
  !!   from one program to another) ;
  !! - ``stat_``: a status indicator, that should be ``0`` when everythin went all right,
  !!   ``< 0`` when an error occured and ``> 0`` if a warning should be issued ;
  !! - ``msg_``: a message describing the warning / error that is reported (several of
  !!   those may be chained with the ``append_msg`` routine).
  !!
  !! All components are private by default, and may be accessed with functions having the
  !! same name as the members minus the final ``_`` (e.g. ``method_`` is accessed by
  !! ``%method()``), and set with a routine starting with ``set_`` (e.g. ``method_`` is
  !! set by ``%set_method( 'method_name' )``). The only exception is for the ``msg_``
  !! member, where messages are appended with the ``%append_msg( 'msg' )`` routine.
  !!
  !! ## Summary of deferred routines
  !!
  !! General routines:
  !!
  !! - ``setup``: setup the QM computation based on parsed input NX file and
  !!   configuration ;
  !! - ``print``: print information about the QM job ;
  !! - ``backup``: copy QM outputs to ``DEBUG`` directory ;
  !! - ``update``: update the QM input files with information from trajectory ;
  !! - ``run``: run the QM computation ;
  !! - ``read_output``: parse the output from the QM job ;
  !! - ``write_geom``: write the coordinates in the format expected by the QM program.
  !!
  !! Routines for handling overlap computation between current and previous step:
  !!
  !! - ``init_overlap``: initialize the files for double-molecule computation ;
  !! - ``prepare_overlap``: prepare the actual computation of AO overlap ;
  !! - ``extract_overlap``: extract the AO overlap matrxi from outputs ;
  !! - ``get_lcao``: extract the AO to MO transformation matrix from the QM job ;
  !! - ``ovl_post``: routine to run after the computation of overlap.
  !!
  !! Routines for handling interaction with the ``cioverlap`` program (state overlap
  !! matrix computations):
  !! 
  !! - ``cio_prepare_files``: prepare the QM-specific files for ``cioverlap``
  !! - ``cio_get_singles_amplitudes``: extract single excitation amplitudes from QM
  !!   output ;
  !! - ``cio_get_mos_energies``: extract energies of MOs from QM outputs.
  !!
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE
  use mod_kinds, only: dp
  use mod_input_parser, only: parser_t
  use mod_orbspace, only: nx_orbspace_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_status_t, only: nx_status_t
  use mod_trajectory, only: nx_traj_t

  implicit none

  private

  public :: nx_qm_generic_t

  type, abstract :: nx_qm_generic_t
     !! Generic QM interface type.
     !!
     !! This type defines the general interface that other objects have to
     !! conform to, 
     private
     character(len=:), allocatable :: method_
     !! Method being used (e.g. 'TDDFT' for Turbomole for instance).
     integer :: prt_mo_ = 20
     !! Save MO file every ``prt_mo`` steps. The files saved should be specified
     !! individually by each interface, in the ``backup`` routine.
   contains
     private
     ! Deferred procedure to be implemented by individual interfaces
     procedure(abs_setup), deferred, public :: setup
     procedure(abs_print), deferred, public :: print
     procedure(abs_to_str), deferred, public :: to_str
     procedure(abs_backup), deferred, public :: backup
     procedure(abs_update), deferred, public :: update
     procedure(abs_run), deferred, public :: run
     procedure(abs_read_output), deferred, public :: read_output
     procedure(abs_write_geometry), deferred, public :: write_geom

     ! Deferred procedures for overlap (double-molecule) computation
     procedure(abs_init_overlap), deferred, public :: init_overlap
     procedure(abs_prepare_overlap), deferred, public :: prepare_overlap
     procedure(abs_extract_overlap), deferred, public :: extract_overlap
     procedure(abs_get_lcao), deferred, public :: get_lcao
     procedure(abs_ovl_run), deferred, public :: ovl_run
     procedure(abs_ovl_post), deferred, public :: ovl_post

     ! Deferred procedures for state-overlap computation (when required)
     procedure(abs_cio_prepare_files), deferred, public :: cio_prepare_files
     procedure(abs_cio_get_singles_amplitudes), deferred, public :: &
          & cio_get_singles_amplitudes
     procedure(abs_cio_get_mos_energies), deferred, public :: &
          & cio_get_mos_energies

     ! General getter procedures
     procedure, public :: method => get_method
     procedure, public :: prt_mo => get_prt_mo

     ! General setter procedures
     procedure, public :: set_method
     procedure, public :: set_prt_mo
  end type nx_qm_generic_t

  abstract interface
     subroutine abs_setup(self, parser, conf, inp_path, stat)
       import :: nx_qm_generic_t, parser_t, nx_config_t, nx_status_t

       class(nx_qm_generic_t), intent(inout) :: self
       type(parser_t), intent(in) :: parser
       type(nx_config_t), intent(in) :: conf
       character(len=*), intent(in) :: inp_path
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_setup

     subroutine abs_print(self, out)
       import :: nx_qm_generic_t

       class(nx_qm_generic_t), intent(in) :: self
       integer, intent(in), optional :: out
     end subroutine abs_print

     function abs_to_str(self) result(res)
       import :: nx_qm_generic_t

       class(nx_qm_generic_t), intent(in) :: self

       character(len=:), allocatable :: res
     end function abs_to_str

     subroutine abs_backup(self, qm_path, chk_path, stat, only_mo)
       import :: nx_qm_generic_t, nx_config_t, nx_traj_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: qm_path
       character(len=*), intent(in) :: chk_path
       type(nx_status_t), intent(inout) :: stat
       logical, intent(in), optional :: only_mo
     end subroutine abs_backup

     subroutine abs_update(self, conf, traj, path, stat)
       import :: nx_qm_generic_t, nx_config_t, nx_traj_t, nx_status_t

       class(nx_qm_generic_t), intent(inout) :: self
       type(nx_config_t), intent(in) :: conf
       type(nx_traj_t), intent(in) :: traj
       character(len=*), intent(in) :: path
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_update

     subroutine abs_run(self, stat)
       import :: nx_qm_generic_t, nx_status_t

       ! Set to ``inout`` for analytical models, where we need to populate the components
       ! of the object.
       ! ``self`` should NOT be modified here without very good reasons !
       class(nx_qm_generic_t), intent(inout) :: self
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_run

     function abs_read_output(self, conf, traj, path, stat) result(info)
       import :: nx_qm_generic_t, nx_config_t, nx_traj_t, nx_qminfo_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       type(nx_config_t), intent(in) :: conf
       type(nx_traj_t), intent(in) :: traj
       character(len=*), intent(in) :: path
       type(nx_status_t), intent(inout) :: stat

       type(nx_qminfo_t) :: info
     end function abs_read_output

     subroutine abs_write_geometry(self, traj, path, print_merged)
       import nx_qm_generic_t, nx_traj_t
       
       class(nx_qm_generic_t), intent(in) :: self
       type(nx_traj_t), intent(in) :: traj
       character(len=*), intent(in) :: path
       logical, intent(in), optional :: print_merged
     end subroutine abs_write_geometry
     

     subroutine abs_init_overlap(self, path_to_qm, stat)
       import nx_qm_generic_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: path_to_qm
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_init_overlap

     subroutine abs_prepare_overlap(self, path_to_qm, stat)
       import nx_qm_generic_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: path_to_qm
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_prepare_overlap

     function abs_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
       import nx_qm_generic_t, dp, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       integer, intent(in) :: dim_ovl
       type(nx_status_t), intent(inout) :: stat
       character(len=*), intent(in), optional :: script_path
       real(dp) :: ovl(dim_ovl)
     end function abs_extract_overlap

     function abs_get_lcao(self, dir_path, nao, stat) result(lcao)
       import nx_qm_generic_t, dp, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: dir_path
       integer, intent(in) :: nao
       type(nx_status_t), intent(inout) :: stat
       real(dp) :: lcao(nao, nao)
     end function abs_get_lcao

     subroutine abs_ovl_run(self, stat)
       import nx_qm_generic_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_ovl_run

     subroutine abs_ovl_post(self, stat)
       import nx_qm_generic_t, nx_status_t
       
       class(nx_qm_generic_t), intent(in) :: self
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_ovl_post

     subroutine abs_cio_prepare_files(self, path_to_qm, orb, stat)
       import nx_qm_generic_t, nx_orbspace_t, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: path_to_qm
       type(nx_orbspace_t), intent(in) :: orb
       type(nx_status_t), intent(inout) :: stat
     end subroutine abs_cio_prepare_files

     subroutine abs_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
       import nx_qm_generic_t, nx_orbspace_t, dp, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: path_to_qm
       type(nx_orbspace_t), intent(in) :: orb
       type(nx_status_t), intent(inout) :: stat
       real(dp), intent(out) :: tia(:, :, :)
       real(dp), intent(out), optional :: tib(:, :, :)
     end subroutine abs_cio_get_singles_amplitudes

     subroutine abs_cio_get_mos_energies(self, path_to_qm, stat, mos)
       import nx_qm_generic_t, nx_orbspace_t, dp, nx_status_t

       class(nx_qm_generic_t), intent(in) :: self
       character(len=*), intent(in) :: path_to_qm
       type(nx_status_t), intent(inout) :: stat
       real(dp), intent(out) :: mos(:)
     end subroutine abs_cio_get_mos_energies
     
  end interface

contains

  ! ===============
  ! GETTER ROUTINES
  ! ===============
  pure function get_method(self) result(res)
    class(nx_qm_generic_t), intent(in) :: self

    character(len=:), allocatable :: res

    res = self%method_
  end function get_method

  pure function get_prt_mo(self) result(res)
    class(nx_qm_generic_t), intent(in) :: self

    integer :: res

    res = self%prt_mo_
  end function get_prt_mo


  ! ===============
  ! SETTER ROUTINES
  ! ===============
  pure subroutine set_method(self, method) 
    class(nx_qm_generic_t), intent(inout) :: self
    character(len=*), intent(in) :: method

    self%method_ = trim(method)
  end subroutine set_method

  pure subroutine set_prt_mo(self, prt_mo) 
    class(nx_qm_generic_t), intent(inout) :: self
    integer, intent(in) :: prt_mo

    self%prt_mo_ = prt_mo
  end subroutine set_prt_mo
end module mod_qm_generic_t
