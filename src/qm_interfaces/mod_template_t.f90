! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_template_t
  !! # Template for QM interface implementation
  !!
  !! This file contains a template that should be used for implementing new interfaces to
  !! QM code / methods in Newton-X. It is a minimal working example, and should be
  !! completed as required.
  !!
  !! All occurences of ``_template_`` **HAVE TO BE REPLACED** by the name chosen for the
  !! interface (/i.e/ for Gaussian that will be ``_gaussian_``.
  !!
  !! For the interface to work correctly, a block should be added to the function
  !! ``nx_qm_create_item`` in the module ``mod_qm_interfaces``.
  use mod_configuration, only: nx_config_t
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_template_t

  type, extends(nx_qm_generic_t) :: nx_template_t
   contains
     private
     procedure, public :: setup => template_setup
     procedure, public :: print => template_print
     procedure, public :: to_str => template_to_str
     procedure, public :: backup => template_backup
     procedure, public :: update => template_update
     procedure, public :: run => template_run
     procedure, public :: read_output => template_read_output
     procedure, public :: write_geom => template_write_geometry
     procedure, public :: init_overlap => template_init_overlap
     procedure, public :: prepare_overlap => template_prepare_overlap
     procedure, public :: extract_overlap => template_extract_overlap
     procedure, public :: ovl_post => template_ovl_post
     procedure, public :: ovl_run => template_ovl_run
     procedure, public :: get_lcao => template_get_lcao
     procedure, public :: cio_prepare_files => template_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & template_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => template_cio_get_mos_energies
  end type nx_template_t
  interface nx_template_t
     module procedure constructor
  end interface nx_template_t

  character(len=*), parameter :: MODNAME = 'mod_template_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo &
       ! OPTIONAL program- and method-specific parameters
       & ) result(res)
    !! Constructor for the ``nx_template_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo

    type(nx_template_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine template_setup(self, parser, conf, inp_path, stat)
    class(nx_template_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo

    prt_mo = -1
    call set(parser, 'template', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )
  end subroutine template_setup

  subroutine template_print(self, out)
    class(nx_template_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
  end subroutine template_print


  function template_to_str(self) result(res)
    class(nx_template_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&template'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//'/'//nl
  end function template_to_str

  subroutine template_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine template_backup

  subroutine template_update(self, conf, traj, path, stat)
    class(nx_template_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_update

  subroutine template_run(self, stat)
    class(nx_template_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_run

  function template_read_output(self, conf, traj, path, stat) result(info)
    class(nx_template_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! Populate the orbital space (when required, else delete this part)
    if (traj%step == conf%init_step) then
       ! For instance, use something like:
       ! info%orb = self%read_orb( path )
    end if
    
  end function template_read_output

  subroutine template_write_geometry(self, traj, path, print_merged)
    class(nx_template_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged
  end subroutine template_write_geometry

  subroutine template_init_overlap(self, path_to_qm, stat)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_init_overlap

  subroutine template_prepare_overlap(self, path_to_qm, stat)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_prepare_overlap

  function template_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_template_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function template_extract_overlap

  function template_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function template_get_lcao

  subroutine template_ovl_run(self, stat)
    class(nx_template_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_ovl_run

  subroutine template_ovl_post(self, stat)
    class(nx_template_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_ovl_post

  subroutine template_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine template_cio_prepare_files

  subroutine template_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine template_cio_get_singles_amplitudes

  subroutine template_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_template_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine template_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  
end module mod_template_t
