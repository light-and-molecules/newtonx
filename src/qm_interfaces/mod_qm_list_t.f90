! Copyright (C) 2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_qm_list_t
  use mod_qm_item_t, only: nx_qm_item_t
  implicit none

  private

  type :: nx_qm_list_t
     type(nx_qm_item_t), allocatable :: 
  end type nx_qm_list_t

contains
  

end module mod_qm_list_t
