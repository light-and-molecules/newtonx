! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_turbomole_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_interface, only: &
       & get_list_of_files, copy, setenv, mkdir, gzip
  use mod_kinds, only: dp
  use mod_logger, only: nx_log, &
       & call_external, check_error, print_conf_ele, &
       & LOG_WARN
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use mod_tools, only: &
       & split_pattern, to_str, split_blanks
  use mod_trajectory, only: nx_traj_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_turbomole_t

  type, extends(nx_qm_generic_t) :: nx_turbomole_t
     private
     character(len=:), allocatable :: turbodir
     integer :: typedft = 1
     !! Type of DFT:
     !!
     !! - 1: Trajectory is on an excited state;
     !! - 2: Trajectory is on the ground state, but excited states
     !! are requested;
     !! - 3: Full ground state trajectory.
     integer :: nnodes = 1
     !! Number of CPU cores to use.
     integer :: mult = 1
     !! Spin multiplicity
     integer :: npre = 1
     !! ???
     integer :: nstart = 1
     !! ???
     character(len=:), allocatable :: grad_prog
     !! Name of the program used for computing gradients.
     character(len=:), allocatable :: en_prog
     !! Name of the program used for computing energies.
     character(len=:), allocatable :: scf_prog
     !! Name of the program used for SCF.
     integer :: vers(2) = [0, 0]
     !! Version of Turbomole, given as an array: version 7.3 is defined by
     !! ``vers = (7, 3)``.
     character(len=5) :: geomfile = 'coord'
     integer :: nstat = -1
     real(dp) :: d1mp2_thres_ = 0.04_dp
     real(dp) :: d1cc2_thres_ = 0.05_dp
   contains
     private
     procedure, public :: setup => turbomole_setup
     procedure, public :: print => turbomole_print
     procedure, public :: to_str => turbomole_to_str
     procedure, public :: backup => turbomole_backup
     procedure, public :: update => turbomole_update
     procedure, public :: run => turbomole_run
     procedure, public :: read_output => turbomole_read_output
     procedure, public :: write_geom => turbomole_write_geometry

     ! Deferred procedures for overlap (double-molecule) computation
     procedure, public :: init_overlap => turbomole_init_overlap
     procedure, public :: prepare_overlap => turbomole_prepare_overlap
     procedure, public :: extract_overlap => turbomole_extract_overlap
     procedure, public :: get_lcao => turbomole_get_lcao
     procedure, public :: ovl_post => turbomole_ovl_post
     procedure, public :: ovl_run => turbomole_ovl_run

     ! Deferred procedures for state-overlap computation (when required)
     procedure, public :: cio_prepare_files => turbomole_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => turbomole_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => turbomole_cio_get_mos_energies

     procedure, public :: complete_setup => turbomole_complete_setup

     ! Private procedures
     procedure :: update_input_dft => tm_update_input_dft
     procedure :: update_input_cc2 => tm_update_input_cc2
     procedure :: update_input_adc2 => tm_update_input_adc2

     procedure :: run_dft => tm_run_dft
     procedure :: run_cc2 => tm_run_cc2
     procedure :: run_adc2 => tm_run_adc2

     procedure :: read_dft => tm_read_dft
     procedure :: read_cc2 => tm_read_cc2
  end type nx_turbomole_t
  interface nx_turbomole_t
     module procedure constructor
  end interface nx_turbomole_t

  integer, parameter :: TM_ERR_UPDATE = 201
  integer, parameter :: TM_ERR_MAIN = 202
  integer, parameter :: CIO_TM_BIN2MAT = 203
  integer, parameter :: TM_OVL_PREPARE = 204
  integer, parameter :: MAX_CONTRIB = 6

  character(len=*), parameter :: MODNAME = 'mod_turbomole_t'

contains

  pure function constructor(&
       ! General parameters
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & turbodir, typedft, nnodes, mult, npre, nstart,&
       & grad_prog, en_prog, scf_prog, vers, nstat &
       & ) result(res)
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    character(len=*), intent(in), optional :: turbodir
    integer, intent(in), optional :: typedft
    integer, intent(in), optional :: nnodes
    integer, intent(in), optional :: mult
    integer, intent(in), optional :: npre
    integer, intent(in), optional :: nstart
    character(len=*), intent(in), optional :: grad_prog
    character(len=*), intent(in), optional :: en_prog
    character(len=*), intent(in), optional :: scf_prog
    integer, intent(in), optional :: vers(2)
    integer, intent(in), optional :: nstat

    type(nx_turbomole_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )
    
    if (present(typedft)) res%typedft = typedft
    if (present(nnodes)) res%nnodes = nnodes
    if (present(mult)) res%mult = mult
    if (present(npre)) res%npre = npre
    if (present(nstart)) res%nstart = nstart
    if (present(grad_prog)) res%grad_prog = grad_prog
    if (present(en_prog)) res%en_prog = en_prog
    if (present(scf_prog)) res%scf_prog = scf_prog
    if (present(vers)) res%vers(:) = vers(:)
    if (present(nstat)) res%nstat = nstat
    if (present(turbodir)) then
       res%turbodir = trim(turbodir)
    else
       res%turbodir = ''
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine turbomole_setup(self, parser, conf, inp_path, stat)
    class(nx_turbomole_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: env
    integer :: ierr, prt_mo

    character(len=*), parameter :: funcname = 'turbomole_setup'

    prt_mo = -1
    call set(parser, 'turbomole', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%turbodir == '') then
       call get_environment_variable('TURBODIR', env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $TURBODIR environment is not defined', &
               & mod=MODNAME, func=funcname)
          return
       else
          self%turbodir = trim(env)
       end if
    end if

    call turbomole_complete_setup(self, conf%nstat)

    call set(parser, 'turbomole', self%nnodes, 'nnodes')
    call set(parser, 'turbomole', self%mult, 'multiplicity')
    call set(parser, 'turbomole', self%npre, 'npre')
    call set(parser, 'turbomole', self%nstart, 'nstart')
    call set(parser, 'turbomole', self%d1cc2_thres_, 'd1cc2_thres_')
    call set(parser, 'turbomole', self%d1mp2_thres_, 'd1mp2_thres_')

    if (self%nnodes > 1) then
       ierr = setenv( 'OMP_NUM_THREADS', to_str(self%nnodes) )
       if (ierr /= 0) then
          call stat%append(&
               & NX_ERROR, &
               & 'TURBOMOLE: Cannot set OMP_NUM_THREADS environment (nnodes > 1)', &
               & mod=MODNAME, func=funcname)
          return

       end if
    end if

    block
      character(len=256), allocatable :: filelist(:)
      character(len=256), allocatable :: group(:)
      integer :: i

      call get_list_of_files(self%turbodir, filelist, ierr)
      do i=1, size(filelist)
         if (index(filelist(i), 'TURBOMOLE_') /= 0) then
            if (index(filelist(i), 'Linux') /=0) then
               group = split_pattern(filelist(i), pattern='_')
               read(group(2)(1:1), *) self%vers(1)
               read(group(2)(2:len(group(2))), *) self%vers(2)
            end if
         end if
      end do
    end block
  end subroutine turbomole_setup

  function turbomole_to_str(self) result(res)
    class(nx_turbomole_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&turbomole'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' nnodes = '//to_str(self%nnodes)//nl
    res = res//' mult = '//to_str(self%mult)//nl
    res = res//' npre = '//to_str(self%npre)//nl
    res = res//' nstart = '//to_str(self%nstart)//nl
    res = res//'/'//nl
  end function turbomole_to_str
  

  subroutine turbomole_print(self, out)
    class(nx_turbomole_t), intent(in) :: self
    integer, intent(in), optional :: out

    character(len=MAX_STR_SIZE) :: env
    integer :: output
    
    output = stdout
    if (present(out)) output = out

    call get_environment_variable("TURBODIR", env)
    write(output, '(A30, A3, I0, A, I0)') &
         & 'TURBOMOLE Version', ' = ', self%vers(1), '.', self%vers(2)
    call print_conf_ele(trim(env), 'TURBODIR path', unit=output)
    call print_conf_ele(self%nnodes, 'nnodes', unit=output)
    call print_conf_ele(self%mult, 'mult', unit=output)
    call print_conf_ele(self%npre, 'npre', unit=output)
    call print_conf_ele(self%nstart, 'nstart', unit=output)
  end subroutine turbomole_print

  subroutine turbomole_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo

    character(len=256), parameter :: bckfile(*) = [ character(len=32) :: &
         & 'scf.out', 'grad.out', 'control.bck', 'control', 'coord', &
         & 'dipl_a', 'sing_a', 'basis', 'mos', 'mos.old', &
         & 'mos.bck' &
         & ]

    integer :: ierr, i
    character(len=256) :: bckfile_with_path(size(bckfile))

    ierr = copy( trim(qm_path)//'/mos', chk_path )
    if (ierr /= 0) &
         call stat%append(&
         & NX_ERROR, 'Error in copying MO files to '//trim(chk_path), &
         & mod=MODNAME, func='turbomole_backup' &
         & )
    ierr = gzip( [trim(chk_path)//'/mos'] )
    if (ierr /= 0) &
         call stat%append(&
         & NX_ERROR, 'Error in gzipping files to '//trim(chk_path), &
         & mod=MODNAME, func='turbomole_backup' &
         & )

    if (.not. only_mo) then
       do concurrent(i=1:size(bckfile))
          bckfile_with_path(i) = trim(qm_path)//'/'//trim(bckfile(i))
       end do

       ierr = copy(bckfile_with_path, chk_path)
       if (ierr /= 0) &
            call stat%append(&
            & NX_ERROR, 'Error in copying BACKUP files to '//trim(chk_path), &
            & mod=MODNAME, func='turbomole_backup' &
            & )
    end if
  end subroutine turbomole_backup

  subroutine turbomole_update(self, conf, traj, path, stat)
    class(nx_turbomole_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt
    integer :: ierr
    character(len=256) :: to_copy(4)

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt
    
    if (traj%step > conf%init_step) then
       to_copy = [ character(len=256) :: &
            & trim(path)//'.old/sing_a', &
            & trim(path)//'.old/energy', &
            & trim(path)//'.old/dipl_a', &
            & trim(path)//'.old/exspectrum' &
            & ]
       ierr = copy(to_copy, trim(path)//'/')
    end if

    ! Update control file
    if (self%method() == 'tddft') then
       call self%update_input_dft(nstatdyn, nstat, path)
    else if (self%method() == 'ricc2') then
       call self%update_input_cc2(nstatdyn, nstat, path)
    else if (self%method() == 'adc2') then
       call self%update_input_adc2(nstatdyn, nstat, path)
    end if

    ! Set the different programs to be used.
    if (self%method() == 'tddft') then
       call tm_set_programs_dft(self)
    else if (self%method() == 'ricc2') then
       call tm_set_programs_cc2(self)
    else if (self%method() == 'adc2') then
       call tm_set_programs_cc2(self)
    end if
  end subroutine turbomole_update

  subroutine turbomole_run(self, stat)
    class(nx_turbomole_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    ! character(len=MAX__SIZE) :: command
    integer :: ierr
    character(len=256) :: cmdmsg

    ! Back up control file
    ierr = copy( 'control', 'control.bck' )
    ierr = copy( 'mos', 'mos.bck' )

    ! SCF step
    call call_external(self%scf_prog, ierr, outfile='scf.out', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running dscf'//trim(cmdmsg)//&
            & '. Please take a look at turbomole.job/dscf.out', & 
            & mod=MODNAME, func='turbomole_run'&
            & )
    end if


    if (self%method() == 'tddft') then
       call self%run_dft(stat)
    else if (self%method() == 'ricc2') then
       call self%run_cc2(stat)
    else if (self%method() == 'adc2') then
       call self%run_adc2(stat)
    end if
  end subroutine turbomole_run


  function turbomole_read_output(self, conf, traj, path, stat) result(info)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    ! call info%append_data('Information about excited states')

    if (traj%step == conf%init_step) then
       call tm_set_orbspace(info%orb, trim(path)//'/control')
    end if

    select case(self%method())
    case ('tddft')
       call self%read_dft(path, conf%nat, traj%nstatdyn, info)
    case('ricc2','adc2')
       call self%read_cc2(path, conf%nat, traj%nstatdyn, info, stat)
    end select
  end function turbomole_read_output


  subroutine turbomole_write_geometry(self, traj, path, print_merged)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    logical :: merged
    integer :: i, u, j

    merged = .false.
    if (present(print_merged)) merged = print_merged

    open(newunit=u, file=trim(path)//'/'//self%geomfile, action='write')
    write(u, '(A6)') '$coord'
    do i=1, size(traj%geom, 2)
       write(u, '(3F20.14, A4)') (traj%geom(j, i), j=1, 3), traj%atoms(i)
    end do
    if (merged) then
       do i=1, size(traj%old_geom, 2)
          write(u, '(3f15.9, A4)') (traj%old_geom(j, i), j=1, 3), traj%atoms(i)
       end do
    end if
    write(u,'(A19)') '$user-defined bonds'
    write(u,'(A4)') '$end'
    close(u)
  end subroutine turbomole_write_geometry
  

  subroutine turbomole_init_overlap(self, path_to_qm, stat)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    logical :: ext
    integer :: nat, nocc, nbas

    character(len=*), parameter :: funcname = 'turbomole_init_overlap'

    ierr = mkdir('double_molecule_input')
    if (ierr /= 0) then
       call stat%append(NX_ERROR, &
            & 'OVL: Cannot create double_molecule_input folder', &
            & mod=MODNAME, func=funcname)
    end if
    

    call tm_get_orbspace(path_to_qm//'/control', nat, nocc, nbas)
    call tm_make_double_control(path_to_qm//'/control', &
         & 'double_molecule_input/control', nat)
    call tm_make_double_mos(nbas, nocc, &
         & 'double_molecule_input/mos')

    ierr = copy(trim(path_to_qm)//'/basis', 'double_molecule_input/basis')
    if (ierr /= 0) then
       call stat%append(NX_ERROR, &
            & 'OVL: Cannot copy basis in double_molecule_input folder', &
            & mod=MODNAME, func=funcname)
    end if

    inquire(file=path_to_qm//'/auxbasis', exist=ext)
    if (ext) then
       ierr = copy(trim(path_to_qm)//'/auxbasis', 'double_molecule_input/auxbasis')
       if (ierr /= 0) then
          call stat%append(NX_ERROR, &
               & 'OVL: Cannot copy auxbasis in double_molecule_input folder', &
               & mod=MODNAME, func=funcname)
       end if
    end if
  end subroutine turbomole_init_overlap

  subroutine turbomole_prepare_overlap(self, path_to_qm, stat)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    logical :: ext

    character(len=*), parameter :: funcname = 'turbomole_prepare_overlap'

    ierr = copy([character(len=256) :: &
         & 'double_molecule_input/control', &
         & 'double_molecule_input/basis', 'double_molecule_input/mos'&
         & ], 'overlap/'&
         & )
    if (ierr /= 0) then
       call stat%append(NX_ERROR, &
            & 'OVL: Cannot copy double_molecule_input/ content to overlap/', &
            & mod=MODNAME, func=funcname)
    end if

    inquire(file='double_molecule_input/auxbasis', exist=ext)
    if (ext) then
       ierr = copy('double_molecule_input/auxbasis', 'overlap/auxbasis')
       if (ierr /= 0) then
          call stat%append(NX_ERROR, &
               & 'OVL: Cannot copy double_molecule_input/auxbasis in overlap/', &
               & mod=MODNAME, func=funcname)
       end if
    end if
  end subroutine turbomole_prepare_overlap


  function turbomole_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_turbomole_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path

    real(dp) :: ovl(dim_ovl)

    integer :: u, ierr
    integer :: id
    character(len=512) :: line
    character(len=13) :: fmt_spec
    integer :: nele
    integer :: remainder
    integer :: j
    character(len=:), allocatable :: source

    ! For TM 7.5 and higher
    integer :: nlines, n, i, nl_in_matrix, dim

    nele = 0                    ! Number of elements already read
    dim = 3
    remainder = mod(dim_ovl, dim)

    source = './overlap/dscf.out'

    write(fmt_spec, '(A4, I0, A7)') '(', remainder, 'E24.14)'
    open(newunit=u, file=source, action='read', status='old')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit
       id = index(line, 'OVERLAP')
       if (id /= 0) then
          read(u, '(a)', iostat=ierr) line ! Read '-----' line

          if (self%vers(2) < 5) then
             do
                read(u, '(a)', iostat=ierr) line
                if (nele < dim_ovl - remainder) then
                   read(line, '(3E24.14)') (ovl(nele+j), j=1, 3)
                   nele = nele + 3
                else if ((nele >= dim_ovl-remainder) .and. (nele <&
                     & dim_ovl)) then
                   read(line, fmt=fmt_spec) (ovl(nele+j), j=1, remainder)
                   nele = nele + remainder
                else
                   exit
                end if
             end do
          else
             if (self%vers(2) == 5) dim = 10
             nl_in_matrix = int((-1 + sqrt(1.0+8*dim_ovl)) / 2)
             ! print *, 'NLINES = ', nl_in_matrix
             do n=1, int(nl_in_matrix)
                nlines = n / dim
                ! print *, 'nlines = ', nlines
                remainder = mod(n, dim)
                ! print *, 'remainder = ', remainder
                do i=1, nlines
                   read(u, *) (ovl(nele+j), j=1,dim)
                   nele = nele + dim
                end do
                if (remainder > 0) then
                   read(u, *) (ovl(nele+j), j=1, remainder)
                   nele = nele + remainder
                end if
             end do
          end if
       end if

    end do
    close(u)

  end function turbomole_extract_overlap

  function turbomole_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    
    real(dp) :: lcao(nao, nao)

    integer :: u, ierr
    integer :: id
    character(len=512) :: buf
    character(len=16) :: fmt_spec
    character(len=:), allocatable :: mos_file

    integer :: nele, remainder, nbLines
    integer :: ll ! For counting the complete lines already read
    integer :: i
    integer :: mo

    ! The mos file is organised in 4 columns, and we have to read this%nao
    ! elements. The number of complete lines is stored in nbLines
    remainder = mod(nao, 4)
    nbLines = int(nao / 4)

    ! Format specifier for incomplete lines
    write(fmt_spec, '(A1, I0, A7)') '(', remainder, 'D20.14)'

    mos_file = trim(dir_path)//'/mos'
    mo = 0
    nele = 0
    open(newunit=u, file=mos_file, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       ! End of file
       if (ierr /= 0) exit

       ! New MO
       id = index(buf, 'eigenvalue')
       if (id /= 0) then
          mo = mo + 1
          nele = 0
       end if

       ! Coefficients
       if ((buf(1:1) .eq. '-') .or. (buf(1:1) .eq. '0')) then
          ll = int(nele / 4)
          if (ll < nbLines) then
             ! If we still have elements in complete lines to read
             read(buf, '(4D20.14)') ( lcao(mo, nele+i), i=1, 4 )
             nele = nele + 4
          else if ((ll == nbLines) .and. (remainder /= 0)) then
             ! Else if incomplete lines exist, read it too
             read(buf, fmt=fmt_spec) ( lcao(mo, nele+i), i=1, remainder )
             nele = nele + remainder
          end if
       end if
    end do
    close(u)

    ! Write lcao
    lcao = transpose(lcao)
  end function turbomole_get_lcao

  subroutine turbomole_ovl_post(self, stat)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine turbomole_ovl_post

  subroutine turbomole_ovl_run(self, stat)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=256) :: cmdmsg

    call call_external('dscf', ierr, outfile='dscf.out', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem with running dscf'//trim(cmdmsg)//&
            & '. Please see overlap/dscf.out for details.', &
            & mod=MODNAME, func='turbomole_ovl_run'&
            & )
    end if
  end subroutine turbomole_ovl_run

  subroutine turbomole_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: env
    integer :: offset

    select case(self%method())
    case ('ricc2','adc2')
       ! This value is for Turbomole version > 7.6
       offset = 48
       
       ! For version 7.3 we need to adapt
       if (self%vers(2) == 3) offset = 56

       call get_environment_variable('CIOVERLAP', env)
       call tm_ricc2_bin2matrix(&
            & path_to_qm, self%nstat, self%mult, &
            & trim(env), orb%nocc, orb%nfrozen, orb%nvirt, offset, &
            & stat )
    end select
       
  end subroutine turbomole_cio_prepare_files

  subroutine turbomole_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    real(dp), intent(out) :: tia(:, :, :)
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out), optional :: tib(:, :, :)

    select case(self%method())
    case('tddft')
       call tm_get_singles_tddft(orb, path_to_qm//'/sing_a', tia)
    case('ricc2','adc2')
       call tm_get_singles_cc2(orb, path_to_qm//'/bin2matrix.out', tia)
    end select
  end subroutine turbomole_cio_get_singles_amplitudes

  subroutine turbomole_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_turbomole_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)

    integer :: u, id, ao, ierr
    character(len=512) :: buf
    ao = 1
    open(newunit=u, file=path_to_qm//'/mos', status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) then
          if (ao < size(mos)) then
             call stat%append(NX_ERROR, &
                  & 'The number of eigenvalues in mos is lower '//&
                  & 'than the number of Atomic Orbitals !', &
                  & mod=MODNAME, func='turbomole_cio_get_mos_energies')
          else
             exit
          end if
       end if
       id = index(buf, 'eigenvalue')
       if (id /= 0) then
          read(buf(id+len('eigenvalue')+1:id+len('eigenvalue')+21),&
               & '(d20.14)') mos(ao)
          ao = ao + 1
       end if
    end do
    close(u)
  end subroutine turbomole_cio_get_mos_energies
  

  ! =========================
  ! Private member procedures
  ! =========================
  subroutine tm_update_input_dft(self, nstatdyn, nstat, path)
    class(nx_turbomole_t), intent(inout) :: self
    integer, intent(in) :: nstatdyn
    integer, intent(in) :: nstat
    character(len=*), intent(in) :: path

    character(len=MAX_STR_SIZE), allocatable :: control(:)
    character(len=MAX_STR_SIZE), allocatable :: keys(:), values(:)

    ! Update control file
    if (nstatdyn > 1) then
       self%typedft = 1
    end if

    if ((nstat > 1) .and. (nstatdyn .eq. 1)) then
       self%typedft = 2
    end if

    if ((nstat .eq. 1) .and. (nstatdyn .eq. 1)) then
       self%typedft = 3
    end if

    control = tm_read_control(trim(path)//'/control')

    if (nstatdyn > 1) then
       allocate( keys(2), values(2) )
       keys(1) = 'exopt'
       values(1) = to_str(nstatdyn - 1)

       keys(2) = 'soes'
       valueS(2) = 'all '//to_str(nstat - 1)

       call tm_change_value(control, keys, values)
    else if ((nstatdyn == 1) .and. (nstat > 1)) then
       call tm_change_value(control, &
            & ['soes'], ['all '//to_str(nstat - 1)])
    end if

    call tm_print_control(control, trim(path)//'/control')
  end subroutine tm_update_input_dft

  subroutine tm_update_input_cc2(self, nstatdyn, nstat, path)
    class(nx_turbomole_t), intent(inout) :: self
    integer, intent(in) :: nstatdyn
    integer, intent(in) :: nstat
    character(len=*), intent(in) :: path

    character(len=4) :: ia
    character(len=MAX_STR_SIZE) :: state_info, myfmt
    character(len=MAX_STR_SIZE), allocatable :: controlarray(:)

    if (self%mult == 1) then
       ia = 'a'
    else if (self%mult == 3) then
       ia = 'a{3}'
    end if

    if (nstatdyn > 1) then
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " exgrad states=(", A, " ", I0, ")", &
            & A, " spectrum states=all operators=diplen" &
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), self%mult, nstat-1, self%npre, self%nstart,&
            & NEW_LINE('c'), ia, nstatdyn-1, NEW_LINE('c')


    else if (nstatdyn == 1) then
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " spectrum states=all operators=diplen"&
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), self%mult, nstat-1, self%npre, self%nstart,&
            & NEW_LINE('c')
    end if

    controlarray = tm_read_control(trim(path)//'/control')
    call tm_change_value(controlarray, ['excitations'], [state_info])

    myfmt = tm_get_value(controlarray, 'ricc2')

    if ( index(myfmt, 'geoopt') == 0 ) then
       call tm_change_value(controlarray, ['ricc2'], &
            & [NEW_LINE('c')//'cc2'//NEW_LINE('c')//'maxiter=300'//NEW_LINE('c')//'geoopt model=cc2']&
            &)
    end if

    call tm_print_control(controlarray, trim(path)//'/control')
  end subroutine tm_update_input_cc2

  subroutine tm_update_input_adc2(self, nstatdyn, nstat, path)
    class(nx_turbomole_t), intent(inout) :: self
    integer, intent(in) :: nstatdyn
    integer, intent(in) :: nstat
    character(len=*), intent(in) :: path

    character(len=4) :: ia
    character(len=MAX_STR_SIZE) :: state_info, ricc2_info, myfmt
    character(len=MAX_STR_SIZE), allocatable :: controlarray(:), controlarray2(:)
    character(len=1) :: nl

    nl = NEW_LINE('c')
    if (self%mult == 1) then
       ia = 'a'
    else if (self%mult == 3) then
       ia = 'a{3}'
    end if

    if (nstatdyn > 1) then
       controlarray = tm_read_control(trim(path)//'/control')
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " exgrad states=(", A, " ", I0, ")", &
            & A, " spectrum states=all operators=diplen" &
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), self%mult, nstat-1, self%npre, self%nstart,&
            & NEW_LINE('c'), ia, nstatdyn-1, NEW_LINE('c')

       call tm_change_value(controlarray, ['excitations'], [state_info])
       ricc2_info = tm_get_value(controlarray, 'ricc2')

       if ( index(ricc2_info, 'geoopt') == 0 ) then
          ! print *, 'CHANGING RICC2'
          ricc2_info = nl//'adc2'//nl//'maxiter=300'//nl//'geoopt model=adc(2)'
          call tm_change_value(controlarray, ['ricc2'], [ricc2_info])
       end if
       call tm_print_control(controlarray, trim(path)//'/control')

    else if (nstatdyn == 1) then
       controlarray = tm_read_control(path//'/control')
       allocate( controlarray2( size(controlarray) ) )
       controlarray2(:) = controlarray(:)

       call tm_delete_keyword(controlarray2, ['excitations'])

       ricc2_info = nl//'mp2'//nl//'maxiter=300'//nl//'geoopt model=mp2'
       call tm_change_value(controlarray2, ['ricc2'], [ricc2_info])
       call tm_print_control(controlarray2, trim(path)//'/control-mp2')

       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " spectrum states=all operators=diplen"&
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), self%mult, nstat-1, self%npre, self%nstart,&
            & NEW_LINE('c')
       call tm_change_value(controlarray, ['excitations'], [state_info])

       ricc2_info = nl//'adc(2)'//nl//'maxiter=300'
       call tm_change_value(controlarray, ['ricc2'], [ricc2_info])
       call tm_print_control(controlarray, trim(path)//'/control-adc2')
    end if
  end subroutine tm_update_input_adc2

  subroutine tm_set_programs_dft(self)
    class(nx_turbomole_t) :: self

    logical :: exists

    inquire(file='auxbasis', exist=exists)
    if (exists) then
       self%scf_prog = 'ridft'
       self%grad_prog = 'rdgrad'
    else
       if (self%nnodes > 1) then
          self%scf_prog = 'dscf_smp'
          self%grad_prog = 'grad_smp'
       else
          self%scf_prog = 'dscf'
          self%grad_prog = 'grad'
       end if
    end if

    if (self%typedft .eq. 1) then
       self%grad_prog = 'egrad'
    else if (self%typedft .eq. 2) then
       self%en_prog = 'escf'
       self%grad_prog = 'grad'
    end if
  end subroutine tm_set_programs_dft

  subroutine tm_set_programs_cc2(self)
    !! Set the TURBOMOLE programs for RICC2 and ADC(2) dynamics.
    class(nx_turbomole_t) :: self

    self%scf_prog = 'dscf'
    self%grad_prog = 'ricc2'
    if (self%nnodes > 1) then
       self%scf_prog = 'dscf_smp'
       self%grad_prog = 'ricc2_smp'
    end if
  end subroutine tm_set_programs_cc2

  subroutine tm_run_dft(self, stat)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=256) :: cmdmsg

    ! Gradients and ES energies
    if (self%typedft == 1) then
       call call_external(self%grad_prog, ierr, outfile='grad.out', cmdmsg=cmdmsg)
       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running '//self%grad_prog//trim(cmdmsg)//&
               & '. Please take a look at turbomole.job/grad.out', & 
               & mod=MODNAME, func='tm_run_dft'&
               & )
       end if
       
    else if (self%typedft == 2) then
       call call_external(self%grad_prog, ierr, outfile='grad_s0.out', cmdmsg=cmdmsg)
       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running '//self%grad_prog//trim(cmdmsg)//&
               & '. Please take a look at turbomole.job/grad_s0.out', & 
               & mod=MODNAME, func='tm_run_dft'&
               & )
       end if

       call call_external(self%en_prog, ierr, outfile='grad.out', cmdmsg=cmdmsg)
       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running '//self%en_prog//trim(cmdmsg)//&
               & '. Please take a look at turbomole.job/grad.out', & 
               & mod=MODNAME, func='tm_run_dft'&
               & )
       end if
    else if (self%typedft == 3) then
       call call_external(self%grad_prog, ierr, outfile='grad.out', cmdmsg=cmdmsg)

       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running '//self%grad_prog//trim(cmdmsg)//&
               & '. Please take a look at turbomole.job/grad.out', & 
               & mod=MODNAME, func='tm_run_dft'&
               & )
       end if
    end if
  end subroutine tm_run_dft


  subroutine tm_run_cc2(self, stat)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=256) :: cmdmsg

    call call_external(self%grad_prog, ierr, outfile='grad.out', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running '//self%grad_prog//&
            & '. Please take a look at turbomole.job/grad.out', & 
            & mod=MODNAME, func='tm_run_cc2'&
            & )
    end if
  end subroutine tm_run_cc2


  subroutine tm_run_adc2(self, stat)
    class(nx_turbomole_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat

    logical :: ext
    integer :: ierr
    character(len=256) :: cmdmsg

    ! This is for GS only dynamics (nstatdyn = 1)
    inquire(file='control-mp2', exist=ext)
    if (ext) then
       ierr = copy('control-mp2', 'control')
    end if

    call call_external(self%grad_prog, ierr, outfile='grad.out', cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running '//self%grad_prog//trim(cmdmsg)//&
            & '. Please take a look at turbomole.job/grad.out', & 
            & mod=MODNAME, func='tm_run_adc2'&
            & )
    end if

    inquire(file='control-adc2', exist=ext)
    if (ext) then
       ierr = copy('control-adc2', 'control')
       call call_external(self%grad_prog, ierr, outfile='grad.out')
       if (ierr /=0 ) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem when running ADC2 job '//self%grad_prog//trim(cmdmsg)//&
               & '. Please take a look at turbomole.job/grad.out', & 
               & mod=MODNAME, func='tm_run_adc2'&
               & )
       end if
    end if
  end subroutine tm_run_adc2


  subroutine tm_read_dft(self, path, nat, nstatdyn, qminfo)
    !! Read TURBBOMOLE DFT outputs and report.
    !!
    !! ``gradfile`` output should come from a gradient computation. The
    !! current potential energies, acceleration, and gradient from the
    !! trajectory are backed up in ``traj%old_*``. The new values are
    !! then read in the output and stored in the ``traj`` object. The
    !! oscillator strength are also reported.
    !! TODO: Implement the reporting of ES .
    class(nx_turbomole_t), intent(in) :: self
    !! ``nx_qm`` object.
    character(len=*), intent(in) :: path
    integer, intent(in) :: nat
    !! Number of atoms (required for reading the gradient file).
    integer, intent(in) :: nstatdyn
    !! Current state (required for filling the ``rgrad`` array).
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    character(len=MAX_STR_SIZE) :: line
    integer :: i, u, j

    integer :: ierr

    character(len=MAX_STR_SIZE), dimension(:, :), allocatable :: contrib

    character(len=:), allocatable :: scffile

    allocate(contrib(size(qminfo%repot)-1, MAX_CONTRIB))
    contrib(:, :) = 'NULL'

    scffile = 'grad.out'
    if (self%typedft == 3) scffile = 'scf.out'
    scffile = trim(path)//'/'//scffile

    ! Find energies, oscillator strengths, and contributions to
    ! excited states (if relevant)
    call tm_parse_output(scffile, qminfo, self%typedft)

    open(newunit=u, file=trim(path)//'/gradient', status='old', action='read')
    ! We are not interested in the first Nat + 2 lines
    do i=1, nat + 2
       read(u, '(A)') line
    end do
    i = 1
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit
       if (line .eq. '$end') exit
       read(line, *) (qminfo%rgrad(nstatdyn, j, i), j=1, 3)
       i = i+1
    end do
    close(u)
  end subroutine tm_read_dft


  subroutine tm_read_cc2(self, path, nat, nstatdyn, qminfo, stat)
    !! Read TURBBOMOLE CC2 outputs and report.
    !!
    !! ``gradfile`` output should come from a gradient computation. The
    !! current potential energies, acceleration, and gradient from the
    !! trajectory are backed up in ``traj%old_*``. The new values are
    !! then read in the output and stored in the ``traj`` object. The
    !! oscillator strength are also reported.
    class(nx_turbomole_t) :: self
    character(len=*), intent(in) :: path
    integer, intent(in) :: nat
    integer, intent(in) :: nstatdyn
    type(nx_qminfo_t), intent(inout) :: qminfo
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: line
    integer :: u, id, ien, i, j, nstat, iosc, ierr
    character(len=MAX_STR_SIZE), allocatable :: splitted(:)
    real(dp) :: entmp, d1mp2, d1cc2

    ien = 0 ! Loop over excited states
    d1mp2 = -1.0_dp
    d1cc2 = -1.0_dp
    iosc = 1 ! Loop over oscillator strengths

    ! Some allocations
    nstat = size(qminfo%repot)
    qminfo%osc_str(:) = -1.0_dp

    open(newunit=u, file=trim(path)//'/grad.out', action='read')
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit

       ! Find Ground state energy
       id = index(line, 'Final CC2 energy')
       if (id /= 0) then

          if (self%method() == 'ricc2') then
             call split_blanks(line, splitted)
             read(splitted(6), *) qminfo%repot(1)
          end if

          read(u, '(a)') line
          read(u, '(a)') line
          call split_blanks(line, splitted)
          read(splitted(5), *) d1cc2
       end if

       id = index(line, 'Final MP2 energy')
       if (id /= 0) then
          if (self%method() == 'adc2') then
             ! Output file sample:
             !**********************************************************************
             !*                                                                    *
             !*   RHF  energy                             :   -328.8936700776      *
             !*   MP2 correlation energy                  :     -0.2124570545      *
             !*                                                                    *
             !*   Final MP2 energy                        :   -329.1061271321      *
             !*                                                                    *
             !*   Norm of MP1 T2 amplitudes               :      0.1842261354      *
             !*                                                                    *
             !*   D1 diagnostic                           :      0.0302            *
             !*                                                                    *
             !**********************************************************************
             call split_blanks(line, splitted)
             read(splitted(6), *) qminfo%repot(1)
             do i=1, 4
                read(u, '(a)') line
             end do

             call split_blanks(line, splitted)
             read(splitted(5), *) d1mp2
          else
             read(u, '(a)') line
             call split_blanks(line, splitted)
             read(splitted(5), *) d1mp2
          end if
       end if

       ! Excited states energies
       id = index(line, 'Energy:')
       if (id /= 0) then
          ! HERE WE UPDATE THE ENERGY INDEX ien
          ! This influences the next parts: find 'type RE0' and find 'norm of
          ! printed elements'
          ien = ien + 1
          call split_blanks(line, splitted)
          read(splitted(2), *) entmp
          qminfo%repot(ien+1) = qminfo%repot(1) + entmp
       end if

       ! Excited state informations
       id = index(line, ' type: RE0')
       if (id /= 0) then
          call qminfo%append_data(line)
          CONTRIB_LOOP: do
             read(u, '(a)') line
             call qminfo%append_data(line)
             if (index(line, 'norm of printed elements') /= 0) then
                call qminfo%append_data('')
                exit CONTRIB_LOOP
             end if
          end do CONTRIB_LOOP
       end if

       id = index(line, 'contributions of excitation levels to')
       if (id /= 0) then
          call qminfo%append_data(line)
          do
             read(u, '(a)') line
             call qminfo%append_data(line)
             if (line == '') exit
          end do
       end if

       ! Oscillator strengths
       id = index(line, 'oscillator strength (length gauge)')
       if (id /= 0) then
          call split_blanks(line, splitted)
          read(splitted(6), *) qminfo%osc_str(iosc)
          iosc = iosc + 1
       end if
    end do
    close(u)

    if (d1mp2 > 0) call qminfo%append_data('D1 diagnostic for MP2: '//to_str(d1mp2))
    if (d1cc2 > 0) call qminfo%append_data('D1 diagnostic for CC2: '//to_str(d1cc2))

    if ((d1mp2 > self%d1mp2_thres_) .or. (d1cc2 > self%d1cc2_thres_)) then
       call stat%append(NX_WARNING, &
            "   D1 diagnostic indicates that MP2"//NEW_LINE('a')// &
            "   and CC2 may be inadequate to describe the"//NEW_LINE('a')//&
            "   ground state for this geometry.          "//NEW_LINE('a'), &
            & mod=MODNAME, func='turbomole_read_cc2')
    end if

    ! if (size(qminfo%repot) > 1) then
    !    do i=1, nstat-1
    !       call qminfo%append_data(&
    !            & 'Oscillator strength ('//to_str(1)//','//to_str(i+1)//') = ' &
    !            & //to_str(qminfo%osc_str(i), fmt='(F10.6)') &
    !            & )
    !    end do
    ! end if

    open(newunit=u, file=trim(path)//'/gradient', status='old', action='read')
    ! We are not interested in the first Nat + 2 lines
    do i=1, nat + 2
       read(u, '(A)') line
    end do
    i = 1
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit
       if (line .eq. '$end') exit
       read(line, *) (qminfo%rgrad(nstatdyn, j, i), j=1, 3)
       i = i+1
    end do
    close(u)
  end subroutine tm_read_cc2

  ! =========================
  ! Private helper procedures
  ! =========================
  function tm_read_control(controlfile) result(res)
    character(len=*), intent(in) :: controlfile

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: u, ierr, i, id
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: temp(:)

    allocate(temp(1024)) ! We assume the control file to have at most 1024 lines

    id = 0
    temp(:) = ''
    open(newunit=u, file=controlfile, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       ! print *, 'buf = ', trim(buf)

       if (index(buf, '$') /= 0) then
          id = id + 1
          temp(id) = trim(buf)
          ! print *, 'id = ', id
       else
          ! print *, 'filling id = ', id
          temp(id) = trim(temp(id))//NEW_LINE('a')//trim(buf)
       end if
    end do
    close(u)

    allocate(res(id))
    do i=1, id
       res(i) = trim(temp(i))
    end do
  end function tm_read_control

  subroutine tm_change_value(controlarray, keys, values)
    character(len=*), allocatable, intent(inout) :: controlarray(:)
    character(len=*), intent(in) :: keys(:)
    character(len=*), intent(in) :: values(:)

    integer :: i, j, notfound
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer, allocatable :: changed(:)

    allocate( changed(size(keys)) )
    changed(:) = 0
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)

       do j=1, size(keys)
          if ( index(split(1), '$'//trim(keys(j))) /= 0 ) then
             ! We found one of the key we want to change
             changed(j) = 1
             controlarray(i) = '$'//trim(keys(j))//' '//trim(values(j))
          end if
       end do
    end do

    notfound = count( changed == 0 )
    if (notfound /= 0) then
       ! Here one of the keys couldn't be found, so we have to extend our initial control
       ! array by the number of keys that we not found.
       block
         character(len=MAX_STR_SIZE) :: tmp( size(controlarray) - 1 + notfound )
         character(len=MAX_STR_SIZE) :: last
         integer :: ssize

         ssize = size(controlarray)

         last = controlarray(ssize)
         do i=1, ssize - 1
            tmp(i) = controlarray(i)
         end do

         do i=1, size(changed)
            if (changed(i) == 0) then
               tmp(ssize) = '$'//trim(keys(i))//' '//trim(values(i))
               ssize = ssize + 1
            end if
         end do

         deallocate( controlarray )
         allocate( controlarray(ssize) )

         do i=1, ssize - 1
            controlarray(i) = tmp(i)
         end do

         controlarray(ssize) = last
       end block
    end if
  end subroutine tm_change_value

  function tm_get_value(controlarray, key) result(res)
    character(len=*) :: controlarray(:)
    character(len=*) :: key

    character(len=:), allocatable :: res

    integer :: i, j
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    character(len=MAX_STR_SIZE) :: tmp

    tmp = ''
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)
       if ( index(split(1), key) /= 0 ) then
          do j=2, size(split)
             tmp = trim(tmp)//' '//trim(split(j))
          end do
       end if
    end do

    res = tmp
  end function tm_get_value

  subroutine tm_print_control(controlarray, filename)
    character(len=*), intent(in) :: controlarray(:)
    character(len=*), intent(in) :: filename

    integer :: u, i

    open(newunit=u, file=filename, action='write')
    do i=1, size(controlarray)
       write(u, '(a)') trim(controlarray(i))
    end do
    close(u)
  end subroutine tm_print_control

  subroutine tm_delete_keyword(controlarray, keys)
    character(len=*), allocatable, intent(inout) :: controlarray(:)
    character(len=*), intent(in) :: keys(:)

    integer :: i, j, ind
    logical :: found

    character(len=MAX_STR_SIZE), allocatable :: temp(:), split(:)

    allocate( temp(size(controlarray) - size(keys) + 1) )

    ind = 1
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)

       found = .false.
       do j=1, size(keys)
          if (index(split(1), keys(j)) /= 0) then
             found = .true.
             exit
          end if
       end do

       if (.not. found) then
          temp(ind) = controlarray(i)
          ind = ind + 1
       end if
    end do

    deallocate(controlarray)
    allocate(controlarray(size(temp)))

    do i=1, size(temp)
       controlarray(i) = temp(i)
    end do
  end subroutine tm_delete_keyword


  subroutine tm_parse_output(filename, info, typedft)
    !! We assume that osc and epot are already initialized to their
    !! right dimension. The dimensions given are general
    character(len=*), intent(in) :: filename
    type(nx_qminfo_t), intent(inout) :: info
    integer, intent(in) :: typedft

    integer :: u, id, ierr
    integer :: iepot, iosc, istate
    character(len=MAX_STR_SIZE) :: line
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split

    iepot = 1
    iosc = 1
    istate = 1

    open(newunit=u, file=filename, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       ! Line contains the total energy
       if (typedft == 3) then
          id = index(line, '|  total energy')
          if (id /= 0) then
             call split_blanks(line, split)
             read(split(5), *) info%repot(iepot)
          end if
       else
          id = index(line, 'Total energy')
          if (id /= 0) then
             call split_blanks(line, split)
             read(split(3), *) info%repot(iepot)
             iepot = iepot + 1
          end if
       end if

       ! Line is an oscillator strength
       id = index(line, 'Oscillator strength:')
       if (id /= 0) then
          FIND_OSC: do

             read(u, '(a)') line
             id = index(line, 'length representation:')
             if (id /= 0) then
                id = index(line, '.')
                if (id /= 0) then
                   read(line(id-3:len(line)), '(E24.16)') info%osc_str(iosc)
                   iosc = iosc + 1
                end if
             end if

             id = index(line, 'Rotatory strength:')
             if (id /= 0) exit FIND_OSC
          end do FIND_OSC
       end if

       ! Dominant contributions
       id = index(line, 'Dominant contributions:')
       if (id /= 0) then
          call info%append_data(trim(line)//' (state '//to_str(istate)//')', unique=.true.)

          read(u, '(a)') line ! This one is empty
          read(u, '(a)') line ! This one contains headers
          call info%append_data(line, unique=.true.)

          do
             read(u, '(a)') line
             if (line == '') exit
             call info%append_data(line)
          end do
          istate = istate + 1
       end if

    end do
    close(u)
  end subroutine tm_parse_output


  subroutine tm_get_orbspace(control, nat, nocc, nbas)
    character(len=*), intent(in) :: control
    integer, intent(out) :: nat
    integer, intent(out) :: nocc
    integer, intent(out) :: nbas

    integer :: u, ierr, id
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    open(newunit=u, file=control, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'natoms') /= 0) then
          id = index(buf, '=')
          read(buf(id+1:), *) nat
       else if (index(buf, 'nbf(AO)') /= 0) then
          id = index(buf, '=')
          read(buf(id+1:), *) nbas
       else if (index(buf, 'closed shells') /= 0) then
          read(u, '(a)') buf
          call split_blanks(buf, split)
          id = index(split(2), '-')
          read(split(2)(id+1:), *) nocc
       end if
    end do
    close(u)
  end subroutine tm_get_orbspace


  subroutine tm_set_orbspace(orbspace, control)
    !! Set the orbital space.
    !!
    !! This routine extracts the relevant information from the input
    !! ``control`` file.
    type(nx_orbspace_t), intent(inout) :: orbspace
    !! Orbital space to populate.
    character(len=*), intent(in) :: control
    !! Name of the ``control`` file.

    integer :: u, ierr, id, id2, length
    character(len=512) :: buf1
    character(len=256), allocatable :: split(:)

    ! The number of frozen orbitals is only relevant for CC type
    ! computations. For TD-DFT this won't get executed and the number
    ! of frozen orbitals will be set to 0.
    ! This number can be found in line of the form:
    !
    ! $freeze
    !  implicit core=    nfrozen virt=    nvirt
    !
    ! We want the nfrozen quantity !
    if (orbspace%nfrozen .eq. -1) then
       length = len('implicit core')
       open(newunit=u, file=control, action='read', status='old')
       do
          read(u, '(a)', iostat=ierr) buf1
          if (ierr /= 0) exit
          if (index(buf1, '$freeze') /= 0) then
             read(u, '(A)') buf1
             call split_blanks(buf1, split)
             read(split(3), *) orbspace%nfrozen
             read(split(5), *) orbspace%ndisc
          end if
       end do
       close(u)
    end if

    ! If no frozen orbital has been found, set to 0
    if (orbspace%nfrozen .eq. -1) then
       orbspace%nfrozen = 0
    end if

    if (orbspace%nbas .eq. -1) then
       open(newunit=u, file=control, action='read', status='old')

       do
          read(u, '(a)', iostat=ierr) buf1
          if (ierr /= 0) exit

          ! First the number of occupied
          id = index(buf1, 'closed shells')
          if (id /= 0) then
             read(u, '(a)') buf1
             id = index(buf1, '-')
             id2 = index(buf1, '(')
             read(buf1(id+1:id2-1), *) orbspace%nocc
          end if

          ! Then the number of AOs
          length = len('nbf(AO)=')
          id = index(buf1, 'nbf(AO)=')
          if (id /= 0) then
             read(buf1(id+length:len(buf1)), *) orbspace%nbas
          end if

       end do
       close(u)

       orbspace%nvirt = orbspace%nbas - orbspace%nocc
    end if

    orbspace%nelec = 2*orbspace%nocc - 2*orbspace%nfrozen + 2*orbspace%nfrozen

  end subroutine tm_set_orbspace

  subroutine tm_make_double_control(original, newfile, nat)
    character(len=*), intent(in) :: original
    character(len=*), intent(in) :: newfile
    integer, intent(in) :: nat

    logical :: ext, ri
    integer :: u, ierr, i, at1, at2
    character(len=MAX_STR_SIZE) :: buf, temp, basis
    character(len=MAX_STR_SIZE) :: atoms, rundim, cshell
    character(len=MAX_STR_SIZE), allocatable :: split(:), group(:), gp(:), control(:)
    character(len=1) :: nl

    ! RI test
    ri = .false.
    inquire(file='auxbasis', exist=ext)
    if (ext) ri = .true.

    nl = NEW_LINE('c')
    basis = ''
    temp = ''
    atoms = nl

    open(newunit=u, file=original, action='read')
    READ_FILE: do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '$atoms') /= 0) then
          ! $atoms section is found: parse the lines for the atom number
          atoms = nl
          READ_ATOMS: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) then
                atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                exit READ_ATOMS ! Exit at end of file
             end if

             if (index(buf, '$') /= 0) then
                atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                exit READ_ATOMS ! Exit at new section

             else if (index(buf, 'basis') /= 0) then
                basis = trim(basis)//trim(buf)//nl

             else if (index(buf, 'cbas') /= 0) then
                basis = trim(basis)//trim(buf)//nl

             else
                ! This line corresponds to an atom description line, with format:
                ! ``atom_name  at1-at2,at3-at4         \``
                ! We want to obtain the corresponding:
                ! ``atom_name  at1+nat-at2+nat,at3+nat-at4+nat         \``

                ! Flush the current values of basis and temp
                if (temp /= '') then
                   atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                end if

                basis = ''
                temp = ''
                call split_blanks(buf, split)

                ! First, save the atom name
                atoms = trim(atoms)//trim(split(1))

                ! Then we build the atom numbers
                if (index(split(2), ',') /= 0) then
                   ! Here we have several groups of atoms, so we split the different
                   ! groups
                   group = split_pattern(split(2), pattern=',')

                   temp = trim(split(2))//','
                   do i=1, size(group)
                      ! Let's see if we have several atoms in each group
                      if (index(group(i), '-') /= 0) then
                         gp = split_pattern(group(i), pattern='-')
                         read(gp(1), *) at1
                         read(gp(2), *) at2
                         at1 = at1 + nat
                         at2 = at2 + nat
                         temp= trim(temp)//to_str(at1)//'-'//to_str(at2)//','
                      else
                         read(group(i), *) at1
                         at1 = at1 + nat
                         temp= trim(temp)//to_str(at1)//','
                      end if
                   end do
                else
                   ! Only one atom of this type, this is the simple case
                   temp = trim(split(2))//','
                   if (index(split(2), '-') /= 0) then
                      gp = split_pattern(split(2), pattern='-')
                      read(gp(1), *) at1
                      read(gp(2), *) at2
                      at1 = at1 + nat
                      at2 = at2 + nat
                      temp= trim(temp)//to_str(at1)//'-'//to_str(at2)//','
                   else
                      read(split(2), *) at1
                      at1 = at1 + nat
                      temp= trim(temp)//to_str(at1)//','
                   end if
                end if

                if (temp(len_trim(temp):len_trim(temp)) == ',') then
                   temp(len_trim(temp):len_trim(temp)) = ''
                end if
             end if
          end do READ_ATOMS

       else if (index(buf, '$rundimensions') /= 0) then
          rundim = nl
          READ_DIM: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_DIM ! Exit at end of file

             if (index(buf, '$') /= 0) exit READ_DIM ! Exit at new section

             group = split_pattern(buf, pattern='=')
             ! do i=1, size(group)
             !    print *, trim(group(i))
             ! end do

             read(group(2), *) at1

             if (index(buf, 'fock,dens') /= 0) then
                at1 = at1 * 4
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'natoms') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'nshell') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'nbf') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'trafo') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             end if
          end do READ_DIM

       else if (index(buf, '$closed shells') /= 0) then
          cshell = nl
          READ_SHELL: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_SHELL ! Exit at end of file

             if (index(buf, '$') /= 0) exit READ_SHELL ! Exit at new section

             block
               character(len=len(cshell)) :: tmp
               group = split_pattern(buf)
               cshell = trim(cshell)//trim(group(1))

               tmp = ''
               do i=3, size(group)
                  tmp = trim(tmp)//trim(group(i))
               end do

               group = split_pattern(group(2), '-')
               read(group(2), *) at1
               at1 = at1*2
               cshell = trim(cshell)//'  '//trim(group(1))//'-'//to_str(at1)//'  '&
                    &//trim(tmp)
             end block

          end do READ_SHELL
       end if
    end do READ_FILE
    close(u)

    ! print *, 'ATOMS'
    ! print *, trim(atoms)
    ! 
    ! print *, 'RUNDIMENSIONS'
    ! print *, trim(rundim)
    ! 
    ! print *, 'CSHELL'
    ! print *, trim(cshell)

    control = tm_read_control(original)
    block
      character(len=MAX_STR_SIZE) :: keys(5), values(5)

      keys(1) = 'atoms'
      values(1) = trim(atoms)
      keys(2) = 'rundimensions'
      values(2) = trim(rundim)
      keys(3) = 'closed shells'
      values(3) = trim(cshell)
      keys(4) = 'intsdebug'
      values(4) = '  sao'
      keys(5) = 'scfiterlimit'
      values(5) = '  0'

      call tm_change_value(control, keys, values)
    end block

    call tm_print_control(control, newfile)
  end subroutine tm_make_double_control

  subroutine tm_make_double_mos(nbas, nocc, newfile)
    integer, intent(in) :: nbas
    integer, intent(in) :: nocc
    character(len=*), intent(in) :: newfile

    real(dp), parameter :: coeff_false = 0.1_dp
    real(dp), parameter :: e_false = 0.0_dp

    integer :: nbas2, nocc2
    integer :: u, i, j, jaux
    character(len=MAX_STR_SIZE) :: buf

    nbas2 = nbas*2
    nocc2 = nocc*2

    open(newunit=u, file=newfile, action='write')
    write(u, '(a)') '$scfmo   expanded   format(4d20.14)'
    do i=1, nocc2
       write(u, '(I6,A,F16.14,A,I0)') &
            & i, '  a      eigenvalue=', e_false, 'D-00   nsaos=', nbas2

       j = 1
       jaux = 1

       buf = ''
       do while (j <= nbas2)

          write(buf, '(A,D20.14)' ) trim(buf), coeff_false
          if ((jaux == 4) .and. (j /= nbas2)) then
             write(u, '(a)') trim(buf)
             jaux = 0
             buf = ''
          end if
          if (j == nbas2) then
             write(u, '(a)') trim(buf)
             buf = ''
          end if

          j = j+1
          jaux = jaux+1
       end do
    end do
    write(u, '(a)') '$end'
    close(u)
  end subroutine tm_make_double_mos

  subroutine tm_ricc2_bin2matrix(path, nstat, mult, ciopath, nocc, nfrozen, nvirt, offset&
       &, stat)
    character(len=*) :: path
    integer :: nstat
    integer :: mult
    character(len=*) :: ciopath
    integer :: nocc
    integer :: nfrozen
    integer :: nvirt
    integer :: offset
    type(nx_status_t), intent(inout) :: stat

    ! integer :: offset
    ! character(len=MAX_STR_SIZE) :: filename
    ! character(len=MAX_CMD_SIZE) :: command
    character(len=:), allocatable :: filename
    character(len=:), allocatable :: command
    integer :: i, ierr
    character(len=256) :: cmdmsg

    do i=1, nstat-1
       filename = trim(path)//'/CCRE0-1--'//to_str(mult)//'---'//to_str(i)
       command = trim(ciopath)//'/bin2matrix -o '//to_str(offset)// &
            & ' -d '//to_str(nocc - nfrozen)//' '//to_str(nvirt)//' '//&
            & trim(filename)//' >> '//path//'/bin2matrix.out'
       ierr = 0
       call execute_command_line(command, exitstat=ierr, cmdmsg=cmdmsg)
       if (ierr /= 0) then
          if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
          call stat%append(NX_ERROR, &
               & 'Problem with running '//command//trim(cmdmsg), &
               & mod=MODNAME, func='tm_ricc2_bin2matrix'&
               & )
       end if
    end do
  end subroutine tm_ricc2_bin2matrix

  subroutine tm_get_singles_tddft(orb, singfile, tia)
    !! Read the singles amplitudes contained in ``singfile``.
    !!
    !! These amplitudes are written in ``singfile`` in
    !! matrix form, in blocks of 4 columns. We have to read a number
    !! of elements corresponding to the total dimension of the CIS
    !! problem (NOCC x NVIRT). For simplicity, these elements are read
    !! in a vector. Once enough element have been read, the vector is
    !! reshaped in matrix form and the matrix is then stored at the
    !! correct position in the ``tia`` array.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    character(len=*), intent(in) :: singfile
    !! File to read.
    real(dp), dimension(:, :, :), intent(out) :: tia
    !! Matrix containing the single amplitudes for each excitation.
    !! The last dimension should correspond to the total number of excitations.

    integer :: u, id, ierr, j, i
    integer :: state
    integer :: full_dim
    integer :: nele
    integer :: nbLines, remainder
    character(len=512) :: buf
    character(len=9) :: fmt_spec
    real(dp), dimension(:), allocatable :: tia_vec
    integer, dimension(2) :: tia_shape

    ! tia_vec is a vector containing all singles amplitudes in line
    ! it is aimed at being reshaped for storage in tia

    ! full_dim = (orb%nocc - orb%nfrozen + 1) * orb%nvirt
    full_dim = size(tia, 1) * size(tia, 2)
    allocate( tia_vec(full_dim) )

    ! For reshaping the vector at the end
    ! tia_shape = (/ orb%nvirt, (orb%nocc - orb%nfrozen + 1) /)
    tia_shape = shape( tia(:, :, 1) )

    ! The nocc * nvirt amplitudes are stored in 4 columns
    ! This amounts to nbLines full lines and rest remaining lines, with
    ! nbLines = int( nocc*nvirt / 4 )
    ! rest = mod( nocc*nvirt / 4)
    ! The remaining lines in sing_a should correspond to deexcitation
    ! amplitudes ???
    nbLines = int( full_dim / 4 )
    remainder = modulo( full_dim, 4 )

    ! Format specifier for reading the remaining lines
    write(fmt_spec, '(a1, i1, a7)') '(', remainder, 'D20.14)'

    state = 0
    nele = 0

    open(newunit=u, file=singfile, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       ! File has ended
       if (ierr /= 0) then
          exit
       end if

       ! Found '$end'
       if (buf(1:4) .eq. '$end') then
          exit
       end if

       ! New state
       id = index(buf, 'eigenvalue')
       if (id /=0 ) then
          state = state+1
          nele = 0

          do i=1, nbLines
             read(u, '(4D20.14)') (tia_vec(nele + j), j=1, 4)
             nele = nele + 4
          end do

          if (remainder /= 0) then
             read(u, fmt=fmt_spec) (tia_vec(nele+j), j=1, remainder)
          end if

          tia(:, :, state) = reshape(tia_vec, tia_shape)
       end if
    end do
    close(u)

    ! do state=1, size(tia, 3)
    !    do i=1, size(tia, 2)
    !       do j=1, size(tia, 1)
    !          if (abs(tia(j, i, state)) > 0.5) then
    !             print '(A,I0,A,I0,A,I0,A,F12.8,A,F12.8,A)', &
    !                  & 'STATE ', state, ': excitation from ', i, ' to ', j, &
    !                  & ' (coeff ', tia(j, i, state), &
    !                  & ', coeff**2 ', tia(j, i, state)**2, ')'
    !          end if
    !       end do
    !    end do
    ! end do

    deallocate(tia_vec)

  end subroutine tm_get_singles_tddft

  subroutine tm_get_singles_cc2(orb, singfile, tia)
    type(nx_orbspace_t) :: orb
    character(len=*) :: singfile
    real(dp), dimension(:, :, :) :: tia

    integer :: u, i
    integer :: istat
    character(len=MAX_STR_SIZE) :: buf

    open(newunit=u, file=singfile, action='read')
    do istat=1, size(tia, 3)
       ! Discard the first line
       read(u, *) buf
       do i=1, orb%nocc - orb%nfrozen
          read(u, *) tia(:, i, istat)
       end do
    end do
    close(u)
  end subroutine tm_get_singles_cc2


  pure subroutine turbomole_complete_setup(self, nstat)
    class(nx_turbomole_t), intent(inout) :: self
    integer, intent(in) :: nstat

    self%npre = nstat - 1
    self%nstart = self%npre
    self%nstat = nstat
  end subroutine turbomole_complete_setup
  
end module mod_turbomole_t
