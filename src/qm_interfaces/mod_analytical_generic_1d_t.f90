! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_analytical_generic_1d_t
  !! # Template for QM interface implementation
  !!
  !! This file contains a template that should be used for implementing new interfaces to
  !! QM code / methods in Newton-X. It is a minimal working example, and should be
  !! completed as required.
  !!
  !! All occurences of ``_template_`` **HAVE TO BE REPLACED** by the name chosen for the
  !! interface (/i.e/ for Gaussian that will be ``_gaussian_``.
  !!
  !! For the interface to work correctly, a block should be added to the function
  !! ``nx_qm_create_item`` in the module ``mod_qm_interfaces``.
  use mod_kinds, only: dp
  use mod_qm_generic_t, only: nx_qm_generic_t
  implicit none

  private

  public :: nx_analytical_generic_1d_t

  type, abstract, extends(nx_qm_generic_t) :: nx_analytical_generic_1d_t
     private
     real(dp), allocatable :: masses_(:)
     !! Particle masses.
     real(dp), allocatable :: geom_(:)
     !! 1D Coordinates of the system.
     real(dp), allocatable :: veloc_(:)
     !! 1D Velocity of the system.
     real(dp), allocatable :: grad_(:, :)
     !! 1D gradients.
     real(dp), allocatable :: nad_(:, :)
     !! 1D non-adiabatic couplings.
     real(dp), allocatable :: epot_(:)
     !! Potential energies.
     logical :: compute_diabpop_ = .false.
     !! Indicates if diabatic populations should be computed or not.
     real(dp), allocatable :: diabpop_(:, :)
     !! Diabatic populations. The computation of these populations is based on Subotnik
     !! et. al. [J, Chem. Phys. 139, 211101 (2013)], and the first dimension of the array
     !! corresponds to the three different models proposed in this paper.
     real(dp), allocatable :: nad_i_(:, :)
     !! (CS-FSSH ONLY) Imaginary part of the 1D non-adiabatic couplings.
     real(dp), allocatable :: gamma_(:)
     !! (CS-FSSH ONLY) Imaginary part of potential energy.

   contains
     private
     procedure, public :: memalloc
     procedure, public :: set_from_traj

     procedure, public :: masses => get_masses

     generic, public :: geom => &
          & get_geom_all, get_position_from_atom
     procedure :: get_geom_all
     procedure :: get_position_from_atom
     
     procedure, public :: veloc => get_veloc
     procedure, public :: grad => get_grad
     procedure, public :: nad => get_nad
     procedure, public :: epot => get_epot
     procedure, public :: compute_diabpop => get_compute_diabpop
     procedure, public :: diabpop => get_diabpop
     procedure, public :: nad_i => get_nad_i
     procedure, public :: gamma => get_gamma

     procedure, public :: set_masses
     procedure, public :: set_geom
     procedure, public :: set_veloc
     procedure, public :: set_grad
     procedure, public :: set_nad
     procedure, public :: set_epot
     procedure, public :: set_compute_diabpop
     procedure, public :: set_diabpop
     procedure, public :: set_nad_i
     procedure, public :: set_gamma
  end type nx_analytical_generic_1d_t
  
contains

  pure subroutine memalloc(self, nat, nstat)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    integer, intent(in) :: nat
    integer, intent(in) :: nstat

    integer :: ncoupl

    ncoupl = (nstat * (nstat - 1)) / 2

    allocate(self%masses_(nat))
    allocate(self%geom_(nat))
    allocate(self%veloc_(nat))
    allocate(self%epot_(nstat))
    allocate(self%grad_(nstat, nat))
    allocate(self%nad_(ncoupl, nat))
    allocate(self%diabpop_(3, nstat))
    allocate(self%gamma_(nstat))
    allocate(self%nad_i_(ncoupl, nat))

    self%masses_ = 0.0_dp
    self%geom_ = 0.0_dp
    self%veloc_ = 0.0_dp
    self%epot_ = 0.0_dp
    self%grad_ = 0.0_dp
    self%nad_ = 0.0_dp
    self%diabpop_ = 0.0_dp
    self%gamma_ = 0.0_dp
    self%nad_i_ = 0.0_dp
  end subroutine memalloc


  pure subroutine set_from_traj(self, geom, veloc)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: geom(:, :)
    real(dp), intent(in) :: veloc(:, :)

    self%geom_(:) = geom(1, :)
    self%veloc_(:) = veloc(1, :)
  end subroutine set_from_traj
  

  ! ===============
  ! SETTER ROUTINES
  ! ===============
  pure subroutine set_masses(self, masses)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: masses(:)

    self%masses_(:) = masses(:)
  end subroutine set_masses

  pure subroutine set_geom(self, geom)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: geom(:)

    self%geom_(:) = geom(:)
  end subroutine set_geom

  pure subroutine set_veloc(self, veloc)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: veloc(:)

    self%veloc_(:) = veloc(:)
  end subroutine set_veloc

  pure subroutine set_grad(self, grad)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: grad(:, :)

    self%grad_(:, :) = grad(:, :)
  end subroutine set_grad

  pure subroutine set_nad(self, nad)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: nad(:, :)

    self%nad_(:, :) = nad(:, :)
  end subroutine set_nad

  pure subroutine set_epot(self, epot)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: epot(:)

    self%epot_(:) = epot(:)
  end subroutine set_epot

  pure subroutine set_compute_diabpop(self, compute_diabpop)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    logical, intent(in) :: compute_diabpop

    self%compute_diabpop_ = compute_diabpop
  end subroutine set_compute_diabpop

  pure subroutine set_diabpop(self, diabpop)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: diabpop(:, :)

    self%diabpop_(:, :) = diabpop(:, :)
  end subroutine set_diabpop

  pure subroutine set_nad_i(self, nad_i)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: nad_i(:, :)

    self%nad_i_(:, :) = nad_i(:, :)
  end subroutine set_nad_i

  pure subroutine set_gamma(self, gamma)
    class(nx_analytical_generic_1d_t), intent(inout) :: self
    real(dp), intent(in) :: gamma(:)

    self%gamma_(:) = gamma(:)
  end subroutine set_gamma


  ! ===============
  ! GETTER ROUTINES
  ! ===============
  pure function get_masses(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%masses_) )

    res(:) = self%masses_(:)
  end function get_masses

  pure function get_geom_all(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%geom_) )

    res(:) = self%geom_(:)
  end function get_geom_all

  pure function get_position_from_atom(self, atom) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self
    integer, intent(in) :: atom

    real(dp) :: res

    res = self%geom_( atom )
  end function get_position_from_atom
  
  pure function get_veloc(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%veloc_) )

    res(:) = self%veloc_(:)
  end function get_veloc
  
  pure function get_grad(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%grad_, 1), size(self%grad_, 2) )

    res(:, :) = self%grad_(:, :)
  end function get_grad
  
  pure function get_nad(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%nad_, 1), size(self%nad_, 2) )

    res(:, :) = self%nad_(:, :)
  end function get_nad
  
  pure function get_epot(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%epot_) )

    res(:) = self%epot_(:)
  end function get_epot
  
  pure function get_compute_diabpop(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    logical :: res

    res = self%compute_diabpop_
  end function get_compute_diabpop
  
  pure function get_diabpop(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%diabpop_, 1), size(self%diabpop_, 2) )

    res(:, :) = self%diabpop_(:, :)
  end function get_diabpop
  
  pure function get_nad_i(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%nad_i_, 1), size(self%nad_i_, 2) )

    res(:, :) = self%nad_i_(:, :)
  end function get_nad_i
  
  pure function get_gamma(self) result(res)
    class(nx_analytical_generic_1d_t), intent(in) :: self

    real(dp) :: res( size(self%gamma_) )

    res(:) = self%gamma_(:)
  end function get_gamma

end module mod_analytical_generic_1d_t

