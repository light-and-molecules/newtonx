! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_qminfo_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Module to gather information extracted from a QM job
  !!
  !! This module defines the ``nx_qminfo_t`` datatype, to hold information such as the
  !! potential energies, gradients, etc. read from any QM output.
  !!
  !! # Usage
  !!
  !! ## General usage
  !!
  !! ```fortran
  !! integer :: nat, nstat  ! Number of atoms and states, resp.
  !! type(nx_qminfo_t) :: qminfo
  !! qminfo = nx_qminfo_t(nstat, nat)
  !!
  !! ! For CS-FSSH, with NAD read from the QM job:
  !! qminfo = nx_qminfo_t(nstat, nat, dc_method=1, run_complex=.true.)
  !! ```
  !!
  !! ## Storing lines from an output
  !!
  !! ```fortran
  !! integer :: u, ierr, id, i
  !! character(len=1024) :: buf
  !! type(nx_qminfo_t) :: qminfo
  !!
  !! qminfo = nx_qminfo_t(3, 10)
  !! open(newunit=u, file='my_output_file', action='read')
  !! do
  !!     read(u, '(A)', iostat=ierr) buf
  !!     if (ierr /= 0) exit
  !!     if (index(buf, 'my_flag') /= 0) then
  !!         call qminfo%append_data(buf)
  !!     end if
  !! end do
  !! close(u)
  !!
  !! ! Now print what we recovered, if we have something
  !! print *, qminfo%dataread
  !!  ```
  !!
  use mod_kinds, only: dp
  use mod_orbspace, only: nx_orbspace_t
  implicit none

  private

  type, public :: nx_qminfo_t
     !! Information read from QM job.
     !!
     !! The object also contains information about what to do with the information, such
     !! as if the nad reovered should be transfered to the main ``nx_traj_t`` object or
     !! not.  That can be useful in come cases (for instance, when using Columbus with
     !! state overlap couplings from ``cioverlap``).
     !!
     !! In the description ``nstat`` is the number of states, ``nat`` the number of atoms
     !! and ``ncoupl = (nstat - 1)*nstat / 2`` is the number of couplings.  All arrays
     !! that have ``ncoupl`` as a dimension are ordered as ``1/2, 1/3, ..., 1/nstat, 2/3,
     !! ..., 2/nstat, ..., nstat-1/nstat``.

     ! General data read from output
     type(nx_orbspace_t) :: orb
     !! Orbital space recovered.
     real(dp), allocatable :: repot(:)
     !! Potential energies. Dimension: ``(nstat)``.
     real(dp), allocatable :: rgrad(:, :, :)
     !! Gradients. Dimension: ``(nstat, 3, nat)``.
     real(dp), allocatable :: rnad(:, :, :)
     !! Non-adiabatic couplings. Dimension: ``(ncoupl, 3, nat)``.
     real(dp), allocatable :: osc_str(:)
     !! Oscillator strengths. Dimension: ``(ncoupl)``.
     real(dp), allocatable :: osc_str_comp(:, :)
     !! (x, y, z) components of transition dipoles. Dimension: ``(3, ncoupl)``.
     real(dp), allocatable :: trdip(:, :)
     !! (x, y, z) components of the transition dipoles. Dimension: ``(3, nstat-1)``.

     ! EXASH - specific things
     real(dp), allocatable :: epot_chrom(:, :)
     !! EXCITON MODEL: electronic potential energies for individual chromophores. The
     !! array should have dimensions ``(nchromophores, max( nstat_array ))``.
     !! *WARNING*: this array shoul be allocated by the interface (we don't have the
     !! information to declare the dimensions here) !
     logical :: use_exciton = .false.
     !! Flag to indicate if exciton models are used.
     real(dp), allocatable :: diab_pop(:)
     !! Diabatic populations.
     real(dp), allocatable :: diab_ham(:, :)
     !! Diabatic Hamiltonian.
     real(dp), allocatable :: diab_en(:)
     !! Diabatic energies.
     
     ! character(len=256), allocatable :: dataread(:)
     character(len=:), allocatable :: dataread
     !! Arbitrary strings recovered from the output. For instance, this is typically used
     !! to store the contributions to the wavefunction.
     ! integer :: dim_dataread
     !! Total number of lines actually written in ``dataread``.

     ! CS-FSSH - specific things.
     real(dp), allocatable :: rnad_i(:, :, :)
     !! CS-FSSH: Imaginary part of the non-adiabatic couplings.
     !! Dimension: ``(ncoupl, 3, nat)``.
     real(dp), allocatable :: rgamma(:)
     !! CS-FSSH: Resonance energy. Dimension: ``(nstat)``.
     logical :: is_reference = .false.
     !! CS-FSSH: Indicate if info are read from reference job (``gamma_model = 2``).

     ! Flags for transfering to ``nx_traj_t``:
     logical :: correct_phase = .true.
     !! For transfering to ``nx_traj_t``: correct the phase of the NAD or not.
     !! (Default: ``.true.``).
     logical :: update_nad = .false.
     !! For transfering to ``nx_traj_t``: Update the NAD or not.
     !! (Default: ``.false.``).

     ! Miscallaneous flags
     logical :: request_adapt_dt = .false.
     !! Flag indicating that adaptation of the time-step is required by the results of
     !! the QM call (orbital rotation detected, for instance). (Default: ``.false.``).
     logical :: has_osc_str = .true.
     logical :: has_osc_comp = .false.
     logical :: has_data_qm = .true.
   contains
     procedure :: destroy
     procedure :: append_data
     procedure :: format_osc_str
  end type nx_qminfo_t
  interface nx_qminfo_t
     module procedure qminfo_alloc
  end interface nx_qminfo_t

contains

  function qminfo_alloc(nstat, nat, dc_method, run_complex, use_exciton) result(res)
    !! Instantiate a ``nx_qminfo_t`` object.
    !!
    !! The function will only allocate memory for the CS-FSSH elements if ``run_complex``
    !! is ``.true.`` (defaults to ``.false.``).  The ``update_nad`` element will be set
    !! to ``.false.`` unless ``dc_method`` is provided and equal to 1 (the default is to
    !! set it to ``.false.``).
    integer, intent(in) :: nstat
    !! Total number of states.
    integer, intent(in) :: nat
    !! Total number of atoms.
    integer, intent(in), optional :: dc_method
    !! Method for obtaining derivative couplings (see ``nx_config_t%dc_method`` for
    !! details).
    logical, intent(in), optional :: run_complex
    !! Perform CS-FSSH or not.
    logical, intent(in), optional :: use_exciton
    !! Use exciton or not.

    type(nx_qminfo_t) :: res

    integer :: ncoupl

    logical :: is_complex, with_exc
    integer :: update_nad

    call res%orb%init()

    ncoupl = (nstat * (nstat-1)) / 2

    ! allocate(res%dataread(1024))
    allocate(res%repot(nstat))
    allocate(res%rgrad(nstat, 3, nat))
    allocate(res%rnad(ncoupl, 3, nat))
    allocate(res%osc_str(ncoupl))
    allocate(res%osc_str_comp(3, ncoupl))
    allocate(res%trdip(3, nstat-1))

    res%repot = 0.0_dp
    res%rgrad = 0.0_dp
    res%rnad = 0.0_dp
    res%osc_str = 0.0_dp
    res%osc_str_comp = 0.0_dp
    res%trdip = 0.0_dp

    res%dataread = 'Information read from QM output'//NEW_LINE('c')//NEW_LINE('c')

    is_complex = .false.
    if (present(run_complex)) is_complex = run_complex

    if (is_complex) then
       allocate(res%rnad_i(ncoupl, 3, nat))
       allocate(res%rgamma(nstat))
       res%rnad_i = 0.0_dp
       res%rgamma = 0.0_dp
    end if

    update_nad = 0
    if (present(dc_method)) update_nad = dc_method
    if (update_nad == 1) then
       res%update_nad = .true.
    end if

    if (present(use_exciton)) res%use_exciton = use_exciton

    if (res%use_exciton) then
       allocate(res%diab_pop(nstat))
       allocate(res%diab_ham(nstat, nstat))
       allocate(res%diab_en(nstat))
    else
       allocate(res%diab_pop(1))
       allocate(res%diab_ham(1, 1))
       allocate(res%diab_en(1))
    end if
    res%diab_ham = 0.0_dp
    res%diab_pop = 0.0_dp
    res%diab_en = 0.0_dp
  end function qminfo_alloc


  subroutine destroy(self)
    !! Clean the memory.
    !!
    class(nx_qminfo_t), intent(inout) :: self
    !! ``nx_qminfo_t`` object.

    deallocate(self%repot)
    deallocate(self%rgrad)
    deallocate(self%rnad)

    if (allocated(self%rgamma)) then
       deallocate(self%rgamma)
       deallocate(self%rnad_i)
    end if
  end subroutine destroy


  subroutine append_data(self, msg, unique)
    !! Append ``msg`` to member string ``dataread``.
    !!
    !! If ``unique`` is ``.true.``, ``msg`` is not added if it can already be found in the
    !! string ``self%dataread``.
    class(nx_qminfo_t), intent(inout) :: self
    !! This object.
    character(len=*), intent(in) :: msg
    !! String to append.
    logical, intent(in), optional :: unique
    !! If ``.true.``, don't append ``msg`` if it is already found in ``self%dataread``.

    logical :: make_unique, add_msg

    make_unique = .false.
    if (present(unique)) make_unique = .true.

    add_msg = .true.
    if (make_unique) then
       if (index(self%dataread, msg) /= 0) add_msg = .false.
    end if

    if (add_msg) then
       if (self%dataread == '') then
          self%dataread = trim(msg)//NEW_LINE('c')
       else
          self%dataread = self%dataread//trim(msg)//NEW_LINE('c')
       end if
    end if
  end subroutine append_data


  pure function format_osc_str(self) result(res)
    class(nx_qminfo_t), intent(in) :: self

    character(len=:), allocatable :: res

    character(len=75) :: msg
    integer :: i, j

    res = 'Oscillator strengths'
    if (self%has_osc_comp) res = res//' and transition moments'
    res = res//NEW_LINE('c')//NEW_LINE('c')

    if (self%has_osc_comp) then
       write(msg, '(5a15)') 'Excitation', 'Osc. str.', 'dx', 'dy', 'dz'
       res = res//trim(msg)//NEW_LINE('c')

       do i=1, size(self%osc_str)
          write(msg, '(i15, 4F15.6)') &
               & i, self%osc_str(i), (self%osc_str_comp(j, i), j=1, 3)
          res = res//msg//NEW_LINE('c')
       end do

    else

       write(msg, '(5a15)') 'Excitation', 'Osc. str.'
       res = res//trim(msg)//NEW_LINE('c')

       do i=1, size(self%osc_str)
          if (self%osc_str(i) > 0) then
             write(msg, '(i15, F15.6)') i, self%osc_str(i)
             res = res//msg//NEW_LINE('c')
          end if
       end do
    end if

  end function format_osc_str



end module mod_qminfo_t
