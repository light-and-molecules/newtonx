! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_conint_t
  use mod_analytical_generic_1d_t, only: nx_analytical_generic_1d_t
  use mod_configuration, only: nx_config_t
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_orbspace, only: nx_orbspace_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_conint_t

  type, extends(nx_analytical_generic_1d_t) :: nx_conint_t
     real(dp) :: mx = 0.0_dp
     real(dp) :: my = 0.0_dp
     real(dp) :: x = 0.0_dp
     real(dp) :: y = 0.0_dp
     real(dp) :: vx = 0.0_dp
     real(dp) :: vy = 0.0_dp
     real(dp) :: alpha = 3.0_dp
     real(dp) :: beta = 1.5_dp
     real(dp) :: pgamma = 0.04_dp
     real(dp) :: delta = 0.01_dp
     real(dp) :: kx = 0.02_dp
     real(dp) :: ky = 0.10_dp
     real(dp) :: x1 = 4.0_dp
     real(dp) :: x2 = 3.0_dp
     real(dp) :: x3 = 3.0_dp
   contains
     private
     procedure, public :: setup => conint_setup
     procedure, public :: print => conint_print
     procedure, public :: to_str => conint_to_str
     procedure, public :: backup => conint_backup
     procedure, public :: update => conint_update
     procedure, public :: run => conint_run
     procedure, public :: read_output => conint_read_output
     procedure, public :: write_geom => conint_write_geometry
     procedure, public :: init_overlap => conint_init_overlap
     procedure, public :: prepare_overlap => conint_prepare_overlap
     procedure, public :: extract_overlap => conint_extract_overlap
     procedure, public :: ovl_post => conint_ovl_post
     procedure, public :: ovl_run => conint_ovl_run
     procedure, public :: get_lcao => conint_get_lcao
     procedure, public :: cio_prepare_files => conint_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & conint_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => conint_cio_get_mos_energies

     procedure :: get_h => conint_h
     procedure :: get_dhdx => conint_dhdx
     procedure :: get_dhdy => conint_dhdy
  end type nx_conint_t
  interface nx_conint_t
     module procedure constructor
  end interface nx_conint_t

  character(len=*), parameter :: MODNAME = 'mod_conint_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, &
       ! MANDATORY program- and method-specific parameters
       nat, nstat, &
       ! OPTIONAL program- and method-specific parameters
       & mx, my, x, y, vx, vy, alpha, beta, gamma, delta, &
       & kx, ky, x1, x2, x3 &
       & ) result(res)
    !! Constructor for the ``nx_conint_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in) :: nat
    integer, intent(in) :: nstat

    real(dp), intent(in), optional :: mx
    real(dp), intent(in), optional :: my
    real(dp), intent(in), optional :: x
    real(dp), intent(in), optional :: y
    real(dp), intent(in), optional :: vx
    real(dp), intent(in), optional :: vy
    real(dp), intent(in), optional :: alpha
    real(dp), intent(in), optional :: beta
    real(dp), intent(in), optional :: gamma
    real(dp), intent(in), optional :: delta
    real(dp), intent(in), optional :: kx
    real(dp), intent(in), optional :: ky
    real(dp), intent(in), optional :: x1
    real(dp), intent(in), optional :: x2
    real(dp), intent(in), optional :: x3

    type(nx_conint_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    call res%set_method(method)
    call res%memalloc(nat, nstat)

    if (present(mx)) res%mx = mx
    if (present(my)) res%my = my
    if (present(x)) res%x = x
    if (present(y)) res%y = y
    if (present(vx)) res%vx = vx
    if (present(vy)) res%vy = vy
    if (present(alpha)) res%alpha = alpha
    if (present(beta)) res%beta = beta
    if (present(gamma)) res%pgamma = gamma
    if (present(delta)) res%delta = delta
    if (present(kx)) res%kx = kx
    if (present(ky)) res%ky = ky
    if (present(x1)) res%x1 = x1
    if (present(x2)) res%x2 = x2
    if (present(x3)) res%x3 = x3
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine conint_setup(self, parser, conf, inp_path, stat)
    class(nx_conint_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    call set(parser, 'con_int', self%alpha, 'alpha')
    call set(parser, 'con_int', self%beta, 'beta')
    call set(parser, 'con_int', self%pgamma, 'gamma')
    call set(parser, 'con_int', self%delta, 'delta')
    call set(parser, 'con_int', self%kx, 'kx')
    call set(parser, 'con_int', self%ky, 'ky')
    call set(parser, 'con_int', self%x1, 'x1')
    call set(parser, 'con_int', self%x2, 'x2')
    call set(parser, 'con_int', self%x3, 'x3')
  end subroutine conint_setup

  subroutine conint_print(self, out)
    class(nx_conint_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%alpha, 'alpha', unit=output)
    call print_conf_ele(self%beta, 'beta', unit=output)
    call print_conf_ele(self%pgamma, 'pgamma', unit=output)
    call print_conf_ele(self%delta, 'delta', unit=output)
    call print_conf_ele(self%kx, 'kx', unit=output)
    call print_conf_ele(self%ky, 'ky', unit=output)
    call print_conf_ele(self%x1, 'x1', unit=output)
    call print_conf_ele(self%x2, 'x2', unit=output)
    call print_conf_ele(self%x3, 'x3', unit=output)
    call print_conf_ele(self%mx, 'mx', unit=output)
    call print_conf_ele(self%my, 'my', unit=output)
  end subroutine conint_print

  function conint_to_str(self) result(res)
    class(nx_conint_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&con_int'//nl
    res = res//' alpha = '//to_str(self%alpha)//nl
    res = res//' beta = '//to_str(self%beta)//nl
    res = res//' pgamma = '//to_str(self%pgamma)//nl
    res = res//' delta = '//to_str(self%delta)//nl
    res = res//' kx = '//to_str(self%kx)//nl
    res = res//' ky = '//to_str(self%ky)//nl
    res = res//' x1 = '//to_str(self%x1)//nl
    res = res//' x2 = '//to_str(self%x2)//nl
    res = res//' x3 = '//to_str(self%x3)//nl
    res = res//'/'//nl
  end function conint_to_str

  subroutine conint_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine conint_backup

  subroutine conint_update(self, conf, traj, path, stat)
    class(nx_conint_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: r1, r2, mb

    ! Transfer new geometry and velocities to the object.
    call self%set_from_traj(traj%geom, traj%veloc)

    self%mx = 2 *traj%masses(1)
    self%my = self%mx * traj%masses(2) / (self%mx + mb)

    r1 = traj%geom(1, 2) - traj%geom(1, 1)
    r2 = traj%geom(1, 3) - traj%geom(1, 2)

    self%x = (r1 + r2) / 2.0_dp
    self%y = (r1 - r2) / 2.0_dp

    self%vx = (traj%veloc(1, 3) - traj%veloc(1, 1)) / 2.0_dp
    self%vy = (2.0_dp * traj%veloc(1, 2) - traj%veloc(1, 3) - traj%veloc(1, 2)) / 2.0_dp
  end subroutine conint_update

  subroutine conint_run(self, stat)
    class(nx_conint_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: h(2, 2), e(2), hdiag(2, 2), work(2, 2)
    real(dp) :: dhdx(2, 2), dhdy(2, 2)
    real(dp) :: dhdxa(2, 2), dhdxb(2, 2), dhdxc(2, 2), g(2, 2), gg(2, 2)
    real(dp) :: grad(2, 3), nad(1, 3)

    integer :: info, i

    ! Build hamiltonian and derivatives in diabatic basis
    h = self%get_h()
    dhdx = self%get_dhdx()
    dhdy = self%get_dhdy()

    dhdxa = - (dhdx + dhdy) / 2.0_dp
    dhdxb = dhdy
    dhdxc = (dhdx - dhdy) / 2.0_dp

    ! Diagonalize to obtain energies and adiabatic basis, stored as hdiag
    hdiag = h
    call dsyev('V', 'L', 2, hdiag, 2, e, work, 5, info)
    if (info /= 0) then
       call stat%append(NX_ERROR, 'Problem in diagonalization: '//to_str(info), &
            & mod=MODNAME, func='conint_run' &
            & )
       return
    end if

    call self%set_epot(e)

    grad = 0.0_dp
    nad = 0.0_dp

    ! first coordinate xa
    gg = matmul(transpose(hdiag), dhdxa)
    g = matmul( gg, hdiag)
    nad(1, 1) = g(1, 2) / (e(1) - e(2))
    grad(1, 1) = g(1, 1)
    grad(2, 1) = g(2, 2)

    ! second coordinate xb
    gg = matmul(transpose(hdiag), dhdxb)
    g = matmul( gg, hdiag)
    nad(1, 2) = g(1, 2) / (e(1) - e(2))
    grad(1, 2) = g(1, 1)
    grad(2, 2) = g(2, 2)

    ! third coordinate xc
    gg = matmul(transpose(hdiag), dhdxc)
    g = matmul( gg, hdiag)
    nad(1, 3) = g(1, 2) / (e(1) - e(2))
    grad(1, 3) = g(1, 1)
    grad(2, 3) = g(2, 2)

    call self%set_grad(grad)
    call self%set_nad(nad)
  end subroutine conint_run

  function conint_read_output(self, conf, traj, path, stat) result(info)
    class(nx_conint_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    info%has_osc_str = .false.
    info%has_data_qm = .false.

    info%repot(:) = self%epot()

    ! Transfer gradients
    info%rgrad(:, 1, :) = self%grad()

    ! Transfer NAD
    info%rnad(:, 1, :) = self%nad()

    ! CS-FSSH
    if (conf%run_complex) then
       info%rnad_i(:, 1, :) = self%nad_i()
       info%rgamma(:) = self%gamma()
    end if
  end function conint_read_output

  subroutine conint_write_geometry(self, traj, path, print_merged)
    class(nx_conint_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged
  end subroutine conint_write_geometry

  subroutine conint_init_overlap(self, path_to_qm, stat)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine conint_init_overlap

  subroutine conint_prepare_overlap(self, path_to_qm, stat)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine conint_prepare_overlap

  function conint_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_conint_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function conint_extract_overlap

  function conint_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function conint_get_lcao

  subroutine conint_ovl_run(self, stat)
    class(nx_conint_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine conint_ovl_run

  subroutine conint_ovl_post(self, stat)
    class(nx_conint_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine conint_ovl_post

  subroutine conint_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine conint_cio_prepare_files

  subroutine conint_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine conint_cio_get_singles_amplitudes

  subroutine conint_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_conint_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine conint_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  pure function conint_h(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    real(dp) :: eexp

    eexp = exp(-self%alpha * (self%x-self%x3)**2 - self%beta * self%y**2)
    res(1, 1) = (self%kx * (self%x-self%x1)**2 + self%ky * self%y**2) / 2.0_dp
    res(2, 2) = (self%kx * (self%x-self%x2)**2 + self%ky * self%y**2) / 2.0_dp + self%delta
    res(1, 2) = self%pgamma * self%y * eexp
    res(2, 1) = res(1, 2)
  end function conint_h


  pure function conint_dhdx(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    res(1, 1) = self%kx * (self%x-self%x1)
    res(2, 2) = self%kx * (self%x-self%x2)
    res(1, 2) = -2.0_dp * self%pgamma * self%alpha * (self%x-self%x3) &
         &      * self%y * exp( -self%beta * self%y**2 - self%alpha * (self%x-self%x3)**2)
    res(2, 1) = res(1, 2)
  end function conint_dhdx


  pure function conint_dhdy(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    res(1, 1) = self%ky * self%y
    res(2, 2) = self%ky * self%y
    res(1, 2) = (self%pgamma - 2.0_dp*self%pgamma*self%beta*self%y**2) &
         &      * exp( -self%beta * self%y**2 - self%alpha * (self%x-self%x3)**2)
    res(2, 1) = res(1, 2)
  end function conint_dhdy
end module mod_conint_t
