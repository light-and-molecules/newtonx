! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_exc_mopac_t
  use mod_async, only: run_async
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, &
       & au2ang, proton, au2debye
  use mod_exash_utils, only: nx_exash_t
  use mod_data_containers, only: &
       & mat2_container_t, mat3_container_t, &
       & geom_to_container
  use mod_interface, only: &
       & copy
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, call_external, &
       & nx_log, LOG_DEBUG
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_with_alloc
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tinker_utils, only: &
       & tinker_get_mm_energy, tinker_get_gradient
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_exc_mopac_t

  type, extends(nx_qm_generic_t) :: nx_exc_mopac_t
     integer :: nchrom = 1
     !! Number of chromophores to be treated.
     integer :: nproc= 1
     !! Total number of processors for the computation.
     integer :: gen_file = 1
     !! Generation of MOPAC input files (default: ``1``):
     !!
     !! - ``1``: Generate MOPAC and Tinker files ;
     !! - ``2``: Generate only MOPAC files ;
     !! - ``3``: Generate only Tinker files.
     character(len=:), allocatable :: mopac_env
     !! Value of the ``$MOPAC`` environment variable.
     character(len=:), allocatable :: tinker_env
     !! Value of the ``$TINKER`` environment variable.
     integer :: tresp = 1
     !! Exciton couplings using transition monopole approximation (``1``).
     integer :: gs = 0
     !! Compute only the ground state informations if "gs > 0".
     integer :: dip = 0
     !! Compute the transition dipole moments if "dip > 0".
     integer, allocatable :: nat_array(:)
     !! Array with the number of atom belonging to each chromophore.
     integer, allocatable :: nstat_array(:)
     !! Array with the number of states belonging to compute in each chromophore.
     type(nx_exash_t) :: exash
     !! EXASH configuration.
   contains
     private
     procedure, public :: setup => exc_mopac_setup
     procedure, public :: print => exc_mopac_print
     procedure, public :: to_str => exc_mopac_to_str
     procedure, public :: backup => exc_mopac_backup
     procedure, public :: update => exc_mopac_update
     procedure, public :: run => exc_mopac_run
     procedure, public :: read_output => exc_mopac_read_output
     procedure, public :: write_geom => exc_mopac_write_geometry
     procedure, public :: init_overlap => exc_mopac_init_overlap
     procedure, public :: prepare_overlap => exc_mopac_prepare_overlap
     procedure, public :: extract_overlap => exc_mopac_extract_overlap
     procedure, public :: ovl_post => exc_mopac_ovl_post
     procedure, public :: ovl_run => exc_mopac_ovl_run
     procedure, public :: get_lcao => exc_mopac_get_lcao
     procedure, public :: cio_prepare_files => exc_mopac_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & exc_mopac_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => exc_mopac_cio_get_mos_energies

     procedure :: gen_input_qm => inp_exc
     procedure :: gen_input_mm => inp_tnk
  end type nx_exc_mopac_t
  interface nx_exc_mopac_t
     module procedure constructor
  end interface nx_exc_mopac_t

  character(len=*), parameter :: MODNAME = 'mod_exc_mopac_t'

  type(nx_exash_t) :: exash_main
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & nchrom, nproc, gen_file, &
       & mopac_env, tinker_env, tresp, gs, dip, &
       & nat_array, nstat_array &
       & ) result(res)
    !! Constructor for the ``nx_exc_mopac_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    integer, intent(in), optional :: nchrom
    integer, intent(in), optional :: nproc
    integer, intent(in), optional :: gen_file
    character(len=*), intent(in), optional :: mopac_env
    character(len=*), intent(in), optional :: tinker_env
    integer, intent(in), optional :: tresp
    integer, intent(in), optional :: gs
    integer, intent(in), optional :: dip
    integer, intent(in), optional :: nat_array(:)
    integer, intent(in), optional :: nstat_array(:)

    type(nx_exc_mopac_t) :: res

    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )
    if (present(nchrom)) res%nchrom = nchrom
    if (present(nproc)) res%nproc = nproc
    if (present(gen_file)) res%gen_file = gen_file
    if (present(mopac_env)) then
       res%mopac_env = mopac_env
    else
       res%mopac_env = ''
    end if

    if (present(tinker_env)) then
       res%tinker_env = tinker_env
    else
       res%tinker_env = ''
    end if

    if (present(tresp)) res%tresp = tresp
    if (present(gs)) res%gs = gs
    if (present(dip)) res%dip = dip

    if (present(nat_array)) then
       allocate(res%nat_array(size(nat_array)))
       res%nat_array(:) = nat_array(:)
    else
       allocate(res%nat_array(1))
       res%nat_array(1) = 1
    end if

    if (present(nstat_array)) then
       allocate(res%nstat_array(size(nstat_array)))
       res%nstat_array(:) = nstat_array(:)
    else
       allocate(res%nstat_array(1))
       res%nstat_array(1) = 1
    end if

  end function constructor


  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine exc_mopac_setup(self, parser, conf, inp_path, stat)
    class(nx_exc_mopac_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo, ierr
    character(len=MAX_STR_SIZE) :: env

    prt_mo = -1
    call set(parser, 'exc_mopac', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%mopac_env == '') then
       call get_environment_variable('MOPAC', env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $MOPAC environment is not defined', &
               & mod=MODNAME, func='exc_mopac_setup')
          return
       else
          self%mopac_env = trim(env)
       end if
    end if

    if (self%tinker_env == '') then
       call get_environment_variable('TINKER', env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $TINKER environment is not defined', &
               & mod=MODNAME, func='exc_mopac_setup')
          return
       else
          self%tinker_env = trim(env)
       end if
    end if

    call set(parser, 'exc_mopac', self%nchrom, 'nchrom')
    call set(parser, 'exc_mopac', self%nproc, 'nproc')
    call set(parser, 'exc_mopac', self%gen_file, 'gen_file')
    call set_alloc(parser, 'exc_inp', self%nat_array, 'nat_array')
    call set_alloc(parser, 'exc_inp', self%nstat_array, 'nstat')

    exash_main = nx_exash_t( self%nchrom, parser, stat )

    if (self%gen_file == 1) then
       call self%gen_input_qm(inp_path)
       call self%gen_input_mm(inp_path, conf%nat)
    else if (self%gen_file == 2) then
       call self%gen_input_qm(inp_path)
    else if (self%gen_file > 2) then
       call self%gen_input_mm(inp_path, conf%nat)
    end if
  end subroutine exc_mopac_setup


  subroutine exc_mopac_print(self, out)
    class(nx_exc_mopac_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%mopac_env, 'MOPAC path', unit=output)
    call print_conf_ele(self%tinker_env, 'TINKER path', unit=output)
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
    call print_conf_ele(self%nchrom, 'nchrom', unit=output)
    call print_conf_ele(self%nproc, 'nproc', unit=output)
    call print_conf_ele(self%gen_file, 'gen_file', unit=output)

    write(output, *) ''
    write(output, *) 'EXASH configuration'
    call exash_main%print( output )
  end subroutine exc_mopac_print

  function exc_mopac_to_str(self) result(res)
    class(nx_exc_mopac_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&exc_mopac'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//' nchrom = '//to_str(self%nchrom)//nl
    res = res//' nproc = '//to_str(self%nproc)//nl
    res = res//' gen_file = '//to_str(self%gen_file)//nl
    res = res//'/'//nl//nl

    res = res//exash_main%to_str()
  end function exc_mopac_to_str

  subroutine exc_mopac_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine exc_mopac_backup

  subroutine exc_mopac_update(self, conf, traj, path, stat)
    class(nx_exc_mopac_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    real(dp) :: ti

    integer :: u, ierr, i
    character(len=256), allocatable :: filelist(:)

    ti = traj%dt * traj%step

    open(newunit=u, file=trim(path)//'/nx2exc.inf', action='write')
    write(u,*) conf%nat
    write(u,*) traj%step
    write(u,*) ti
    write(u,*) traj%dt
    write(u,*) traj%nstatdyn
    write(u,*) conf%nstat
    close(u)

    if (traj%step /= conf%init_step) then
       do i=1, self%nchrom
          ierr = copy( &
               & trim(path)//'.old/exc_mop'//to_str(i)//'_nx.mopac_oldvecs', &
               & trim(path)//'/exc_mop'//to_str(i)//'_nx.mopac_oldvecs' &
               & )
       end do
    end if

    call nx2tnk(trim(path), trim(conf%init_input)//'/fullMM_tnk.xyz', trim(path)//'/fullMM_tnk.xyz')
  end subroutine exc_mopac_update

  subroutine exc_mopac_run(self, stat)
    class(nx_exc_mopac_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    character(len=MAX_STR_SIZE) :: env, cmdmsg
    character(len=MAX_STR_SIZE) :: job_list(self%nchrom)
    integer :: i, ierr

    ierr = 0

    call call_external( &
         & self%tinker_env//'/testgrad.x fullMM_tnk.xyz -k fullMM_tnk.key Y N N', ierr, &
         & outfile='fullMM_tnk.out', cmdmsg=cmdmsg &
         & )
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running tinker'//trim(cmdmsg)//&
            & '. Please take a look at exc_mopac.job/ log files.', & 
            & mod=MODNAME, func='exc_mopac_run' &
            & )
    end if

    do i=1, size(job_list)
       job_list(i) = self%mopac_env//'/mopac2002.x exc_mop'//to_str(i)//' > mopac_tmp.out'
    end do
    call run_async(job_list, self%nproc, ierr)

    if (ierr /= 0) then
       call stat%append(NX_ERROR, &
            & 'Problem when running mopac'//&
            & '. Please take a look at exc_mopac.job/ log files.', & 
            & mod=MODNAME, func='exc_mopac_run' &
            & )
    end if
  end subroutine exc_mopac_run

  function exc_mopac_read_output(self, conf, traj, path, stat) result(info)
    class(nx_exc_mopac_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info
    real(dp), allocatable :: grad_mm(:, :), array_gs(:), omega(:), &
         & dipoles(:, :, :), osc_str(:)
    real(dp) :: enMM
    integer :: i, nstat_tot
    character(len=64), allocatable :: basename(:)

    type(mat2_container_t), allocatable :: esp(:), coord(:), ovl(:)
    type(mat3_container_t), allocatable :: grad_qm(:)

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex, &
         & use_exciton=.true. &
         & )

    ! Create list of filenames
    allocate(basename(self%nchrom))
    do concurrent (i=1:self%nchrom)
       basename(i) = trim(path)//'/exc_mop'//to_str(i)
    end do

    ! ===========
    ! COORDINATES
    ! ===========
    allocate(coord(self%nchrom))
    coord = geom_to_container(trim(path)//'/geom', self%nat_array)

    ! ========
    ! ENERGIES
    ! ========
    ! Extract site energies
    call exc_mop_get_site_energies(self, basename, omega, array_gs)

    ! Read MM energies
    enMM = tinker_get_mm_energy(trim(path)//'/fullMM_tnk.out')

    ! =========
    ! GRADIENTS
    ! =========
    ! MM gradient
    allocate(grad_mm(3, conf%nat))
    grad_mm = tinker_get_gradient(trim(path)//'/fullMM_tnk.out', conf%nat)
    
    ! QM gradients
    allocate(grad_qm( self%nchrom ))
    do i=1, self%nchrom
       allocate( grad_qm(i)%m(3, conf%nat, self%nstat_array(i)+1) )
       grad_qm(i)%m = mopac_extract_gradient(&
            & trim(path)//'/exc_mop'//to_str(i)//'_nx.grad.all', &
            & conf%nat, self%nstat_array(i)+1 &
            & )
    end do

    ! ===========
    ! ESP CHARGES
    ! ===========
    allocate(esp(self%nchrom))
    do i=1, self%nchrom
       esp(i)%m = mopac_get_electro_charges( &
            & trim(basename(i))//'.out', self%nstat_array(i), self%nat_array(i) &
            & )
    end do

    ! =================
    ! EXASH COMPUTATION
    ! =================

    ! Energies and gradients
    call exash_main%compute_epot_grad( &
         & traj%nstatdyn, omega, array_gs, enMM, coord, esp, grad_qm, &
         & grad_mm, stat &
         & )
    info%diab_ham = exash_main%diab_ham()
    info%diab_en = exash_main%diab_en()
    info%repot = exash_main%adiab_en()
    info%rgrad(traj%nstatdyn, :, :) = exash_main%adiab_grad()
    info%diab_pop = exash_main%diab_pop()

    ! Transition dipole and oscillator strengths
    if (.not. exash_main%gs())  then
       if (exash_main%dip()) then
          info%has_osc_str = .true.
          
          do i=1, self%nchrom
             dipoles = mopac_get_dipoles(trim(basename(i))//'.out', self%nstat_array(i)+1)
          end do

          nstat_tot = sum(self%nstat_array(:))
          call exash_main%compute_dipoles(dipoles)
          info%trdip = exash_main%trdip()
          info%osc_str(1:nstat_tot+1) = exash_main%osc_str()
       end if
    end if

    ! CI overlap matrix
    if (traj%step /= conf%init_step) then
       allocate(ovl(self%nchrom))
       do i=1, self%nchrom
          ovl(i)%m = mopac_get_cioverlap_from_file( &
               & trim(basename(i))//'_nx.run_cioverlap.log', self%nstat_array(i)+1&
               & )
       end do
       call exash_main%write_ovl(trim(path)//'/overlap.out', ovl)
    end if
  end function exc_mopac_read_output


  subroutine exc_mopac_write_geometry(self, traj, path, print_merged)
    class(nx_exc_mopac_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    integer :: u, i, j
    real(dp) :: mass

    ! Now write the updated 'geom' file
    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)

       ! Columbus limit in mass format
       mass = traj%masses(i) / proton
       if (mass >= 100.0_dp) then
          mass = 99.9_dp
       end if

       write(u, '(1x,a2,2x,f5.1,4f14.8)') &
            & traj%atoms(i), traj%Z(i), (traj%geom(j, i), j=1, 3), &
            & mass
    end do
    close(u)
  end subroutine exc_mopac_write_geometry

  subroutine exc_mopac_init_overlap(self, path_to_qm, stat)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_mopac_init_overlap

  subroutine exc_mopac_prepare_overlap(self, path_to_qm, stat)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_mopac_prepare_overlap

  function exc_mopac_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_exc_mopac_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function exc_mopac_extract_overlap

  function exc_mopac_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function exc_mopac_get_lcao

  subroutine exc_mopac_ovl_run(self, stat)
    class(nx_exc_mopac_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_mopac_ovl_run

  subroutine exc_mopac_ovl_post(self, stat)
    class(nx_exc_mopac_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_mopac_ovl_post

  subroutine exc_mopac_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine exc_mopac_cio_prepare_files

  subroutine exc_mopac_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine exc_mopac_cio_get_singles_amplitudes

  subroutine exc_mopac_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine exc_mopac_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  subroutine inp_exc(self, path)
    !! It generates the mopac input files in the first time step the dyncamis
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path

    integer :: i, j, k
    integer :: u
    integer :: nlines
    integer :: io
    integer :: nat
    integer :: nchrom
    integer :: natf
    integer :: nati
    integer :: nlines_ok
    integer :: nlines2
    integer :: ibuf
    integer :: nlink
    integer :: nl

    integer, allocatable, dimension(:) :: ind
    integer, allocatable, dimension(:) :: atom_lnk_pos

    integer, allocatable, dimension(:,:) :: at_pos

    character(len=200) :: nameqm
    character(len=200) :: string
    character(len=200) :: card

    character(len=200), allocatable, dimension(:) :: tot_txt
    character(len=200), allocatable, dimension(:) :: lab
    character(len=200), allocatable, dimension(:) :: add_info
    character(len=200), allocatable, dimension(:) :: name_mopac
    character(len=200), allocatable, dimension(:) :: tot_txt2
    character(len=200), allocatable, dimension(:) :: tot_txt3
    character(len=200), allocatable, dimension(:) :: atom_lnk_label

    real(dp), allocatable, dimension(:,:) :: geom

    logical :: file_exists
    logical :: conferma

    nlines = 0

    ! -- Checks the number of lines
    open(newunit=u, file=trim(path)//'/layout_exc')
    do
       read(u,*,iostat=io)
       if (io /= 0) exit
       nlines = nlines + 1
    end do
    close (u)

    ! --Reads the tinker file in order to take the geometry:
    open(newunit=u, file=trim(path)//'/fullMM_tnk.xyz', &
         & status='old', form='formatted')
    read(u,*) nat
    allocate( ind(nat), lab(nat), geom(nat,3) )
    ind(:) = 0
    lab(:) = ""
    geom(:,:) = 0._dp
    do i = 1, nat
       read(u,*) ind(i), lab(i), (geom(i,j), j = 1, 3)
    end do
    close(u)

    ! --Reads the list
    open(newunit=u, file=trim(path)//'/list_exc', &
         & status='old', form='formatted')
    read(u,*) nchrom
    allocate( at_pos(nchrom,2) )
    at_pos(:,:) = 0
    do i = 1, nchrom
       read(u,*) (at_pos(i,j), j = 1, 2)
    end do
    ! --Read link atoms (if it is available)
    rewind u

    call search("LINK ATOMS", card, u, conferma)
    if ( conferma ) then
       do i = 1 , nchrom
          read(u,*) ibuf, nlink
          if (nlink .gt. 0) then
             allocate(atom_lnk_pos(nlink))
             atom_lnk_pos(:) = 0
             allocate(atom_lnk_label(nlink))
             atom_lnk_label(:) = ""
             do j = 1, nlink
                read(u,*) atom_lnk_pos(j), atom_lnk_label(j)
             end do
             do j = 1, nlink
                lab(atom_lnk_pos(j)) = atom_lnk_label(j)
             end do
             deallocate(atom_lnk_pos)
             deallocate(atom_lnk_label)
          end if
       end do
    end if
    close(u)

    ! -- Array which contains the name of the Mopac's files
    allocate( name_mopac(nchrom) )
    name_mopac(:) = ""
    do i = 1, nchrom
       name_mopac(i) = trim(path)//'/exc_mop'//to_str(i)//'.dat'
    end do

    ! --Ignores the blak lines
    allocate( tot_txt(nlines) )
    tot_txt(:) = ""
    nlines_ok = 0
    open(newunit=u, file=trim(path)//'/layout_exc', &
         & status='old', form='formatted')
    rewind u
    do i = 1, nlines
       read(u,'(a)') string
       if (string == ' ') then
          continue
       else
          nlines_ok = nlines_ok + 1
          tot_txt(nlines_ok) = string
       end if
    end do
    close(u)

    allocate( tot_txt2(nlines_ok) )
    tot_txt2(:) = ""
    do i = 1, nlines_ok
       tot_txt2(i) = tot_txt(i)
    end do

    ! --Checks if the file "layout_exc2" exists
    inquire(file=trim(path)//"/layout_exc2", exist=file_exists)
    if (file_exists) then

       nlines2 = 0
       open(newunit=u, file=trim(path)//'/layout_exc2')
       do
          read(u,*,iostat=io)
          if (io /= 0) exit
          nlines2 = nlines2 + 1
       end do
       close (u)

       allocate( tot_txt3(nlines2) )
       tot_txt3(:) = ""
       open(newunit=u, file=trim(path)//'/layout_exc2', &
            & status='old', form='formatted')
       do i = 1, nlines2
          read(u,'(a)') string
          tot_txt3(i) = string
       end do
       close(u)

    end if

    ! --Generates the input files
    do i = 1, nchrom

       open(newunit=u, file=trim(path)//'/list_exc', &
            & status='unknown', form='formatted')
       nl = 0
       call search("ADD INF", card, u, conferma)
       if ( conferma ) then
          if ( i .gt. 1 ) then
             do j = 1, i-1
                read(u,*) ibuf, nl
                if (nl .eq. 0) then
                   continue
                else
                   do k = 1, nl
                      read(u,*)
                   end do
                end if
             end do
          end if
          read(u,*) ibuf, nl
          if (nl .eq. 0) then
             continue
          else
             allocate ( add_info(nl) )
             add_info(:) = ""
             do j = 1, nl
                read(u,'(a)') add_info(j)
             end do
          end if
       end if
       close(u)

       open(newunit=u, file=name_mopac(i), status='unknown', form='formatted')
       if (at_pos(i,1) .eq. 1) then

          natf = nat - at_pos(i,2)

          nameqm = 'QMMM='//to_str(natf)//' +'
          write(u, *) nameqm

          do j = 1, nlines_ok
             write(u,*) tot_txt2(j)
          end do

          write(u,*)
          do j = 1, at_pos(i,2)
             write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
          end do

          if (file_exists) then
             write(u,*)
             do j = 1, nlines2
                write(u,*) tot_txt3(j)
             end do
          end if

          if (nl /= 0) then
             write(u,*) ""
             do j = 1, nl
                write(u,*) add_info(j)
             end do
             write(u,*) ""
             deallocate( add_info )
          end if

       else

          nati = at_pos(i,1) - 1
          natf = nat - at_pos(i,2)

          nameqm = 'QMMM='//to_str(natf)//' MMQM='//to_str(nati)//' +'
          write(u, *) nameqm

          do j = 1, nlines_ok
             write(u,*) tot_txt2(j)
          end do
          write(u,*)

          do j = at_pos(i,1), at_pos(i,2)
             write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
          end do

          if (file_exists) then
             write(u,*)
             do j = 1, nlines2
                write(u,*) tot_txt3(j)
             end do
          end if

          if (nl /= 0) then
             write(u,*) ""
             do j = 1, nl
                write(u,*) add_info(j)
             end do
             write(u,*) ""
             deallocate( add_info )
          end if

       end if

       close(u)

    end do

100 format (a5, 1x, *(f12.6, i2))

  end subroutine inp_exc


  subroutine inp_tnk(self, path, nat_tot)
    !! It generates the tinker input files in the first time step the dyncamis
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path
    integer, intent(in) :: nat_tot

    integer :: i, j, k, l
    integer :: u
    integer :: nchrom
    integer :: nel
    integer :: nat
    integer :: nlines
    integer :: io
    integer :: nat_last
    integer :: gen_file
    integer :: ibuf
    integer :: nl
    integer :: numb

    integer, allocatable, dimension(:) :: vecnumb
    integer, allocatable, dimension(:) :: atype
    integer, allocatable, dimension(:) :: atype_new
    integer, allocatable, dimension(:,:) :: atom_group
    integer, allocatable, dimension(:,:) :: lnk
    integer, allocatable, dimension(:,:) :: at_ty

    character(len=200) :: card
    character(len=200) :: line

    character(len=200), allocatable, dimension(:) :: name_mopac
    character(len=200), allocatable, dimension(:) :: coord_one
    character(len=200), allocatable, dimension(:) :: coord_two
    character(len=200), allocatable, dimension(:) :: veclab
    character(len=200), allocatable, dimension(:) :: save_key
    character(len=200), allocatable, dimension(:) :: groups

    real(dp), allocatable, dimension(:,:) :: geom

    logical :: conferma

    gen_file = self%gen_file

    ! --Reads the "list_exc" file
    open(newunit=u, file=trim(path)//'/list_exc', &
         & form='formatted', status='old')
    read(u,*) nchrom
    allocate( atom_group(nchrom,2) )
    atom_group(:,:) = 0
    do i = 1, nchrom
       read(u,*) (atom_group(i,j), j = 1, 2)
    end do
    nat_last = atom_group(nchrom, 2)
    if (gen_file /= 5) then
       call search("ATOM TYPE", card, u, conferma)
       read(u,*) nel
       allocate( at_ty(nel,2) )
       at_ty(:,:) = 0
       do i = 1, nel
          read(u,*) (at_ty(i,j), j = 1, 2)
       end do
    else
       allocate(at_ty(1,1))
       at_ty(:, :) = 0
    end if
    close(u)

    ! --Array with the name of the Mopac's files
    allocate( name_mopac(nchrom) )
    name_mopac(:) = ""
    do i = 1, nchrom
       name_mopac(i) = trim(path)//'/exc_mop'//to_str(i)
    end do

    if (gen_file /= 5) then
       ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
       !        and add "zeros" in the end of each line;
       !
       ! -- 2°: Reads all the coluns of the strings.
       open (newunit=u, file=trim(path)//'/fullMM_tnk.xyz', &
            & status="old", form="formatted")
       read(u,*) nat
       allocate( coord_one(nat), coord_two(nat) )
       coord_one(:) = ""
       coord_two(:) = ""
       do i = 1, nat
          read (u, '(a)') line
          coord_one(i) = line
          coord_two(i) = trim(line)//"     "//"0     0     0	0	0	0	0"
       end do
       close(u)

       allocate( vecnumb(nat), veclab(nat), geom(nat,3), atype(nat), &
            & lnk(nat,8) )
       vecnumb(:) = 0
       veclab(:) = ""
       geom(:,:) = 0._dp
       atype(:) = 0
       lnk(:,:) = 0

       do i = 1, nat
          read(coord_two(i),*) vecnumb(i), veclab(i), (geom(i,j), j = 1, 3), &
               & atype(i), (lnk(i,j), j = 1, 8)
       end do

       ! --Loop over the chromophores
       !  - Switch the atoms type
       do i = 1, nchrom

          allocate( atype_new(nat) )
          atype_new(:) = 0
          atype_new = atype

          do j = atom_group(i,1), atom_group(i,2)
             do k = 1, nel
                if (atype(j) .eq. at_ty(k,2)) then
                   atype_new(j) = at_ty(k,1)
                end if
             end do
          end do

          open(newunit=u, file=trim(name_mopac(i))//'_tnk.xyz', status='unknown', &
               form='formatted')
          rewind u
          write(u,*) nat
          do j = 1, nat
             write(u,10) vecnumb(j), veclab(j), (geom(j,k), k = 1,3), &
                  & atype_new(j), (lnk(j,l), l = 1, 8)
          end do
          close(u)

          deallocate( atype_new )

       end do

    end if

10  format (4x,i2,2x,a2,2x,f12.5,1x,f12.5,1x,f12.5,1x,i6,*(3x,i3))

    if (gen_file == 3 .or. gen_file == 5 .or. gen_file == 1) then
       !
       ! => Generate the tinker.key files
       !

       ! --Checks the number of lines
       nlines = 0
       open(newunit=u, file=trim(path)//'/fullMM_tnk.key')
       do
          read(u,*,iostat=io)
          if (io /= 0) exit
          nlines = nlines + 1
       end do
       close (u)


       ! --Read all the lines layout lines
       allocate( save_key(nlines) )
       save_key(:) = ""
       open(newunit=u, file=trim(path)//'/fullMM_tnk.key', &
            & status='old', form='formatted')
       do i = 1, nlines
          read(u,'(a)') line
          save_key(i) = line
       end do
       close(u)

       ! --Loop over the chromophores
       do i = 1, nchrom
          conferma = .false.
          open(newunit=u, file=trim(path)//'/fullMM_tnk.key', &
               & form='formatted', status='old')
          call search("GROUPS", card, u, conferma)
          if (conferma) then
             if (i == 1) then
                read(u,*) ibuf, nl
                allocate( groups(nl) )
                do j = 1, nl
                   read(u,'(a)') groups(j)
                end do
             else
                do j = 1, i-1
                   read(u,*) ibuf, nl
                   do k = 1, nl
                      read(u,*)
                   end do
                end do
                read(u,*) ibuf, nl
                allocate( groups(nl) )
                do j = 1, nl
                   read(u,'(a)') groups(j)
                end do
             end if
          end if
          close(u)

          open(newunit=u, file=trim(name_mopac(i))//'_tnk.key', status='unknown', &
               form='formatted')
          rewind u

          do j = 1, nlines
             write(u,'(a)') save_key(j)
          end do
          write(u,*)
          write(u,'(a19)') '# Partial Structure'

          if (conferma) then
             do j = 1, nl
                write(u,'(a)') groups(j)
             end do
             deallocate( groups )
          else
             if (i == 1) then
                numb = atom_group(i,1)*(-1)
                write(u,12) 'GROUP', 1, numb, atom_group(i,2)
                numb = (atom_group(i,2) + 1)*(-1)
                write(u,12) 'GROUP', 2, numb, nat
                write(u,11) 'GROUP-SELECT', 1, 1, 0.0
                write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                write(u,11) 'GROUP-SELECT', 2, 2, 1.0
             else
                if ( i == nchrom) then
                   if (nat_last /= nat_tot) then
                      numb = atom_group(i,1) - 1
                      write(u,12) 'GROUP', 1, -1, numb
                      numb = atom_group(i,1)*(-1)
                      write(u,12) 'GROUP', 2, numb, atom_group(i,2)
                      numb = (atom_group(i,2) + 1)*(-1)
                      write(u,12) 'GROUP', 3, numb, nat
                      write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                      write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                      write(u,11) 'GROUP-SELECT', 1, 3, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 3, 1.0
                      write(u,11) 'GROUP-SELECT', 3, 3, 1.0
                   else
                      numb = atom_group(1,1)*(-1)
                      write(u,12) 'GROUP', 1, numb, (atom_group(i,1)-1)
                      numb = (atom_group(i,1))*(-1)
                      write(u,12) 'GROUP', 2, numb, nat
                      write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                      write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                   end if
                else
                   numb = atom_group(i,1) - 1
                   write(u,12) 'GROUP', 1, -1, numb
                   numb = atom_group(i,1)*(-1)
                   write(u,12) 'GROUP', 2, numb, atom_group(i,2)
                   numb = (atom_group(i,2) + 1)*(-1)
                   write(u,12) 'GROUP', 3, numb, nat
                   write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                   write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                   write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                   write(u,11) 'GROUP-SELECT', 1, 3, 1.0
                   write(u,11) 'GROUP-SELECT', 2, 3, 1.0
                   write(u,11) 'GROUP-SELECT', 3, 3, 1.0
                end if
             end if
          end if
          close(u)
       end do
    end if

12  format (a5, 8x, i2, 1x, i10, 1x, i10)
11  format (a12, 1x, i2, 1x, i2, 2x, f3.1)

  end subroutine inp_tnk


  subroutine nx2tnk(path, origin, newfile)
    !! It updates the tinker coordinates for the full-MM calculation
    character(len=*), intent(in) :: path
    character(len=*), intent(in) :: origin
    character(len=*), intent(in) :: newfile

    integer :: i, j, k
    integer :: u
    integer :: nat

    integer, allocatable, dimension(:) :: vecnumb
    integer, allocatable, dimension(:) :: atype

    integer, allocatable, dimension(:,:) :: lnk

    real(dp) :: rbuff

    real(dp), allocatable, dimension(:,:) :: coord

    character(len=200) :: line
    character(len=200) :: cbuff

    character(len=200), allocatable, dimension(:) :: coord_one
    character(len=200), allocatable, dimension(:) :: coord_two
    character(len=200), allocatable, dimension(:) :: veclab

    !
    ! => Reads the old tinker file in order to extract the numb of atoms,
    ! atom types and link atoms
    !

    ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
    !        and add "zeros" in the end of each line;
    !
    ! -- 2°: Reads all the coluns of the strings.
    !open (newunit=u, file=trim(trim(nx_qm%job_folder))//'/fullMM_tnk.xyz', &
    open (newunit=u, file=origin, &
         & status="old", form="formatted")
    rewind u
    read(u,*) nat
    allocate( coord_one(nat), coord_two(nat) )
    coord_one(:) = ""
    coord_two(:) = ""
    do i = 1, nat
       read (u, '(a)') line
       coord_one(i) = line
       coord_two(i) = trim(line)//"     "//"0   0   0   0   0   0	  0"
    end do
    close(u)

    allocate( vecnumb(nat), veclab(nat), atype(nat), lnk(nat,8) )
    vecnumb(:) = 0
    veclab(:) = ""
    atype(:) = 0
    lnk(:,:) = 0
    do i = 1, nat
       read(coord_two(i),*) vecnumb(i), veclab(i), rbuff, rbuff, rbuff, &
            & atype(i), (lnk(i,j), j = 1, 8)
    end do

    !
    ! => Reads the current geometry from Newton-X
    !
    allocate( coord(nat,3) )
    coord(:,:) = 0._dp

    open(newunit=u, file=trim(path)//'/geom', status='old', form='formatted')
    rewind u
    do i = 1, nat
       read(u,*) cbuff, rbuff, (coord(i,j), j = 1, 3), rbuff
    end do
    close(u)

    coord = coord * au2ang

    !
    ! => Writes the updated geometry tinker's file
    !
    open(newunit=u, file=newfile, status='unknown', form='formatted')
    rewind u
    write(u, '(3x, i6)') nat
    do i = 1, nat
       write(u,100) vecnumb(i), veclab(i), (coord(i,j), j=1,3), atype(i), &
            (lnk(i,k), k = 1,8)
    end do
    close(u)

100 format (4x,i2,2x,a2,2x,f12.5,1x,f12.5,1x,f12.5,1x,i6,*(3x,i3))

  end subroutine nx2tnk


  subroutine search(targt, stringa, nfiles, conferma)
    !! Search a string "target"
    character (len=*), intent (in)  :: targt
    character (len=*), intent (out) :: stringa
    integer, intent(in)  :: nfiles
    logical, intent(out) :: conferma

    integer :: icod

    intrinsic Index

    conferma=.false.
    do
       if (conferma) exit
       read(nfiles,'(A)',iostat=icod)stringa
       if (icod < 0) exit
       conferma=(Index(stringa,targt) /= 0)
    end do

    return
  end subroutine search


  function mopac_get_electro_charges(filename, nstat, nat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nstat
    integer, intent(in) :: nat

    real(dp), allocatable :: res(:, :)

    integer :: u, i, j, ierr, dum
    character(len=MAX_STR_SIZE) :: buf
    character(len=2) :: dumc

    allocate(res(nstat, nat))

    open(newunit=u, file=filename, action='read')
    FIND_CHARGES: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES') /= 0) &
            & then
          do i=1, nat
             read(u, *) dum, dumc, (res(j, i), j=1, nstat)
          end do
          exit FIND_CHARGES
       end if
    end do FIND_CHARGES
    close(u)
  end function mopac_get_electro_charges


  function mopac_extract_geom(filename, nat_array) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat_array(:)

    type(mat2_container_t) :: res( size(nat_array) )

    integer :: i, j, u, nchrom
    character(len=2) :: dumc
    real(dp) :: dumr

    open(newunit=u, file=filename, action='read')
    do nchrom=1, size(nat_array)
       allocate(res(nchrom)%m (3, nat_array(nchrom)))
       do i=1, nat_array(nchrom)
          read(u, *) dumc, dumr, (res(nchrom)%m(j, i), j=1, 3), dumr
       end do
    end do
    close(u)
  end function mopac_extract_geom


  function mopac_extract_gradient(filename, nat, nstat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nat
    integer, intent(in) :: nstat

    real(dp) :: res(3, nat, nstat)

    integer :: u, stat, i, j
    open(newunit=u, file=filename, action='read')
    do stat=1, nstat
       do i=1, nat
          read(u, *) (res(j, i, stat), j=1, 3)
       end do
    end do
    close(u)
  end function mopac_extract_gradient


  subroutine exc_mop_get_site_energies(self, basename, omega, array_gs)
    class(nx_exc_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: basename(:)
    real(dp), allocatable, intent(out) :: omega(:)
    real(dp), allocatable, intent(out) :: array_gs(:)

    integer :: i, j, k, l, u
    integer :: nstat_tot

    allocate(array_gs( self%nchrom )) ! GS energies
    nstat_tot = sum( self%nstat_array(:) )
    
    if (self%gs == 0) then
       allocate(omega( nstat_tot ))
    else
       allocate(omega(1))
    end if
    omega(:) = 0.0_dp
    
    j = 0
    k = 0
    do i=1, self%nchrom
       j = j + 1
       open(newunit=u, file=trim(basename(i))//'_nx.epot', action='read')
       read(u, *) array_gs(j)

       if (self%gs == 0) then
          do l=1, self%nstat_array(i)
             k = k+1
             read(u, *) omega(k)
             omega(k) = omega(k) - array_gs(j)
          end do
       end if
       close(u)
    end do
  end subroutine exc_mop_get_site_energies


  function mopac_get_dipoles(filename, nstat) result(res)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: nstat

    real(dp), allocatable :: res(:, :, :)

    integer :: ndipoles, stati, statj
    integer :: u, ierr, i, j
    character(len=1024) :: buf

    ndipoles = (nstat**2 + nstat) / 2
    allocate(res(3, nstat, nstat))
    res = 0.0_dp

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Dipoles (au)') /= 0) then
          read(u, *)

          ! Now we parse the dipole lines, of the form:
          !    state I     state J    D_IJ(x) D_IJ(y) D_IJ(z)
          !
          ! This line corresponds to dipoles(k, state I, state J), with k=1 .. 3
          !
          do i=1, ndipoles
             read(u, *) stati, statj, (res(j, stati, statj), j=1, 3)
          end do
       end if
    end do
  end function mopac_get_dipoles


  function mopac_get_cioverlap_from_file(filename, nstat) result(res)
    character(len=*), intent(in) :: filename
    integer :: nstat

    real(dp), allocatable :: res(:, :)

    integer :: u, i, j

    allocate(res(nstat, nstat))
    open(newunit=u, file=filename, action='read')
    read(u, *)
    do i=1, nstat
       read(u, *) (res(i, j), j=1, nstat)
    end do
    close(u)
  end function mopac_get_cioverlap_from_file
end module mod_exc_mopac_t
