! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_openmolcas_t
  ! The only clause is used to specify which components from each module are being used in mod_oqp_t.
  ! mod_tools, mod_interface, mod_constant may be different depending on the module
    use mod_configuration, only: nx_config_t
    use mod_constants, only: MAX_STR_SIZE, au2ang
    use mod_interface, only: &
        & copy 
    use mod_kinds, only: dp
    use mod_logger, only: print_conf_ele, &
        & call_external  
    use mod_input_parser, only: &
         & parser_t, set => set_config
    use mod_orbspace, only: nx_orbspace_t
    use mod_qm_generic_t, only: nx_qm_generic_t
    use mod_qminfo_t, only: nx_qminfo_t
    use mod_tools, only: split_pattern, to_str, split_blanks
    use mod_trajectory, only: nx_traj_t
    use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
    use iso_fortran_env, only: stdout => output_unit
    implicit none
  
    private
  
    public :: nx_openmolcas_t
  
    type, extends(nx_qm_generic_t) :: nx_openmolcas_t
       character(len=:), allocatable :: openmolcas_path
       integer :: all_grads = 0
       integer :: print_osc = 1
     contains
       private
       procedure, public :: setup => openmolcas_setup
       procedure, public :: print => openmolcas_print
       procedure, public :: to_str => openmolcas_to_str
       procedure, public :: backup => openmolcas_backup
       procedure, public :: update => openmolcas_update
       procedure, public :: run => openmolcas_run
       procedure, public :: read_output => openmolcas_read_output
       procedure, public :: write_geom => openmolcas_write_geometry
  
       !!Deferred procedures for overlap (double-molecule) computation
       procedure, public :: init_overlap => openmolcas_init_overlap
       procedure, public :: prepare_overlap => openmolcas_prepare_overlap
       procedure, public :: extract_overlap => openmolcas_extract_overlap
       procedure, public :: ovl_post => openmolcas_ovl_post
       procedure, public :: ovl_run => openmolcas_ovl_run
       procedure, public :: get_lcao => openmolcas_get_lcao
  
       !!Deferred procedures for state-overlap computation (when required)
       procedure, public :: cio_prepare_files => openmolcas_cio_prepare_files
       procedure, public :: cio_get_singles_amplitudes => &
            & openmolcas_cio_get_singles_amplitudes
       procedure, public :: cio_get_mos_energies => openmolcas_cio_get_mos_energies
    
       !! Private procedures
  
  
    end type nx_openmolcas_t
    interface nx_openmolcas_t
       module procedure constructor
    end interface nx_openmolcas_t
  
    character(len=*), parameter :: MODNAME = 'mod_openmolcas_t'
    
  contains
  
    pure function constructor(&
         ! General parameters 
         & method, prt_mo, &
         ! OPTIONAL program- and method-specific parameters
         & openmolcas_path, all_grads, print_osc &
         & ) result(res)
      !! Constructor for the ``nx_openmolcas_t`` object.
      !!
      !! This function should be the same for *ALL* interfaces defined !
      character(len=*), intent(in) :: method
      integer, intent(in), optional :: prt_mo
      integer, intent(in), optional :: all_grads
      integer, intent(in), optional :: print_osc
      character(len=*), intent(in), optional :: openmolcas_path
      
       ! Define the output
      type(nx_openmolcas_t) :: res
      ! Initialize private components from nx_qm_generic_t type
      ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
      ! or with the ``setup`` routine with a ``parser`` object.
      call res%set_method(method)
      if (present(prt_mo)) call res%set_prt_mo( prt_mo )
      if (present(openmolcas_path)) then
           res%openmolcas_path = trim(openmolcas_path)
      else
           res%openmolcas_path = ''
      end if
      if (present(all_grads)) res%all_grads = all_grads
      if (present(print_osc)) res%print_osc = print_osc
    end function constructor
  
    ! ================================= !
    ! DEFERRED ROUTINES IMPLEMENTATIONS !
    ! ================================= !
    subroutine openmolcas_setup(self, parser, conf, inp_path, stat)
      class(nx_openmolcas_t), intent(inout) :: self
      type(parser_t), intent(in) :: parser
      type(nx_config_t), intent(in) :: conf
      character(len=*), intent(in) :: inp_path
      type(nx_status_t), intent(inout) :: stat
  
      integer :: prt_mo, ierr, all_grads, print_osc
      character(len=MAX_STR_SIZE) :: env
  
      prt_mo = -1
      call set(parser, 'openmolcas', prt_mo, 'prt_mo')
      if (prt_mo /= -1) call self%set_prt_mo( prt_mo )
  
      if (self%openmolcas_path == '') then
        call get_environment_variable("MOLCAS", env, status=ierr)
        if (ierr == 1) then
           call stat%append(&
                & NX_ERROR, &
                & 'Setup ABORTED: $MOLCAS environment is not defined', &
                & mod=MODNAME, func='openmolcas_setup')
           return
        else
           self%openmolcas_path = trim(env)
        end if
     end if
  
  !   all_grads = 0
     call set(parser, 'openmolcas', self%all_grads, 'all_grads')
  !   if (all_grads /= 0) call self%set_all_grads( all_grads )
     call set(parser, 'openmolcas', self%print_osc, 'print_osc')
  
    end subroutine openmolcas_setup
  
  ! SUBROUTINE PRINT  
    subroutine openmolcas_print(self, out)
      class(nx_openmolcas_t), intent(in) :: self
      integer, intent(in), optional :: out
  
      integer :: output
      
      output = stdout
      if (present(out)) output = out
  
      write(output, '(A)') '  Method used: '//self%method()
      write(output, '(A)') ''
      call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
      call print_conf_ele(self%openmolcas_path, 'MOLCAS path', unit=output)
      call print_conf_ele(self%all_grads, 'all_grads', unit=output)
      call print_conf_ele(self%print_osc, 'print_osc', unit=output)    
    end subroutine openmolcas_print
  
  
    function openmolcas_to_str(self) result(res)
      class(nx_openmolcas_t), intent(in) :: self
  
      character(len=:), allocatable :: res
      character(len=1), parameter :: nl = NEW_LINE('c')
  
      res = '&openmolcas'//nl
      res = res//' prt_mo = '//to_str(self%prt_mo())//nl
      res = res//' all_grads = '//to_str(self%all_grads)//nl
      res = res//' print_osc = '//to_str(self%print_osc)//nl
      res = res//'/'//nl
    end function openmolcas_to_str
  
  ! SUBROUTINE BACKUP  
    subroutine openmolcas_backup(self, qm_path, chk_path, stat, only_mo)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: qm_path
      character(len=*), intent(in) :: chk_path
      type(nx_status_t), intent(inout) :: stat
      logical, intent(in), optional :: only_mo
    end subroutine openmolcas_backup
  
  
  ! SUBROUTINE UPDATE
  ! In OpenMOLCAS we don't have to update the input - Ely
    subroutine openmolcas_update(self, conf, traj, path, stat)
      class(nx_openmolcas_t), intent(inout) :: self
      type(nx_config_t), intent(in) :: conf
      type(nx_traj_t), intent(in) :: traj
      character(len=*), intent(in) :: path
      character(len=1000) :: line 
      type(nx_status_t), intent(inout) :: stat
  
      integer :: ierr, finp, stati, i
  
      ! Difference between RASSCF, CASPT2 and CS-PDFT
      select case(self%method())
      case('rasscf') 
          ! Add the flag MCLR and ALASKA for analytical gradient and COPY for information saving in the input
          open(newunit=finp, file=trim(path)//'/openmolcas.inp', position='append', action='write')
  
          write(finp, '(A)') ''
          
          if (self%all_grads == 0) then
           write(finp, '(A)') '&ALASKA'
           write(finp, '(A)') 'Root = '//to_str(traj%nstatdyn)
           write(finp, '(A)') 'PNEW'
           write(finp, '(A)') ''
          else if (self%all_grads == 1) then
         ! Obtain all gradients, not only the traj%nstatdyn
           do stati=1, conf%nstat
             write(finp, '(A)') '&ALASKA'
             write(finp, '(A)') 'Root = '//to_str(stati)
             write(finp, '(A)') 'PNEW'  
             write(finp, '(A)') ''
           end do
          end if
  
        ! Add the NACs
          if (conf%dc_method == 1) then
   
            do stati=2, conf%nstat
             do i=1, stati - 1
               write(finp, '(A)') '&ALASKA'
               write(finp, '(A)') 'NAC = '//to_str(stati)//' '//to_str(i)
               write(finp, '(A)') ''
             end do
            end do
     
          end if    
          
          if (self%print_osc == 1) then
            ! Oscillator strength 
            write(finp, '(A)') '>>>COPY $Project.JobIph JOB001'
            write(finp, '(A)') ''     
            write(finp, '(A)') '&RASSI'
            write(finp, '(A)') 'Nr of JobIph'
            write(finp, '(A)') '1 '//to_str(conf%nstat)
     
            do i = 1, conf%nstat
              write(finp, '(A)') ''//to_str(i)
            end do
          
            write(finp, '(A)') 'EJOB'
            write(finp, '(A)') 'DIPR = 0'
          
          end if
  
          write(finp, '(A)') ''
          write(finp, '(A)') '>> COPY $WorkDir/openmolcas.RasOrb $CurrDir/openmolcas.RasOrb'
          write(finp, '(A)') '>> COPY $WorkDir/openmolcas.JobIph $CurrDir/openmolcas.JobIph'
          write(finp, '(A)') ''
          close(finp)
  
      case('caspt2') 
        ! Add the flag ALASKA for gradient and COPY for information saving in the input
        open(newunit=finp, file=trim(path)//'/openmolcas.inp', position='append', action='write')
        
        write(finp, '(A)') 'GRDT'
        write(finp, '(A)') ''
  
        if (self%all_grads == 0) then
          write(finp, '(A)') '&ALASKA'
          write(finp, '(A)') 'Root = '//to_str(traj%nstatdyn)
          write(finp, '(A)') 'PNEW'
          write(finp, '(A)') ''
        else if (self%all_grads == 1) then
        ! Obtain all gradients, not only the traj%nstatdyn
          do stati=1, conf%nstat
            write(finp, '(A)') '&ALASKA'
            write(finp, '(A)') 'Root = '//to_str(stati)
            write(finp, '(A)') 'PNEW'  
            write(finp, '(A)') ''
          end do
        end if
  
        ! Add the NACs, if dc_method = 1
        if (conf%dc_method == 1) then
   
         do stati=2, conf%nstat
          do i=1, stati - 1
            write(finp, '(A)') '&ALASKA'
            write(finp, '(A)') 'NAC = '//to_str(stati)//' '//to_str(i)
            write(finp, '(A)') ''
          end do
         end do
  
       end if     
       
       if (self%print_osc == 1) then
          ! Oscillator strength 
          write(finp, '(A)') '>>>COPY $Project.JobMix JOB001'
          write(finp, '(A)') ''     
          write(finp, '(A)') '&RASSI'
          write(finp, '(A)') 'Nr of JobIph'
          write(finp, '(A)') '1 '//to_str(conf%nstat)
   
          do i = 1, conf%nstat
            write(finp, '(A)') ''//to_str(i)
          end do
        
          write(finp, '(A)') 'EJOB'
          write(finp, '(A)') 'DIPR = 0'
       end if
  
        write(finp, '(A)') ''
        write(finp, '(A)') '>> COPY $WorkDir/openmolcas.RasOrb $CurrDir/openmolcas.RasOrb'
        write(finp, '(A)') '>> COPY $WorkDir/openmolcas.JobMix $CurrDir/openmolcas.JobMix'
        write(finp, '(A)') ''
        close(finp)
  
      case('cspdft') 
        ! Add the flag GRDT, MSPDFT in &MCPDFT ALASKA for analytical gradient and COPY for information saving in the input
        open(newunit=finp, file=trim(path)//'/openmolcas.inp', position='append', action='write')
  
        write(finp, '(A)') 'GRAD'
        write(finp, '(A)') 'MSPDFT'
        write(finp, '(A)') ''
        
        if (self%all_grads == 0) then
         write(finp, '(A)') '&ALASKA'
         write(finp, '(A)') 'Root = '//to_str(traj%nstatdyn)
         write(finp, '(A)') 'PNEW'
         write(finp, '(A)') ''
        else if (self%all_grads == 1) then
       ! Obtain all gradients, not only the traj%nstatdyn
         do stati=1, conf%nstat
           write(finp, '(A)') '&ALASKA'
           write(finp, '(A)') 'Root = '//to_str(stati)
           write(finp, '(A)') 'PNEW'  
           write(finp, '(A)') ''
         end do
        end if
  
      ! Add the NACs
        if (conf%dc_method == 1) then
  
          do stati=2, conf%nstat
           do i=1, stati - 1
             write(finp, '(A)') '&ALASKA'
             write(finp, '(A)') 'NAC = '//to_str(stati)//' '//to_str(i)
             write(finp, '(A)') ''
           end do
          end do
   
        end if    
        
        if (self%print_osc == 1) then
          ! Oscillator strength 
          write(finp, '(A)') '>>>COPY $Project.JobIph JOB001'
          write(finp, '(A)') ''     
          write(finp, '(A)') '&RASSI'
          write(finp, '(A)') 'Nr of JobIph'
          write(finp, '(A)') '1 '//to_str(conf%nstat)
   
          do i = 1, conf%nstat
            write(finp, '(A)') ''//to_str(i)
          end do
        
          write(finp, '(A)') 'EJOB'
          write(finp, '(A)') 'DIPR = 0'
        
        end if
  
        write(finp, '(A)') ''
        write(finp, '(A)') '>> COPY $WorkDir/openmolcas.RasOrb $CurrDir/openmolcas.RasOrb'
        write(finp, '(A)') '>> COPY $WorkDir/openmolcas.JobIph $CurrDir/openmolcas.JobIph'
        write(finp, '(A)') ''
        close(finp)
  
  
      end select
    end subroutine openmolcas_update
  
  ! SUBROUTINE RUN
    subroutine openmolcas_run(self, stat)
      class(nx_openmolcas_t), intent(inout) :: self
      type(nx_status_t), intent(inout) :: stat
  
      integer :: ierr
      character(len=MAX_STR_SIZE) :: cmdmsg
      character(len=:), allocatable :: cmd
  
      cmd = 'pymolcas -f openmolcas.inp > openmolcas.log'
      print *, 'CMD = ', cmd
      call call_external(cmd, ierr, cmdmsg=cmdmsg)
  
     if (ierr /= 0) then
        if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
        call stat%append(NX_ERROR, &
             & 'Problem when running OpenMOLCAS'//trim(cmdmsg)//&
             & '. Please take a look at openmolcas.job/openmolcas.log', &
             & mod=MODNAME, func='openmolcas_run'&
             & )
     end if
    end subroutine openmolcas_run
  
  ! SUBROUTINE READ_OUTPUT
    function openmolcas_read_output(self, conf, traj, path, stat) result(info)
      class(nx_openmolcas_t), intent(in) :: self
      type(nx_config_t), intent(in) :: conf
      type(nx_traj_t), intent(in) :: traj
      character(len=*), intent(in) :: path
      type(nx_status_t), intent(inout) :: stat
      type(nx_qminfo_t) :: info
  
      ! Defina local variables
      integer :: u, v, id, state_id, id1, id2, i, ncoupl, gradient_state, nac_state, icoupl, stati, statj, ndipoles
      real(dp) :: en
      integer :: ierr
      character(len=MAX_STR_SIZE) :: buf
      character(len=MAX_STR_SIZE), allocatable :: split(:)
      character(len=*), parameter :: funcname = 'molcas_read_output'
      character(len=:), allocatable :: logfile
  
      ! Construct the log file path
      logfile = trim(path)//'/openmolcas.log'
  
      info = nx_qminfo_t(&
           & conf%nstat, conf%nat, &
           & dc_method=conf%dc_method, run_complex=conf%run_complex&
           & )
  
      state_id = 1
  
      ! Open the log file for reading
      open(newunit=u, file=logfile, action='read')
  
      ! Initialize gradient state counter
      gradient_state = 1
      ncoupl = (conf%nstat * (conf%nstat - 1)) / 2
      nac_state = 1
      ndipoles = ncoupl
  
      do
         ! Read a line from log file
         read(u, '(A)', iostat=ierr) buf
  
         ! Exit the loop if there's an error reading the file
         if (ierr /= 0) exit
  
         ! READ THE ENERGIES: Consider the energies written in TEMP/openmolcas.log/
         ! Loop for each state
         do i=1, conf%nstat
             ! Difference between RASSCF, CASPT2 and MC-PDFT
             select case(self%method())
         
             case('rasscf')
                ! Check for RASSCF energy
                id = index(buf, 'RASSCF root number')
                if (id /= 0) then
                   ! Split the line into words     
                   call split_blanks(buf, split)
                   ! Read the root number from the 5th word
                   read(split(5), *) id
                   ! If the root number matches i, read the energy
                   if (id == i) then
                       ! Read the energy from the 8th word
                       read(split(8), *) info%repot(state_id)
                       state_id = state_id + 1
                   end if
                end if
  
             case('caspt2')
                ! Check for CASPT2 energy
                id = index(buf, 'CASPT2 Root ')
                   if (id /= 0) then
                       ! Split the line into words     
                       call split_blanks(buf, split)
                       ! Read the root number from the 4th word
                       read(split(4), *) id
                       ! If the root number matches i, read the energy
                       if (id == i) then
                           ! Read the energy from the 7th word
                           read(split(7), *) info%repot(state_id)
                           state_id = state_id + 1
                       end if
                   end if
  
                  case('cspdft')
                    ! Check for CMS-PDFT energy
                    id = index(buf, 'PDFT Root ')
                       if (id /= 0) then
                           ! Split the line into words     
                           call split_blanks(buf, split)
                           ! Read the root number from the 4th word
                           read(split(4), *) id
                           ! If the root number matches i, read the energy
                           if (id == i) then
                               ! Read the energy from the 7th word
                               read(split(7), *) info%repot(state_id)
                               state_id = state_id + 1
                           end if
                       end if                 
  
             end select
         end do
  
         ! READ THE GRADIENTS: Consider the gradients written in TEMP/openmolcas.log/
         if (self%all_grads == 0) then
          id = index(buf, 'Molecular gradients')
          if (id /= 0) then
            ! Read 6 lines (no information in those)
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            do i=1, conf%nat
               ! Read a line and split it into words
               read(u, '(a)') buf
               call split_blanks(buf, split)
               ! Read the gradients from the 2nd, 3rd, and 4th words
               read(split(2), *) info%rgrad(traj%nstatdyn, 1, i)
               read(split(3), *) info%rgrad(traj%nstatdyn, 2, i)
               read(split(4), *) info%rgrad(traj%nstatdyn, 3, i)
            end do
          end if
  
        else if (self%all_grads == 1) then
         ! Save all gradients, not only the traj%nstatdyn
         if (index(buf, 'Molecular gradients') /= 0) then
          ! Check if the gradient state is within the expected range
          if (gradient_state <= conf%nstat) then
            ! Read 6 lines (no information in those)
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
            read(u, *) buf
           
            do i=1, conf%nat
               ! Read a line and split it into words
               read(u, '(a)') buf
               call split_blanks(buf, split)
               ! Read the gradients from the 2nd, 3rd, and 4th words
               read(split(2), *) info%rgrad(gradient_state, 1, i)
               read(split(3), *) info%rgrad(gradient_state, 2, i)
               read(split(4), *) info%rgrad(gradient_state, 3, i)
            end do
  
            ! Increment the gradient_state counter
            gradient_state = gradient_state + 1
          end if
         end if
        end if
      
       ! READ THE NACs: Consider the vectors written in TEMP/openmolcas.log/
       if (index(buf, ' Total derivative coupling') /= 0) then
        ! Check if the gradient state is within the expected range
        if (nac_state <= ncoupl) then
          ! Read 6 lines (no information in those)
          read(u, *) buf
          read(u, *) buf
          read(u, *) buf
          read(u, *) buf
          read(u, *) buf
          read(u, *) buf
          do i=1, conf%nat
             ! Read a line and split it into words
             read(u, '(a)') buf
             call split_blanks(buf, split)
             ! Read the NACs from the 2nd, 3rd, and 4th words
             read(split(2), *) info%rnad(nac_state, 1, i)
             read(split(3), *) info%rnad(nac_state, 2, i)
             read(split(4), *) info%rnad(nac_state, 3, i)
          end do
          ! Increment the gradient_state counter
          nac_state = nac_state + 1
        end if
     end if     
     
     
     if (self%print_osc == 1) then
       ! READ THE OSCILATTOR STRENGTHs: Consider the vectors written in TEMP/openmolcas.log/
       if (index(buf, 'Dipole transition strengths (spin-free states)') /= 0) then
       ! Read header lines until the table starts
       ! Read some lines (no information in those)
       read(u, *) buf
       read(u, *) buf
       read(u, *) buf
      
       ! Initialize coupling index
       icoupl = 1
         do i = 1, ndipoles
          read(u, '(A)', iostat=ierr) buf
          if (ierr /= 0) exit
          call split_blanks(buf, split)
          ! Read the states and oscillator strength
          read(split(1), *) stati
          read(split(2), *) statj
          read(split(3), *) info%osc_str(icoupl)
          ! Increment the coupling index
          icoupl = icoupl + 1
         end do
       end if
     end if
           
      end do
      
      
      close(u)
  
    end function openmolcas_read_output
  
  ! SUBROUTINE WRITE_GEOMETRY  
    subroutine openmolcas_write_geometry(self, traj, path, print_merged)
      class(nx_openmolcas_t), intent(in) :: self
      type(nx_traj_t), intent(in) :: traj
      character(len=*), intent(in) :: path
      logical, intent(in), optional :: print_merged
    
      integer :: i, u, j
  
      open(newunit=u, file=trim(path)//'/openmolcas.xyz', action='write', status='unknown')
      write(u, '(I0)') size(traj%geom, 2)     
      write(u, '(A)') ''                      
      do i=1, size(traj%geom, 2)                
        write(u, '(A4, 3F12.8)') traj%atoms(i), (traj%geom(j, i)*au2ang, j=1, 3)
      end do
      close(u)  
    
    end subroutine openmolcas_write_geometry
  
    subroutine openmolcas_init_overlap(self, path_to_qm, stat)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: path_to_qm
      type(nx_status_t), intent(inout) :: stat
    end subroutine openmolcas_init_overlap
  
    subroutine openmolcas_prepare_overlap(self, path_to_qm, stat)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: path_to_qm
      type(nx_status_t), intent(inout) :: stat
    end subroutine openmolcas_prepare_overlap
  
    function openmolcas_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
      class(nx_openmolcas_t), intent(in) :: self
      integer, intent(in) :: dim_ovl
      type(nx_status_t), intent(inout) :: stat
      character(len=*), intent(in), optional :: script_path
      real(dp) :: ovl(dim_ovl)
    end function openmolcas_extract_overlap
  
    function openmolcas_get_lcao(self, dir_path, nao, stat) result(lcao)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: dir_path
      integer, intent(in) :: nao
      type(nx_status_t), intent(inout) :: stat
      real(dp) :: lcao(nao, nao)
    end function openmolcas_get_lcao
  
    subroutine openmolcas_ovl_run(self, stat)
      class(nx_openmolcas_t), intent(in) :: self
      type(nx_status_t), intent(inout) :: stat
    end subroutine openmolcas_ovl_run
  
    subroutine openmolcas_ovl_post(self, stat)
      class(nx_openmolcas_t), intent(in) :: self
      type(nx_status_t), intent(inout) :: stat
    end subroutine openmolcas_ovl_post
  
    subroutine openmolcas_cio_prepare_files(self, path_to_qm, orb, stat)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: path_to_qm
      type(nx_orbspace_t), intent(in) :: orb
      type(nx_status_t), intent(inout) :: stat
    end subroutine openmolcas_cio_prepare_files
  
    subroutine openmolcas_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: path_to_qm
      type(nx_orbspace_t), intent(in) :: orb
      type(nx_status_t), intent(inout) :: stat
      real(dp), intent(out) :: tia(:, :, :)
      real(dp), intent(out), optional :: tib(:, :, :)
    end subroutine openmolcas_cio_get_singles_amplitudes
  
    subroutine openmolcas_cio_get_mos_energies(self, path_to_qm, stat, mos)
      class(nx_openmolcas_t), intent(in) :: self
      character(len=*), intent(in) :: path_to_qm
      type(nx_status_t), intent(inout) :: stat
      real(dp), intent(out) :: mos(:)
    end subroutine openmolcas_cio_get_mos_energies
  
    ! ================================= !
    ! PRIVATE ROUTINES IMPLEMENTATIONS  !
    ! ================================= !
  
  end module mod_openmolcas_t
  