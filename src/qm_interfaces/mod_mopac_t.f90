! Copyright (C) 2021-2023  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_mopac_t
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, timeunit, au2debye, proton
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, &
       & call_external
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_interface, only: &
       & copy
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_generic_t, only: nx_qm_generic_t
  use mod_qminfo_t, only: nx_qminfo_t
  use mod_tools, only: norm, to_str
  use mod_trajectory, only: nx_traj_t
  use mod_status_t, only: nx_status_t, NX_ERROR, NX_WARNING
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_mopac_t

  type, extends(nx_qm_generic_t) :: nx_mopac_t
     character(len=:), allocatable :: mopac_env
     !! Value of the ``$MOPAC`` environment.
   contains
     private
     procedure, public :: setup => mopac_setup
     procedure, public :: print => mopac_print
     procedure, public :: to_str => mopac_to_str
     procedure, public :: backup => mopac_backup
     procedure, public :: update => mopac_update
     procedure, public :: run => mopac_run
     procedure, public :: read_output => mopac_read_output
     procedure, public :: write_geom => mopac_write_geometry
     procedure, public :: init_overlap => mopac_init_overlap
     procedure, public :: prepare_overlap => mopac_prepare_overlap
     procedure, public :: extract_overlap => mopac_extract_overlap
     procedure, public :: ovl_post => mopac_ovl_post
     procedure, public :: ovl_run => mopac_ovl_run
     procedure, public :: get_lcao => mopac_get_lcao
     procedure, public :: cio_prepare_files => mopac_cio_prepare_files
     procedure, public :: cio_get_singles_amplitudes => &
          & mopac_cio_get_singles_amplitudes
     procedure, public :: cio_get_mos_energies => mopac_cio_get_mos_energies
  end type nx_mopac_t
  interface nx_mopac_t
     module procedure constructor
  end interface nx_mopac_t

  character(len=*), parameter :: MODNAME = 'mod_mopac_t'
  
contains

  pure function constructor(&
       ! General parameters 
       & method, prt_mo, &
       ! OPTIONAL program- and method-specific parameters
       & mopac_env) result(res)
    !! Constructor for the ``nx_mopac_t`` object.
    !!
    !! This function should be the same for *ALL* interfaces defined !
    character(len=*), intent(in) :: method
    integer, intent(in), optional :: prt_mo
    character(len=*), intent(in), optional :: mopac_env

    type(nx_mopac_t) :: res
    ! Initialize private components from nx_qm_generic_t type
    ! prt_mo is set later, either through the argument ``prt_mo`` from the function call,
    ! or with the ``setup`` routine with a ``parser`` object.
    call res%set_method(method)
    if (present(prt_mo)) call res%set_prt_mo( prt_mo )

    if (present(mopac_env)) then
       res%mopac_env = trim(mopac_env)
    else
       res%mopac_env = ''
    end if
  end function constructor

  ! ================================= !
  ! DEFERRED ROUTINES IMPLEMENTATIONS !
  ! ================================= !
  subroutine mopac_setup(self, parser, conf, inp_path, stat)
    class(nx_mopac_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf
    character(len=*), intent(in) :: inp_path
    type(nx_status_t), intent(inout) :: stat

    integer :: prt_mo, ierr
    character(len=MAX_STR_SIZE) :: env

    prt_mo = -1
    call set(parser, 'mopac', prt_mo, 'prt_mo')
    if (prt_mo /= -1) call self%set_prt_mo( prt_mo )

    if (self%mopac_env == '') then
       call get_environment_variable('MOPAC', env, status=ierr)
       if (ierr == 1) then
          call stat%append(&
               & NX_ERROR, &
               & 'Setup ABORTED: $MOPAC environment is not defined', &
               & mod=MODNAME, func='mopac_setup')
          return
       else
          self%mopac_env = trim(env)
       end if
    end if
    
  end subroutine mopac_setup

  subroutine mopac_print(self, out)
    class(nx_mopac_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output
    
    output = stdout
    if (present(out)) output = out

    write(output, '(A)') '  Method used: '//self%method()
    write(output, '(A)') ''
    call print_conf_ele(self%prt_mo(), 'prt_mo', unit=output)
  end subroutine mopac_print

  function mopac_to_str(self) result(res)
    class(nx_mopac_t), intent(in) :: self

    character(len=:), allocatable :: res
    character(len=1), parameter :: nl = NEW_LINE('c')

    res = '&mopac'//nl
    res = res//' prt_mo = '//to_str(self%prt_mo())//nl
    res = res//'/'//nl
  end function mopac_to_str

  subroutine mopac_backup(self, qm_path, chk_path, stat, only_mo)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: qm_path
    character(len=*), intent(in) :: chk_path
    type(nx_status_t), intent(inout) :: stat
    logical, intent(in), optional :: only_mo
  end subroutine mopac_backup

  subroutine mopac_update(self, conf, traj, path, stat)
    class(nx_mopac_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    integer :: u, ierr

    open(newunit=u, file=trim(path)//'/nx2exc.inf', action='write')
    write(u,*) conf%nat
    write(u,*) traj%step
    write(u,*) traj%dt * timeunit * traj%step
    write(u,*) traj%dt * timeunit
    write(u,*) traj%nstatdyn
    write(u,*) conf%nstat
    close(u)

    if (traj%step /= conf%init_step) then
       ierr = copy( &
            & trim(path)//'.old/mopac_nx.mopac_oldvecs', &
            & trim(path)//'/mopac_nx.mopac_oldvecs' &
            & )
    end if
  end subroutine mopac_update

  subroutine mopac_run(self, stat)
    class(nx_mopac_t), intent(inout) :: self
    type(nx_status_t), intent(inout) :: stat

    integer :: ierr
    character(len=MAX_STR_SIZE) :: cmdmsg
    character(len=:), allocatable :: cmd

    cmd = trim(self%mopac_env)//'/mopac2002.x mopac'

    print *, 'CMD = ', cmd
    call call_external(cmd, ierr, cmdmsg=cmdmsg)
    if (ierr /= 0) then
       if (cmdmsg /= '') cmdmsg = ': '//trim(cmdmsg)
       call stat%append(NX_ERROR, &
            & 'Problem when running mopac'//trim(cmdmsg)//&
            & '. Please take a look at mopac.job/ log files.', & 
            & mod=MODNAME, func='mopac_run'&
            & )
    end if
  end subroutine mopac_run


  function mopac_read_output(self, conf, traj, path, stat) result(info)
    class(nx_mopac_t), intent(in) :: self
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    type(nx_status_t), intent(inout) :: stat

    type(nx_qminfo_t) :: info

    integer :: u, v, i, j, ierr, stati, statj, ncoupl, icoupl
    integer :: ndipoles
    real(dp), allocatable :: dipoles(:, :, :), delta_en(:), mod_dipoles(:)
    character(len=MAX_STR_SIZE) :: buf

    info = nx_qminfo_t(&
         & conf%nstat, conf%nat, &
         & dc_method=conf%dc_method, run_complex=conf%run_complex&
         & )

    info%has_osc_comp = .true.

    open(newunit=u, file=trim(path)//'/mopac_nx.epot', action='read')
    do i = 1, conf%nstat
       read(u,*) info%repot(i)
    end do
    close(u)

    open(newunit=u, file=trim(path)//'/mopac_nx.grad', action='read')
    do i = 1, conf%nat
       read(u,*) (info%rgrad(traj%nstatdyn, j, i), j=1, 3)
    end do
    close(u)

    ! Read the matrix of dipoles.
    ndipoles = (conf%nstat**2 + conf%nstat) / 2
    allocate(dipoles(3, conf%nstat, conf%nstat))
    open(newunit=u, file=trim(path)//'/mopac.out', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Dipoles (au)') /= 0) then
          read(u, *)

          ! Now we parse the dipole lines, of the form:
          !    state I     state J    D_IJ(x) D_IJ(y) D_IJ(z)
          !
          ! This line corresponds to dipoles(k, state I, state J), with k=1 .. 3
          !
          icoupl = 1
          do i=1, ndipoles
             read(u, '(a)') buf
             read(buf, *) stati, statj, (dipoles(j, stati, statj), j=1, 3)
             if (stati /= statj) then
                read(buf, *) stati, statj, (info%osc_str_comp(j, icoupl), j=1, 3)
                icoupl = icoupl + 1
             end if
          end do
       end if
    end do
    close(u)

    ! Ultimately, we are just interested in the elements for which state J = 1
    info%trdip(:, :) = dipoles(:, 2:conf%nstat, 1)

    allocate(delta_en(conf%nstat - 1))
    do i=1, conf%nstat - 1
       delta_en(i) = info%repot(i+1) - info%repot(i)
    end do

    allocate(mod_dipoles(conf%nstat - 1))
    do i=1, conf%nstat-1
       mod_dipoles(i) = norm(info%trdip(:, i))
    end do
    
    info%osc_str(:) = 2.0_dp / 3.0_dp * delta_en(:) * mod_dipoles(:)**2

    ! Overlap matrix
    open(newunit=u, file=trim(path)//'/mopac_nx.run_cioverlap.log', action='read')
    read(u, *)
    open(newunit=v, file=trim(path)//'/overlap.out', action='write')
    write(v, '(A)') 'CI overlap matrix'
    write(v, *) conf%nstat, conf%nstat
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(A)') trim(buf)
    end do
    close(v)
    close(u)

    ! Non-adiabatic couplings
    ncoupl = (conf%nstat * (conf%nstat - 1)) / 2
    open(newunit=u, file=trim(path)//'/mopac_nx.nad_vectors', action='read')
    do stati=1,  ncoupl
       do i=1, conf%nat
          read(u, *) (info%rnad(stati, j, i), j=1, 3)
       end do
    end do
    close(u)
  end function mopac_read_output


  subroutine mopac_write_geometry(self, traj, path, print_merged)
    class(nx_mopac_t), intent(in) :: self
    type(nx_traj_t), intent(in) :: traj
    character(len=*), intent(in) :: path
    logical, intent(in), optional :: print_merged

    real(dp) :: mass
    integer :: i, j, u

    open(newunit=u, file=trim(path)//'/geom', action='write')
    do i=1, size(traj%geom, 2)

       ! Columbus limit in mass format
       mass = traj%masses(i) / proton
       if (mass >= 100.0_dp) then
          mass = 99.9_dp
       end if

       write(u, '(1x,a2,2x,f5.1,4f14.8)') &
            & traj%atoms(i), traj%Z(i), (traj%geom(j, i), j=1, 3), &
            & mass
    end do
    close(u)
  end subroutine mopac_write_geometry

  subroutine mopac_init_overlap(self, path_to_qm, stat)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine mopac_init_overlap

  subroutine mopac_prepare_overlap(self, path_to_qm, stat)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
  end subroutine mopac_prepare_overlap

  function mopac_extract_overlap(self, dim_ovl, stat, script_path) result(ovl)
    class(nx_mopac_t), intent(in) :: self
    integer, intent(in) :: dim_ovl
    type(nx_status_t), intent(inout) :: stat
    character(len=*), intent(in), optional :: script_path
    real(dp) :: ovl(dim_ovl)
  end function mopac_extract_overlap

  function mopac_get_lcao(self, dir_path, nao, stat) result(lcao)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: dir_path
    integer, intent(in) :: nao
    type(nx_status_t), intent(inout) :: stat
    real(dp) :: lcao(nao, nao)
  end function mopac_get_lcao

  subroutine mopac_ovl_run(self, stat)
    class(nx_mopac_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine mopac_ovl_run

  subroutine mopac_ovl_post(self, stat)
    class(nx_mopac_t), intent(in) :: self
    type(nx_status_t), intent(inout) :: stat
  end subroutine mopac_ovl_post

  subroutine mopac_cio_prepare_files(self, path_to_qm, orb, stat)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
  end subroutine mopac_cio_prepare_files

  subroutine mopac_cio_get_singles_amplitudes(self, path_to_qm, orb, tia, stat, tib)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_orbspace_t), intent(in) :: orb
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: tia(:, :, :)
    real(dp), intent(out), optional :: tib(:, :, :)
  end subroutine mopac_cio_get_singles_amplitudes

  subroutine mopac_cio_get_mos_energies(self, path_to_qm, stat, mos)
    class(nx_mopac_t), intent(in) :: self
    character(len=*), intent(in) :: path_to_qm
    type(nx_status_t), intent(inout) :: stat
    real(dp), intent(out) :: mos(:)
  end subroutine mopac_cio_get_mos_energies

  ! ================================= !
  ! PRIVATE ROUTINES IMPLEMENTATIONS  !
  ! ================================= !
  
end module mod_mopac_t
