! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_orbspace
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module defines the ``nx_orbspace_t`` type that holds
  !! information about the orbital space used in the electronic
  !! structure computation.
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_orbspace_t

  type nx_orbspace_t
     !! Object holding information about the orbital space in the
     !! electronic structure computation. It holds general information
     !! on the number of orbitals and their distribution.
     integer :: nfrozen = -1
     !! Number of frozen (or core) orbitals.
     integer :: nocc = -1
     !! Number of occupied orbital (not frozen).
     integer :: nvirt = -1
     !! Number of virtual orbitals.
     integer :: ndisc = -1
     !! Number of frozen virtual orbitals
     integer :: ninact = -1
     !! Number of inactive (doubly occupied) orbitals.
     integer :: nelec = -1
     !! Number of electrons.
     integer :: nbas = -1
     !! Number of basis functions.

     ! For UHF cases
     integer :: nocc_b = -1
     integer :: nvirt_b = -1

     ! Specific to Gaussian
     integer :: mseek = -1
   contains
     procedure :: init
     procedure :: print
     procedure :: import_orb

  end type nx_orbspace_t

contains

  subroutine init(this)
    !! Initialize all values in the object.
    !!
    !! All values are initialized to -1.
    class(nx_orbspace_t) :: this
    !! This object.

    this%nfrozen = -1
    this%nocc = -1
    this%nvirt = -1
    this%ndisc = -1
    this%ninact = -1
    this%nelec = -1

    this%nocc_b = -1
    this%nvirt_b = -1

    this%mseek = -1
  end subroutine init


  subroutine print(this, out)
    class(nx_orbspace_t) :: this
    integer, intent(in), optional :: out

    character(len=14) :: fmt

    integer :: output

    output = stdout
    if (present(out)) output = out

    fmt = '(a24, a3, a10)'

    write(output, '(a)') ' '
    write(output, '(a)') repeat('#', 40)
    write(output, '(a)') repeat(' ', 13)//'ORBITAL SPACE:'//repeat(' ', 13)
    write(output, '(a)') ''

    if (this%nbas /= -1) then
       call print_conf_ele(this%nbas, 'Nr. of basis functions', prtfmt=fmt, unit=output)
    end if

    if (this%nfrozen /= -1) then
       call print_conf_ele(this%nfrozen, 'Nr. of frozen core orb', prtfmt=fmt, unit=output)
    end if

    if (this%nocc /= -1) then
       call print_conf_ele(this%nocc, 'Nr. of occupied orb', prtfmt=fmt, unit=output)
    end if

    if (this%nvirt /= -1) then
       call print_conf_ele(this%nvirt, 'Nr. of virtual orb', prtfmt=fmt, unit=output)
    end if

    if (this%nocc_b /= -1) then
       call print_conf_ele(this%nocc_b, 'Nr. of occupied orb (b)', prtfmt=fmt, unit=output)
    end if

    if (this%nvirt_b /= -1) then
       call print_conf_ele(this%nvirt_b, 'Nr. of virtual orb (b)', prtfmt=fmt, unit=output)
    end if

    if (this%ndisc /= -1) then
       call print_conf_ele(this%ndisc, 'Nr. of frozen virtual orb', prtfmt=fmt, unit=output)
    end if

    if (this%ninact /= -1) then
       call print_conf_ele(this%ninact, 'Nr. of inactive orb', prtfmt=fmt, unit=output)
    end if

    if (this%nelec /= -1) then
       call print_conf_ele(this%nelec, 'Nr. of electrons', prtfmt=fmt, unit=output)
    end if

    if (this%mseek /= -1) then
       call print_conf_ele(this%mseek, 'Nr. of roots', prtfmt=fmt, unit=output)
    end if
    write(output, '(a)') repeat('#', 40)
    write(output, '(a)') ''
  end subroutine print


  subroutine import_orb(this, filename)
   class(nx_orbspace_t), intent(inout) :: this
   character(len=*), intent(in) :: filename

   integer :: u

   open(newunit=u, file=filename, action='read')
   read(u,*) this%nfrozen
   read(u,*) this%nocc
   read(u,*) this%nocc_b
   read(u,*) this%nvirt
   read(u,*) this%nvirt_b
   read(u,*) this%nelec
   close(u)
  end subroutine import_orb

end module mod_orbspace
