! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh_traj_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-06-11
  !!
  !! This module defines the SH trajectory type ``nx_sh_traj_t`` for
  !! handling the evolution of the electronic population during the
  !! integration of the time-dependant Schrödinger equation (TDSE),
  !! and for computing the surface hopping probabilities.
  !!
  !! The evolution of the wavefunction is implemented as
  !! described in Ferretti, Granucci et al. J. Chem. Phys., 104, (1996)
  !! [10.1063/1.471791](http://dx.doi.org/10.1063/1.471791), with the
  !! following coupled equations to solve:
  !!
  !! \[ \dot{A_K}(t) = -\sum_{K \neq L} A_L(t) e^{i\gamma_{KL}}
  !! \sigma_{KL}^{NAD}, \]
  !! \[ \gamma_K(t) = \int_{0}^{t} E_K(t')dt'. \]
  !!
  !! In addition, the probabilities \(g_{KL}\) of hopping from state \(K\) to
  !! \(L\) are computed with the Tully method:
  !!
  !! \[ b_{KL}(t) = -2 \mathrm{Re} (A_j(t)A_j^{*}(t)
  !!    e^{i\gamma_{KL}} \sigma^{NAD}_{KL}(t)) \]
  !! \[ g_{KL} = \frac{\int_{t}^{t+dt} dt b_{LK}(t)}{|A_K(t)|^2}.\]
  !!
  !! The components of ``nx_sh_traj_t`` can be split into the
  !! ``fgeneric_c`` and ``fgeneric_r`` derived types ``nx_coeff_t``,
  !! ``nx_phase_t`` and ``nx_shprob_t`` to handle the evolution of
  !! the coefficients, phases, and probabilities respectively, with
  !! the functions defined in ``mod_integrators.f08``.

  ! !! with decoherence
  ! !! corrections from Granucci, Persico and Toniolo, J. Chem. Phys. 114
  ! !! (2001) [10.1063/1.1376633](http://dx.doi.org/10.1063/1.1376633).

  !! In this documentation we adopt the following notations:
  !!
  !! - \(T\): time of the classical trajectory ;
  !! - \(\Delta T\): time-step of the classical trajectory ;
  !! - \(t\): time of the SH trajectory ;
  !! - \(\delta t\): time-step of the SH trajectory ;
  !! - \(t_0 = T - \Delta T\): starting time for the SH trajectory ;
  !! - \(t_1 = T\): ending time for the SH trajectory.
  use mod_kinds, only: dp
  use mod_tools, only: to_str
  use mod_sh_coeff_t, only: nx_coeff_t
  use mod_sh_prob_t, only: nx_shprob_t
  use mod_sh_phase_t, only: nx_phase_t
  implicit none

  private

  public :: nx_sh_traj_t

  type :: nx_sh_traj_t
     !! Object handling the TDSE trajectory.
     !!
     !! The main member is the electronic coefficient ``acoef``, and the
     !! corresponding derivative.  The electronic population can be obtained
     !! by squaring ``acoef``.  As stated in Ferretti, Granucci et al., the
     !! coefficients follow:
     !!
     !! \[\dot{A}_L = - \sum_{K \neq L}
     !!         A_K e^{-i\gamma_{KL}} \sigma^{NAD}_{KL}
     !! \],
     !!
     !! where \(\gamma_{KL}\) is the phase between states \(K\) and
     !! \(L\), and \(\sigma^{NAD}_{KL}\) is the dot product of the
     !! atomic velocity and the non-adiabatic coupling vector between
     !! states states \(K\) and \(L\).  These two quantities are also part of
     !! the object.
     !!
     !! The velocity, potential energy and non-adiabatic vectors are
     !! obtained by interpolating between the corresponding values from
     !! the main ``nx_traj`` object.  For example, at each micro-iteration,
     !! ``nx_sh_traj_t%veloc`` is the result of a linear interpolation
     !! between ``nx_traj%old_veloc`` and ``nx_traj%veloc``.

     private
     
     ! Parameters for the dynamic frame (usually obtained by interpolating the quantities
     ! from the main dynamics)
     real(dp), allocatable :: veloc(:, :)
     !! Current velocity.
     real(dp), allocatable :: sigma(:)
     !! Current derivative couplings (product for the velocity and NACV).
     real(dp), allocatable :: epot(:)
     !! Current potential energies.

     ! Integration quantities
     type(nx_coeff_t) :: fcoeffs
     type(nx_phase_t) :: fphase
     type(nx_shprob_t) :: fprob

     ! Surface hopping results
     integer :: isurf
     !! Current active potential energy surface

     ! CS-FSSH
     logical :: run_complex
     !! Indicate if complex surfaces are used or not.
     real(dp), dimension(:), allocatable :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings
     real(dp), dimension(:), allocatable :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy (\( \Gamma \) in the equations from
     !! paper Kossoski and Barbatti, Chem. Science (2020), 11
     
   contains
     private
     procedure, public :: compute_adot => sh_traj_compute_adot
     procedure, public :: to_str => sh_traj_to_str

     ! Setters
     procedure, public :: set_veloc => sh_traj_set_veloc
     procedure, public :: set_sigma => sh_traj_set_sigma
     procedure, public :: set_epot => sh_traj_set_epot
     procedure, public :: set_wf => sh_traj_set_wf
     procedure, public :: set_gamma => sh_traj_set_gamma
     procedure, public :: set_isurf => sh_traj_set_isurf
     procedure, public :: set_sigma_i => sh_traj_set_sigma_i
     procedure, public :: set_epot_i => sh_traj_set_epot_i

     ! Getters
     procedure, public :: get_fphase => sh_traj_get_fphase
     procedure, public :: get_fcoeff => sh_traj_get_fcoeff
     procedure, public :: get_fprob => sh_traj_get_fprob
     procedure, public :: get_veloc => sh_traj_get_veloc
     procedure, public :: get_acoef => sh_traj_get_acoef
     procedure, public :: get_sigma => sh_traj_get_sigma
     procedure, public :: get_epot => sh_traj_get_epot
     procedure, public :: get_epot_from_state => sh_traj_get_epot_from_state
     procedure, public :: get_wf => sh_traj_get_wf
     procedure, public :: get_gamma => sh_traj_get_gamma
     procedure, public :: get_isurf => sh_traj_get_isurf
     procedure, public :: get_sigma_i => sh_traj_get_sigma_i
     procedure, public :: get_epot_i => sh_traj_get_epot_i
     procedure, public :: is_complex => sh_traj_is_complex
  end type nx_sh_traj_t
  interface nx_sh_traj_t
     module procedure constructor
  end interface nx_sh_traj_t

contains

  function constructor(nat, nstat, run_complex) result(res)
    integer, intent(in) :: nat
    !! Number of atoms in the system.
    integer, intent(in) :: nstat
    !! Number of states included in the computation.
    logical, intent(in) :: run_complex
    !! Indicate if complex surfaces are used or not.

    type(nx_sh_traj_t) :: res

    integer :: ncoupl

    ncoupl = nstat * (nstat-1) / 2

    res%run_complex = run_complex

    allocate( res%veloc(3, nat) )
    allocate( res%epot(nstat) )
    allocate( res%sigma(ncoupl) )

    res%fcoeffs = nx_coeff_t(nstat, run_complex)
    res%fphase = nx_phase_t(nstat)
    res%fprob = nx_shprob_t(nstat, run_complex)

    if (run_complex) then
       allocate( res%epot_i(nstat) )
       allocate( res%sigma_i(ncoupl) )
    else
       allocate( res%epot_i(1) )
       allocate( res%sigma_i(1) )
    end if

    res%veloc(:, :) = 0.0_dp
    res%epot(:) = 0.0_dp
    res%sigma(:) = 0.0_dp
    res%isurf = -1
    res%epot_i = 0.0_dp
    res%sigma_i = 0.0_dp
  end function constructor

  subroutine sh_traj_compute_adot(self)
    class(nx_sh_traj_t), intent(inout) :: self

    self%fcoeffs%adot = self%fcoeffs%get_deriv()
    self%fphase%adot = self%fphase%get_deriv()
    self%fprob%adot = self%fprob%get_deriv()
  end subroutine sh_traj_compute_adot

  pure function sh_traj_to_str(self) result(str)
    class(nx_sh_traj_t), intent(in) :: self

    character(len=:), allocatable :: str

    character(len=4096) :: buf

    buf = 'nx_sh_traj_t('//NEW_LINE('c')
    buf = trim(buf)//self%fphase%to_str()//self%fcoeffs%to_str()//self%fprob%to_str()
    buf = trim(buf)//NEW_LINE('c')//')'
    str = trim(buf)
  end function sh_traj_to_str

  ! =======
  ! Setters
  ! =======
  subroutine sh_traj_set_veloc(self, veloc)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: veloc(:, :)

    self%veloc(:, :) = veloc(:, :)
  end subroutine sh_traj_set_veloc

  
  subroutine sh_traj_set_sigma(self, sigma)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: sigma(:)

    self%sigma(:) = sigma(:)
    self%fcoeffs%sigma(:) = sigma(:)
    self%fprob%sigma(:) = sigma(:)
  end subroutine sh_traj_set_sigma


  subroutine sh_traj_set_epot(self, epot)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: epot(:)

    self%epot(:) = epot(:)
    self%fphase%epot(:) = epot(:)
  end subroutine sh_traj_set_epot


  subroutine sh_traj_set_gamma(self, gamma)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: gamma(:)

    self%fphase%acoef(:) = gamma(:)
    self%fcoeffs%gamma(:) = gamma(:)
    self%fprob%gamma(:) = gamma(:)
  end subroutine sh_traj_set_gamma


  subroutine sh_traj_set_wf(self, wf)
    class(nx_sh_traj_t), intent(inout) :: self
    complex(dp), intent(in) :: wf(:)

    self%fcoeffs%acoef(:) = wf(:)
    self%fprob%atraj(:) = wf(:)
  end subroutine sh_traj_set_wf


  subroutine sh_traj_set_isurf(self, isurf)
    class(nx_sh_traj_t), intent(inout) :: self
    integer, intent(in) :: isurf

    self%fprob%isurf = isurf
    self%isurf = isurf
  end subroutine sh_traj_set_isurf


  subroutine sh_traj_set_epot_i(self, epot_i)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: epot_i(:)

    self%epot_i(:) = epot_i(:)
    self%fcoeffs%epot_i(:) = epot_i(:)
    self%fprob%epot_i(:) = epot_i(:)
  end subroutine sh_traj_set_epot_i


  subroutine sh_traj_set_sigma_i(self, sigma_i)
    class(nx_sh_traj_t), intent(inout) :: self
    real(dp), intent(in) :: sigma_i(:)

    self%sigma_i(:) = sigma_i(:)
    self%fcoeffs%sigma_i(:) = sigma_i(:)
    self%fprob%sigma_i(:) = sigma_i(:)
  end subroutine sh_traj_set_sigma_i


  ! =======
  ! Getters
  ! =======

  pure function sh_traj_get_fphase(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    type(nx_phase_t) :: res

    res = self%fphase
  end function sh_traj_get_fphase


  pure function sh_traj_get_fcoeff(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    type(nx_coeff_t) :: res

    res = self%fcoeffs
  end function sh_traj_get_fcoeff


  pure function sh_traj_get_fprob(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    type(nx_shprob_t) :: res

    res = self%fprob
  end function sh_traj_get_fprob
  
  
  pure function sh_traj_get_veloc(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%veloc, 1), size(self%veloc, 2))

    res(:, :) = self%veloc(:, :)
  end function sh_traj_get_veloc
  
  pure function sh_traj_get_acoef(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    complex(dp) :: res( size(self%fcoeffs%acoef) )

    res(:) = self%fcoeffs%acoef(:)
  end function sh_traj_get_acoef
  
  pure function sh_traj_get_sigma(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%sigma) )

    res(:) = self%sigma(:)
  end function sh_traj_get_sigma
  
  pure function sh_traj_get_epot(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%epot) )

    res(:) = self%epot(:)
  end function sh_traj_get_epot

  pure function sh_traj_get_epot_from_state(self, state) result(res)
    class(nx_sh_traj_t), intent(in) :: self
    integer, intent(in) :: state

    real(dp) :: res

    res = self%epot(state)
  end function sh_traj_get_epot_from_state
  
  pure function sh_traj_get_gamma(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%fphase%acoef) )

    res(:) = self%fphase%acoef(:)
  end function sh_traj_get_gamma
  
  pure function sh_traj_get_isurf(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    integer :: res

    res = self%isurf
  end function sh_traj_get_isurf
  
  pure function sh_traj_get_sigma_i(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%sigma_i) )

    res(:) = self%sigma_i(:)
  end function sh_traj_get_sigma_i
  
  pure function sh_traj_get_epot_i(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%epot_i) )

    res(:) = self%epot_i(:)
  end function sh_traj_get_epot_i


  pure function sh_traj_get_wf(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    complex(dp) :: res( size(self%epot) )

    res = self%fcoeffs%acoef
  end function sh_traj_get_wf

  pure function sh_traj_is_complex(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    logical :: res

    res = self%run_complex
  end function sh_traj_is_complex


  pure function sh_traj_inline_veloc(self) result(res)
    class(nx_sh_traj_t), intent(in) :: self

    real(dp) :: res( size(self%veloc, 1) * size(self%veloc, 2) )

    integer :: i, j, ii

    ii = 1
    do i=1, size(self%veloc, 2)
       do j=1, size(self%veloc, 1)
          res(ii) = self%veloc(j, i)
          ii = ii + 1
       end do
    end do
  end function sh_traj_inline_veloc
end module mod_sh_traj_t
