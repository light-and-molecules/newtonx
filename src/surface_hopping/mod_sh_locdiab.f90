! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_sh_locdiab
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2023-07
  !!
  !! # Local diabatization
  !!
  !! This module handles the conversion from adiabatic to diabatic basis for
  !! local diabatization approaches in Newton-X.  The implementation is described in the
  !! following papers:
  !! 
  !! - G. Granucci, M. Persico, A. Toniolo, /J. Chem. Phys./ **114** (2001),
  !! DOI: [https://doi.org/10.1063/1.1376633](10.1063/1.1376633) for the description of
  !! the diabatization scheme and equations ;
  !! - F. Plasser et al., /J. Chem. Phys./, **137** (2012), DOI:
  !! [https://doi.org/10.1063/1.4738960](10.1063/1.4738960) for the original Newton-X
  !! implementation.
  !!
  !! ## Contributions
  !!
  !! - Adaptation for Newton-X by Felix Plasser (felix.plasser@univie.ac.at)
  !! (2010-Mar-15) ;
  !! - New implementation by Baptiste Demoulin (baptiste.demoulin@univ-amu.fr) (2023-07)
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_logger_t, nx_log, &
       & LOG_DEBUG, LOG_WARN, LOG_INFO, LOG_ERROR
  use mod_numerics, only: lowdin
  use mod_tools, only: to_str
  use mod_constants, only: MAX_CMD_SIZE
  implicit none

  private

  public :: ld_compute_coeffs_and_prob

contains

  subroutine ld_compute_coeffs_and_prob(&
       & new_epot, old_epot, old_coeff, dt, nstatdyn, ciovl, prob, new_coeff, logger)
    real(dp), intent(in) :: new_epot(:)
    real(dp), intent(in) :: old_epot(:)
    complex(dp), intent(in) :: old_coeff(:)
    real(dp), intent(in) :: dt
    integer, intent(in) :: nstatdyn
    real(dp), intent(in) :: ciovl(:, :)
    real(dp), intent(inout) :: prob(:)
    complex(dp), intent(inout) :: new_coeff(:)
    type(nx_logger_t), intent(inout) :: logger

    real(dp), allocatable :: stmat(:, :)
    complex(dp), allocatable :: rotation(:, :)
    integer, allocatable :: intruders(:)
    integer :: i, status, nstat, new_nstatdyn

    nstat = size(new_epot)

    allocate(stmat, mold=ciovl)
    stmat = ld_check_intruder_states(ciovl, intruders)
    if (allocated(intruders)) then
       do i=1, size(intruders)
          call logger%log(LOG_WARN,&
               & 'LD: Intruder state found: '//to_str(intruders(i)))
       end do
    end if

    call logger%log(LOG_DEBUG, stmat, title='S-matrix, before Lowdin')
    call lowdin(stmat, status)
    if (status > 0) then
       call logger%log(LOG_ERROR, 'LD: lowdin_new -> dsyev failed to converge')
       return
    else if (status < 0) then
       call logger%log(LOG_ERROR, &
            & 'LD: lowdin_new -> illegal argument '//to_str(status)//' to dsyev')
       return
    end if
    call logger%log(LOG_DEBUG, stmat, title='S-matrix, after Lowdin')

    allocate(rotation(nstat, nstat))
    rotation = ld_compute_full_rotation(new_epot, old_epot, 0.0_dp, dt, stmat)
    call logger%log(LOG_DEBUG, real(rotation), title='Rotation matrix (real)')
    call logger%log(LOG_DEBUG, aimag(rotation), title='Rotation matrix (imag)')

    call logger%log(LOG_DEBUG, old_coeff, title='Old coefficients')
    do i=1, nstat
       new_coeff(i) = sum ( rotation(i, :) * old_coeff(:) )
    end do
    call logger%log(LOG_DEBUG, new_coeff, title='New coefficients')

    call ld_check_tmat_for_transition(stmat, nstatdyn, prob, new_nstatdyn)
    if (new_nstatdyn < 0) then
       prob = ld_compute_probabilities(rotation, old_coeff, new_coeff, nstatdyn)
    end if
  end subroutine ld_compute_coeffs_and_prob
  

  function ld_check_intruder_states(smat, intruders, epsilon) result(res)
    !! Check a matrix for the presence of intruder states.
    !!
    !! Such states are detected as states for which the corresponding line and column in
    !! ``smat`` are all very small.  For each state, we simply compute the sum of the
    !! squared coefficients in the corresponding line and column.  The threshold for
    !! considering a state as an intruder state is given by the optional parameter
    !! ``epsilon`` (default: ``1.0e-3``).
    !!
    !! When an intruder state is detected, it is added to the array ``intruders``. If no
    !! intruder state is found, ``intruders`` won't be allocated.
    !!
    !! The function returns a matrix equal to the input ``smat``, with intruder states
    !! replaced by a column / line with all 0, except the diagonal element which is set
    !! to 1.
    !!
    real(dp), intent(in) :: smat(:, :)
    !! Input matrix.
    integer, allocatable, intent(out) :: intruders(:)
    !! Array of intruder states. If no intruder state is found, the array is not
    !! allocated.
    real(dp), intent(in), optional :: epsilon
    !! Optional: precision of the detection. (Default: ``1.0e-3``).

    real(dp) :: res(size(smat, 1), size(smat, 2))

    integer :: i, j
    integer :: nstat, tmp_int(size(smat, 1))
    integer :: ind
    real(dp) :: s, eps

    eps = 1.0e-3
    if (present(epsilon)) eps = epsilon

    nstat = size(smat, 1)
    ind = 0
    res = smat
    do i=1, nstat
       s = 0.0_dp
       do j=1, nstat
          s = s + smat(i, j)**2
          if (i /= j) then
             s = s + smat(j, i)**2
          end if
       end do

       if (s < eps) then
          ind = ind + 1
          tmp_int(ind) = i
          do j=1, nstat
             res(i, j) = 0.0_dp
             res(j, i) = 0.0_dp
          end do
          res(i, i) = 0.0_dp
       end if
    end do

    if (ind > 0) then
       allocate(intruders(ind))
       do i=1, ind
          intruders(i) = tmp_int(i)
       end do
    end if
  end function ld_check_intruder_states


  function ld_compute_full_rotation(eci, eciold, ezero, time, tmat) result(rotation)
    !! Compute the local diabatization propagator.
    !!
    !! This subroutine implements equations (B9), (B10) and (B11) from paper
    !! [https://doi.org/10.1063/1.1376633](10.1063/1.1376633).
    !!
    !! Equation (B9) is only valid when the Hamiltonian at the beginning (\(H(0)\) and
    !! end of the step \(H(\Delta T)\) commute. When it is not the case, we have to
    !! divide the expression into ``N`` rotations.  The value of ``N`` is
    !! obtained with function ``ld_get_nsteps``.  For each step ``K``, we compute a
    !! matrix:
    !!
    !! \(\mathbf{M}_K = \frac{i \tau}{2N} \left [ (2N-K)\mathbf{H}(0) +
    !! K\mathbf{H}(\Delta T)  \right ] \),
    !!
    !! and we obtain the correspding rotation by taking the exponential:
    !!
    !! \(\mathbf{R}_K = \exp (\mathbf{M}_K) \).
    !!
    !! The full rotation matrix is then given by:
    !!
    !! \(\mathbf{R} = \prod_{K}^{N} \mathbf{R}_{K}
    !!
    !! The matrix returned in ``rotation`` is the matrix \(\mathbf{U}\) from the paper,
    !! /i.e./ from equation (16):
    !!
    !! \(\mathbf{U} = \mathbf{T}^{\dagger} \mathbf{R},
    !!
    !! where \(\mathbf{T}\) is the Löwdin-transformed state-overlap matrix.
    !!
    real(dp), intent(in) :: eci(:)
    !! Adiabatic energies at \(\Delta T\).
    real(dp), intent(in) :: eciold(:)
    !! Adiabatic energies at \(\0\).
    real(dp), intent(in) :: ezero
    !! Reference energy (typically set to 0).
    real(dp), intent(in) :: time
    !! Value of the time-step (in a.u.).
    real(dp), intent(in) :: tmat(:, :)
    !! Löwdin-transformed state-overlap matrix.
    
    complex(dp) :: rotation(size(eci), size(eci))
    !! Resulting rotation matrix used to evolve the coefficients.

    complex(dp), allocatable :: tmprot(:, :)
    real(dp), allocatable :: hdia(:, :), hdx(:, :), ex(:), work(:)
    integer :: nsteps, istep, i, nstat, lwork, info
    real(dp) :: fac, timetau

    nstat = size(eci)
    allocate(hdia(nstat, nstat))
    allocate(hdx(nstat, nstat))
    allocate(tmprot(nstat, nstat))
    allocate(ex(nstat))

    hdia = build_diabatic_hamiltonian(tmat, eci, ezero)

    nsteps = get_nsteps(hdia, eci, time)

    !  The full propagator is split in NSTEP propagators
    timetau = time / nsteps

    ! Inititalize `rotation` as the identity matrix
    rotation = complex(0.0_dp, 0.0_dp)
    do concurrent (i=1:size(rotation, 1))
       rotation(i, i) = complex(1.0_dp, 0.0_dp)
    end do

    lwork = max(5, nstat**2)
    allocate(work(lwork))
    do istep=1, nsteps
       fac = (2.0_dp*istep - 1.0_dp) / (2.0_dp*nsteps)
       hdx = fac*hdia
       do concurrent (i=1:nstat)
          hdx(i,i) = hdx(i,i) + (1.0_dp - fac)*(eciold(i) - ezero)
       end do
       hdx = hdx*timetau

       ! --- spectral resolution of exp(-i HDX tau)
       call dsyev('V','L',nstat, hdx, nstat, ex, work, lwork, info)
       tmprot = a_xdiag_at_sym_c64(hdx, cmplx(cos(ex), -sin(ex), kind=dp))
       rotation = matmul(tmprot, rotation)
    end do
    deallocate(work)

    rotation = matmul(transpose(tmat), rotation)
  end function ld_compute_full_rotation


  subroutine ld_check_tmat_for_transition(tmat, nstatdyn, prob, new_nstatdyn)
    !! Check for trivial hops.
    !!
    !! Trivial hops correspond to cases where the current state ``nstatdyn`` at \(\Delta
    !! T\) has no overlap with the same state at \(0\).  That means that a hop has to
    !! occur, but without rescaling the velocities (we want to stay on the same diabatic
    !! state).
    !!
    !! The state to which we hop is the one that has the largest overlap with
    !! ``nstatdyn``, and the hopping probability to this state is set to 1, while all
    !! other are set to 0.
    !!
    !! If a trivial hop occurs, the corresponding state is returned as ``new_nstatdyn``.
    !! Else, ``new_nstatdyn`` is set to -1.
    !!
    real(dp), intent(in) :: tmat(:, :)
    !! Löwdin-transformed state-overlap matrix.
    integer, intent(in) :: nstatdyn
    !! Current dynamics state.
    real(dp), intent(inout) :: prob(:)
    !! Hopping probabilities.
    integer, intent(out) :: new_nstatdyn
    !! If a trivial hop is found, new state of the system. Else, set to -1.

    real(dp), parameter :: eps_switch = 1.0e-9_dp
    integer :: lx(1)
    real(dp) :: tswitch

    new_nstatdyn = -1

    tswitch = abs( tmat(nstatdyn, nstatdyn) )
    if (tswitch < eps_switch) then
       prob(:) = 0.0_dp
       lx = maxloc( abs(tmat(nstatdyn, :)) )
       prob(lx(1)) = 1.0_dp
       new_nstatdyn = lx(1)
    end if
  end subroutine ld_check_tmat_for_transition


  function ld_compute_probabilities(rotation, old_coeff, new_coeff, nstatdyn) result(prob)
    !! Compute the hopping probabilities.
    !!
    !! This routine implements equation (19) from the original paper
    !! [https://doi.org/10.1063/1.1376633](10.1063/1.1376633).
    !!
    !! Negative probabilities are reset to 0.
    complex(dp), intent(in) :: rotation(:, :)
    !! Rotation matrix (time-evolution of the coefficients).
    complex(dp), intent(in) :: old_coeff(:)
    !! Wavefunction coefficients at \(0\).
    complex(dp), intent(in) :: new_coeff(:)
    !! Wavefunction coefficients at \(\Delta T\).
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.

    real(dp) :: prob(size(old_coeff))

    real(dp), parameter :: eps = 1.d-25
    real(dp) :: den, num, new_pop, old_pop

    integer :: nstat, i

    nstat = size(old_coeff)

    old_pop = abs(old_coeff(nstatdyn))**2
    new_pop = abs(new_coeff(nstatdyn))**2

    den = new_pop &
         & - real( &
         &   rotation(nstatdyn, nstatdyn) * old_coeff(nstatdyn) * conjg(new_coeff(nstatdyn))&
         & )

    num = (new_pop - old_pop) / old_pop

    if (abs(den) < eps) then
       prob(:) = 0.0_dp
    else
       prob(:) = - real( &
            & rotation(nstatdyn, :) * old_coeff(:) * conjg(new_coeff(nstatdyn)) &
            & ) &
            & * num / den
       prob(nstatdyn) = 0.0_dp
    end if

    where (prob(:) < 0) prob(:) = 0.0_dp
  end function ld_compute_probabilities
  


  ! =============================
  ! Helper functions and routines
  ! =============================
  function build_diabatic_hamiltonian(transfo, epot, ezero) result(res)
    real(dp), intent(in) :: transfo(:, :)
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: ezero

    real(dp) :: res(size(epot), size(epot))

    integer :: i, j

    do concurrent (i=1:size(epot))
       do j=1, i
          res(i, j) = sum( transfo(i, :) * (epot(:) - ezero) * transfo(j, :) )
          res(j, i) = res(i, j)
       end do
    end do
  end function build_diabatic_hamiltonian


  function get_nsteps(hamiltonian, epot, dt) result(res)
    real(dp), intent(in) :: hamiltonian(:, :)
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: dt

    integer :: res

    real(dp) :: epot_diff_matrix(size(epot), size(epot))
    real(dp) :: difc
    integer :: i

    forall(i=1:size(epot))
       epot_diff_matrix(:, i) = epot(:) - epot(i)
    end forall

    difc = maxval( abs ( hamiltonian * epot_diff_matrix ) )

    difc = abs( sin( difc * dt**2 / 3.0_dp ) )
    if (difc < 0.01) then
       res = 1
    elseif (difc < 0.5) then
       res = 5
    else
       res = 20
    endif

    ! write(*, *) 'DIFC, NSTEPS = ', difc, res

  end function get_nsteps


  function a_xdiag_at_sym_r64(a, x) result(res)
    real(dp), intent(in) :: a(:, :)
    real(dp), intent(in) :: x(:)

    real(dp) :: res(size(a, 1), size(a, 2))

    integer :: i, j

    do concurrent (i=1:size(x), j=1:size(x))
       do j=1, i
          res(i, j) = sum( a(i, :) * (x(:)) * a(j, :) )
          res(j, i) = res(i, j)
       end do
    end do

  end function a_xdiag_at_sym_r64


  function a_xdiag_at_sym_c64(a, x) result(res)
    real(dp), intent(in) :: a(:, :)
    complex(dp), intent(in) :: x(:)

    complex(dp) :: res(size(a, 1), size(a, 2))

    integer :: i, j

    do concurrent (i=1:size(x), j=1:size(x))
       do j=1, i
          res(i, j) = sum( a(i, :) * (x(:)) * a(j, :) )
          res(j, i) = res(i, j)
       end do
    end do

  end function a_xdiag_at_sym_c64

end module mod_sh_locdiab
