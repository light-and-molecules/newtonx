! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_sh_prob_t
  use mod_kinds, only: dp
  use mod_integrators, only: fgeneric_r
  use mod_tools, only: to_str, find_index
  implicit none

  private

  public :: nx_shprob_t
  
  type, extends(fgeneric_r) :: nx_shprob_t
     !! Probabilities in the Tully - Hammes-Schiffer method.
     !!
     !! The probabilities are given as the derivatives of this function.
     complex(dp), allocatable :: atraj(:)
     !! Electronic coefficients.
     real(dp), allocatable :: sigma(:)
     !! Derivative couplings, generated from ``nad`` and ``veloc``, or
     !! read directly from ``nx_traj`` if ``cioverlap`` has been used.
     real(dp), allocatable :: gamma(:)
     !! Phases.
     integer :: isurf
     !! Current potential energy surface.
     logical :: run_complex
     !! Indicate if CS-FSSH should be used.
     real(dp), allocatable, dimension(:) :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy.
     real(dp), allocatable, dimension(:) :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings.     
   contains
     procedure, pass :: get_deriv => deriv_shprob
     procedure, pass :: rebuild => rebuild_shprob
     procedure, pass :: to_str => to_str_shprob
     procedure, pass :: destroy => destroy_shprob
  end type nx_shprob_t
  interface nx_shprob_t
     module procedure constructor
  end interface nx_shprob_t

contains

  pure function constructor(nstat, run_complex) result(res)
    integer, intent(in) :: nstat
    !! Number of states.
    logical, intent(in) :: run_complex
    !! Indicate if CS-FSSH is used.

    type(nx_shprob_t) :: res

    integer :: ncoupl
    ncoupl = nstat * (nstat - 1) / 2

    allocate(res%acoef(nstat))
    allocate(res%adot(nstat))
    allocate(res%atraj(nstat))
    allocate(res%sigma(ncoupl))
    allocate(res%gamma(ncoupl))
    if (run_complex) then
       allocate(res%sigma_i(ncoupl))
       allocate(res%epot_i(nstat))
    else
       allocate(res%sigma_i(1))
       allocate(res%epot_i(1))
    end if
    res%run_complex = run_complex

    res%acoef(:) = 0.0_dp
    res%adot(:) = 0.0_dp
    res%atraj(:) = 0.0_dp
    res%sigma(:) = 0.0_dp
    res%gamma(:) = 0.0_dp
    res%isurf = 0
    res%sigma_i(:) = 0.0_dp
    res%epot_i(:) = 0.0_dp
  end function constructor

  subroutine destroy_shprob(self)
    class(nx_shprob_t), intent(inout) :: self

    deallocate(self%gamma)
    deallocate(self%sigma)
    deallocate(self%atraj)
    deallocate(self%adot)
    deallocate(self%acoef)
    deallocate(self%sigma_i)
    deallocate(self%epot_i)
  end subroutine destroy_shprob


  function deriv_shprob(func, x) result(res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{A}_{KL}(t) = -2 \mathrm{Re} (A_j(t)A_j^{*}(t)
    !! e^{i\gamma_{KL}} \sigma^{NAD}_{KL}(t)) \]
    !!
    class(nx_shprob_t), intent(in) :: func
    real(dp), intent(in), optional :: x

    real(dp), dimension(size(func%adot)) :: res

    integer :: k, ki, isurf
    complex(dp) :: phase_ki
    real(dp) :: sigma_ki, sigma_i_ki

    res(:) = 0.0_dp
    sigma_ki = 0.0_dp
    sigma_i_ki = 0.0_dp
    phase_ki = 0.0_dp
    isurf = func%isurf
    res(isurf) = 0.0_dp

    do k=1, size(func%adot)
       if (k == isurf) then
          cycle
       else
          ki = find_index(k, isurf)
          if (k .gt. isurf) then
             phase_ki = complex(0.0_dp, func%gamma(ki))
             sigma_ki = func%sigma(ki)

             if (func%run_complex) then
                sigma_i_ki = func%sigma_i(ki)
             end if

          else if (k .lt. isurf) then
             phase_ki = -complex(0.0_dp, func%gamma(ki))
             sigma_ki = -func%sigma(ki)
             if (func%run_complex) then
                sigma_i_ki = func%sigma_i(ki)
             end if
          end if
          ! print *, 'k = ', k, '; i = ', isurf, '; ki = ', ki
          ! print *, 'sigma = ', sigma_ki, '; phase = ', phase_ki
          ! print *, 'coef = ', func%atraj(isurf), '; conj = ', conjg(func%atraj(k))
          res(k) = real(&
               & func%atraj(isurf) * conjg(func%atraj(k)) *&
               & exp(phase_ki)&
               & )
          ! print *, 'res(k = )', res(k)
          res(k) = -res(k) * sigma_ki * 2.0_dp

          if (func%run_complex) then
             res(k) = res(k) &
                  & + aimag(&
                  &     func%atraj(isurf) * conjg(func%atraj(k)) * exp(phase_ki)&
                  &   ) * sigma_i_ki * 2.0_dp
          end if

          if (res(k) < 0.0_dp) then
             res(k) = 0.0_dp
          end if
       end if
    end do
  end function deriv_shprob


  pure function to_str_shprob(self) result(str)
    class(nx_shprob_t), intent(in) :: self

    character(len=:), allocatable :: str
    character(len=4096) :: buf

    buf = 'nx_coeff_t('//NEW_LINE('a')//'    sigma = ('//to_str(self%sigma)//')'
    buf = trim(buf)//NEW_LINE('c')//'    gamma = ('//to_str(self%gamma)//')'
    buf = trim(buf)//NEW_LINE('c')//'    atraj = ('//to_str(self%gamma)//')'

    if (self%run_complex) then
       buf = trim(buf)//NEW_LINE('c')//'    epot_i = ('//to_str(self%epot_i)//')'
       buf = trim(buf)//NEW_LINE('c')//'    sigma_i = ('//to_str(self%epot_i)//')'
    end if
    buf = trim(buf)//NEW_LINE('c')//'    acoef = ('//to_str(self%acoef)//')'
    buf = trim(buf)//NEW_LINE('c')//'    adot = ('//to_str(self%adot)//')'
    buf = trim(buf)//NEW_LINE('c')//')'
    str = trim(buf)
  end function to_str_shprob


  subroutine rebuild_shprob(func, prev, step)
    !! Dummy function (compliance with parent class).
    class(nx_shprob_t), intent(inout) :: func
    class(fgeneric_r), intent(in) :: prev
    integer, intent(in) :: step

    continue
  end subroutine rebuild_shprob
end module mod_sh_prob_t
