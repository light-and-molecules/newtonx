! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_sh_phase_t
  use mod_kinds, only: dp
  use mod_tools, only: to_str
  use mod_integrators, only: &
       & fgeneric_r
  implicit none

  private

  public :: nx_phase_t

  type, extends(fgeneric_r) :: nx_phase_t
     !! Function for determining the phase evolution
     !!
     real(dp), allocatable, dimension(:) :: epot
     !! Current potential energies.
   contains
     procedure, pass :: get_deriv => deriv_phase
     procedure, pass :: rebuild => rebuild_phase
     procedure, pass :: to_str => to_str_phase
     procedure, pass :: destroy => destroy_phase
  end type nx_phase_t
  interface nx_phase_t
     module procedure constructor
  end interface nx_phase_t

contains

  pure function constructor(nstat) result(res)
    integer, intent(in) :: nstat
    !! Number of states.

    type(nx_phase_t) :: res

    integer :: ncoupl

    ncoupl = nstat * (nstat - 1) / 2
    allocate(res%acoef(ncoupl))
    allocate(res%adot(ncoupl))
    allocate(res%epot(nstat))

    res%acoef(:) = 0.0_dp
    res%adot(:) = 0.0_dp
    res%epot(:) = 0.0_dp
  end function constructor
  

  subroutine destroy_phase(self)
    class(nx_phase_t), intent(inout) :: self
    !! ``nx_phase_t`` object.

    deallocate(self%epot)
    deallocate(self%acoef)
    deallocate(self%adot)
  end subroutine destroy_phase


  function deriv_phase(func, x) result(res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{\gamma{KL}(t)} = E_K(t) - E_L(t) \]
    !!
    class(nx_phase_t), intent(in) :: func
    !! Current object.
    real(dp), intent(in), optional :: x
    !! Dummy variable (abstract type compliance).

    real(dp), dimension(size(func%adot)) :: res

    integer :: k, l, kl

    kl = 1
    do k=1, size(func%epot)
       do l=1, k-1
          res(kl) = func%epot(k) - func%epot(l)
          kl = kl + 1
       end do
    end do
  end function deriv_phase


  subroutine rebuild_phase(func, prev, step)
    !! Dummy function (compliance with parent class).
    class(nx_phase_t), intent(inout) :: func
    class(fgeneric_r), intent(in) :: prev
    integer, intent(in) :: step

    continue
  end subroutine rebuild_phase


  pure function to_str_phase(self) result(str)
    class(nx_phase_t), intent(in) :: self

    character(len=:), allocatable :: str

    character(len=4096) :: buf

    buf = 'nx_phase_t('//NEW_LINE('c')//'    epot = ('//to_str(self%epot)//')'
    buf = trim(buf)//NEW_LINE('c')//'    acoef = ('//to_str(self%acoef)//')'
    buf = trim(buf)//NEW_LINE('c')//'    adot = ('//to_str(self%adot)//')'
    buf = trim(buf)//NEW_LINE('c')//')'
    str = trim(buf)
  end function to_str_phase

end module mod_sh_phase_t
