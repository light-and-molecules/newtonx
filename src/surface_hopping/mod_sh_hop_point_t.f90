module mod_sh_hop_point_t
  use mod_constants, only: au2ev
  use mod_kinds, only: dp
  use mod_tools, only: compute_ekin
  implicit none

  type, public :: nx_hop_point_t
     !! Hopping point descriptor.
     !!
     !! This object details a point at which the dynamics hops from one surface to
     !! another (real hopping only).
     logical :: rejected = .false.
     !! Indicate if the hopping has been rejected.
     integer :: step = -1
     !! Step at which hopping occurs.
     integer :: ms_step = -1
     !! Micro-step at which hopping occurs.
     real(dp) :: time = 0.0_dp
     !! Time at which hopping occurs.
     integer :: old = -1
     !! Old state (hopping from this state).
     integer :: new = -1
     !! New state (hopping to this state).
     real(dp), allocatable :: old_veloc(:, :)
     !! Atomic velocities before hopping.
     real(dp), allocatable :: new_veloc(:, :)
     !! Atomic velocities after hopping.
     real(dp), allocatable :: epot(:)
     !! Potential energies at the time of hopping.
     real(dp) :: old_ekin = 0.0_dp
     !! Kinetic energy in the old state.
     real(dp) :: new_ekin = 0.0_dp
     !! Kinetic energy in the new state.
     real(dp) :: sh_prob = 0.0_dp
     !! Surface hopping probability at the time of hopping.
     real(dp) :: gen_rx = 0.0_dp
     !! Random number generated at the time of hopping.
   contains
     procedure :: create => nx_point_create
     procedure :: write => nx_point_write_info
  end type nx_hop_point_t
  interface nx_hop_point_t
     module procedure new_nx_hop_point_t
  end interface nx_hop_point_t
  
contains

  function new_nx_hop_point_t(nstat, nat) result(res)
    integer, intent(in) :: nstat
    integer, intent(in) :: nat

    type(nx_hop_point_t) :: res

    allocate(res%old_veloc(3, nat))
    allocate(res%new_veloc(3, nat))
    allocate(res%epot(nstat))
  end function new_nx_hop_point_t


  subroutine nx_point_create(self, &
       & step, ms_step, time, old, new, old_veloc, new_veloc, &
       & epot, sh_prob, gen_rx, rejected, masses &
       & )
    class(nx_hop_point_t), intent(inout) :: self
    !! New object.
    integer, intent(in) :: step
    !! Step at which hopping occurs.
    integer, intent(in) :: ms_step
    !! Micro-step at which hopping occurs.
    real(dp), intent(in) :: time
    !! Time at which hopping occurs.
    integer, intent(in) :: old
    !! Old state (hopping from this state).
    integer, intent(in) :: new
    !! New state (hopping to this state).
    real(dp), intent(in) :: old_veloc(:, :)
    !! Atomic velocities before hopping.
    real(dp), intent(in) :: new_veloc(:, :)
    !! Atomic velocities after hopping.
    real(dp), intent(in) :: epot(:)
    !! Potential energies at the time of hopping.
    real(dp), intent(in) :: sh_prob
    !! Surface hopping probability at the time of hopping.
    real(dp), intent(in) :: gen_rx
    !! Random number generated at the time of hopping.
    logical, intent(in) :: rejected
    !! Indicate if the hopping is rejected.
    real(dp), intent(in) :: masses(:)
    !! Atomic masses (for computing kinetic energies).

    self%step = step
    self%ms_step = ms_step
    self%time = time
    self%old = old
    self%new = new
    self%old_veloc = old_veloc
    self%new_veloc = new_veloc
    self%epot = epot
    self%sh_prob = sh_prob
    self%gen_rx = gen_rx
    self%rejected = rejected

    self%old_ekin = compute_ekin(old_veloc, masses)
    self%new_ekin = compute_ekin(new_veloc, masses)
  end subroutine nx_point_create


  subroutine nx_point_write_info(self, filename)
    class(nx_hop_point_t), intent(in) :: self
    character(len=*), intent(in) :: filename

    integer :: u, i, j
    real(dp) :: de

    de = self%epot(self%old) - self%epot(self%new)
    
    open(newunit=u, file=filename, action='write', position='append')
    write(u, '(A)') repeat('*', 80)
    if (self%rejected) then
       write(u, '(A, F20.12, A, A, I0)') &
            & 'FRUSTRATED SURFACE HOPPING at t = ', self%time, ' fs., ', &
            & 'STEP ', self%step
    else
       write(u, '(A, F20.12, A, A, I0)') &
            & 'SURFACE HOPPING at t = ', self%time, ' fs., ', &
            & 'STEP ', self%step
    end if
    
    write(u, '(A)') ''
    write(u, '(A20,I0)') 'Micro step: ', self%ms_step
    write(u, '(A20,I0)') 'From state: ', self%old
    write(u, '(A20,I0)') '  To state: ', self%new
    write(u, '(A)') ''
    write(u, '(A)') 'Velocity before hopping (a.u.):'
    do i=1, size(self%old_veloc, 2)
       write(u, '(3F20.12)') (self%old_veloc(j, i), j=1, 3)
    end do
    write(u, '(A)') 'Velocity after hopping (a.u.):'
    do i=1, size(self%new_veloc, 2)
       write(u, '(3F20.12)') (self%new_veloc(j, i), j=1, 3)
    end do
    write(u, '(A)') ''
    write(u, '(A)') 'Kinetic energies (a.u.):'
    write(u, '(A,F20.12)') 'ekin(before) = ', self%old_ekin
    write(u, '(A,F20.12)') ' ekin(after) = ', self%new_ekin
    write(u, '(A)') ''
    write(u, '(A)') 'Potential energies (a.u.):'
    write(u, '(A,F20.12)') '       epot(before) = ', self%epot(self%old)
    write(u, '(A,F20.12)') '        epot(after) = ', self%epot(self%new)
    write(u, '(A,F20.12,A)') ' de(before - after) = ', de, ' au'
    write(u, '(A,F20.12,A)') ' de(before - after) = ', de * au2ev, ' eV'
    write(u, '(A)') ''
    write(u, '(A)') 'Total energies (a.u.):'
    write(u, '(A,F20.12)') &
         & '       ETOT(before) = ', self%epot(self%old) + self%old_ekin
    write(u, '(A,F20.12)') &
         & '        ETOT(after) = ', self%epot(self%new) + self%new_ekin
    write(u, '(A)') ''
    write(u, '(A)') 'Surface hopping parameters:'
    write(u, '(A,F20.12)') 'Computed probability: ', self%sh_prob
    write(u, '(A,F20.12)') '       Random number: ', self%gen_rx
    write(u, '(A)') repeat('*', 80)
    write(u, '(A)') ''
    close(u)
  end subroutine nx_point_write_info
  
  
end module mod_sh_hop_point_t
