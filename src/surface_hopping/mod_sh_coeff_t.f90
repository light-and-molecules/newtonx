! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
module mod_sh_coeff_t
  use mod_kinds, only: dp
  use mod_tools, only: find_index, to_str
  use mod_integrators, only: &
       & fgeneric_c
  implicit none

  private

  public :: nx_coeff_t

  type, extends(fgeneric_c) :: nx_coeff_t
     !! Type for describing electronic coefficients.
     !!
     real(dp), allocatable, dimension(:) :: sigma
     !! Derivative couplings, generated from ``nad`` and ``veloc``, or
     !! read directly from ``nx_traj`` if ``cioverlap`` has been used.
     real(dp), allocatable, dimension(:) :: gamma
     !! Phases.
     logical :: run_complex
     !! Indicate if CS-FSSH should be used.
     real(dp), allocatable, dimension(:) :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy.
     real(dp), allocatable, dimension(:) :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings.
   contains
     procedure, pass :: get_deriv => deriv_coeff
     procedure, pass :: rebuild => rebuild_coeff
     procedure, pass :: destroy => destroy_coeff
     procedure, pass :: to_str => to_str_coeff
  end type nx_coeff_t
  interface nx_coeff_t
     module procedure constructor
  end interface nx_coeff_t

contains

  type(nx_coeff_t) pure function constructor(&
       & nstat, run_complex, &
       & acoef, adot, gamma, sigma, sigma_i, epot_i&
       & )
    integer, intent(in) :: nstat
    !! Number of states.
    logical, intent(in) :: run_complex
    !! Indicate if CS-FSSH is used.
    complex(dp), intent(in), optional :: acoef(:)
    !! Electronic coefficients.
    complex(dp), intent(in), optional :: adot(:)
    !! Derivative of the electronic coefficients.
    real(dp), intent(in), optional :: gamma(:)
    !! Array of phases.
    real(dp), intent(in), optional :: sigma(:)
    !! Array of time-derivative couplings.
    real(dp), intent(in), optional :: sigma_i(:)
    !! (CS-FSSH ONLY) Imaginary part of the derivative couplings
    real(dp), intent(in), optional :: epot_i(:)
    !! (CS-FSSH ONLY) Imaginary part of the energy.

    integer :: ncoupl

    ncoupl = nstat * (nstat - 1) / 2
    allocate(constructor%acoef(nstat))
    allocate(constructor%adot(nstat))
    allocate(constructor%gamma(ncoupl))
    allocate(constructor%sigma(ncoupl))

    if (run_complex) then
       allocate(constructor%sigma_i(ncoupl))
       allocate(constructor%epot_i(nstat))
    else
       allocate(constructor%sigma_i(1))
       allocate(constructor%epot_i(1))
    end if
    constructor%run_complex = run_complex

    constructor%acoef(:) = 0.0_dp
    constructor%adot(:) = 0.0_dp
    constructor%gamma(:) = 0.0_dp
    constructor%sigma(:) = 0.0_dp
    constructor%sigma_i(:) = 0.0_dp
    constructor%epot_i(:) = 0.0_dp

    if (present(acoef)) constructor%acoef = acoef
    if (present(adot)) constructor%adot = adot
    if (present(gamma)) constructor%gamma = gamma
    if (present(sigma)) constructor%sigma = sigma
    if (present(sigma_i)) constructor%sigma_i = sigma_i
    if (present(epot_i)) constructor%epot_i = epot_i
  end function constructor


  function deriv_coeff(func, x) result (res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{A}_L = -\sum_K A_K \mathrm{e}^{-i\gamma_{KL}}
    !! \sigma^{NAD}_{KL} \]
    !!
    !!
    !! Usage:
    !!
    !!     this%adot(:) = this%get_deriv()
    !!
    !! ``x`` is a dummy variable necessary for general compatibility.
    class(nx_coeff_t), intent(in) :: func
    !! Current object.
    real(dp), intent(in), optional :: x
    !! Dummy variable

    complex(dp), dimension(size(func%adot)) :: res

    integer :: nsurf
    integer :: k, kl, l
    complex(dp) :: ak
    complex(dp) :: phase_kl
    real(dp) :: sigma_kl, sigma_i_kl

    nsurf = size(func%acoef)
    sigma_i_kl = 0.0_dp
    do k=1, nsurf
       ak = complex(0.0_dp, 0.0_dp)
       do l=1, nsurf
          ! kl is the index of func%nad where is found the coupling
          ! (k, l). If kl = -1, then k = l and we keep going !
          kl = find_index(k, l)

          if (k .gt. l) then
             ! print '(A,I0,A,I0,A,I0)', 'l = ', l, '; k = ', k, '; kl = ', kl
             phase_kl = complex(0.0_dp, func%gamma(kl))
             sigma_kl = func%sigma(kl)
             if (func%run_complex) then
                sigma_i_kl = func%sigma_i(kl)
             end if

          else if (k .lt. l) then
             ! print '(A,I0,A,I0,A,I0)', 'l = ', l, '; k = ', k, '; kl = ', kl
             phase_kl = -complex(0.0_dp, func%gamma(kl))
             sigma_kl = -func%sigma(kl)
             if (func%run_complex) then
                sigma_i_kl = func%sigma_i(kl)
             end if

          else
             cycle
          end if
          ! print *, 'phase_kl = ', phase_kl, ' ; sigma_kl = ', sigma_kl, ' ; sigma_i_kl = ', sigma_i_kl

          if (func%run_complex) then
             ! CS-FSSH: Add contribution from inmaginary part of derivative coupling.
             ak = ak - func%acoef(l) * exp(phase_kl) * complex(sigma_kl, sigma_i_kl)
          else
             ak = ak - func%acoef(l) * exp(phase_kl) * sigma_kl
          end if
          ! print *, 'ak = ', ak
       end do

       if (func%run_complex) then
          ! CS-FSSH: Add contribution from the dissipative term to `ak`
          res(k) = ak - complex( func%epot_i(k) / 2.0_dp, 0.0_dp) * func%acoef(k)
       else
          res(k) = ak
       end if

       ! print *, 'final ak = ', ak
    end do
  end function deriv_coeff


  subroutine rebuild_coeff(func, prev, step)
    !! Perform a linear interpolation in ``step`` steps.
    !!
    !! The parameter ``sigma`` of ``func`` will be interpolated betwee
    !! the original set and the parameters of ``prev`` in ``step``
    !! steps.
    class(nx_coeff_t), intent(inout) :: func
    !! Object for which we want to modify the parameters.
    class(fgeneric_c), intent(in) :: prev
    !! Starting point for the interpolation.
    integer, intent(in) :: step
    !! Number of step for the interpolation.

    select type (prev)
    type is(nx_coeff_t)
       func%sigma = (func%sigma + prev%sigma) / step
       if (func%run_complex) then
          func%sigma_i = (func%sigma_i + prev%sigma_i) / step
       end if
    end select
  end subroutine rebuild_coeff


  pure function to_str_coeff(self) result(str)
    class(nx_coeff_t), intent(in) :: self

    character(len=:), allocatable :: str
    character(len=4096) :: buf

    buf = 'nx_coeff_t('//NEW_LINE('a')//'    sigma = ('//to_str(self%sigma)//')'
    buf = trim(buf)//NEW_LINE('c')//'    gamma = ('//to_str(self%gamma)//')'

    if (self%run_complex) then
       buf = trim(buf)//NEW_LINE('c')//'    epot_i = ('//to_str(self%epot_i)//')'
       buf = trim(buf)//NEW_LINE('c')//'    sigma_i = ('//to_str(self%epot_i)//')'
    end if
    buf = trim(buf)//NEW_LINE('c')//'    acoef = ('//to_str(self%acoef)//')'
    buf = trim(buf)//NEW_LINE('c')//'    adot = ('//to_str(self%adot)//')'
    buf = trim(buf)//NEW_LINE('c')//')'
    str = trim(buf)
  end function to_str_coeff


  subroutine destroy_coeff(self)
    class(nx_coeff_t), intent(inout) :: self
    !! ``nx_coeff_t`` object.

    deallocate(self%gamma)
    deallocate(self%sigma)
    deallocate(self%acoef)
    deallocate(self%adot)
    deallocate(self%sigma_i)
    deallocate(self%epot_i)
  end subroutine destroy_coeff
end module mod_sh_coeff_t
