! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh
   !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
   !!
   !! # Surface hopping algorithm implementation
   !!
   !! ## Description
   !!
   !! This module implements the resolution of the time-dependent Schrödinger equation, as
   !! well as a surface hopping algorithm on top, based on the fewest switch method from
   !! Tully.
   !!
   !! ## Details
   !!
   !! Here we assume that two different time-steps will be used: one to propagate the
   !! dynamics, written \(\Delta T\), and one to propagate the wavefunction, written
   !! \(dt\).  The two are related through the ``ms`` parameter (see ``mod_sh_t.f90``)
   !! through \( \Delta T = dt / ms \).
   !!
   !! Dynamic quantities (geometry, velocity) and quantities arising from electronic
   !! structure computation are all available only at \(T\) and \(T + \Delta T\).  For the
   !! times in between, they are obtained through linear interpolation, with the exception
   !! of the potential energy, obtained through polynomial interpolation.
   !!
   !! ### Electronic populations
   !!
   !! The current implementation consists in solving the following coupled equations:
   !!
   !! \[ \gamma_K(t) = \int_{0}^{t} E_K(t')dt', \]
   !! \[ \dot{A_K}(t) = -\sum_{K \neq L} A_L(t) e^{i\gamma_{KL}}
   !! \sigma_{KL}^{NAD}. \]
   !!
   !! For each time-step, we thus need the energies to evolve the phases, and the product
   !! between the atomic velocities and the non-adiabatic coupling vectors (or the
   !! derivative couplings when the method doesn't provide analytic NACV).  We also need
   !! the velocities for deciding if hops are possible or not (see later), so it will be
   !! required anyway.
   !!
   !! ### Surface hopping probabilities
   !!
   !! The hopping probability is related to the evolution of the diagonal components of
   !! the density matrix:
   !!
   !! \[ \dot{a}_{KK} = \sum_L b_{KL}, \]
   !! \[ b_{KL}(t) = -2 \mathrm{Re} (A_L(t)A_K^{*}(t)
   !!    e^{i\gamma_{KL}} \sigma^{NAD}_{KL}(t)), \]
   !!
   !! The hopping probability from state \(K\) to \(L\) is then defined by either:
   !!
   !! \[ g_{KL} = \frac{1}{a_{KK}} dt b_{KL} \]
   !!
   !! in the standard case (``tully = 0``), or :
   !!
   !! \[ g_{KL} = \frac{1}{a_{KK}} \int_t^{t+dt} b_{KL}(t')dt' \]
   !!
   !! for the Hammes-Schiffer - Tully method (``tully = 1``).
   !!
   !! No further quantities are needed here besides the one required for the electronic
   !! coefficients. 
   !! 
   !! 
   use mod_kinds, only: dp
   use mod_constants, only: deg2rad, timeunit, au2ev, MAX_STR_SIZE
   use mod_cs_fssh, only: nx_csfssh_params_t
   use mod_logger, only: &
        & nx_logger_t, nx_log, &
        & LOG_DEBUG, LOG_TRIVIA, LOG_INFO, LOG_WARN, LOG_ERROR, &
        & check_error
   use mod_tools, only: &
        & find_index, compute_ekin, compute_ekin_sub, assert_equal, &
        & norm, reshape_epot, nx_random_number, scalar_product, norm, &
        & to_str
   use mod_configuration, only: nx_config_t
   use mod_trajectory, only: nx_traj_t
   use mod_sh_t, only: nx_sh_t
   use mod_sh_hop_point_t, only: nx_hop_point_t
   use mod_sh_traj_t, only: nx_sh_traj_t
   use mod_io, only: nx_output_t
   use mod_integrators, only: &
        & fholder_c, fholder_r, &
        & finite_element_int, butcher_int, upropagator_int
   use mod_decoherence, only: &
        & set_wp, evolrwp, add_prob_wp, &
        & decoh_edc => edc_decoherence
   use mod_sh_locdiab, only: &
        local_diabatization => ld_compute_coeffs_and_prob
   use mod_numerics, only: polint
   implicit none
 
   private
 
   type, public :: nx_sh_solver_t
      !! Surface hopping solver.
      !!
      ! General parameters for the solver
      type(nx_sh_t) :: params
      !! Set of SH parameters, extracted from a preexisting
      !! ``nx_sh_t`` object.
      integer :: step = -1
      !! Current integration step.
      integer :: init_step = -1.
      !! Initial step for the integration (useful when restarting a computation).
      integer :: lvprt = 1
      !! Debugging produced by the SH routines:
      !!
      !! - 1 (default): no specific debug file is provided
      !! - 2: basic debug info
      real(dp) :: dts = 0.0_dp
      !! Time-step for the integration.
      type(nx_logger_t) :: logger
      !! Logger
 
      ! Immutable quantities
      real(dp), allocatable :: masses(:)
      !! Atomic masses
      logical :: has_qmmm = .false.
      !! Flag to indicate if we are using QM/MM or not. (Default: ``.false.``).
      logical, allocatable :: qm_filter(:)
      !! (QM/MM ONLY) Filter for QM atoms (``.true.`` for QM atoms, ``.false.`` for the
      !! others).
      integer :: nat = -1
      !! Total number of atoms in the system (QM + MM).
      integer :: qmnat = -1
      !! Number of QM atoms in the system.
      integer :: nintc = -1
      !! Number of internal coordinates.
      integer :: nstat = -1
      !! Number of states.
      integer :: ncoupl = -1
      !! Number of couplings.
 
      ! TDSE trajectory
      type(nx_sh_traj_t), allocatable :: shtraj(:)
      !! Array containing the trajectory history. The size of the array is given by the
      !! integration scheme used for TDSE (``nx_sh_t%integrator``). The current step is
      !! always stored as the first element of the array.  All history elements come after.
 
      ! TDSE resolution process with function from ``mod_integrators``
      ! We need these holders to use the integration functions.  Each element of those
      ! arrays correspond to a member ``nx_coeff_t``, ``nx_phase_t`` or ``nx_prob_t`` of
      ! the ``shtraj`` member of the object.
      type(fholder_c), allocatable :: coeffs(:)
      !! Holder for coefficient evolution, contains ``shtraj(:)%fcoeffs``.
      type(fholder_r), allocatable :: probs(:)
      !! Holder for probability evolution, contains ``shtraj(:)%fprob``.
      type(fholder_r), allocatable :: phases(:)
      !! Holder for phase evolution, contains ``shtraj(:)%fphase``.
 
      ! Interpolation process
      ! Here are defined all elements related to the interpolation, where the quantities
      ! are obtained iteratively as:
      !
      !      cur_veloc = cur_epot + tinc * incveloc
      !
      real(dp) :: tinc = 0.0_dp
      !! Time increment.
      real(dp), dimension(:, :), allocatable :: incveloc
      !! Increment of velocity for the linear interpolation between times ``t``
      !! and ``t+dt``.  It is typically equal to ``traj%veloc - traj%old_veloc``.
      real(dp), dimension(:, :), allocatable  :: incgeom
      !! Increment of geometry for the linear interpolation between times ``t``
      !! and ``t+dt``.  It is typically equal to ``traj%geom - traj%old_geom``.
      real(dp), dimension(:, :, :), allocatable :: incnad
      !! Increment of NAD for the linear interpolation between times ``t``
      !! and ``t+dt``.  It is typically equal to ``traj%nad - traj%old_nad``.
      real(dp), dimension(:, :, :), allocatable :: incgrad
      !! Increment of NAD for the linear interpolation between times ``t``
      !! and ``t+dt``.  It is typically equal to ``traj%nad - traj%old_nad``.
      real(dp), dimension(:, :), allocatable :: epot_all
      !! Array of potential energies, with first dimension corresponding to the number of
      !! time steps taken into account (typically 4 for ``t+dt``, ``t``, ``t-dt`` and
      !! ``t-2dt`` corresponding to the values in ``traj%epot`` (``t+dt``) and
      !! ``traj%old_epot (all others)), and second dimension corresponding to the number of
      !! states.  This array is created with the routine ``reshape_epot``.
      real(dp), dimension(:), allocatable :: en_times
      !! Array corresponding to the times at which we have the potential
      !! energies computed, typically ``0, dt, 2dt, 3dt``, with ``0`` being set
      !! for time ``t-2dt``.  This array is also created by the ``reshape_epot``
      !! routine.
      real(dp), allocatable :: cio(:, :)
      !! State overlap matrix, obtained from cioverlap typically (this
      !! is only needed with local diabatization).
      real(dp), dimension(:, :), allocatable :: cur_veloc
      !! Current velocity.  The starting value will be taken from ``traj%old_veloc``.
      real(dp), dimension(:, :), allocatable :: cur_geom
      !! Current geometry.  The starting value will be taken from ``traj%old_geom``.
      real(dp), dimension(:, :, :), allocatable :: cur_nad
      !! Current NACV (or NAD).  The starting value will be taken from
      !! ``traj%old_nad``.
      real(dp), dimension(:, :, :), allocatable :: cur_grad
      !! Current gradient.  The starting value will be taken from
      !! ``traj%old_grad``.
      real(dp), dimension(:), allocatable :: cur_epot
      !! Current potential energy. The values will be obtained by calling the
      !! ``interpolate_epot`` function.
 
      ! Results
      real(dp), allocatable :: rx(:)
      !! Random number generated.
      real(dp), allocatable :: shprob(:, :)
      !! Computed probabilities.
      ! complex(dp), allocatable :: wf(:)
      !! Wavefunction coefficients.
 
      ! SH 
      integer :: last_hop = -1
      !! Last hopping step.
      integer :: irk = 0
      !! Flag indicating hopping type.
 
      ! CS-FSSH
      type(nx_csfssh_params_t) :: csfssh
      logical :: run_complex = .false.
      real(dp), dimension(:, :, :), allocatable :: incnad_i
      !! Increment of NAD for the linear interpolation between times ``t``
      !! and ``t+dt``.  It is typically equal to ``traj%nad_i - traj%old_nad_i``.
      real(dp), dimension(:, :), allocatable :: epot_i_all
      !! Array of imaginary part of potential energies, with first dimension corresponding
      !! to the number of time steps taken into account (typically 4 for ``t+dt``, ``t``,
      !! ``t-dt`` and ``t-2dt`` corresponding to the values in ``traj%epot_i`` and
      !! ``traj%old_epot_i), and second dimension corresponding to the number of states.
      !! This array is created with the routine ``reshape_epot``.
      real(dp), allocatable :: epot_ref(:, :)
      !! (CS-FSSH ONLY, ``gamma_model = 2``) Array containing the reference energies from
      !! the main trajectory.
      real(dp), dimension(:, :, :), allocatable :: cur_nad_i
      !! Current imaginary part of NACV (or NAD).  The starting value will be taken from
      !! ``traj%old_nad``.
      real(dp), dimension(:), allocatable :: cur_epot_i
      !! Current resonance energy. The values will be obtained by calling the
      !! ``interpolate_epot`` function.
      
    contains
      procedure :: destroy => sh_solver_destroy
      procedure :: prepare_loop => prepare_sh_loop
      procedure :: run => sh_solver_run
      procedure :: set => sh_solver_set_data
      procedure :: init_qmmm => sh_set_qmmm
      procedure :: init_csfssh => sh_set_csfssh
      procedure :: csfssh_compute_gamma => sh_csfssh_compute_gamma
      procedure :: ekin_res => sh_get_ekin_reservoir
      procedure :: build_sigma => sh_build_sigma
      procedure :: get_epot => sh_get_epot
      procedure :: get_epot_vec => sh_get_epot_vec
      procedure :: get_isurf => sh_get_isurf
      procedure :: get_veloc => sh_get_veloc
      procedure :: get_qm_veloc => sh_get_qm_veloc
      procedure :: get_wf => sh_get_wf
      procedure :: get_wf_on_state => sh_get_wf_on_state
      procedure :: get_phase => sh_get_phase
      procedure :: get_populations => sh_get_populations
      procedure :: rescale_vel_mom => vel_rescale_momentum_direction
      ! procedure :: rescale_vel_dir => vel_rescale_in_direction
      procedure :: adjust_incveloc => sh_adjust_vinc
      procedure :: sh_integration
   end type nx_sh_solver_t
   interface nx_sh_solver_t
      module procedure sh_solver_init
   end interface nx_sh_solver_t
 
   ! Size of the integration history.  The array has the value of ``sh%integrator`` as
   ! index + 1, and the corresponding size as the value.
   integer, parameter :: INTEG_SIZE(4) = [&
        & 3, 3, 2, 3 &
        & ]
 
 contains
 
   function sh_solver_init(&
        & shconf, conf, masses, &
        & wf, phase, qm_filter, csfssh, geom &
        & ) result(res)
     !! Initialize  the solver.
     !!
     type(nx_sh_t), intent(in) :: shconf
     !! SH configuration object.
     type(nx_config_t), intent(in) :: conf
     !! Main NX configuration object.
     real(dp), intent(in) :: masses(:)
     !! Atomic masses.
     complex(dp), intent(in), optional :: wf(:)
     !! Initial wavefunction. If not provided, all the population is assumed to be on
     !! state ``nstatdyn``.
     real(dp), intent(in), optional :: phase(:)
     !! Initial phases. If not provided, all phases are assumed to be 0.
     logical, intent(in), optional :: qm_filter(:)
     !! Filter for QM/MM atoms. If not present, QM only is assumed.
     type(nx_csfssh_params_t), intent(in), optional :: csfssh
     !! (CS-FSSH ONLY) CS-FSSH parameters.
     real(dp), intent(in), optional :: geom(:, :)
     !! (OVL DECOHERENCE ONLY) Initial geometry, only useful for setting up overlap-based
     !! decoherence schemes (``shconf%decohmod = 2``).
 
     type(nx_sh_solver_t) :: res
 
     integer :: i, history_size, ncoupl, qmnat
     real(dp), allocatable :: init_sigma(:), init_phase(:)
     complex(dp), allocatable :: init_wf(:)
     logical, allocatable :: is_qm_atoms(:)
 
     ! Initialize logger
     call res%logger%init( &
          conf%kt, conf%init_step, conf%lvprt, &
          logfile='sh.out', errfile='sh.out' &
          & )
 
     ! Set all configuration parameters
     res%params = shconf
     res%step = conf%init_step
     res%init_step = conf%init_step
     res%lvprt = conf%lvprt
     res%dts = conf%dt / shconf%ms
 
     ! QMNAT can be recomputed with `sh_set_qmmm` for QM/MM computations.
     ! QM_FILTER is also set by the `sh_set_qmmm` routine
     res%qmnat = size(masses)
     res%nat = size(masses)
 
     res%nstat = conf%nstat
     res%ncoupl = conf%nstat * (conf%nstat - 1) / 2
 
     if (shconf%integrator == 2) then
        allocate(res%cio(conf%nstat, conf%nstat))
     else
        allocate(res%cio(1,1))
     end if
     res%cio(:, :) = 0.0_dp
 
     ! Number of internal coordinates
     if (res%nat > 2) then
        res%nintc = 3*res%nat - 6
     else
        res%nintc = 3*res%nat - 5
     end if
 
     allocate(res%masses( size(masses) ))
     res%masses(:) = masses(:)
     history_size = INTEG_SIZE(shconf%integrator + 1)
     allocate(res%shtraj(history_size))
     do i=1, history_size
        res%shtraj(i) = nx_sh_traj_t(conf%nat, conf%nstat, conf%run_complex)
     end do
     call res%shtraj(1)%set_isurf( conf%nstatdyn )
 
     allocate(res%coeffs( history_size ))
     allocate(res%probs( history_size ))
     allocate(res%phases( history_size ))
 
     ! Set the wavefunction as the components of the first shtraj element
     allocate(init_wf(conf%nstat))
     if (present(wf)) then
        init_wf(:) = wf(:)
     else
        init_wf(:) = complex(0.0_dp, 0.0_dp)
        init_wf(conf%nstatdyn) = complex(1.0_dp, 0.0_dp)
     end if
     call res%shtraj(1)%set_wf( init_wf )
 
     ! Set the initial phase if present
     if (present(phase)) then
        allocate(init_phase(size(phase)))
        init_phase(:) = phase(:)
        call res%shtraj(1)%set_gamma( init_phase )
     end if
 
     ! Interpolation
     res%tinc = 1.0_dp / shconf%ms
     ncoupl = conf%nstat * (conf%nstat - 1) / 2
     allocate( res%incgeom(3, conf%nat) )
     allocate( res%incveloc(3, conf%nat) )
     allocate( res%incgrad(conf%nstat, 3, conf%nat) )
     allocate( res%incnad(ncoupl, 3, conf%nat) )
     res%incgeom(:, :) = 0.0_dp
     res%incveloc(:, :) = 0.0_dp
     res%incgrad(:, :, :) = 0.0_dp
     res%incnad(:, :, :) = 0.0_dp
 
     allocate( res%cur_geom(3, conf%nat) )
     allocate( res%cur_veloc(3, conf%nat) )
     allocate( res%cur_grad(conf%nstat, 3, conf%nat) )
     allocate( res%cur_nad(ncoupl, 3, conf%nat) )
     res%cur_geom(:, :) = 0.0_dp
     res%cur_veloc(:, :) = 0.0_dp
     res%cur_grad(:, :, :) = 0.0_dp
     res%cur_nad(:, :, :) = 0.0_dp
 
     allocate(res%epot_all(conf%nstat + 1, conf%nstat))
     res%epot_all(:, :) = 0.0_dp
 
     allocate(res%cur_epot(conf%nstat))
     res%cur_epot(:) = 0.0_dp
 
     ! Results
     allocate(res%shprob(shconf%ms, conf%nstat))
     res%shprob(:, :) = -1.0_dp
     allocate(res%rx(shconf%ms))
     res%rx(:) = -1.0_dp
 
     ! CS-FSSH
     allocate(res%incnad_i(1, 1, 1))
     allocate(res%epot_i_all(1, 1))
     allocate(res%epot_ref(1, 1))
     res%epot_i_all(:, :) = 0.0_dp
     res%incnad_i(:, :, :) = 0.0_dp
     res%epot_ref(:, :) = 0.0_dp
 
     allocate(res%cur_nad_i(1, 1, 1))
     allocate(res%cur_epot_i(1))
     res%cur_epot_i(:) = 0.0_dp
     res%cur_nad_i(:, :, :) = 0.0_dp
 
     ! Decoherence
     if (shconf%decohmod == 2) then
        call set_wp( &
             & res%shtraj(1)%get_veloc(), res%shtraj(1)%get_epot(), &
             & res%shtraj(1)%get_isurf(), res%params, res%masses, &
             & res%lvprt, geom &
             & )
     end if
   end function sh_solver_init
 
 
   subroutine sh_set_qmmm(self, qm_filter)
     class(nx_sh_solver_t), intent(inout) :: self
     logical, intent(in) :: qm_filter(:)
 
     integer :: i
 
     ! The presence of QM filters implies that we have a QM/MM system.
     self%has_qmmm = .true.
     if (allocated(self%qm_filter)) deallocate(self%qm_filter)
 
     allocate(self%qm_filter(size(qm_filter)))
     self%qm_filter(:) = qm_filter(:)
 
     self%qmnat = 0
     do i=1, size(qm_filter)
        if (qm_filter(i)) self%qmnat = self%qmnat + 1
     end do
   end subroutine sh_set_qmmm
 
 
   subroutine sh_set_csfssh(self, csfssh)
     class(nx_sh_solver_t), intent(inout) :: self
     type(nx_csfssh_params_t), intent(in) :: csfssh
 
     self%run_complex = .true.
 
     if (allocated(self%incnad_i)) deallocate(self%incnad_i)
     if (allocated(self%epot_i_all)) deallocate(self%epot_i_all)
     allocate(self%incnad_i(self%ncoupl, 3, self%nat))
     allocate(self%epot_i_all(self%nstat+1, self%nstat))
     self%epot_i_all(:, :) = 0.0_dp
     self%incnad_i(:, :, :) = 0.0_dp
 
     if (allocated(self%cur_nad_i)) deallocate(self%cur_nad_i)
     if (allocated(self%cur_epot_i)) deallocate(self%cur_epot_i)
     allocate(self%cur_nad_i(self%ncoupl, 3, self%nat))
     allocate(self%cur_epot_i(self%nstat))
     self%cur_epot_i(:) = 0.0_dp
     self%cur_nad_i(:, :, :) = 0.0_dp
 
     if (csfssh%gamma_model == 2) then
        if (allocated(self%epot_ref)) deallocate(self%epot_ref)
        allocate(self%epot_ref(self%nstat+1, self%nstat))
        self%epot_ref(:, :) = 0.0_dp
     end if
   end subroutine sh_set_csfssh
 
 
   subroutine sh_solver_destroy(this)
     !! Destructor for the solver object.
     !!
     class(nx_sh_solver_t), intent(inout) :: this
     !! Current solver.
 
     integer :: i
 
     deallocate(this%masses)
     if (this%has_qmmm) deallocate(this%qm_filter)
     deallocate(this%cio)
 
     ! do i=1, size(this%shtraj)
     !    call this%shtraj(i)%destroy()
     ! end do
     deallocate(this%shtraj)
 
     deallocate(this%coeffs)
     deallocate(this%probs)
     deallocate(this%phases)
     deallocate(this%incveloc)
     deallocate(this%incgeom)
     deallocate(this%incnad)
     deallocate(this%incgrad)
     deallocate(this%epot_all)
     if (allocated(this%en_times)) deallocate(this%en_times)
 
     deallocate(this%shprob)
     ! deallocate(this%wf)
 
     if (this%run_complex) then
        deallocate(this%incnad_i)
        deallocate(this%epot_i_all)
     end if
   end subroutine sh_solver_destroy
 
 
   subroutine sh_solver_set_data(self, traj, is_init)
     !! Read content from a trajectory object into the solver.
     !!
     !! The routine will extract information for populating the
     !! ``incv``, ``incnad``, ``cio``,  ``epot_all`` and ``en_times``
     !! members.
     class(nx_sh_solver_t), intent(inout) :: self
     !! Solver object.
     type(nx_traj_t), intent(in) :: traj
     !! Main trajectory object.
     logical, intent(in), optional :: is_init
     !! Flag to indicate if the call is done to set the first element of SH trajectory
     !! (typically, just after call ``sh_solver = nx_sh_solver_t(...)``). (Default:
     !! ``.false.``).
 
     integer :: nstat, ncoupl
     real(dp), allocatable :: init_sigma(:), sigma(:)
     logical :: do_initialization
 
     nstat = self%nstat
     ncoupl = self%ncoupl
 
     if (self%params%integrator == 2) self%cio(:, :) = traj%cio(:, :)
 
     self%incgeom(:, :) = traj%geom(:, :) - traj%old_geom(:, :)
     self%incveloc(:, :) = traj%veloc(:, :) - traj%old_veloc(:, :)
     self%incnad(:, :, :) = traj%nad(:, :, :) - traj%old_nad(:, :, :)
     ! self%incgrad(:, :, :) = traj%grad(:, :, :) - traj%old_grad(:, :, :)
     self%incgrad(:, :, :) = traj%grad(:, :, :)
 
     self%cur_geom(:, :) = traj%old_geom(:, :)
     self%cur_veloc(:, :) = traj%old_veloc(:, :)
     self%cur_nad(:, :, :) = traj%old_nad(:, :, :)
     self%cur_grad(:, :, :) = traj%old_grad(:, :, :)
 
     do_initialization = .false.
     if (present(is_init)) do_initialization = is_init
     if (do_initialization) then
        allocate(sigma(ncoupl))
        sigma = self%build_sigma(traj%veloc, traj%nad)
        call self%shtraj(1)%set_isurf( traj%nstatdyn )
        call self%shtraj(1)%set_wf( traj%wf )
        call self%shtraj(1)%set_epot( traj%epot )
        call self%shtraj(1)%set_sigma( sigma )
        call self%shtraj(1)%compute_adot()
     end if
 
     call reshape_epot(&
          & traj%step, traj%dt, traj%epot, traj%old_epot, nstat, &
          & self%epot_all, self%en_times&
          &)
 
     if (traj%run_complex) then
        self%incnad_i = traj%nad_i - traj%old_nad_i
        self%cur_nad_i = traj%old_nad_i
 
        if (do_initialization) then
           if (.not. allocated(sigma)) allocate(sigma(ncoupl))
           sigma = self%build_sigma(traj%veloc, traj%nad_i)
           call self%shtraj(1)%set_epot_i( traj%gamma )
           call self%shtraj(1)%set_sigma_i( sigma )
           call self%shtraj(1)%compute_adot()
        end if
        
        call reshape_epot(&
             & traj%step, traj%dt, traj%gamma, traj%old_gamma, nstat, &
             & self%epot_i_all, self%en_times&
             &)
 
        if (self%csfssh%gamma_model == 2) then
           call reshape_epot(&
                & traj%step, traj%dt, [traj%epot_ref], traj%epot_ref_old, 1, &
                & self%epot_ref, self%en_times&
                &)
        end if
     end if
   end subroutine sh_solver_set_data
 
 
   subroutine sh_solver_run(this, traj, txtout)
     !! Main routine for running the integration.
     !!
     !! The routine is actually a loop over ``ms``, where the
     !! following steps take place:
     !!
     !! 0. Preparation of the new integration (reallocation of data,
     !!    reset, ...) ;
     !! 1. Construction of the parameters for the new step ;
     !! 2. Integration and computation of the hopping probabilities ;
     !! 3. Surface hopping algorithm (does it hop or not ?)
     !! 4. Decoherence correction ;
     !!
     !! After all steps have been completed, we rescale the main
     !! velocity if required by surface hopping, and we transfer the
     !! new parameters (new surface, coefficients, ...) to the main
     !! trajectory.
     class(nx_sh_solver_t), intent(inout) :: this
     !! Solver object.
     type(nx_traj_t), intent(inout) :: traj
     !! Main trajectory object.
     type(nx_output_t), intent(inout) :: txtout
     !! Logging object.
 
     ! Interpolation
     real(dp), allocatable :: itpnad(:, :, :)
     real(dp), allocatable :: itpgeom(:, :)
     real(dp), allocatable :: itpveloc(:, :)
     real(dp), allocatable :: itpgrad(:, :, :)
     real(dp), allocatable :: itpepot(:)
     real(dp), allocatable :: sigma(:)
     real(dp), allocatable :: itpnad_i(:, :, :)
     real(dp), allocatable :: itpepot_i(:)
     real(dp), allocatable :: sigma_i(:)
     
     ! Misc. variables
     integer :: i, step, ntraj
     real(dp) :: relt
     integer :: switch_to
     integer :: nstat, nat
     character(len=128) :: str
     real(dp) :: decotime
     ! logical :: zdecovlp
     real(dp) :: totpop
     complex(dp), allocatable :: coeff_renorm(:)
     integer :: hop_counter(3)
     ! This one counts the number of hopping, with the same convention
     ! as for traj%nrofhops.
 
     ! Velocity rescale
     real(dp), allocatable :: d_rescale(:, :)
     real(dp), allocatable :: veloc_for_ekin_res(:, :)
     real(dp), allocatable :: tmp2(:, :)
     real(dp), allocatable :: newveloc(:, :)
     real(dp) :: de, ekin_res
     integer :: ierr, ncoupl, j, surf
 
     write(str, '(A, I20)') 'MACRO STEP', traj%step
     call this%logger%log(LOG_INFO, str)
 
     nstat = this%nstat
     nat = this%nat
     ncoupl = this%ncoupl
 
     hop_counter(:) = 0
 
     ! Set interpolated quantities to starting values
     allocate(itpepot(nstat))
     allocate(sigma(ncoupl))
     itpepot = 0.0_dp
     sigma = 0.0_dp
 
     if (this%run_complex) then
        ! allocate(itpepot_i(nstat))
        allocate(sigma_i(ncoupl))
        ! allocate(itpnad_i, mold=itpnad)
        ! itpepot_i(:) = 0.0_dp
        sigma_i(:) = 0.0_dp
        ! itpnad_i(:, :, :) = traj%old_nad_i
     end if
 
     ! For integration
     ntraj = size(this%shtraj)
 
     ! Decoherence corrections
     allocate(coeff_renorm(this%nstat))
 
     ! Velocity rescale
     allocate(newveloc, mold=this%incveloc)
 
     SH_LOOP: do step=1, this%params%ms
 
        this%step = this%step + 1
 
        write(str, '(A, I20)') 'Micro step', this%step
        call this%logger%log(LOG_INFO, str)
 
        ! 0. Copy information t-ndt -> t-(n-1)dt, ..., t-2dt -> t-dt
        !    Then, initialize a new object for current step
        call this%prepare_loop()
 
        ! Compute the new interpolate quantities, and set them to shtraj(1)
        this%cur_nad = this%cur_nad + this%tinc * this%incnad
        this%cur_veloc = this%cur_veloc + this%tinc * this%incveloc
        this%cur_geom = this%cur_geom + this%tinc * this%incgeom
        this%cur_epot = interpolate_epot(this%epot_all, this%en_times, this%params%ms, step)
        sigma = this%build_sigma(this%cur_veloc, this%cur_nad)
 
        call this%shtraj(1)%set_veloc(this%cur_veloc)
        call this%shtraj(1)%set_sigma(sigma)
        call this%shtraj(1)%set_epot(this%cur_epot)
 
        call this%logger%log(LOG_TRIVIA, sigma, title='|v.h|')
        call this%logger%log(LOG_DEBUG, this%cur_epot, title='Interpolated energies')
 
        ! Do the same for complex surface quantities, if required
        if (this%run_complex) then
           this%cur_nad_i = this%cur_nad_i + this%tinc * this%cur_nad_i
           sigma_i = this%build_sigma(this%cur_veloc, this%cur_nad_i)
           call this%logger%log(LOG_TRIVIA, sigma_i, title='|v.h| imag')
           
           this%cur_epot_i = this%csfssh_compute_gamma(this%cur_epot, step)
           call this%logger%log(LOG_DEBUG, this%cur_epot_i, title='Intberpolated resonance energies')
        
           call this%shtraj(1)%set_sigma_i(sigma_i)
           call this%shtraj(1)%set_epot_i(this%cur_epot_i)
        end if
 
        ! Everythin is now properly set, so we can compute the derivatives to finish the
        ! initialization of shtraj(1).
        call this%shtraj(1)%compute_adot()
 
        ! Compute and report initial total population
        totpop = sum(abs(this%get_wf())**2)
        write(str, '(A,F20.12)') 'Total population (before integration): ', totpop
        call this%logger%log(LOG_DEBUG, str)
        call this%logger%log(LOG_DEBUG, &
             & abs(this%get_wf(step=2))**2, &
             & title='Populations (before integration)')
 
        ! 2. Integrate the TDSE and get the hopping probabilities
        if (this%params%integrator == 2) then
 
           ! call local_diabatization(this%shtraj(1), this%shtraj(2), this%dts, &
           !      & this%cio, this%shprob(1, :), this%irk, this%logger, coeff_renorm &
           !      & )
           ! This indicates that a state switch occured during the transformation of the
           ! state overlap matrix. In LD framework, we are now on the new state, which
           ! CORRESPONDS to the old one.  See `trans_ld` code for more details.
           ! call this%shtraj(1)%set_isurf(switch_to)
           call local_diabatization(&
                & this%shtraj(1)%get_epot(), this%shtraj(2)%get_epot(), &
                & this%shtraj(2)%get_wf(), this%dts, this%get_isurf(), &
                & this%cio, &
                & this%shprob(1, :), coeff_renorm, &
                & this%logger&
                & )
           call this%shtraj(1)%set_wf(coeff_renorm)
        else
           call this%sh_integration(coeff_renorm, this%shprob(step, :))
        end if
 
        ! Recompute and report new total population
        totpop = sum( this%get_populations() )
        write(str, '(A,F20.12)') 'Total population (after integration): ', totpop
        call this%logger%log(LOG_DEBUG, str)
        call this%logger%log(LOG_DEBUG, &
             & this%get_populations(), &
             & title='Populations (after integration)')
 
        ! 3. Decide on wether to hop or not
        this%rx(step) = 1.0_dp
        ! switch_to = -1
        HOPPING_ATTEMPT: if (this%params%nohop > 0) then
           ! In this case we bypass the algorithm, and force the hop if nohop = step
           if (this%step == this%params%nohop) then
              this%rx(step) = 0.0_dp
              switch_to = this%params%forcesurf
              call this%logger%log(LOG_WARN, &
                   & "'nohop' = "//to_str(this%params%nohop)//": forcing hopping now !")
              exit HOPPING_ATTEMPT
           end if
        else if (this%params%nohop == 0) then
           if ((this%step - this%last_hop) > this%params%nrelax) then
              ! If we are still relaxing, don't try to hop.
              call sh_fewest_switch(&
                   & this%shprob(step, :), this%rx(step), switch_to, &
                   & probmin=this%params%probmin &
                   & )
           end if
        end if HOPPING_ATTEMPT
 
        ! 4. Try to hop
        if (switch_to > 0) then
           write(str, '(A,F10.3,A,I0,A,I0,A,I0)') &
                & 'Hopping event !! (t = ', traj%t, ' ; microstep = ', step, ')' &
                & //new_line('c')// &
                & 'Attempt to switch from ', this%get_isurf(), ' to ', switch_to
           call nx_log%log(LOG_WARN, str)
           call this%logger%log(LOG_WARN, str)
 
           ! Kinetic energy reservoir for hopping corresponds to the velocity of QM atoms only.
           ! veloc_for_ekin_res = this%get_qm_veloc()
 
           call this%logger%log(LOG_WARN, &
                & this%get_qm_veloc(), title='Velocity at hopping time', transpose=.true.)
           if (this%params%adjmom /= 2) then
              ! Rescaling in the momentum direction, we compute the total kinetic energy
              ! of the system (QM or QM/MM)
              ekin_res = this%ekin_res( this%get_qm_veloc(), ierr)
           else
              ! Rescaling in an arbitrary direction in the (g, h) plane, in this case we
              ! have to compute the expression derived in DOI: 10.1063/5.0113908 (Eq. 20)
              if (.not. allocated(d_rescale)) allocate(d_rescale(3, nat))
              d_rescale = direction_of_rescale(this%cur_grad, this%cur_nad, &
                   & this%params%adjtheta, switch_to, this%get_isurf() &
                   & )
              ekin_res = this%ekin_res( this%get_qm_veloc(), ierr, direction=d_rescale)
 
              call this%logger%log(LOG_DEBUG, &
                   & d_rescale, title='Direction of rescale', transpose=.true.)
           end if
           call this%logger%log(LOG_DEBUG, 'Kinetic energy reservoir: '//to_str(ekin_res))
 
           ! Now we use this reservoir to decide if the hopping is possible or not, and to
           ! compute the new velocity.
           de = this%get_epot( this%get_isurf() ) - this%get_epot( switch_to )
           call this%logger%log(LOG_DEBUG, 'Epot(old) - Epot(new): '//to_str(de))
           if (ekin_res + de < 0) then
              ! Frustrated hopping (not enough kinetic energy)
              newveloc = this%get_veloc()
              newveloc = newveloc * this%params%mom
              this%irk = 2
           else
              if (this%params%adjmom /=2 ) then
                 ! We adjust the velocity in the direction of the momentum. This is always
                 ! possible !
                 newveloc = this%rescale_vel_mom(this%get_veloc(), switch_to)
                 this%irk = 1
              else
                 ! We adjust in a direction in the plane (g, h).  This is not always
                 ! possible, as the kinetic energy may not be high enough to allow for a
                 ! proper rescale.
                 newveloc = vel_rescale_in_direction(&
                      & this%get_veloc(), d_rescale, this%params%mom, &
                      & de, this%masses, &
                      & irk=this%irk &
                      & )
              end if
           end if
           block
             type(nx_hop_point_t) :: hop
             logical :: frustrated
 
             frustrated = .false.
             if (this%irk /= 1) frustrated = .true.
             
             hop = nx_hop_point_t(nstat, nat)
             call hop%create(&
                  & step=traj%step, &
                  & ms_step=this%step, &
                  & time=traj%t, &
                  & old=traj%nstatdyn, &
                  & new=switch_to, &
                  & old_veloc=this%get_veloc(), &
                  & new_veloc=newveloc, &
                  & epot=this%get_epot_vec(), &
                  & sh_prob=this%shprob(step, switch_to), &
                  & gen_rx=this%rx(step),&
                  & rejected=(this%irk /= 1),&
                  & masses=this%masses&
                  & )
             call txtout%set_txtout_path()
             call hop%write(txtout%txtout%mhopf)
           end block
           call this%shtraj(1)%set_veloc(newveloc)
           this%cur_veloc = newveloc
           
           ! In case of surface hopping we need to rescale the velocities at
           ! times t and t+dt to continue the interpolation. To this end we set
           ! up a "virtual" interpolation (what would it look like if it was
           ! carried out on the new surface). At step i this corresponds to:
           !
           !  oldv = oldv - acc(newsurf) * i * dt / ms
           !  newv = oldv + acc(newsurf) * (ms-i) * dt / ms
           !
           ! where dt is the macroscopic time step. We just need the difference
           ! between these two for the next interpolation, which amounts to:
           !
           !  incv = dt * acc(newsurf)
           !
           ! If the hopping is frustrated, then we need to scale the velocities
           ! by the ``sh%mom`` factor.
           this%incveloc = this%adjust_incveloc(this%irk, switch_to)
 
           ! Report the outcome
           select case(this%irk)
           case(1)
              call this%shtraj(1)%set_isurf(switch_to)
              call nx_log%log(LOG_WARN, '! SURFACE HOPPING OCCURS !')
              call this%logger%log(LOG_WARN, '! SURFACE HOPPING OCCURS !')
              this%last_hop = this%step
           case(2)
              call nx_log%log(LOG_WARN, &
                   & '   FRUSTRATED HOPPING (not enough kinetic energy)')
              call this%logger%log(LOG_WARN, &
                   & '   FRUSTRATED HOPPING (not enough kinetic energy)')
           case(3)
              call nx_log%log(LOG_WARN, &
                   & '   FRUSTRATED HOPPING (velocity condition)')
              call this%logger%log(LOG_WARN, &
                   & '   FRUSTRATED HOPPING (velocity condition)')
           end select
 
           if (this%irk /= 0) then
              hop_counter(this%irk) = hop_counter(this%irk) + 1
           end if
        end if
 
        ! 4. Decoherence corrections
        coeff_renorm(:) = this%shtraj(1)%get_wf()
        if (this%params%decohmod == 1) then
 
           ekin_res = compute_ekin(this%get_qm_veloc(), this%masses)
           call this%logger%log(LOG_DEBUG, 'ISURF='//to_str(this%get_isurf()))
           coeff_renorm(:) = decoh_edc(&
                & this%get_wf(), this%get_isurf(), &
                & this%get_epot_vec(), ekin_res, this%params%decay, this%dts, &
                & run_complex=this%run_complex &
                & )
 
        else if (this%params%decohmod == 2) then
           if (mod(this%step, this%params%ms * this%params%iatau) == 0) then
              decotime = this%dts * this%params%ms * this%params%iatau
              
              write(str, '(a,i0)') 'Calling evolwrp at step = ', this%step
              call this%logger%log(LOG_DEBUG, str)
              write(str, '(a,i0)') 'decotime = ', decotime
              call this%logger%log(LOG_DEBUG, str)
 
              if (this%has_qmmm) then
                 call evolrwp( &
                      & this%shtraj(1)%get_veloc(), this%shtraj(1)%get_wf(), &
                      & this%shtraj(1)%get_epot(), this%params, decotime, .false., this%logger,&
                      & this%masses, traj%geom, this%step, qm_filter=this%qm_filter&
                      & )
              else
                 call evolrwp( &
                      & this%shtraj(1)%get_veloc(), this%shtraj(1)%get_wf(), &
                      & this%shtraj(1)%get_epot(), this%params, decotime, .false., this%logger,&
                      & this%masses, traj%geom, this%step)
              end if
 
              coeff_renorm = this%shtraj(1)%get_wf()
              call add_prob_wp(coeff_renorm, this%logger, this%step)
 
              ! Renormalization
              ! coeff_renorm(:) = this%get_wf()
              totpop = sum(abs(coeff_renorm)**2)
              coeff_renorm(:) = coeff_renorm(:) / sqrt(totpop)
           end if
        end if
        call this%shtraj(1)%set_wf(coeff_renorm)
 
        write(str, '(A,F20.12)') &
             & 'Total population (after decoherence): ', totpop
        call this%logger%log(LOG_INFO, str)
        call this%logger%log(LOG_INFO, abs(this%get_wf())**2, &
             & title='Populations (after decoherence)')
 
        if (.not. this%run_complex) then
           if ((totpop < 1.0_dp - this%params%popdev) &
                & .or. (totpop > 1.0_dp + this%params%popdev)) then
              write(str, '(A,F8.6,A,F8.6,A)') &
                   & 'Total population (', totpop, &
                   & ') deviated by more than popdev (popdev = ', &
                   & this%params%popdev, ')'
              call nx_log%log(LOG_ERROR, str)
              ERROR STOP
           end if
        end if
 
        ! Account for the change of speed and recompute both ``sigma`` if
        ! required and the derivatives
        if (this%irk == 1) then
           sigma = this%build_sigma(this%get_veloc(), this%cur_nad)
           call this%shtraj(1)%set_sigma(sigma)
        end if
 
        ! 6. Re-compute the derivative including decoherence
        ! corrections, when we actually need them
        if (this%params%integrator /= 2) then
           call this%shtraj(1)%compute_adot()
        end if
 
        call this%logger%log_blank(LOG_INFO, n=1)
     end do SH_LOOP
 
     ! Update the hopping counter
     do i=1, 3
        traj%nrofhops(i) = traj%nrofhops(i) + hop_counter(i)
     end do
 
     ! End of the trajectory: reset the old velocity, adjust the new
     ! one in case of surface hopping, if it is possible
     ! VEL_RESCALE: if (sh_traj(1)%irk .eq. 1) then
     VEL_RESCALE: if (traj%nstatdyn /= this%shtraj(1)%get_isurf()) then
        write(*, *) 'SURFACE HOPPING OCCURED !'
        write(*, '(A22,I0,A4,I0)') &
             & '   HOPPING FROM STATE ', traj%nstatdyn, &
             & ' TO ', this%get_isurf()
 
        ! veloc_itp(:, :) = traj%veloc(:, :)
        newveloc(:, :) = traj%veloc(:, :)
 
        select case (this%params%adjmom)
        case(0:1)
           d_rescale = traj%veloc
        case(2)
        end select
 
        call nx_log%log(LOG_DEBUG, this%get_qm_veloc(), &
             & title='VELOC END INTERPOL', transpose=.true.)
        
        call rescale_main_velocity(traj, this%params, this%shtraj(1)%get_isurf())
 
        call nx_log%log(LOG_DEBUG, traj%veloc, &
             & title='VELOC MAIN', transpose=.true.)
        call txtout%write_hop_veloc(newveloc, traj%veloc, traj%t, traj%step, 0)
     end if VEL_RESCALE
 
     ! Now copy information into the main trajectory
     traj%old_nstatdyn = traj%nstatdyn
     ! traj%nstatdyn = this%shtraj(1)%isurf
     traj%nstatdyn = this%get_isurf()
     traj%wf(:) = this%get_wf()
     traj%population(:) = abs(traj%wf(:))**2
     traj%shprob(:, :) = this%shprob(:, :)
     traj%rx(:) = this%rx(:)
     traj%sh_phase(:) = this%get_phase()
 
     call this%logger%log_blank(LOG_INFO, n=3)
   end subroutine sh_solver_run
 
   ! =============================
   ! NON-MEMBER INTERNAL FUNCTIONS
   ! =============================
 
   function interpolate_epot(epot, times, ms, step) result(res)
     !! Interpolate the potentiale energies.
     !!
     !! The energy is interpolated with a polynomial method, with a
     !! degree depending on the number of energies already available
     !! (we use polynomials with degrees up to 4).
     !!
     !! The ``epot`` and ``times`` inputs should be the one created by
     !! the ``reshape_epot`` routine.
     real(dp), intent(in) :: epot(:, :)
     !! Array containing the potential energies.
     real(dp), intent(in) :: times(:)
     !! Array containing the times corresponding to the potential energies.
     integer, intent(in) :: ms
     !! Total numbe of steps in the SH integration routine.
     integer, intent(in) :: step
     !! Current integration step.
 
     real(dp), dimension(size(epot, 2)) :: res
 
     real(dp) :: t, dt, de
     integer :: i, deg
 
     deg = size(times)
     dt = times(2) - times(1)
     t = times(deg-1) + step*dt / ms
     do i=1, size(epot, 2)
        call polint(times, epot(:, i), deg, t, res(i), de)
     end do
   end function interpolate_epot
 
 
   function csfssh_interpolate_gamma(ref_resonance, epot, epot_ref, ress_shift) result(res)
     use mod_numerics, only: uvip3p
     real(dp), intent(in) :: ref_resonance(:, :, :)
     real(dp), intent(in) :: epot(:)
     real(dp), intent(in) :: epot_ref
     real(dp), intent(in) :: ress_shift
 
     real(dp) :: res( size(epot) )
 
     real(dp) :: xa( size(ref_resonance, 3) ), ya( size(ref_resonance, 3) )
     real(dp) :: x( size(epot) )
     integer :: n, i
     character(len=MAX_STR_SIZE) :: msg
 
 
     x(:) = epot(:) - epot_ref - ress_shift
 
     n = size(ref_resonance, 3)
 
     do i=1, size(epot)
        write(msg, '(A,I0,A,F16.10,A,F16.10,A,F16.10)') &
             & 'i = ', i, ' e = ', epot(i), ' e_ref = ', epot_ref, ' ress_e = ', x(i)
        call nx_log%log(LOG_DEBUG, msg)
        ya(:) = ref_resonance(i, 2, :)
        xa(:) = ref_resonance(i, 1, :)
        call uvip3p(3, n, xa, ya, 1, x(i), res(i))
     end do
   end function csfssh_interpolate_gamma
 
 
   subroutine will_i_hop(shprob, rx, switch_to)
     !! Fewest-switch algorithm.
     !!
     !! The routine returns the generated random number.
     !! ``switch_to`` is set to -1 if no hopping occur, and the
     !! corresponding state number if hopping is possible.
     real(dp), intent(in) :: shprob(:)
     !! SH probabilities.
     real(dp), intent(inout) :: rx
     !! Generated random number.
     integer, intent(out) :: switch_to
     !! State to which we can hop (-1 if no hop is possible).
 
     real(dp) :: bt
     integer :: k
 
     call nx_random_number(rx)
 
     bt = 0.0_dp
     switch_to = -1
     do k=1, size(shprob)
        bt = bt + shprob(k)
        if (rx <= bt) then
           switch_to = k
           exit
        end if
     end do
   end subroutine will_i_hop
 
 
   subroutine rescale_main_velocity(traj, sh, switch_to)
     !! Rescale the main trajectory velocity.
     !!
     !! If a surface hopping has occured, we need to rescale the main
     !! velocity to ensure energy conservation.
     type(nx_traj_t), intent(inout) :: traj
     !! Main trajectory object.
     type(nx_sh_t), intent(in) :: sh
     !! SH configuration.
     integer, intent(in) :: switch_to
     !! State to which the system has hopped.
 
     real(dp) :: de, ekin, discri
     real(dp), allocatable :: rescale_direction(:, :)
     integer :: i
 
     allocate(rescale_direction, mold=traj%veloc)
 
     de = traj%epot(switch_to) - traj%epot(traj%nstatdyn)
 
     select case (sh%adjmom)
     case(0:1)
        do i=1, 3
           rescale_direction(i, :) = traj%veloc(i, :) * traj%masses(:)
        end do
     case(2)
        rescale_direction = direction_of_rescale(&
             & traj%grad, traj%nad, sh%adjtheta, switch_to, traj%nstatdyn &
             )
     end select
 
     ekin = compute_ekin(traj%veloc, traj%masses)
 
     discri = 1.0_dp - de / ekin
 
     if (discri >= 0) then
        traj%veloc = vel_rescale_in_direction(traj%veloc, rescale_direction, sh%mom,&
             & -de, traj%masses)
     else
        block
          character(len=MAX_STR_SIZE) :: str
          write(str, '(A,F15.9,A,F15.9,A,A,F15.9,A)') &
               & 'SH: de (', de, ') > ekin (', ekin, ') -> rescaling velocity by factor ',&
               & ' (1 - de/ekin)^(1/2) (', discri, ')'
          call nx_log%log(LOG_WARN, str)
        end block
        traj%veloc(:, :) = - traj%veloc(:, :) * sqrt(discri)
     end if
   end subroutine rescale_main_velocity
 
 
   subroutine prepare_sh_loop(self)
     !! Prepare SH loop.
     !!
     !! The routine "propagates" the existing elements of the ``shtraj`` array:
     !!
     !!    shtraj(t) -> shtraj(t-dt)
     !!    shtraj(t-dt) -> shtraj(t-2dt)
     !!    shtraj(t-2dt) -> shtraj(t-3dt)
     !!    ...
     !!
     !! We then reinitialize the first element of the array, and set its
     !! ``isurf`` and ``step`` parameters.
     class(nx_sh_solver_t), intent(inout) :: self
 
     integer :: nstat, nat
     integer :: ntraj, i
 
     ntraj = size(self%shtraj)
     nstat = self%nstat
     nat = self%nat
 
     do i=1, ntraj - 1
        self%shtraj(ntraj - i + 1) = self%shtraj(ntraj - i)
     end do
 
     ! Generate a new, empty first element
     self%shtraj(1) = nx_sh_traj_t(nat, nstat, self%run_complex)
 
     ! Set the surface to the previous one
     call self%shtraj(1)%set_isurf(self%shtraj(2)%get_isurf())
     call self%shtraj(1)%set_wf(self%shtraj(2)%get_wf())
   end subroutine prepare_sh_loop
 
 
   function vel_rescale_in_direction(veloc, d, mom, de, masses, irk) result(res)
     !! Compute and adjustment to velocity in the given direction, if
     !! possible.
     !!
     !! The adjusted velocity is returned in ``new_veloc``. The optional
     !! ``irk`` value is either 1 (the rescaling is possible) or 2
     !! (impossible), and is used only during surface hopping (it is
     !! useless in adjusting the classical velocity).
     !! If the rescaling cannot be done (energy conservation cannot be
     !! ensured), then it is simply multiplied by ``mom``, i.e. it is
     !! either kept or reversed.
     real(dp), dimension(:, :), intent(in) :: veloc
     !! Current nuclear velocity.
     real(dp), dimension(:, :), intent(in) :: d
     !! Direction in which the adjustment is to be carried out.
     integer, intent(in) :: mom
     !! In case of frustrated hopping, indicate if we should keep the momentum (1) or
     !! revert it (-1).
     real(dp), intent(in) :: de
     !! Energy difference (old - new).
     real(dp), intent(in) :: masses(:)
     !! Atomic masses.
     integer, optional, intent(out) :: irk
     !! Indicate if the hop is allowed (1) or frustrated (3).
 
     real(dp) :: res( size(veloc, 1), size(veloc, 2) )
 
     real(dp) :: A, B, delta, alpha, sigm1, sigm2, eps
     integer :: k, i
 
     A = 0.0_dp
     B = 0.0_dp
     eps = 0.000000001_dp
 
     do k=1, size(masses)
        do i=1, 3
           A = A + d(i, k)**2 / masses(k)
           B = B + veloc(i, k) * d(i, k)
        end do
     end do
 
     delta = B**2 + 2.0_dp*A*de
 
     ! print *, 'A = ', A
     ! print *, 'B = ', B
     ! print *, 'D = ', delta
 
     if (delta < 0) then
        ! In that case there is no solution, so no hopping can
        ! occur and we repeat the same procedure as for
        ! frustrated hoppings
        res = mom * veloc
        if (present(irk)) irk = 3
     else
        ! Here the hopping is allowed and we can rescale the
        ! velocity along the chosen vector.
        sigm1 = (-B + sqrt(delta)) / (A)
        sigm2 = (-B - sqrt(delta)) / (A)
        alpha = min(dabs(sigm1), dabs(sigm2))
 
        if (abs(alpha - abs(sigm1)) < eps) alpha = sigm1
        if (abs(alpha - abs(sigm2)) < eps) alpha = sigm2
        ! print *, 'SGM1 = ', sigm1
        ! print *, 'SGM1 = ', sigm2
        ! print *, 'ALPH = ', alpha
        ! alpha = sigm1
        ! if ( abs(sigm2) <= abs(sigm1) ) then
        !    alpha = sigm2
        ! end if
 
        do i=1, 3
           res(i, :) = veloc(i, :) + alpha * d(i, :) / masses(:)
        end do
 
        if (present(irk)) irk = 1
     end if
   end function vel_rescale_in_direction
 
 
   function direction_of_rescale(grad, nad, angle, surf1, surf2) result(d)
     !! Construct the vector defining the direction in which to adjust
     !! the momentum.
     !!
     !! Return a unit vector.
     real(dp), dimension(:, :, :) :: grad
     !! Gradient array.
     real(dp), dimension(:, :, :) :: nad
     !! Non-adiabatic couplings array.
     real(dp) :: angle
     !! Angle between ``g`` and ``h``.
     integer :: surf1
     !! State the switch has been predicted to.
     integer :: surf2
     !! old state.
     
     real(dp), dimension(3, size(grad, 3)) :: d
     !! Resulting vector.
 
     real(dp), dimension(:, :), allocatable :: g, h
     real(dp) :: mu
     integer :: kl
 
     allocate( g(3, size(grad, 3) ))
     allocate( h(3, size(nad, 3) ))
 
     mu = angle * deg2rad
     g = (grad(surf1, :, :) - grad(surf2, :, :)) / 2.0_dp
     kl = find_index(surf1, surf2)
     h = nad(kl, :, :)
 
     if (assert_equal( norm(g), 0.0_dp )) then
        g(:, :) = 0.0_dp
     else
        g = g / norm(g)
     end if
     
     if (assert_equal( norm(h), 0.0_dp )) then
        h(:, :) = 0.0_dp
     else
        h = h / norm(h)
     end if
        
     d(:, :) = sin(mu) * g(:, :) + cos(mu) * h(:, :)
   end function direction_of_rescale
 
 
   subroutine adjust_vel_increment(incv, irk, grad, masses, new_surf, mom, dt)
     real(dp), intent(inout) :: incv(:, :)
     integer, intent(in) :: irk
     real(dp), intent(in) :: grad(:, :, :)
     real(dp), intent(in) :: masses(:)
     integer, intent(in) :: new_surf
     integer, intent(in) :: mom
     real(dp), intent(in) :: dt
 
     real(dp), allocatable :: acc_after_hop(:, :)
     integer :: i
 
     if (irk == 1) then
        allocate( acc_after_hop, mold=incv )
 
        do i=1, size(acc_after_hop, 2)
           acc_after_hop(:, i) = -grad(new_surf, :, i) / masses(i)
        end do
 
        incv(:, :) = dt  * acc_after_hop(:, :)
 
     else if ((irk == 2) .or. (irk == 3)) then
        ! In this case we have a frustrated hopping, and
        ! possibly we have reverted the velocities.
        incv = mom * incv
     end if
   end subroutine adjust_vel_increment
 
 
   ! subroutine local_diabatization(traj, traj_prev, dt, stmat, b, irk, logger, new_coeff)
   !   !! Perform a local diabatization to compute the electronic
   !   !! coefficients.
   !   !!
   !   !! This routine is a wrapper around the functions found in
   !   !! ``mod_sh_locdiab.f08``, and implements the method described in
   !   !! [Granucci et al., JCP 114 (2001)](https://doi.org/10.1063/1.1376633)
   !   type(nx_sh_traj_t), intent(in)  :: traj
   !   !! SH trajectory at the current integration step.
   !   type(nx_sh_traj_t), intent(in)  :: traj_prev
   !   !! Sh trajectory at the step before.
   !   real(dp), intent(in) :: dt
   !   !! Integration time step.
   !   real(dp), dimension(:, :), intent(inout) :: stmat
   !   !! State overlap matrix.
   !   real(dp), dimension(:), intent(inout) :: b
   !   !! Computed transition probabilities.
   !   integer, intent(inout) :: irk
   !   type(nx_logger_t), intent(inout) :: logger
   !   complex(dp), intent(out) :: new_coeff(:)
   !   ! integer, intent(out) :: surf
 
   !   real(dp), dimension(:, :), allocatable :: rotr, roti
   !   real(dp), dimension(:), allocatable :: acoer0,acoei0,acoer1,acoei1
   !   real(dp), dimension (:), allocatable  :: ecitmp,eciotmp
   !   real(dp) :: ezero
 
   !   integer           :: i,dim, isurf, status
 
   !   real(dp), allocatable :: trans(:, :)
   !   complex(dp), allocatable :: rotation(:, :)
 
   !   isurf = traj%get_isurf()
   !   ! B.D: Allocate memory (same dimensions as ``b``)
   !   dim = size(b)
   !   allocate(rotr(dim, dim), roti(dim, dim))
   !   allocate(acoer0(dim), acoei0(dim), acoer1(dim), acoei1(dim))
   !   allocate(eciotmp(dim), ecitmp(dim))
 
   !   ! write(*,*) 'Starting local diabatization routine ...'
   !   ezero = 0.0_dp ! maybe a different reference energy should be chosen
 
   !   call logger%log(LOG_DEBUG, stmat, &
   !        & title='S-matrix, read in from run_cioverlap.log')
 
   !   ! eq. (15)
   !   ! call get_tmat(stmat,dim,logger)
   !   call lowdin_new(stmat, i)
   !   if (i > 0) then
   !      call logger%log(LOG_ERROR, 'LD: lowdin_new -> dsyev failed to converge')
   !      return
   !   else if (i < 0) then
   !      call logger%log(LOG_ERROR, 'LD: lowdin_new -> illegal argument '//to_str(i)//' to dsyev')
   !      return
   !   end if
   !   call logger%log(LOG_DEBUG, stmat, title='S-matrix, after Lowdin')
 
   !   ! Old procedure
   !   ! acoer0(:) = real(traj_prev%get_wf())
   !   ! acoei0(:) = aimag(traj_prev%get_wf())
   !   ! ! eq. (16)
   !   ! call propag_ld(traj%get_epot(), traj_prev%get_epot(), 0.d0, dt, stmat&
   !   !      &, rotr, roti, acoer0, acoei0, acoer1, acoei1, dim, logger)
   !   ! new_coeff = cmplx(acoer1(:),acoei1(:), kind=dp)
   !   ! call logger%log(LOG_DEBUG, new_coeff, title='New coefficients')
   !   ! 
   !   ! ! eq. (19), the transition probabilitites are computed here while all the variables
   !   ! ! are still available
   !   ! ! surf = isurf
   !   ! call trans_ld(stmat, rotr, roti, acoer0, acoei0, acoer1, acoei1,&
   !   !      & b, isurf, irk, dim, logger)
 
   !   ! New procedure
   !   allocate(rotation(dim, dim))
   !   call ld_propagation(traj%get_epot(), traj_prev%get_epot(), &
   !        & 0.0_dp, dt, stmat, rotation, logger&
   !        & )
   !   do i=1, dim
   !      new_coeff(i) = sum ( rotation(i, :) * traj_prev%get_wf() )
   !   end do
   !   call logger%log(LOG_DEBUG, new_coeff, title='New coefficients')
     
   !   call ld_check_tmat_for_transition(stmat, isurf, b, status)
   !   if (status == 0) then
   !      call ld_compute_probabilities(rotation, traj_prev%get_wf(), new_coeff, &
   !           & isurf, b, irk, logger&
   !           & )
   !   end if
   ! end subroutine local_diabatization
 
 
   ! subroutine build_hamiltonian(hamiltonian, coeff1, coeff2)
   !   complex(dp), intent(inout) :: hamiltonian(:, :)
   !   type(nx_coeff_t), intent(in) :: coeff1
   !   type(nx_coeff_t), intent(in) :: coeff2
   ! 
   !   integer :: k, l, kl, nsurf
   !   complex(dp) :: sx, sx1
   ! 
   !   nsurf = size(hamiltonian, 1)
   !   do k=1, nsurf
   !      do l=1, k-1
   !         kl = find_index(k, l)
   !         sx = coeff1%sigma(kl) * exp(cmplx(0, -coeff1%gamma(kl), kind=dp))
   !         sx1 = coeff2%sigma(kl) * exp(cmplx(0, -coeff2%gamma(kl), kind=dp))
   !         hamiltonian(k, l) = (sx + sx1)
   !         hamiltonian(l, k) = -conjg(hamiltonian(k, l))
   !      end do
   !   end do
   ! 
   ! end subroutine build_hamiltonian
 
 
   subroutine sh_write_hopping_info(solver, txtout, epot, old_veloc, new_veloc, old_state,&
        & new_state, masses)
     type(nx_sh_solver_t), intent(in) :: solver
     type(nx_output_t), intent(inout) :: txtout
     real(dp), intent(in) :: epot(:)
     real(dp), intent(in) :: old_veloc(:, :)
     real(dp), intent(in) :: new_veloc(:, :)
     integer, intent(in) :: old_state
     integer, intent(in) :: new_state
     real(dp), intent(in) :: masses(:)
 
     character(len=:), allocatable :: filename
     integer :: u, step, i, j
     real(dp) :: t, de, ekin_before, ekin_after
 
     call txtout%set_txtout_path()
     filename = txtout%txtout%mhopf
     step = mod(solver%step, solver%params%ms) + 1
     t = solver%step * solver%dts * timeunit
 
     ekin_before = compute_ekin(old_veloc, masses)
     ekin_after = compute_ekin(new_veloc, masses)
 
     de = epot(new_state) - epot(old_state)
 
     open(newunit=u, file=filename, action='write', position='append')
     write(u, '(A)') repeat('*', 80)
     write(u, '(A, F20.12, A, A, I0, A)') 'SURFACE HOPPING at t = ', t, ' fs. ', &
          & '(MICRO STEP ', step, ')'
     write(u, '(A,I0)') 'From state: ', old_state
     write(u, '(A,I0)') '  To state: ', new_state
     write(u, '(A)') ''
     write(u, '(A)') 'Velocity before hopping (a.u.):'
     do i=1, size(old_veloc, 2)
        write(u, '(3F20.12)') (old_veloc(j, i), j=1, 3)
     end do
     write(u, '(A)') 'Velocity after hopping (a.u.):'
     do i=1, size(new_veloc, 2)
        write(u, '(3F20.12)') (new_veloc(j, i), j=1, 3)
     end do
     write(u, '(A)') ''
     write(u, '(A)') 'Kinetic energies (a.u.):'
     write(u, '(A,F20.12)') 'ekin(before) = ', ekin_before
     write(u, '(A,F20.12)') ' ekin(after) = ', ekin_after
     write(u, '(A)') ''
     write(u, '(A)') 'Potential energies (a.u.):'
     write(u, '(A,F20.12)') '       epot(before) = ', epot(old_state)
     write(u, '(A,F20.12)') '        epot(after) = ', epot(new_state)
     write(u, '(A,F20.12)') ' de(before - after) = ', de
     write(u, '(A,F20.12,A)') ' de(before - after) = ', de * au2ev, ' eV'
     write(u, '(A)') ''
     write(u, '(A)') 'Total energies (a.u.):'
     write(u, '(A,F20.12)') &
          & '       ETOT(before) = ', epot(old_state) + ekin_before
     write(u, '(A,F20.12)') &
          & '        ETOT(after) = ', epot(new_state) + ekin_after
     write(u, '(A)') ''
     write(u, '(A)') 'Surface hopping parameters:'
     write(u, '(A,F20.12)') 'Computed probability: ', solver%shprob(step, new_state)
     write(u, '(A,F20.12)') '       Random number: ', solver%rx(step)
     write(u, '(A)') repeat('*', 80)
     write(u, '(A)') ''
     close(u)
   end subroutine sh_write_hopping_info
 
 
   function sh_csfssh_compute_gamma(self, epot, step) result(res)
     class(nx_sh_solver_t), intent(inout) :: self
     real(dp), intent(in) :: epot(:)
     integer, intent(in) :: step
 
     real(dp) :: res( size(epot) )
 
     real(dp) :: epot_ref_for_step( self%nstat )
 
     if (self%run_complex) then
        if (self%csfssh%gamma_model /= 2) then
           res = interpolate_epot(self%epot_i_all, self%en_times, self%params%ms, step)
        else
           epot_ref_for_step = interpolate_epot(&
                & self%epot_ref, self%en_times, self%params%ms, step&
                & )
           res = csfssh_interpolate_gamma(&
                & self%csfssh%ref_resonance, epot, epot_ref_for_step(1), self%csfssh%ress_shift&
                & )
        end if
     end if
   end function sh_csfssh_compute_gamma
 
 
   ! function csfssh_get_gamma_model_2(self, traj, epot, ref_resonance, ress_shift, step) result(res)
   !   class(nx_sh_solver_t), intent(inout) :: self
   !   type(nx_traj_t), intent(in) :: traj
   !   real(dp), intent(in) :: epot(:)
   !   real(dp), intent(in) :: ref_resonance(:, :, :)
   !   real(dp), intent(in) :: ress_shift
   !   integer, intent(in) :: step
   ! 
   !   real(dp) :: res( size(traj%epot) )
   ! 
   !   real(dp), allocatable :: epot_ref(:, :), epot_ref_for_step(:)
   !   call reshape_epot(traj%step, traj%dt, [traj%epot_ref], traj%epot_ref_old, 1, epot_ref, self%en_times)
   !   epot_ref_for_step = interpolate_epot(epot_ref, self%en_times, self%params%ms, step)
   !   res = csfssh_interpolate_gamma(ref_resonance, epot, epot_ref_for_step(1), ress_shift)
   ! end function csfssh_get_gamma_model_2
 
 
   pure function sh_build_sigma(self, veloc, nad) result(sigma)
     class(nx_sh_solver_t), intent(in) :: self
     real(dp), intent(in) :: veloc(:, :)
     real(dp), intent(in) :: nad(:, :, :)
 
     real(dp) :: sigma(self%ncoupl)
 
     integer :: i
     
     if (self%params%vdoth == 0) then
        do i=1, size(sigma)
           sigma(i) = sum( veloc(:, :) * nad(i, :, :) )
        end do
     else
        sigma(:) = nad(:, 1, 1)
     end if
     
   end function sh_build_sigma
   
 
   real(dp) pure function sh_get_epot(self, surf, step)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in) :: surf
     integer, intent(in), optional :: step
 
     integer :: n
 
     n = 1
     if (present(step)) n = step
     
     sh_get_epot = self%shtraj(n)%get_epot_from_state(surf)
   end function sh_get_epot
 
   pure function sh_get_epot_vec(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     real(dp) :: res( self%nstat )
     integer :: n
 
     if (present(step)) then
        res(:) = self%shtraj(step)%get_epot()
     else
        res(:) = self%shtraj(1)%get_epot()
     end if
   end function sh_get_epot_vec
 
 
   integer pure function sh_get_isurf(self, step)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     sh_get_isurf = self%shtraj(1)%get_isurf()
     if (present(step)) sh_get_isurf = self%shtraj(step)%get_isurf()
   end function sh_get_isurf
 
 
   pure function sh_get_veloc(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     real(dp) :: res( size(self%incveloc, 1), size(self%incveloc, 2) )
 
     if (present(step)) then
        res = self%shtraj(step)%get_veloc()
     else
        res = self%shtraj(1)%get_veloc()
     end if
   end function sh_get_veloc
 
 
   pure function sh_get_wf(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     complex(dp) :: res( self%nstat )
     if (present(step)) then
        res = self%shtraj(step)%get_wf()
     else
        res = self%shtraj(1)%get_wf()
     end if
   end function sh_get_wf
 
 
   pure function sh_get_phase(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     real(dp), allocatable :: res(:)
 
     if (present(step)) then
        res = self%shtraj(step)%get_gamma()
     else
        res = self%shtraj(1)%get_gamma()
     end if
   end function sh_get_phase
 
 
   pure function sh_get_wf_on_state(self, state, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in) :: state
     integer, intent(in), optional :: step
 
     complex(dp) :: res
 
     complex(dp) :: wf( self%nstat )
     
     if (present(step)) then
        wf = self%shtraj(step)%get_wf()
     else
        wf = self%shtraj(1)%get_wf()
     end if
 
     res = wf(state)
   end function sh_get_wf_on_state
   
 
   function sh_adjust_vinc(self, irk, switch_to) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in) :: irk
     integer, intent(in) :: switch_to
 
     real(dp) :: res( size(self%incveloc, 1), size(self%incveloc, 2) )
     
     real(dp), allocatable :: acc_after_hop(:, :)
     integer :: i
 
     if (irk == 1) then
        allocate( acc_after_hop, mold=self%incveloc )
 
        do i=1, size(acc_after_hop, 2)
           acc_after_hop(:, i) = -self%incgrad(switch_to, :, i) / self%masses(i)
        end do
 
        res(:, :) = self%dts * self%params%ms * acc_after_hop(:, :)
 
     else if ((irk == 2) .or. (irk == 3)) then
        ! In this case we have a frustrated hopping, and
        ! possibly we have reverted the velocities.
        res = self%params%mom * self%incveloc
     end if
   end function sh_adjust_vinc
 
 
   function sh_get_ekin_reservoir(self, veloc, ierr, direction) result(ekin)
     !! Compute the kinetic energy reservoir for hopping.
     !!
     !! This function returns the available kinetic energy to perform a surface hopping.
     !! The kinetic energy computed corresponds to the ``veloc`` argument, so in case of
     !! QM/MM, the ``veloc`` array should be pre-built accordingly to the system
     !! considered.
     !!
     !! The kinetic energy computed depends on the value of ``self%params%adjmom``:
     !! 
     !! - ``adjmom = 0``: Standard kinetic energy scaled with the number of internal
     !!   coordinates ;
     !! - ``adjmom = 1``: Standard kinetic energy ;
     !! - ``adjmom = 2``: Kinetic energy in the direction of hopping (see equation 19 from
     !! [10.1063/5.0113908](http://dx.doi.org/10.1063/5.0113908) )
     !!
     !! ``ierr`` is a status indicator, and will be either:
     !!
     !! - ``0``: normal termination
     !! - ``1``: ``direction`` is absent but required (``adjmom = 2`` only)
     !! - ``2``: ``self%nintc`` is 0 or negative (``adjmom = 0`` only)
     class(nx_sh_solver_t), intent(in) :: self
     !! SH solver object.
     real(dp), intent(in) :: veloc(:, :)
     !! Velocity for computing the kinetic energy reservoir.
     integer, intent(out) :: ierr
     !! Status indicator.
     real(dp), intent(in), optional :: direction(:, :)
     !! Direction of hopping.
 
     real(dp) :: ekin
 
     real(dp) :: num, den, tmp
     integer :: i
 
     ierr = 0
     ekin = -1.0_dp
 
     if (self%params%adjmom /= 2) then
        ! We use the momentum direction, so we just need the total kinetic energy
        ekin = compute_ekin(veloc, self%masses)
        
        if (self%params%adjmom == 0) then
 
           ! TODO: Add check to main object !!
           ! if (self%nintc <= 0) then
           !    ierr = 2
           !    return
           ! end if
 
           ! Divide by the number of degrees of freedom
           ekin = ekin / self%nintc
        end if
 
     else
        ! We use an arbitrary direction, so we use the amount of kinetic energy in this
        ! direction.
        if (.not. present(direction)) then
           ierr = 1
           return
        end if
        
        num = scalar_product(veloc, direction) ** 2
        den = 0.0_dp
        do i=1, size(veloc, 2)
           den = den + norm(direction(:, i))**2 / self%masses(i)
        end do
        ekin = num / (2*den)
     end if
   end function sh_get_ekin_reservoir
   
 
   function vel_rescale_momentum_direction(self, veloc, switch_to) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     real(dp), intent(in) :: veloc(:, :)
     integer, intent(in) :: switch_to
 
     real(dp) :: res( size(veloc, 1), size(veloc, 2))
 
     real(dp) :: alpha, ekin, de
     integer :: i
 
     if (self%has_qmmm) then
        ekin = compute_ekin(self%get_veloc(), self%masses, qm_filter=self%qm_filter)
     else
        ekin = compute_ekin(self%get_veloc(), self%masses)
     end if
     res = veloc
     de = self%get_epot( self%get_isurf() ) - self%get_epot( switch_to )
 
     alpha = (de / ekin) + 1
     alpha = self%params%mom * sqrt(alpha)
 
     if (self%has_qmmm) then
        do i=1, self%nat
           if (self%qm_filter(i)) then
              res(:, i) = alpha * res(:, i)
           end if
        end do
     else
        res = res * alpha
     end if
   end function vel_rescale_momentum_direction
 
 
   pure function sh_get_qm_veloc(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     real(dp), allocatable :: res(:, :)
 
     real(dp), allocatable :: tmp2(:, :)
     integer :: i, ind
 
     allocate(res(3, self%qmnat))
     
     if (self%has_qmmm) then
        
        allocate(tmp2( 3, self%nat))
        tmp2 = self%get_veloc(step)
 
        ind = 1
        do i=1, size(self%qm_filter)
           if (self%qm_filter(i)) then
              res(:, ind) = tmp2(:, i)
              ind = ind + 1
           end if
        end do
     else
        res = self%get_veloc(step)
     end if
     
   end function sh_get_qm_veloc
   
 
   subroutine sh_integration(self, new_coeff, shprob)
     class(nx_sh_solver_t), intent(inout) :: self
     complex(dp), intent(out) :: new_coeff(:)
     real(dp), intent(out) :: shprob(:)
 
     integer :: i, ntraj
     integer :: integrator_step
 
     ntraj = size(self%shtraj)
 
     integrator_step = self%step - self%init_step
     
     ! Evolve the phases
     do i=1, ntraj
        self%phases(i) = fholder_r( self%shtraj(i)%get_fphase() )
     end do
     call self%logger%log(LOG_DEBUG, self%phases(2)%f%acoef, title='Old phases')
     call finite_element_int(self%phases, self%dts, integrator_step, approx=.false.)
     call self%logger%log(LOG_DEBUG, self%phases(1)%f%acoef, title='New phases')
 
     ! Transfer to coefficients
     call self%shtraj(1)%set_gamma( self%phases(1)%f%acoef )
     call self%shtraj(1)%compute_adot()
 
     ! Evolve the coefficients
     do i=1, ntraj
        self%coeffs(i) = fholder_c( self%shtraj(i)%get_fcoeff() )
     end do
     call self%logger%log(LOG_DEBUG, self%coeffs(2)%f%acoef, title='Old coefficients')
     call self%logger%log(LOG_DEBUG, self%coeffs(2)%f%get_deriv(), title='Old derivatives')
     select case(self%params%integrator)
     case(0)
        call finite_element_int(self%coeffs, self%dts, integrator_step, approx=.true.)
     case(1)
        call butcher_int(self%coeffs, self%dts, integrator_step)
     end select
     call self%logger%log(LOG_DEBUG, self%coeffs(1)%f%acoef, title='New coefficients')
     call self%logger%log(LOG_DEBUG, &
          & self%coeffs(1)%f%get_deriv(), title='New derivatives'&
          & )
 
     ! Transfer to probabilities
     call self%shtraj(1)%set_wf( self%coeffs(1)%f%acoef )
     call self%shtraj(1)%compute_adot()
 
     ! Evolve the probabilities
     do i=1, ntraj
        self%probs(i) = fholder_r( self%shtraj(i)%get_fprob() )
     end do
     select case(self%params%tully)
     case(0)
        self%probs(1)%f%acoef(:) = self%dts * self%probs(1)%f%get_deriv()
     case(1)
        call finite_element_int(self%probs, self%dts, integrator_step, approx=.false.)
     end select
     do i=1, self%nstat
        if (self%probs(1)%f%acoef(i) < 0.0_dp) self%probs(1)%f%acoef(i) = 0.0_dp
     end do
 
     ! Scale the probability by the population of the current state.
     self%probs(1)%f%acoef = &
          & self%probs(1)%f%acoef / abs(self%get_wf_on_state(self%get_isurf()))**2
     call self%logger%log(LOG_DEBUG, self%probs(1)%f%acoef, title='New probabilities')
 
     shprob(:) = self%probs(1)%f%acoef(:)
     new_coeff(:) = self%coeffs(1)%f%acoef(:)
   end subroutine sh_integration
 
 
   pure function sh_get_populations(self, step) result(res)
     class(nx_sh_solver_t), intent(in) :: self
     integer, intent(in), optional :: step
 
     real(dp) :: res( self%nstat )
 
     integer :: n
 
     n = 1
     if (present(step)) n = step
 
     res = abs(self%get_wf(step=n))**2
   end function sh_get_populations
 
 
   subroutine sh_fewest_switch(shprob, rx, switch_to, probmin)
     real(dp), intent(in) :: shprob(:)
     real(dp), intent(out) :: rx
     integer, intent(out) :: switch_to
     real(dp), intent(in), optional :: probmin
 
     integer :: k
     real(dp) :: bt
     real(dp) :: minimal_probability
 
     minimal_probability = 0.0_dp
     if (present(probmin)) minimal_probability = probmin
 
     switch_to = -1 
     rx = 1.0_dp ! By default, no hopping is possible (probability of hopping is 0)
 
     call nx_random_number(rx)
 
     bt = 0.0_dp
     switch_to = -1
     do k=1, size(shprob)
        bt = bt + shprob(k)
        if (rx <= bt) then
           switch_to = k
 
           if (rx <= minimal_probability) then
              ! If the probability is too low, don't attempt to hop.
              switch_to = -1
           end if
           return
        end if
     end do
   end subroutine sh_fewest_switch
 
   ! subroutine sh_hop_and_rescale(veloc, epot, adjmom, d_rescale, irk, newveloc)
     !! Attempt surface hopping with total enrgy conservation.
     !!
     !! This routine will attempt to hop from surface ``old_surf`` to surface ``new_surf``
     !! if total energy conservation can be enforced.  This will be done by trying to
     !! rescale the velocity ``veloc``, either in an arbitrary direction in the \(g, h\)
     !! plane (``adjmom = 2``), or in the direction of the momentum (``adjmom < 2``).
     !!
     !! We derive this routine from Braun et al.
     !! ([10.1063/5.0113908](http://dx.doi.org/10.1063/5.0113908)), and assume we jump
     !! from state \(J\) to state \(K\). Starting from
     !!
     !! \[ v_{\alpha}(K) = v_{\alpha}(J) + \gamma_{JK}
     !! \frac{\boldsymbol{u}_{\alpha}}{M_{\alpha}} \]
     !!
     !! for any atom \(\alpha\) with mass \(M_{\alpha}\) and \(\boldsymbol{u}_{\alpha}\)
     !! the direction of hopping, we claim that surface hopping will be possible only if
     !! \(\gamma_{JK}\) is real.
     !!
     !! In the following we note \(\Delta E_{KJ}\) the potential energy difference between
     !! \(K\) and \(J\) (e.g. \( \Delta E_{KJ} = E_{K} - E_{J}\). 
     !!
     !! ## Momentum direction
     !! 
     !! When rescaling in the momentum direction, the expression above becomes
     !!
     !! \[ v_{\alpha}(K) = \gamma_{JK}'  v_{\alpha}(J), \gamma_{JK}' = 1 + \gamma_{JK}. \]
     !!
     !! Here we note \(E_{kin}^{o}\) the kinetic energy before hopping. The conservation of
     !! total energy leads to:
     !!
     !! \[ \gamma_{JK}' = \sqrt{ 1 - \frac{\Delta E_{KJ}}{E_{kin}^{o}}}, \]
     !!
     !! which leads to the condition:
     !!
     !! \[ E_{kin}^{o} - \Delta E_{KJ}  \ge 0. \]
     !!
     !! If the system is large, it may be useful to set ``adjmom = 0``, which will replace
     !! \(E_{kin}^{o}\) by \(\frac{E_{kin}^{o}}{N_{int}}\), where \(N_{int}\) is the
     !! number of internal coordinates in the system.
     !!
     !! ## Direction in the \((g, h)\) plane
     !!
     !! In this case, the total energy expression can be developped into:
     !!
     !! \[ \gamma_{JK}^2 \sum_{\alpha} \frac{\boldsymbol{u}_{\alpha}^2}{M_{\alpha}}
     !!           + \gamma_{JK} \sum_{\alpha} v_{\alpha}(J) \cdot \boldsymbol{u}_{\alpha}
     !!           + \Delta E_{KJ} = 0, \]
     !!
     !! which can give values of \(\gamma_{JK}\) that are real only when:
     !!
     !! \[ \frac{ \left ( \sum_{\alpha} v_{\alpha}(J) \cdot
     !!                                 \boldsymbol{u}_{\alpha} \right )^2
     !!         }{2 \sum_{\alpha} \frac{\boldsymbol{u}_{\alpha}^2}{M_{\alpha} }}
     !!    - \Delta E_{KJ} \ge 0. \]
     !!
     !! The latter expression is analogous to the one obtained when rescaling in the
     !! momentum direction, we just need a different expression of the kinetic energy.
     !!
     !! ## Implementation
     !!
     !! We will use a /kinetic energy reservoir/, which depends on the value of ``adjmom``
     !! for deciding if the hopping is real, or frustrated.
     !!
     ! real(dp), intent(in) :: veloc(:, :)
     ! real(dp), intent(in) :: epot(:)
     ! real(dp), intent(in) :: masses(:)
     ! integer, intent(in) :: adjmom
     ! integer, intent(in) :: isurf
     ! integer, intent(in) :: switch_to
     ! integer, intent(in) :: mom
     ! integer, intent(out) :: irk
     ! real(dp), intent(out) :: newveloc(:, :)
     ! real(dp), intent(out), optional :: ekin_res
     ! real(dp), intent(in), optional :: d_rescale(:, :)
     ! integer, intent(in), optional :: nintc
     ! 
     ! real(dp) :: ekin_reservoir, a, b, de
     ! integer :: n_int_coords
     ! 
     ! n_int_coords = -1
     ! if (present(nintc)) n_int_coords = nintc
     ! 
     ! irk = -1
     ! if (adjmom == 2) then
     !    if (.not. present(d_rescale)) then
     !       ! With adjmom = 2, a direction in which the rescaling has to be done is
     !       ! mandatory !
     !       irk = -2
     !       return
     !    end if
     ! else if ((adjmom == 0) .and. (nintc < 0))
     !    irk = -3
     !    return
     ! end if
     ! 
     ! ! Compute the kinetic energy reservoir
     ! if (adjmom /= 2) then
     !    ekin_reservoir = compute_ekin(veloc, masses)
     ! 
     !    if (adjmom == 0) ekin_reservoir = ekin_reservoir / nintc
     ! else
     !    b = scalar_product(veloc, d_rescale)
     !    a = 0.0_dp
     !    do i=1, size(veloc, 2)
     !       a = a + norm(d_rescale(:, i))**2 / (2*masses(i))
     !    end do
     !    ekin_reservoir = b**2 / (4 * a)
     ! end if
     ! 
     ! ! Save value if required
     ! if (present(ekin_res)) ekin_res = ekin_reservoir
     ! 
     ! de = epot(switch_to) - epot(isurf)
     ! if (ekin_reservoir - de < 0) then
     !    irk = 2
     !    newveloc(:, :) = mom * veloc(:, :)
     ! else
     !    if (adjmom /= 2) then
     !       newveloc(:, :) = 
     
     
   ! end subroutine sh_hop_and_rescale
   
 
 end module mod_sh
 