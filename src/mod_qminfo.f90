! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_qminfo
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Module to gather information extracted from a QM job
  !!
  !! This module defines the ``nx_qminfo_t`` datatype, to hold information such as the
  !! potential energies, gradients, etc. read from any QM output.
  !!
  !! # Usage
  !!
  !! ## General usage
  !!
  !! ```fortran
  !! integer :: nat, nstat  ! Number of atoms and states, resp.
  !! type(nx_qminfo_t) :: qminfo
  !! qminfo = nx_qminfo_t(nstat, nat)
  !!
  !! ! For CS-FSSH, with NAD read from the QM job:
  !! qminfo = nx_qminfo_t(nstat, nat, dc_method=1, run_complex=.true.)
  !! ```
  !!
  !! ## Storing lines from an output
  !!
  !! ```fortran
  !! integer :: u, ierr, id, i
  !! character(len=1024) :: buf
  !! type(nx_qminfo_t) :: qminfo
  !!
  !! qminfo = nx_qminfo_t(3, 10)
  !! open(newunit=u, file='my_output_file', action='read')
  !! do
  !!     read(u, '(A)', iostat=ierr) buf
  !!     if (ierr /= 0) exit
  !!     if (index(buf, 'my_flag') /= 0) then
  !!         call qminfo%append_data(buf)
  !!     end if
  !! end do
  !! close(u)
  !!
  !! ! Now print what we recovered, if we have something
  !! if ( qminfo%dim_dataread > 0) then
  !!     do i=1, qminfo%dim_dataread
  !!         write(*, '(A)') trim( qminfo%dataread(i) )
  !!     end do
  !!  end if
  !!  ```
  !!
  use mod_kinds, only: dp
  implicit none

  private

  type, public :: nx_qminfo_t
     !! Information read from QM job.
     !!
     !! The object also contains information about what to do with the information, such
     !! as if the nad reovered should be transfered to the main ``nx_traj_t`` object or
     !! not.  That can be useful in come cases (for instance, when using Columbus with
     !! state overlap couplings from ``cioverlap``).
     !!
     !! In the description ``nstat`` is the number of states, ``nat`` the number of atoms
     !! and ``ncoupl = (nstat - 1)*nstat / 2`` is the number of couplings.

     ! General data read from output
     real(dp), allocatable :: repot(:)
     !! Potential energies. Dimension: ``(nstat)``.
     real(dp), allocatable :: rgrad(:, :, :)
     !! Gradients. Dimension: ``(nstat, 3, nat)``.
     real(dp), allocatable :: rnad(:, :, :)
     !! Non-adiabatic couplings. Dimension: ``(ncoupl, 3, nat)``.
     character(len=256), allocatable :: dataread(:)
     !! Arbitrary strings recovered from the output. For instance, this is typically used
     !! to store the contributions to the wavefunction.
     integer :: dim_dataread
     !! Total number of lines actually written in ``dataread``.

     ! CS-FSSH - specific things.
     real(dp), allocatable :: rnad_i(:, :, :)
     !! CS-FSSH: Imaginary part of the non-adiabatic couplings.
     !! Dimension: ``(ncoupl, 3, nat)``.
     real(dp), allocatable :: rgamma(:)
     !! CS-FSSH: Resonance energy. Dimension: ``(nstat)``.
     logical :: is_reference = .false.
     !! CS-FSSH: Indicate if info are read from reference job (``gamma_model = 2``).

     ! Flags for transfering to ``nx_traj_t``:
     logical :: correct_phase = .true.
     !! For transfering to ``nx_traj_t``: correct the phase of the NAD or not.
     !! (Default: ``.true.``).
     logical :: update_nad = .false.
     !! For transfering to ``nx_traj_t``: Update the NAD or not.
     !! (Default: ``.false.``).
   contains
     procedure :: destroy
     procedure :: append_data
  end type nx_qminfo_t
  interface nx_qminfo_t
     module procedure qminfo_alloc
  end interface nx_qminfo_t

contains

  function qminfo_alloc(nstat, nat, dc_method, run_complex) result(res)
    !! Instantiate a ``nx_qminfo_t`` object.
    !!
    !! The function will only allocate memory for the CS-FSSH elements if ``run_complex``
    !! is ``.true.`` (defaults to ``.false.``).  The ``update_nad`` element will be set
    !! to ``.false.`` unless ``dc_method`` is provided and equal to 1 (the default is to
    !! set it to ``.false.``).
    integer, intent(in) :: nstat
    !! Total number of states.
    integer, intent(in) :: nat
    !! Total number of atoms.
    integer, intent(in), optional :: dc_method
    !! Method for obtaining derivative couplings (see ``nx_config_t%dc_method`` for
    !! details).
    logical, intent(in), optional :: run_complex
    !! Perform CS-FSSH or not.

    type(nx_qminfo_t) :: res
    
    integer :: ncoupl

    logical :: is_complex
    integer :: update_nad

    ncoupl = (nstat * (nstat-1)) / 2

    allocate(res%dataread(1024))
    allocate(res%repot(nstat))
    allocate(res%rgrad(nstat, 3, nat))
    allocate(res%rnad(ncoupl, 3, nat))
    res%dim_dataread = 0
    res%repot = 0.0_dp
    res%rgrad = 0.0_dp
    res%rnad = 0.0_dp

    is_complex = .false.
    if (present(run_complex)) is_complex = run_complex
    
    if (is_complex) then
       allocate(res%rnad_i(ncoupl, 3, nat))
       allocate(res%rgamma(nstat))
       res%rnad_i = 0.0_dp
       res%rgamma = 0.0_dp
    end if

    update_nad = 0
    if (present(dc_method)) update_nad = dc_method
    if (update_nad == 1) then
       res%update_nad = .true.
    end if
  end function qminfo_alloc


  subroutine destroy(self)
    !! Clean the memory.
    !!
    class(nx_qminfo_t), intent(inout) :: self
    !! ``nx_qminfo_t`` object.

    deallocate(self%dataread)
    deallocate(self%repot)
    deallocate(self%rgrad)
    deallocate(self%rnad)

    if (allocated(self%rgamma)) then
       deallocate(self%rgamma)
       deallocate(self%rnad_i)
    end if
  end subroutine destroy


  subroutine append_data(self, msg)
    !! Append ``msg`` to member array ``dataread``.
    !!
    !! Each time this routine is called, the ``dim_dataread`` member is incremented.  The
    !! incrementation is done *BEFORE* appeding ``msg`` to ``dataread``.
    class(nx_qminfo_t), intent(inout) :: self
    !! This object.
    character(len=*), intent(in) :: msg
    !! String to append.

    self%dim_dataread = self%dim_dataread + 1
    self%dataread(self%dim_dataread) = trim(msg)
  end subroutine append_data
  
  
end module mod_qminfo
