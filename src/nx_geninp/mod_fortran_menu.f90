! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_fortran_menu
  !! # Simple interactive menu for Fortran.
  !!
  !! ## Description
  !!
  !! This module implements simple interactive menus and selection.  The module is
  !! composed of two main parts:
  !!
  !! - a ``fmenu_selection_menu`` that can print a menu on screen, and wait for a
  !! selection from the user ;
  !! - an interface ``ask`` that can ask questions and return an answer ;
  !! - function ``ask_y_or_n`` that asks yes or no type questions.
  !!
  !! The usual workflow is to have a menu that directs to a set of questions, or another
  !! menu, until all tasks have been completed.
  !!
  !! ## Limitations
  !!
  !! - Questions having a predefined set of answers (e.g. options that can take only a
  !! few values) must be set as ``characters``, and be ``read`` later in the proper type
  !! if needed.  This is due to the simple implementation of ```pair_t`` we use.
  !!
  use mod_keyval_pairs, only: pair_t
  use mod_kinds, only: dp
  use mod_tools, only: to_str, split_pattern
  use iso_fortran_env, only: &
       & stdout => output_unit, stdin => input_unit

  interface ask
     !! Ask a question to the user.
     !!
     module procedure ask_int
     module procedure ask_real
     module procedure ask_char
     module procedure ask_char_with_options
  end interface ask
  public :: ask
  public :: ask_y_or_n

  character(len=1), parameter :: nl = NEW_LINE('a')
  character(len=1), parameter :: tab = achar(9)

contains

  function fmenu_selection_menu(options, with_exit, exit_message) result(res)
    !! Menu-type selection.
    !!
    !! A menu is simply a list of options (array of ``character`` of arbitrary length),
    !! with an optional exit option and exit message.  The function prints on ``STDOUT``
    !! (``output_unit`` from module ``iso_fortran_env``) a menu of the form:
    !!
    !!     1. options(1)
    !!     2. options(2)
    !!     ...
    !!
    !! If ``with_exit`` is ``.true.`` then a supplementary option is provided at the end
    !! of the list with message ``exit_message``, that can be used for better flow
    !! control (for instance, go back to previous menu).  The function then reads an
    !! input from ``STDIN`` (``input_unit`` from module ``iso_fortran_env``), and returns
    !! this value only if it is within the boundaries defined by the length of the array
    !! ``options`` (and the optional ``exit_message``).
    character(len=*), intent(in) :: options(:)
    !! Array defining the different options offered by the menu.
    logical, intent(in), optional :: with_exit
    !! Flag to indicate if an exit option should be provided.  In this case, the default
    !! message printed is simply ``Exit``.
    character(len=*), intent(in), optional :: exit_message
    !! If ``with_exit``, then this message can be printed instead of ``Exit``.

    integer :: res

    integer :: ierr, i
    logical :: error, with_exit_opt
    integer :: minval, maxval

    with_exit_opt = .false.
    if (present(with_exit)) with_exit_opt = with_exit

    minval = 1
    maxval = size(options)
    if (with_exit) then
       maxval = size(options) + 1
    end if

    write(stdout, '(A)') tab//"Please select the task to be performed:"//nl//nl

    do i=1, size(options)
       write(stdout, '(A)') tab//to_str(i)//'. '//trim(options(i))//nl
    end do
    if (with_exit) then
       if (present(exit_message)) then
          write(stdout, '(A)') tab//to_str(i)//'. '//trim(exit_message)//nl
       else
          write(stdout, '(A)') tab//to_str(i)//'. '//'Exit'//nl
       end if
    end if

    MAIN_LOOP: do while (.true.)
       error = .false.
       write(stdout, '(A)', advance='no') "Enter the desired choice: "
       read(stdin, *, iostat=ierr) res
       if (ierr /= 0) then
          error = .true.
       else if (res < minval .or. res > maxval) then
          error = .true.
       end if

       if (error) then
          write(stdout, '(A)') "Invalid value !!"//nl
       else
          exit MAIN_LOOP
       end if
    end do MAIN_LOOP

  end function fmenu_selection_menu

  function ask_int(name, description, val, unit, has_def) result(res)
    !! Ask for an answer of type ``integer``.
    !!
    !! The function prints on screen (``STDOUT``) a text of the form:
    !!
    !!     <name>: <description>
    !!     Current value (in <unit>): <val>
    !!     Enter the desired value: _
    !!
    !! The line ``Current value`` won't be printed when ``has_def`` is ``.false.``.  The
    !! function only terminates when an ``integer`` is read.  If an invalid answer is
    !! given, then
    !!
    !!     Invalid value (integer wanted) !
    !!
    !! is printed on screen, and the function asks for another value.  If no value is
    !! entered, then the function returns ``val``.  If ``val`` isn't set, then we get
    !! back to the wrong answer case.
    character(len=*), intent(in) :: name
    !! Name of the variable that is set.
    character(len=*), intent(in) :: description
    !! Description of the variable.
    integer, intent(in) :: val
    !! Default value.
    character(len=*), intent(in), optional :: unit
    !! Optional unit of the variable set (example: kg, s, ...).
    logical, intent(in), optional :: has_def
    !! Flag to indicate if the default value makes sense. (Default: ``.true.``)

    integer :: res

    character(len=:), allocatable :: un
    character(len=256) :: temp
    logical :: cont, prt_def
    integer :: ierr, test

    un = ''
    if (present(unit)) un = ' (in '//trim(unit)//')'
    write(stdout, '(A)') trim(name)//': '//trim(description)

    prt_def = .true.
    if (present(has_def)) then
       prt_def = has_def
    end if
    if (prt_def) then
       write(stdout, '(A)') 'Current value'//un//': '//to_str(val)
    end if

    cont = .true.
    do while (cont)
       write(stdout, '(A)', advance='no') 'Enter the desired value: '
       read(stdin, '(A)') temp
       if (temp == '') then
          if (.not. prt_def) then
             write(stdout, '(A)') 'Invalid value (integer wanted) !'
          else
             res = val
             write(stdout, *) nl
             cont = .false.
          end if
       else
          read(temp, *, iostat=ierr) test

          if (ierr /= 0) then
             write(stdout, '(A)') 'Invalid value (integer wanted) !'
          else
             read(temp, *) res
             write(stdout, *) nl
             cont = .false.
          end if
       end if
    end do
  end function ask_int


  function ask_real(name, description, val, unit, has_def, fmt) result(res)
    !! Ask for an answer of type ``real``.
    !!
    !! The function prints on screen (``STDOUT``) a text of the form:
    !!
    !!     <name>: <description>
    !!     Current value (in <unit>): <val>
    !!     Enter the desired value: _
    !!
    !! The line ``Current value`` won't be printed when ``has_def`` is ``.false.``.  The
    !! function only terminates when an ``integer`` is read.  If an invalid answer is
    !! given, then
    !!
    !!     Invalid value (real wanted) !
    !!
    !! is printed on screen, and the function asks for another value.  If no value is
    !! entered, then the function returns ``val``.  If ``val`` isn't set, then we get
    !! back to the wrong answer case.
    character(len=*), intent(in) :: name
    !! Name of the variable that is set.
    character(len=*), intent(in) :: description
    !! Description of the variable.
    real(dp), intent(in) :: val
    !! Default value.
    character(len=*), intent(in), optional :: unit
    !! Optional unit of the variable set (example: kg, s, ...). (Default: ``''``).
    logical, intent(in), optional :: has_def
    !! Flag to indicate if the default value makes sense. (Default: ``.true.``)
    character(len=*), intent(in), optional :: fmt
    !! Optional formatter for real (used to print ``val`` if required). (Default:
    !! ``'(F12.3)'``)

    real(dp) :: res

    character(len=:), allocatable :: un
    character(len=:), allocatable :: ffmt
    character(len=256) :: temp
    logical :: cont, prt_def
    integer :: ierr
    real(dp) :: test

    ffmt = '(F12.3)'
    if (present(fmt)) ffmt = fmt

    un = ''
    if (present(unit)) un = ' (in '//trim(unit)//')'
    write(stdout, '(A)') trim(name)//': '//trim(description)

    prt_def = .true.
    if (present(has_def)) then
       prt_def = has_def
    end if
    if (prt_def) then
       write(stdout, '(A)') 'Current value'//un//': '//to_str(val, fmt=ffmt)
    end if

    cont = .true.

    do while (cont)
       write(stdout, '(A)', advance='no') 'Enter the desired value: '
       read(stdin, '(A)') temp
       if (temp == '') then
          if (.not. prt_def) then
             write(stdout, '(A)') 'Invalid value (real wanted) !'
          else
             res = val
             write(stdout, *) nl
             cont = .false.
          end if
       else
          read(temp, *, iostat=ierr) test

          if (ierr /= 0) then
             write(stdout, '(A)') 'Invalid value (real wanted) !'
          else
             read(temp, *) res
             write(stdout, *) nl
             cont = .false.
          end if
       end if
    end do
  end function ask_real


  function ask_char(name, description, val, unit, has_def, should_be) result(res)
    !! Ask for an answer of type ``character``.
    !!
    !! The function prints on screen (``STDOUT``) a text of the form:
    !!
    !!     <name>: <description>
    !!     Current value (in <unit>): <val>
    !!     Enter the desired value: _
    !!
    !! The line ``Current value`` won't be printed when ``has_def`` is ``.false.``.  The
    !! function only terminates when an ``integer`` is read.  As this function wants a
    !! character string as an answer, everything can match.  To help answer validation, a
    !! ``should_be`` option can be specified.  The following are implemented:
    !!
    !! - ``'array of int'``: test if the input can be translated into an array of
    !! integers, /i.e./ if it is of the form ``i1, i2, i3`` where ``iN`` is an integer.
    !!
    !! If ``should_be`` is present, and the string entered does not match, then an line
    !!
    !!     Invalid value (should be ``should be``) !
    !!
    !! is printed on screen
    !!
    !! is printed on screen, and the function asks for another value.  If no value is
    !! entered, then the function returns ``val``.  If ``val`` isn't set, then the line
    !!
    !!     Invalid value (no default provided) !
    !!
    !! is printed, and the function asks for another value.
    character(len=*), intent(in) :: name
    !! Name of the variable that is set.
    character(len=*), intent(in) :: description
    !! Description of the variable.
    character(len=*), intent(in) :: val
    !! Default value.
    character(len=*), intent(in), optional :: unit
    !! Optional unit of the variable set (example: kg, s, ...).
    logical, intent(in), optional :: has_def
    !! Flag to indicate if the default value makes sense. (Default: ``.true.``)
    character(len=*), intent(in), optional :: should_be
    !! Optional flag for input validation. (Default: ``''``)

    character(len=:), allocatable :: res

    character(len=:), allocatable :: un
    character(len=256) :: temp
    logical :: cont, prt_def
    integer :: ierr

    un = ''
    if (present(unit)) un = ' (in '//trim(unit)//')'
    write(stdout, '(A)') trim(name)//': '//trim(description)

    prt_def = .true.
    if (present(has_def)) then
       prt_def = has_def
    end if
    if (prt_def) then
       write(stdout, '(A)') 'Current value'//un//': '//val
    end if

    cont = .true.

    do while (cont)
       write(stdout, '(A)', advance='no') 'Enter the desired value: '
       read(stdin, '(A)') temp
       if (temp == '') then
          if (.not. prt_def) then
             write(stdout, '(A)') 'Invalid value (no default provided) !'
          else
             res = val
             write(stdout, *) nl
             cont = .false.
          end if
       else
          if (present(should_be)) then
             if (should_be == 'array of int') then
                if (.not. is_array_of_int(temp)) then
                   write(stdout, '(A)') 'Invalid value (should be array of int) !'
                else
                   res = temp
                   write(stdout, *) nl
                   cont = .false.
                end if
             end if
          else
             res = temp
             write(stdout, *) nl
             cont = .false.
          end if
       end if
    end do
  end function ask_char


  function ask_char_with_options(name, description, val, options, unit, has_def)&
       & result(res)
    !! Ask for an answer of type ``character``.
    !!
    !! This function works essentially as ``ask_char``, with the exception that the given
    !! answer must be one of the keys defined in ``options``.  If not, the answer is
    !! considered invalid.
    character(len=*), intent(in) :: name
    !! Name of the variable that is set.
    character(len=*), intent(in) :: description
    !! Description of the variable.
    character(len=*), intent(in) :: val
    !! Default value.
    type(pair_t), intent(in) :: options
    !! List of key / value pairs constituting the available answers.
    character(len=*), intent(in), optional :: unit
    !! Optional unit of the variable set (example: kg, s, ...).
    logical, intent(in), optional :: has_def
    !! Flag to indicate if the default value makes sense. (Default: ``.true.``)

    character(len=:), allocatable :: res

    character(len=:), allocatable :: un
    character(len=256) :: temp
    logical :: cont, prt_def
    integer :: ierr, test

    un = ''
    if (present(unit)) un = ' (in '//trim(unit)//')'
    write(stdout, '(A)') trim(name)//': '//trim(description)

    call options%print(stdout, prefix='  -')

    prt_def = .true.
    if (present(has_def)) then
       prt_def = has_def
    end if
    if (prt_def) then
       write(stdout, '(A)') 'Current value'//un//': '//trim(val)
    end if

    cont = .true.
    do while (cont)
       write(stdout, '(A)', advance='no') 'Enter the desired value: '
       read(stdin, '(a)') temp

       if (temp == '') then
          if (.not. prt_def) then
             write(stdout, '(A)') 'Please select a value (no default provided) !'
          else
             res = val
             write(stdout, *) nl
             cont = .false.
          end if
       else
          if (options%has(temp)) then
             res = temp
             write(stdout, *) nl
             cont = .false.
          else
             write(stdout, '(A)') 'Invalid choice: '//trim(temp)//' not available !'
          end if
       end if
    end do
  end function ask_char_with_options


  function ask_y_or_n(question) result(res)
    !! Ask a yes or no question.
    !!
    !! Accepted answer are one of ``y, n, Y, N, yes, no, Yes, No``.  ``question`` is
    !! asked, and a question mark is added at the end, along with the indication
    !! ``(y/n)``, resulting in:
    !!
    !!     <question> ? (y/n)
    !!
    character(len=*), intent(in) :: question
    !! Question (without question mark).

    logical :: res

    character(len=256) :: answer
    logical :: done

    write(stdout, '(A)', advance='no') trim(question)//' ? (y/n) '
    read(stdin, '(A)') answer

    done = .false.
    do while (.not. done)
       if (   answer /= 'y' .and. answer /= 'Y' .and. &
            & answer /= 'yes' .and. answer /= 'YES' .and. &
            & answer /= 'Yes' .and. &
            & answer /= 'n' .and. answer /= 'N' .and. &
            & answer /= 'no' .and. answer /= 'No' .and. &
            & answer /= 'No') then
          write(stdout, '(A)') "Please answer 'yes' or 'no' ('y' or 'n') !"
          read(stdin, '(A)') answer
       else
          if (index(answer, 'y') /= 0 .or. index(answer, 'Y') /=0) then
             res = .true.
          else
             res = .false.
          end if
          done = .true.
       end if
    end do
  end function ask_y_or_n


  ! ===================
  ! Validator functions
  ! ===================

  function is_array_of_int(answer)
    !! Check if string can be converted to 1D-array of ``integers``.
    !!
    !! An array of integers is a list of comma-separated integers.
    character(len=*), intent(in) :: answer
    !! String to test

    logical :: is_array_of_int

    character(len=256), allocatable :: split(:)
    integer :: dum, ierr, i

    split = split_pattern(answer, pattern=',')

    is_array_of_int = .true.
    if (size(split) == 1) then
       read(split(1), *, iostat=ierr) dum
       if (ierr /= 0) then
          is_array_of_int = .false.
       end if
    else if (size(split) > 1) then
       do i=1, size(split)
          read(split(i), *, iostat=ierr) dum
          if (ierr /= 0) then
             is_array_of_int = .false.
          end if
       end do
    end if
  end function is_array_of_int
end module mod_fortran_menu
