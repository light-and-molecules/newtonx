module mod_nxinp_menu
  use mod_constants, only: MAX_STR_SIZE
  use mod_tools, only: to_str
  use iso_fortran_env, only: &
       & stdin => input_unit, stdout => output_unit
  implicit none

  private

  type, public :: nxinp_item_t
     character(len=:), allocatable :: title
     procedure(item_action), pointer, nopass :: action
     type(nxinp_item_t), pointer :: next => null()
   contains
     procedure, pass :: add => nxinp_item_add
     procedure, pass :: count => nxinp_item_count
  end type nxinp_item_t

  abstract interface
     subroutine item_action(dum)
       integer, optional :: dum
     end subroutine item_action
  end interface

  type, public :: nxinp_menu_t
     character(len=:), allocatable :: title
     integer :: exit_option
     type(nxinp_item_t) :: item
     type(nxinp_menu_t), pointer :: next => null()
   contains
     procedure, pass :: set_title => nxinp_menu_set_title
     procedure, pass :: set_exit => nxinp_menu_set_exit
     procedure, pass :: print => nxinp_menu_print
     procedure, pass :: add => nxinp_menu_add
  end type nxinp_menu_t

contains

  subroutine nxinp_menu_set_title(self, title)
    class(nxinp_menu_t), intent(inout) :: self
    character(len=*), intent(in) :: title

    self%title = title
  end subroutine nxinp_menu_set_title


  subroutine nxinp_menu_set_exit(self, opt)
    class(nxinp_menu_t), intent(inout) :: self
    integer, intent(in) :: opt

    self%exit_option = opt
  end subroutine nxinp_menu_set_exit
  

  subroutine nxinp_menu_print(self)
    class(nxinp_menu_t), target, intent(in) :: self

    character(len=4), parameter :: spacer = repeat(' ', 4)

    integer :: i, choice, ierr
    logical :: cont
    type(nxinp_item_t), pointer :: item

    cont = .true.
    do while(cont)
       call execute_command_line('clear')
       i = 1

       write(stdout, '(A)') trim(self%title)
       write(stdout, '(A)') ''
       write(stdout, '(A)') ''
       write(stdout, '(A)') ''

       ierr = self%item%count()
       item => self%item
       do while(ierr > 0)
          write(stdout, '(A)') & 
               & spacer//to_str(i)//'. '//trim(item%title)
          item => self%item%next
          i = i + 1
          ierr = ierr - 1
       end do
       
       if (self%exit_option > 0) then
          write(stdout, '(A)') &
               & spacer//to_str(i)//'. Exit'
       end if

       read(stdin, *, iostat=ierr) choice
       if (ierr /= 0) then
          write(stdout, '(A)') 'Invalid input !'
          write(stdout, '(A)') ''
          write(stdout, '(A)') ''
          call execute_command_line('sleep 1')
       else if (choice == i) then
          cont = .false.
       end if
    end do
    
  end subroutine nxinp_menu_print


  subroutine nxinp_menu_add(self)
    class(nxinp_menu_t), intent(inout) :: self
  end subroutine nxinp_menu_add


  recursive subroutine nxinp_item_add(self, title, action)
    class(nxinp_item_t), intent(inout) :: self
    character(len=*), intent(in) :: title
    procedure(item_action) :: action

    if (allocated(self%title)) then
       if (.not. associated(self%next)) allocate(self%next)
       call nxinp_item_add(self%next, title, action)
    else
       self%title = title
       self%action => action
    end if
  end subroutine nxinp_item_add


  recursive function nxinp_item_count(self) result(c)
    class(nxinp_item_t), intent(in) :: self

    integer :: c

    c = 0
    if (allocated(self%title)) then
       c = c + 1
       if (associated(self%next)) then
          c = c + nxinp_item_count(self%next)
       end if
    else
       c = 0
    end if
  end function nxinp_item_count
  
end module mod_nxinp_menu
