module mod_tinker_g16_mmp
  !! author: Bondanza Mattia <mattia.bondanza@phd.unipi.it>
  !! date: 2020-08-27
  !!
  !! This modules contains all routines and functions related to
  !! running electronic structure computations in MMPol environment
  !! performed with Tinker/g16-mmp interface.
  !! It uses the ``nx_qm_t`` type described in ``mod_qm_t``.
  !! The module exports function necessary to setup, print, update
  !! the input for, run and read information from, the Tinker/QMMMPol
  !! job.  All the exported functions are meant to
  !! be used by the general handler ``mod_qm_general``.
  !!
  !! All functions and routines defined in this module should be
  !! prefixed with ``tg16mmp_``.
  !!
  !! LIMITATIONS:
  !! - It is completely experimental!
  !!
  use mod_kinds, only: dp
  use mod_logger, only: &
       & call_external, &
       & print_conf_ele
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_constants, only: &
       & MAX_CMD_SIZE, MAX_STR_SIZE, au2ang, au2kcalm
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_gaussian, only: gau_read_orb
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: tg16mmp_setup, tg16mmp_print
  public :: tg16mmp_update_input
  public :: tg16mmp_run
  public :: tg16mmp_read
  public :: tg16mmp_set_qmmm_atoms

  ! Interface for setting environment variables
  interface
     subroutine setenv(name, value, overwrite) bind(C, name='setenv')
       use iso_c_binding
       implicit none
       character(kind=C_char), intent(in) :: name(*)
       integer(C_int), intent(in) :: value
       integer(C_int), value :: overwrite
     end subroutine  setenv
  end interface

  integer, parameter :: MAX_CONTRIB = 6
  !! Maximum number of dominant contributions printed (DFT)

  ! Termination codes
  !! TODO integer, parameter :: TM_ERR_PERL = 201
  !! TODO integer, parameter :: TM_ERR_MAIN = 202

contains

  subroutine tg16mmp_setup(nx_qm, parser)
    !! Setup Tinker-Gaussian16-MMP.
    !!
    type(nx_qm_t), intent(inout) :: nx_qm
    !! ``nx_qm_t`` object to setup.
    type(parser_t), intent(in) :: parser

    call set(parser, 'tinker_g16mmp', nx_qm%tg16mmp_interface, 'interface_path')

  end subroutine tg16mmp_setup


  subroutine tg16mmp_print(nx_qm, out)
    !! Print information related to Tinker-g16-mmp execution.
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM object to print.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("TINKER_G16", env)
    call print_conf_ele(trim(env), 'TINKER_G16 path', unit=output)
    call get_environment_variable("g16root", env)
    call print_conf_ele(trim(env), 'g16root path', unit=output)
    call get_environment_variable("GAUSS_EXEDIR", env)
    call print_conf_ele(trim(env), 'GAUSS_EXEDIR path', unit=output)
    call print_conf_ele(nx_qm%tg16mmp_interface, 'G16 interface', unit=output)
  end subroutine tg16mmp_print


  subroutine tg16mmp_update_input(nx_qm, traj, conf)
    !! Update the tinker-g16-mmp input.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt

    character(len=MAX_CMD_SIZE) :: command
    character(len=MAX_STR_SIZE) :: env
    character(len=MAX_STR_SIZE) :: utils

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt

    call get_environment_variable("NXHOME", env)
    env = trim(env)//'/utils/'

    ! Clean up if it is the second run
    if (step > init_step) then
       utils = trim(env)//'/tg16mmp_utils.pl clean'
       write(command, '(A, I2, I10)') trim(utils)//' ', lvprt, step
       call execute_command_line(command)
    else
       utils = trim(env)//'/tg16mmp_utils.pl init_copy'
       write(command, '(3A)') &
            & trim(utils)//' ', trim(nx_qm%job_folder)//' ', &
            & trim('frame00000')//' '
        call execute_command_line(command)
    end if

    ! Update files (?)
    utils = trim(env)//'/tg16mmp_utils.pl update'
    write(command, '(3A, 3I4)') &
         & trim(utils)//' ', trim(nx_qm%job_folder)//' ', &
         & trim('frame00000')//' ', &
         & nstatdyn, nstat, 1
    call execute_command_line(command)

  end subroutine tg16mmp_update_input


  subroutine tg16mmp_run(nx_qm)
    !! Execute Tinker-g16-MMP.
    !!
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.

    character(len=MAX_CMD_SIZE) :: command
    integer :: ierr

    write(command, '(A)') trim(nx_qm%tg16mmp_interface)//&
    &' '//'tg16mmp.xyz'
    call call_external(command, ierr, outfile='tinker.out', errfile='stdout')
    !! TODO call check_error(...)
  end subroutine tg16mmp_run


  subroutine tg16mmp_read(nx_qm, traj, conf, qminfo, ierr)
    !! Read tinker-g16-mmp DFT outputs and report.
    !!
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo
    integer, intent(inout) :: ierr
    !! Status.

    integer :: step, init_step
    integer :: kt
    integer :: nat
    integer :: nstatdyn

    character(len=MAX_STR_SIZE) :: line, namefile
    integer :: i, u

    integer :: sec_id, tg16_natom_grd, tg16_nstates, tg16_istate

    !! TODO real(dp), dimension(:), allocatable :: osc
    !! TODO character(len=MAX_STR_SIZE), dimension(:, :), allocatable :: contrib

    !! TODO allocate(osc(size(repot)-1))
    !! TODO allocate(contrib(size(repot)-1, MAX_CONTRIB))
    !! TODO contrib(:, :) = 'NULL'

    kt = conf%kt
    nat = conf%nat
    step = traj%step
    nstatdyn = traj%nstatdyn
    init_step = conf%init_step

    ! First step: populate the orb object
    if (step == init_step) then
       namefile = 'gaussian'
       call gau_read_orb(nx_qm, namefile)
       call nx_qm%orb%print()
    end if

    ! rgrad(:, :, :) = 0.0_dp
    ! repot(:) = 0.0_dp

    open(newunit=u, file='interface.dat', status='old', action='read')

    sec_id = 1
    i = 0
    do
      read(u, '(a)', iostat=ierr) line
      if (ierr /= 0) exit
      if (sec_id == 1) then
        ! POTENTIAL section
        if( i < 1 ) then
          read(line(10:), '(i10)') tg16_nstates
        else
          read(line, '(f20.8)') qminfo%repot(i)
        end if
        if( i .eq. tg16_nstates) then
          sec_id = sec_id + 1
          i = 0
        else
          i = i + 1
        end if
      else if(sec_id == 2) then
        ! GRADIENT section
        if(i < 1) then
          read(line(9:), "(2i10)") tg16_istate, tg16_natom_grd
          ! TODO if tg16_natom_grd /= nat => ERROR
          ! TODO if tg16_istate /= nstatedyn => ERROR
        else
          read(line, '(3f20.8)') qminfo%rgrad(nstatdyn,:,i)
        end if

        if(i == tg16_natom_grd) then
          sec_id = sec_id + 1
        else
          i = i + 1
        end if
      else
        exit
      end if
    end do

    ! Convert potential in atomic units
    qminfo%repot = qminfo%repot / au2kcalm
    ! Convert gradients in atomic units
    qminfo%rgrad = qminfo%rgrad * au2ang / au2kcalm

  end subroutine tg16mmp_read

  subroutine tg16mmp_set_qmmm_atoms(nx_qm, is_qm_atom, nat)
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    integer, intent(in) :: nat
    logical, intent(out) :: is_qm_atom(nat)
    integer, allocatable :: qmat(:)
    integer :: i, j
    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: line, filename

    allocate( qmat(nat) )
    qmat = 0
    i = 0

    filename = trim(nx_qm%job_folder)//'/frame00000.key'
    open(newunit=u, file=filename, &
       & status='old', action='read')

    do
      read(u, '(a)', iostat=ierr) line
      if (ierr /= 0) exit
      line = adjustl(line)
      if(line(1:8) == 'qmatoms ') then
        read(line(8:), *, iostat=ierr) (qmat(j), j=i+1,nat)

        do while( qmat(i+1) > 0 )
          i = i + 1
        end do
      end if
    end do


    is_qm_atom = .false.
    do j=1, i
      is_qm_atom(qmat(j)) = .true.
    end do

    write(*, *) is_qm_atom

  end subroutine tg16mmp_set_qmmm_atoms

end module mod_tinker_g16_mmp
