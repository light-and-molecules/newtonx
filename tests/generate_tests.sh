#!/bin/bash

mkdir -p tests/

curdir=$(pwd)

for i in $NXHOME/examples/*
do
    name=$(echo $i | sed "s#$NXHOME/examples/##")
    if [ "$name" == "analysis" ]; then
	continue
    fi
    name=$(echo $name | tr '[:upper:]' '[:lower:]')
    echo "#!/bin/bash" > tests/test-${name}
    echo "" >> tests/test-${name}
    echo "export NXHOME=${NXHOME}" >> tests/test-${name}
    echo "export NX_TEST_ENV=1" >> tests/test-${name}
    echo "$curdir/nx_test -p ${name} --mdexe ${nxbin} --dir ${testdir} --make_env" >> tests/test-${name}
    chmod 755 tests/test-${name}
done
