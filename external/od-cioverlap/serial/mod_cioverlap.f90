
       module genvar
       implicit none
       integer,parameter::ounit=500 !NX will read final output from this
       integer,parameter::ounit2=501 !overall phases
       character*20,save::faoin,cioldf,cicurr_rawf,oldphases,tstep,nstat
       character*255,save::ao_fmt
       !general use
       integer::i,j,k,l,ii,jj,kk,ll,ip,jp
       integer::begg,endd,rate
       real(8)::time
       character*255::cjunk
       end module genvar

       module ovlps
       implicit none
       real(8),parameter::fs2au=41.3413745758d0

       !inputs
       integer,save::istep,ntot2,nact
       integer,save::ne,natoms,ntot,ncore,nroot,nov,nocc,nvirt
       real(8),save::dt,clshell
       integer,allocatable,save::low(:),up(:)!applicable for any root
       real(8),allocatable,save::ovlpao(:,:),geo12ao(:,:)
       real(8),allocatable,save::mocurr_raw(:,:),moold(:,:),mocurr(:,:)
       real(8),allocatable,save::cicurr_raw(:,:),ciold(:,:),cicurr(:,:),&
                                 cicoefcurr(:,:,:),cicoefold(:,:,:)

       !matching procedure for MOs
       logical,save::matchmos,matchci
       real(8),allocatable::omat(:,:)
       !matching procedure for CIcoefs
       integer::inov
       real(8),allocatable::onov(:,:),cinorm(:,:)
       real(8),allocatable,save::geo12mo_raw(:,:)
       logical,save::rootflip

       !ovlps
       real(8),allocatable::ovlpmo(:,:),tmpmo(:,:),tmpket(:,:)
       real(8),allocatable,save::geo12mo(:,:),ovlpci(:,:),onlyci(:,:)
       real(8),allocatable,save::dell(:,:),cdell(:,:)

       !other
       integer,allocatable,save::maporbtonov(:,:)
       real(8)::det
       integer::oold,ocurr,vold,vcurr
       !mkl stuff
       real(8),parameter::alpha=1.d0,beta=0.d0 !used for DGEMM
       integer,allocatable::ipiv(:)
       integer::info
       end module ovlps
