! Created: Oct 27 2015 
! Author: Jayashree Nagesh 
! Evaluates CI overlap matrix, given two CIS/TD wavefunctions,
! one at t-dt, and second at t for evaluating time-derivative couplings.
! In case of TD, we use the Casida ansatz.
!
! Input: NX generated files
! Output: 'CI overlap matrix' to be read in by run_cioverlap_g09.pl
!
! Notes:
! -- Uses O Matrix method to match MO and CI coefficients of two
! geometries.
! -- Need to use a nuclear time step of say, <= 0.2fs to get close to converged results,
! this is also important to avoid big orbital changes like orbital swaps
! or big CI coefficient changes. This manifests as a diagonally dominant
! O matrix for MO and CI coefficient overlaps, and is obtained as a
! diagonal matrix with +/-1 on the diagonal. The orbital swapping is a
! problem for det scheme as well; both OD and det scheme will be equally
! bad for large time steps. In fact, the det scheme accumulates small
! errors due to non-zero value of several determinant overlaps that are
! supposed to be zero. Apart from this, the OD and det scheme will agree with each other
! for all time steps.
! -- In cases where there is substantial offdiag MO ovlp leading to root
! flips, I switch to detscheme, because the odscheme will need the CIO
! matrix with values of all dets. (Mar 11 2016).
! -- No screening is needed. 
! -- For sake of saving time, I only evaluate <Phi_ia(t-dt)|Phi_ia(t)> for the 
! CI O matrix, assuming a small enough time step; this automatically
! assumes a diagonal matrix for now. If there are orbital swaps, a zero
! will appear on the MO O matrix diagonal and will issue a warning. The
! MO O matrix procedure will successfully swap orbitals to match with
! MOs of t-dt and t, but at the CI level, this is more complicated, since we
! need to evaluate the full NOV x NOV O matrix to properly swap the CI
! coefficients--this will be as costly as the full determinant method.
! To avoid this, a warning will be issued at the CI level, and the
! program will stop. Reduce the time step and check if the problem goes
! away.
! -- Uses same procedure as Pittner's in the last step of matching
! overall signs of overlaps. Thanks, Pittner for this piece of the code! 
! -- tested for CIS,TD: Oct 27 2015. 
! -- assumes closed shell for now : Oct 27 2015
! -- since CIS/TDDFT is not reliable for gs-es dynamics, the gs-es nacs are set
! to zero.
!
! Reference: J.Chem.Phys.Lett.,6,4200 (2015)
! 

        program cioverlap
        use genvar,only:ounit,&
        faoin,cioldf,cicurr_rawf,oldphases,tstep,nstat
        use ovlps,only:matchmos,matchci,rootflip
        implicit none

        faoin='cioverlap.input'
        cioldf='tddft.old'
        cicurr_rawf='tddft.current'
        oldphases='phases.old'
        tstep='tstep'
        nstat='nstates'

        open(unit=ounit,file='cioverlap.out',status='unknown')
        call timestamp(ounit)
        call gaussin_nx()

        matchmos=.true.; matchci=.true.
        rootflip=.false.
        write(ounit,*)'Matching MOs and computing MO ovlps'
        write(ounit,*)'Matchmos = ',matchmos
        call mosmatch()
        write(ounit,*)'Matching CIcoefs '
        write(ounit,*)'MatchCI = ',matchci
        if (rootflip.eqv..false.)then
        call cimatch()
        end if

        write(ounit,*)'Computing CI ovlps'
        call ciovlps()

        close(ounit)
        end program cioverlap
