#!/usr/bin/perl -w

# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use diagnostics;

use lib join('/', $ENV{"NXHOME"}, 'perllib');

use NX::Tinker_g16MMP qw( tg16mmp_init_copy tg16mmp_clean_dir
                      tg16mmp_update_input
                      );

my $action = $ARGV[0];

if ("$action" eq "clean") {
    my $debug = $ARGV[1];
    my $step = $ARGV[2];
    tg16mmp_clean_dir($debug, $step);
}

elsif ("$action" eq "init_copy") {
    my $job_folder = $ARGV[1];
    my $basename = $ARGV[2];
    tg16mmp_init_copy($job_folder, $basename);
}

elsif ("$action" eq "update") {
    my $job_folder = $ARGV[1];
    my $basename = $ARGV[2];
    my $nstatdyn = $ARGV[3];
    my $nstat = $ARGV[4];
    my $typedft = $ARGV[5];
    tg16mmp_update_input($job_folder, $basename, $nstatdyn, $nstat, $typedft);
}
