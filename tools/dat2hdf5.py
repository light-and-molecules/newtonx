#!/usr/bin/env python3

import h5py
import numpy as np
import os
import sys


def create_file(filename):
    with h5py.File(filename, 'a') as new_data:
        new_data.create_group('h5md/author')
        new_data.create_group('h5md/creator')

        new_data.create_group('particles/all/box')

    return 


def create_nad_dataset(h5_filename, dat_filename, n_atoms, n_steps, dim, n_coups):
    step_dat = np.empty(shape=(n_steps), dtype=np.int16)
    time_dat = np.empty(shape=(n_steps), dtype=np.float64)
    nad_dat = np.empty(shape=(n_steps, n_atoms, dim, n_coups), dtype=np.float64)

    with open(dat_filename, 'r') as nad_f:
        lines = nad_f.readlines()

        for step in range(n_steps):
            # read time and step
            time_line = step*(2+n_atoms*n_coups)
            line = lines[time_line].split()

            time_dat[step] = float(line[0])
            step_dat[step] = int(line[1])

            # read nad vectors
            for state in range(n_coups):
                for atom in range(n_atoms):
                    line_idx = 2 + time_line + state*n_atoms + atom
                    nad_val = [float(x) for x in lines[line_idx].split()]
                    nad_dat[step, atom, :, state] = nad_val[:]


    with h5py.File(h5_filename, 'a') as new_data:
        if 'particles/all/nad' in new_data:
            del new_data['particles/all/nad']

        new_data.create_dataset('particles/all/nad/step', data=step_dat, dtype=np.int16)
        new_data.create_dataset('particles/all/nad/time', data=time_dat, dtype=np.float64)
        new_data.create_dataset('particles/all/nad/value', data=nad_dat, dtype=np.float64)

    return


def create_gradient_dataset(h5_filename, dat_filename, n_atoms, n_steps, dim, n_states):
    step_dat = np.empty(shape=(n_steps), dtype=np.int16)
    time_dat = np.empty(shape=(n_steps), dtype=np.float64)
    grad_dat = np.empty(shape=(n_steps, n_atoms, dim, n_states), dtype=np.float64)

    with open(dat_filename, 'r') as grad_f:
        lines = grad_f.readlines()

        for step in range(n_steps):
            # read time and step
            time_line = step*(2+n_atoms*n_states)
            line = lines[time_line].split()

            time_dat[step] = float(line[0])
            step_dat[step] = int(line[1])

            # read nad vectors
            for state in range(n_states):
                for atom in range(n_atoms):
                    line_idx = 2 + time_line + state*n_atoms + atom
                    grad_val = [float(x) for x in lines[line_idx].split()]
                    grad_dat[step, atom, :, state] = grad_val[:]


    with h5py.File(h5_filename, 'a') as new_data:
        if 'particles/all/gradients' in new_data:
            del new_data['particles/all/gradients']

        new_data.create_dataset('particles/all/gradients/step', data=step_dat, dtype=np.int16)
        new_data.create_dataset('particles/all/gradients/time', data=time_dat, dtype=np.float64)
        new_data.create_dataset('particles/all/gradients/value', data=grad_dat, dtype=np.float64)

    return


def create_position_dataset(h5_filename, dat_filename, n_atoms, n_steps, dim):
    step_dat = np.empty(shape=(n_steps), dtype=np.int16)
    time_dat = np.empty(shape=(n_steps), dtype=np.float64)
    geom_dat = np.empty(shape=(n_steps, n_atoms, dim), dtype=np.float64)

    with open(dat_filename, 'r') as geom_f:
        lines = geom_f.readlines()

        for step in range(n_steps):
            # read time and step
            time_line = step*(2+n_atoms) + 1
            line = lines[time_line].split()

            time_dat[step] = float(line[0])
            step_dat[step] = int(line[1])

            # read geometries
            for atom in range(n_atoms):
                line_idx = 1 + time_line + atom
                geom_val = [float(x) for x in lines[line_idx].split()[1:]]
                geom_dat[step, atom, :] = geom_val[:]


    with h5py.File(h5_filename, 'a') as new_data:
        if 'particles/all/position' in new_data:
            del new_data['particles/all/position']

        new_data.create_dataset('particles/all/position/step', data=step_dat, dtype=np.int16)
        new_data.create_dataset('particles/all/position/time', data=time_dat, dtype=np.float64)
        new_data.create_dataset('particles/all/position/value', data=geom_dat, dtype=np.float64)

    return


def create_velocities_dataset(h5_filename, dat_filename, n_atoms, n_steps, dim):
    step_dat = np.empty(shape=(n_steps), dtype=np.int16)
    time_dat = np.empty(shape=(n_steps), dtype=np.float64)
    vel_dat = np.empty(shape=(n_steps, n_atoms, dim), dtype=np.float64)

    with open(dat_filename, 'r') as vel_f:
        lines = vel_f.readlines()

        for step in range(n_steps):
            # read time and step
            time_line = step*(2+n_atoms) + 1
            line = lines[time_line].split()

            time_dat[step] = float(line[0])
            step_dat[step] = int(line[1])

            # read geometries
            for atom in range(n_atoms):
                line_idx = 1 + time_line + atom
                vel_val = [float(x) for x in lines[line_idx].split()[:]]
                vel_dat[step, atom, :] = vel_val[:]


    with h5py.File(h5_filename, 'a') as new_data:
        if 'particles/all/velocities' in new_data:
            del new_data['particles/all/velocities']

        new_data.create_dataset('particles/all/velocities/step', data=step_dat, dtype=np.int16)
        new_data.create_dataset('particles/all/velocities/time', data=time_dat, dtype=np.float64)
        new_data.create_dataset('particles/all/velocities/value', data=vel_dat, dtype=np.float64)

    return


def create_energy_datasets(h5_filename, dat_filename):
    energy_dat = np.loadtxt(dat_filename)

    n_steps = len(energy_dat)
    n_states = len(energy_dat[0]) - 6

    with h5py.File(h5_filename, 'a') as new_data:
        if 'observables/kinetic_energy' in new_data:
            del new_data['observables/kinetic_energy']

        new_data.create_dataset('observables/kinetic_energy/step', 
                                data=energy_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/kinetic_energy/time', 
                                data=energy_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/kinetic_energy/value', 
                                data=energy_dat[:,4], 
                                dtype=np.float64)

        if 'observables/nstatdyn' in new_data:
            del new_data['observables/nstatdyn']

        new_data.create_dataset('observables/nstatdyn/step', 
                                data=energy_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/nstatdyn/time', 
                                data=energy_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/nstatdyn/value', 
                                data=energy_dat[:,2].astype(int), 
                                dtype=np.int16)
        
        if 'observables/potential_energy' in new_data:
            del new_data['observables/potential_energy']

        new_data.create_dataset('observables/potential_energy/step', 
                                data=energy_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/potential_energy/time', 
                                data=energy_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/potential_energy/value', 
                                data=energy_dat[:,5:-1], 
                                dtype=np.float64)

        if 'observables/total_energy' in new_data:
            del new_data['observables/total_energy']

        new_data.create_dataset('observables/total_energy/step', 
                                data=energy_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/total_energy/time', 
                                data=energy_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/total_energy/value', 
                                data=energy_dat[:,3], 
                                dtype=np.float64)

    return n_steps, n_states


def create_wf_dataset(h5_filename, dat_filename):
    wf_dat = np.loadtxt(dat_filename)

    n_steps = len(wf_dat)
    n_states = int((len(wf_dat[0]) - 2)/2)

    with h5py.File(h5_filename, 'a') as new_data:
        if 'observables/wavefunctions' in new_data:
            del new_data['observables/wavefunctions']

        new_data.create_dataset('observables/wavefunctions/step', 
                                data=wf_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/wavefunctions/time', 
                                data=wf_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/wavefunctions/value', 
                                data=wf_dat[:,2:].reshape((n_steps,n_states,2)), 
                                dtype=np.float64)
        
    return


def create_population_dataset(h5_filename, dat_filename):
    pop_dat = np.loadtxt(dat_filename)

    n_states = len(pop_dat[0]) - 2

    with h5py.File(h5_filename, 'a') as new_data:
        if 'observables/populations' in new_data:
            del new_data['observables/populations']

        new_data.create_dataset('observables/populations/step', 
                                data=pop_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/populations/time', 
                                data=pop_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/populations/value', 
                                data=pop_dat[:,2:], 
                                dtype=np.float64)

    return


def create_oscilator_str_dataset(h5_filename, dat_filename):
    osc_dat = np.loadtxt(dat_filename)

    with h5py.File(h5_filename, 'a') as new_data:
        if 'observables/oscillator_strengths' in new_data:
            del new_data['observables/oscillator_strengths']

        new_data.create_dataset('observables/oscillator_strengths/step', 
                                data=osc_dat[:,1].astype(int), 
                                dtype=np.int16)
        new_data.create_dataset('observables/oscillator_strengths/time', 
                                data=osc_dat[:,0], 
                                dtype=np.float64)
        new_data.create_dataset('observables/oscillator_strengths/value', 
                                data=osc_dat[:,2:], 
                                dtype=np.float64)

    return


def create_particles_dataset(h5_filename, dat_filename):
    atom_names = []
    species = []
    masses = []

    with open(dat_filename, 'r') as f:
        lines = f.readlines()

        for line in lines:
            line = line.split()

            if len(line) == 0:
                continue

            atom_names.append(line[0])
            species.append(line[1])
            masses.append(line[-1])


    atom_names = [a.encode("utf-8", "ignore") for a in atom_names]
    species = np.array(species, dtype=np.float64)
    masses = np.array(masses, dtype=np.float64)
    n_atoms = len(species)


    with h5py.File(h5_filename, 'a') as new_data:
        if 'particles/all/names' in new_data:
            del new_data['particles/all/names']

        new_data.create_dataset('particles/all/names', (len(atom_names),), dtype='S10', data=atom_names)

        if 'particles/all/species' in new_data:
            del new_data['particles/all/species']

        new_data.create_dataset('particles/all/species', data=species, dtype=np.float64)

        if 'particles/all/mass' in new_data:
            del new_data['particles/all/mass']

        new_data.create_dataset('particles/all/mass', data=masses, dtype=np.float64)

    return n_atoms


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print(
f"""
Please define the path to the trajectory folder and optionally the name of the hdf5 file generated:
{sys.argv[0]} /path/to/TRAJECTORIES/TRAJ1 [dyn.h5]
""")
        exit()

    traj_path = sys.argv[1] + '/'
    result_path = traj_path + 'RESULTS/'

    if len(sys.argv) == 3:
        h5_filename = sys.argv[2]
    else:
        h5_filename = f'{result_path}/dyn.h5'

    dim = 3


    if os.path.exists(h5_filename):
        print(f'file { h5_filename} already exists')
        exit()


    create_file(h5_filename)


    energies_file = result_path + 'energies.dat'
    n_steps, n_states = create_energy_datasets(h5_filename, energies_file)
    n_coups = int(n_states*(n_states - 1) / 2)


    geom_file = traj_path + 'geom.orig'
    n_atoms = create_particles_dataset(h5_filename, geom_file)


    nad_file = result_path + 'nad.dat'
    if os.path.exists(nad_file):
        create_nad_dataset(h5_filename, nad_file, n_atoms, n_steps, dim, n_coups)


    grad_file = result_path + 'gradients.dat'
    if os.path.exists(grad_file):
        create_gradient_dataset(h5_filename, grad_file, n_atoms, n_steps, dim, n_states)


    positions_file = result_path + 'geometries.xyz'
    if os.path.exists(positions_file):
        create_position_dataset(h5_filename, positions_file, n_atoms, n_steps, dim)


    velocities_file = result_path + 'velocities.dat'
    if os.path.exists(velocities_file):
        create_velocities_dataset(h5_filename, velocities_file, n_atoms, n_steps, dim)


    wf_file = result_path + 'wf.dat'
    if os.path.exists(wf_file):
        create_wf_dataset(h5_filename, wf_file)


    population_file = result_path + 'populations.dat'
    if os.path.exists(population_file):
        create_population_dataset(h5_filename, population_file)


    osc_str_file = result_path + 'osc_str.dat'
    if os.path.exists(osc_str_file):
        create_oscilator_str_dataset(h5_filename, osc_str_file)


