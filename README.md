## Description

[Newton-X](http://newtonx.org/) is a general-purpose program package for simulating the dynamics of
electronically excited molecules and molecular assemblies.

This repository contains everything needed to propagate a dynamics with the trajectory 
surface-hopping method, with energies and gradients available through the following models:

- Analytic models: spin-boson Hamiltonian, recoherence model, 1D models, 2D conical intersections
- Columbus 7: MRCI, MCSCF
- Gaussian 16: TD-DFT
- Orca 5: TD-DFT
- MOPAC: FOMO-CI
- Turbomole 7.3, 7.6: TD-DFT, ADC(2), CC2

## Installation

Newton-X can be built and installed using `meson` and `ninja`. To install in `$HOME/softs/newtonx`:

        export NXHOME="$HOME/softs/newtonx"
		cd /path/to/nx/source/
		export CC=gcc FC=gfortran
		meson setup -Dprefix=$NXHOME build
		meson compile -C build -j 4
		meson install -C build
		
More details are provided in file `INSTALL.md`.

## Contact

The preferred way to get help is through our mailling
list `newtonx [at] freelists.org`. You can subscribe to this list by
sending email to `newtonx-request [at] freelists.org` with 'subscribe' in the
Subject field OR by visiting the list page at <https://www.freelists.org/list/newtonx>.

For bug reports, please fill an issue here on Gitlab, or contact us through the mailling list.
