
# Table of Contents

1.  [Description](#orgc61f039)
2.  [Installation](#org7d2eff0)
    1.  [Pre-requisites](#orgb2cf877)
        1.  [Perl (at least version 5.18)](#org1dec0da)
        2.  [Compiler and libraries](#org12fccf4)
        3.  [External programs (time-derivatives)](#orgd1375ff)
    2.  [Building Newton-X](#orgbcb894e)
        1.  [Testing](#orgabe38ba)
    3.  [HDF5 integration](#org8b6a34c)
        1.  [Ubuntu](#org0edc3e3)
        2.  [Manual installation](#orgac80887)
    4.  [Documentation](#org8c97484)
3.  [Contact](#org79227a5)



<a id="orgc61f039"></a>

# Description

[Newton-X](http://newtonx.org/) is a general-purpose program package for simulating the dynamics of
electronically excited molecules and molecular assemblies.
It is a platform for performing all steps of the simulation, from the
generation of the initial conditions to the statistical analysis of the
results.


<a id="org7d2eff0"></a>

# Installation


<a id="orgb2cf877"></a>

## Pre-requisites


<a id="org1dec0da"></a>

### Perl (at least version 5.18)

-   Newton-X requires the following Perl libraries to work:
    -   `YAML::Tiny`
    -   `Math::VectorReal`
    -   `File::Path`
    -   `File::Copy::Recursive`
    -   `Getopt::Long`
    -   `Sys::Hostname`

-   A convenient way to install Perl libraries is to use  [`cpanminus`](https://metacpan.org/pod/App::cpanminus) (packaged in most Linux
    distribution). If you do not have `sudo` permission on your system, you can download the executable
    with:
    
        cd ~/bin
        curl -L https://cpanmin.us/ -o cpanm
        chmod +x cpanm
    
    To install `YAML::Tiny` for instance, you can execute the following command:
    
        cpanm YAML::Tiny
    
    The libraries will be installed under `~/perl5/lib/perl5/`. For Perl to find these libraries, add
    the following line to `.bashrc`:
    
        export PERL5LIB=$HOME/perl5/lib/perl5
    
    or to `.cshrc`:
    
        setenv PERL5LIB $HOME/perl5/lib/perl5


<a id="org12fccf4"></a>

### Compiler and libraries

-   Newton-X is built using `autotools` and `make`.
-   A reasonably recent version of `gfortran` is required to compile the code (at least `gfortan 7.5`).
-   Intel compilers **have not** been tested.
-   BLAS and Lapack are also needed.
-   **Optional**:  [HDF5 libraries](https://portal.hdfgroup.org/display/support/Downloads)


<a id="orgd1375ff"></a>

### External programs (time-derivatives)

-   The derivative couplings can be obtained through the state overlap matrix if they can't
    be derived from non-adiabatic coupling vectors (for instance in TD-DFT computations).
    If you obtained Newton-X in tarball version, these programs are included in the `cioverlap`
    folder.  If you cloned the `git` version, please make sure to extract them from the tarball
    somewhere on your computer !

-   Newton-X will look for these program in a directory aliased by the `CIOVERLAP` environment:
    
        export CIOVERLAP=/path/to/cioverlap/binaries/


<a id="orgbcb894e"></a>

## Building Newton-X

-   In this part we assume that Newton-X will be installed in `$HOME/softs/newtonx/`, with the
    source code in `/path/to/nx/source/`.

-   Newton-X relies on the `NXHOME` environment variable for running. `NXHOME` should be set
    to the path where it is installed, so in our example:
    
        export =NXHOME=$HOME/softs/newtonx/=

-   **If you obtained Newton-X through Gitlab**: you have to generate the required configuration
    files using the provided `autogen.sh` script. Please ensure that you have Autotools installed
    on your system:
    
        ./autogen.sh

-   We recommend to build Newton-X out-of-tree:
    
        cd /path/to/nx/source/
        mkdir build && cd build
        ../configure --prefix=$NXHOME
        make && make install

-   A complete list of options for the `configure` script is available with `../configure --help`.


<a id="orgabe38ba"></a>

### Testing

-   A testsuite is available and can be run after build with `make check`.

-   By default, only the tests for the analytical models will be done.  Others will simply be
    skipped (please refer to the manual for information about setting up other interfaces).

-   All tests will run under `tests/testdir` in the build directory.

-   The main logfile is `test-suite.log`, and the individual tests are logged in `tests/test-interface.pl.log`,
    where `interface` is replaced by the name of the interface.

-   In case of failure, you can inspect the content of the dynamics logfile `md.out` in each individual
    test folder.


<a id="org8b6a34c"></a>

## HDF5 integration

-   Newton-X can be built with support for HDF5 for producing its main output. You can enable this
    feature by setting `--with-hdf5` in the `configure` step.

-   HDF5 libraries and compiler wrapper (either `h5fc` or `h5pfc`) will be required. They will be
    searched under `$HDF5` and under `$PATH`.  Please note that Newton-X will not use the parallel
    versions of HDF5.

-   The libraries can be either installed via the package manager or manually compiled. If you choose
    to install them with the package manager, please make sure to use the same `gfortran` compiler to
    compile Newton-X after, as `mod` files in Fortran are compiler specific !


<a id="org0edc3e3"></a>

### Ubuntu

-   On Ubuntu you need find the `hdf5-tools` and `libhdf5-dev` packages:
    
        sudo apt-get install hdf5-tools libhdf5-dev
    
    The executables will then be found in `/usr/bin/`, so you can set your environement:
    
        export HDF5=/usr/


<a id="orgac80887"></a>

### Manual installation

-   NewtonX will not make use of the parallel version of HDF5. So you just need to compile the serial
    version (you can use the parallel version as well of course).
    
        export HDF5=/path/to/install/directory
        ./configure --enable-fortran --prefix=$HDF5
        make && make install


<a id="org8c97484"></a>

## Documentation

The documentation is generated by [FORD](https://github.com/Fortran-FOSS-Programmers/ford), which can be installed using `pip`, and requires at
least `python 3.7`:

    pip install ford

If you are in the build directory, building the documentation is done with:

    ford -o ../doc ../doc.md

You can then open `../doc/index.html` with a web browser.


<a id="org79227a5"></a>

# Contact

The preferred way to report bugs and get help is through our mailling
list <mailto:newtonx@freelists.org>. You can subscribe to this list by
sending email to <mailto:newtonx-request@freelists.org> with 'subscribe' in the
Subject field OR by visiting the list page at <https://www.freelists.org/list/newtonx>.

