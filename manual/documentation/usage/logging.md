title: Logging system

# Modules used

- `mod_print_utils`: routines for printing various elements.
- `mod_logger`: logging facility.

# Usage

The preferred way of logging information in Newton-X is through the use of a `nx_logger_t` type
object.  The main logger `nx_log` is automatically exported by the module `mod_logger` and is thus
available to all module that `mod_logger`.  This main logger is initialized in the `md` program.

It is possible to define new loggers in different modules, with the following syntax:

```fortran
use mod_logger, only: nx_logger_t

type(nx_logger_t) :: my_logger

integer :: kt, step, lvprt
logical :: tstamp ! Add time stamp to logged information

kt = 1
step = 1
lvprt = 3 ! (LOG_INFO level)
tstamp = .true.

my_logger%init( kt, step, lvprt, logfile='my_log.log', errfile='my_err.err', tstamp=tstamp)
```

Information will then be logged in `my_log.log`, and error messages printed to `my_err.err`. If only
`logfile` is specified, it will also be used for `errfile`. 

The logger can log:

- simple messages (/i.e/ character strings) ;
- arrays of character strings ;
- content of a file ;
- vectors and matrices (`integer`, `real64` and `complex(real64)`) ;
- blank lines.

The specific routines are implemented in `mod_print_utils`, and are available through
`my_logger%log()` call (or `my_logger%log_blank()` and `my_logger%log_file()`).  In general logging
will be done with the following calls.

```fortran
my_logger%log(LOG_INFO, 'My info message')
my_logger%log(LOG_DEBUG, matrix, title='Debugging matrix')
my_logger%log(LOG_ERROR, 'Error message')
```

This should result in the following output:

```
[INFO]<Current time 1>: My info message
[DEBUG]<Current time 2>: Debugging matrix
matrix(1, 1) matrix(1, 2) ...
matrix(2, 1) matrix(2, 2) ...
...
[ERROR]<Current time 3>: Error message
```

For the implementation details of each logging function, please refer to the documentation of the
module `mod_print_utils`.

# In code examples

- The module `mod_sh` defines its own logger `sh_log` for internal use, and prints information to
  `sh.out`. 



