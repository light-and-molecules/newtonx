title: Implementation of new interfaces (new version)

The interaction between electronic structure computation, either through the usage of
external QM software, or *via* analytical models, is handled by the modules implemented in
the directory `src/qm_interfaces/`.  In particular, each individual interface should have
its own module `mod_interface_t`, defining a type `nx_interface_t` which in turn extends
the general abstract type `nx_qm_generic_t` defined in `mod_qm_generic_t`.  To make it easier
to write new interfaces, the file `mod_template_t.f90` is provided.  This file contains
the minimal component required to setup a working interface.

## General process

Let's say we want to write a new interface for the AwesomeQM software.  The process should
go as follows:

- Copy `mod_template_t.f90` as `mod_awesomeqm_t.f90` ;
- Remove the documentation lines at the beginning of the file ;
- Replace all occurences of `_template_` in the file by `_awesomeqm_` ;
- Add the file name `mod_awesomeqm_t.f90` to the `srcs_common` list in file `src/qm_interfaces/meson.build` ;
- Try to compile the as explained in `INSTALL.md`. Normally the code should compile properly. If
  not, ensure that you followed the steps above carefully, and look at the compiler
  messages.
- Now you can start filling the missing pieces in the interface, by looking at other
  implementation and reading the documentation of the module `mod_qm_generic_t.f90`. 
  
For actually using the new interface in Newton-X, you then need to add a block to the
function `nx_qm_create_interface` in the module `mod_qm_interfaces.f90`, with options
relevant for your interface.
  
## Components

The QM "engine" of Newton-X is composed of two layers: 

- the individual interfaces deriving from `nx_qm_generic_t` ;
- a wrapper class `nx_qm_item_t`, that contains an object of class `nx_qm_generic_t` as a
  member. 

This wrapper object is the world-facing object, /i.e/ the only one that is exposed to the
rest of the Newton-X program.  It has routines wrapping the ones from `nx_qm_generic_t`
for handling all parts of the QM jobs, from set-up to output processing, with the added
capacity of handling the directory structure.

Information from the QM computation (energies, geometries, ...) are returned as component
of a `nx_qminfo_t` object.




  


