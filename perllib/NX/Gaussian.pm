# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Gaussian;

use strict;
use warnings;
use diagnostics;

use File::Copy;

use NX::Configuration;

use Data::Dumper;

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( gau_parse_input gau_update_input gau_prepare_fake_double
                     gau_prepare_cioverlap );
}


sub gau_parse_input {
    # Parse the given Gaussian input file.
    #
    # The function returns several arrays:
    # - @link0 : list of link0 commands ;
    # - @route : list of all space-separated commands in the route section ;
    # - @title : title of the jobs
    # - @charge_mult : charge and multiplicity ;
    # - @geom : geometry ;
    # - @other : all other sections contained in the original input ;
    #
    # The gaussian input has the following structure:
    #
    # %chkfile=...
    # %...
    # %any number of lines starting with '%'
    # #Any number of lines
    # #Starting with '#'
    #
    # Title
    #
    # Charge Multiplicity
    # At1 x y z
    # At2 x y z
    # ...
    #
    # Additional sections

    my ($init_input) = @_;
    open(my $fh, '<:encoding(UTF-8)', "$init_input")
        or die "Couldn't open $init_input ($!), stopped at";

    my (@link0, @route, @title, @charge_mult, @geom, @other);

    # Count the number of blank lines.
    my $blank = 0;

    while (<$fh>) {
        chomp;
        s/^\s*//;
        s/\s*$//;

        if (/^%/) {
            # Link0 commands
            push @link0, "$_";
        }

        if (/^#/) {
            # Route section
            s/^#[p]*//;
            my @tmp = split /\s+/, $_;
            foreach my $ele (@tmp) {
                push @route, "$ele";
            }
        }

        if ($_ eq "") {
            $blank++;
        }

        if ($blank == 1) {
            $_ = <$fh>;
            chomp;
            s/^\s*//;
            s/\s*$//;
            push @title, "$_";
        }

        if (($blank == 2) and (m/^\s*[0-9]/)) {
            chomp;
            s/^\s*//;
            s/\s*$//;
            my @tmp = split /\s+/, $_;
            push @charge_mult, ($tmp[0], $tmp[1]);
        }

        if (($blank == 2) and (m/^\s*[a-z]/i)) {
            push @geom, "$_";
        }

        if ($blank == 3) {
            push @other, "$_";
        }
    }
    close($fh);

    return \@link0, \@route, \@title, \@charge_mult, \@geom, \@other;
}


sub gau_generate_route {
    # Generate the route section for a Gaussian job.
    #
    # The route is read from the original input file, and some information
    # based on the general NX configuration is added.  The "nosymm" keyword is
    # added if it is not present in the original input.  The "force" keyword
    # is not added here, as we may need to add it only to the second job
    # ("link1") depending on "typedft".
    my ($init_route, $mocoef, $td_st, $ld_thr, $step, $nstat, $nstatdyn, $init_step) = @_;

    my $nstat_gau = $nstat - 1;
    my $nstatdyn_gau = $nstatdyn - 1;

    my @route = @{$init_route};
    my @route_tmp = ();

    # Initial guess generation
    if (($mocoef == 1) and ($step != $init_step)) {
        push @route, "Guess=Read";
    }
    if ($mocoef == 2) {
        push @route, "Guess=Read";
    }

    # Read excited states from previous step ?
    my $method_input = '';
    foreach my $ele (@route) {
        if (($ele =~ m/^td/i) or ($ele =~ m/^cis/i)) {
            $method_input = $ele;
	    $method_input =~ s/root\=[0-9]*/root=$nstatdyn_gau/i;
	    $method_input =~ s/nstates\=[0-9]*/nstates=$nstat_gau/i;
        } else {
            push @route_tmp, $ele;
        }
    }
    if ($mocoef != 0) {
        if ($td_st != 0) {
            $method_input =~ s/\(/(Read,/;
        }
    }
    # At this point, maybe we did not find the td section because it does
    # not exist in the original file. So, let's add it if required !
    if ($nstat > 1) {
	if ($method_input eq '') {
	    $method_input = "td(root=$nstatdyn_gau,nstates=$nstat_gau)";
	}
    }

    push @route_tmp, $method_input;
    @route = @route_tmp;

    # Linear dependency threshold
    push @route, "IOP(3/59=$ld_thr)";

    my $found = 0;
    foreach my $ele (@route) {
        if ($ele =~ m/nosymm/i) {
            $found = 1;
        }
    }
    if ($found == 0) {
        push @route, "nosymm";
    }

    return @route;
}


sub gau_update_input {
    # Update the Gaussian input.
    #
    # The function reads the initial input file (in JOB_AD for example) and
    # produces Gaussian input with the information provided as arguments.  If
    # required, it also copies reference checkpoint (gaussian.chk.ini) file to
    # the current directory.
    #
    # First, we parse the original input.  Then the route section is updated
    # wi mocoef, ld_thr, td_st, nstatdyn and nstat.  Then the link, route,
    # title and charge and mult sections are concatenated, with the content of
    # a geometry file (named 'geom').  All remaining sections are appended
    # after.  With type_dft = 2 (dynamics on ground state, with multiple
    # states included), a second job is included for computing the gradient.
    my ($folder, $nstatdyn, $step, $type_dft) = @_;

    my $NXHOME=$ENV{NXHOME};
    my $default_config_file = "$NXHOME"."/data/default.yml";
    if (! -f $default_config_file) {
	$default_config_file = "$NXHOME"."/share/newtonx/data/default.yml";
    }
    my $main_config = NX::Configuration->new($default_config_file);
    $main_config->import_nml('configuration.inp');

    my $mocoef = $main_config->value('gaussian', 'mocoef');
    my $td_st = $main_config->value('gaussian', 'td_st');
    my $ld_thr = $main_config->value('gaussian', 'ld_thr');
    my $nstat = $main_config->value('nxconfig', 'nstat');
    my $init_step = $main_config->value('nxconfig', 'init_step');

    my $init_input = $folder.'/gaussian.com';
    my @parsed = gau_parse_input($init_input);
    my @link = @{$parsed[0]};
    my @route = @{$parsed[1]};
    my @title = @{$parsed[2]};
    my @charge_mult = @{$parsed[3]};
    my $charge = $charge_mult[0];
    my $mult = $charge_mult[1];
    my @other = @{$parsed[5]};

    my @new = gau_generate_route(\@route, $mocoef, $td_st, $ld_thr, $step, $nstat, $nstatdyn, $init_step);
    @route = @new;

    # Copy 'basis' files, for using overlap computation later.
    if ($step == $init_step) {
        if (-e "$folder/basis") {
            copy("$folder/basis", "./basis")
                or die "Couldn't copy $folder/basis ($!), stopped at";
        }
        if (-e "$folder/basis2") {
            copy("$folder/basis2", "./basis2")
                or die "Couldn't copy $folder/basis2 ($!), stopped at";
        }
    }

    if ($step == $init_step) {
        if ($mocoef != 0) {
            if (-e "$folder/gaussian.chk.ini") {
                copy("$folder/gaussian.chk.ini", "./gaussian.chk");
            }
        }
    }

    if ($mocoef == 2) {
        copy("$folder/gaussian.chk.ini", "./gaussian.chk")
            or die "Couldn't copy chk file $folder/gaussian.chk.ini ($!), stopped at";
    }

    my @geom = ();
    # 'geom' is intended to be printed as a simple xyz file !!
    open(my $gg, "<:encoding(UTF-8)", 'geom')
        or die "Couldn't open 'geom' ($!), stopped at";
    while (<$gg>) {
        s/^\s+//; s/\s*$//;
        push @geom, "$_";
    }
    close($gg);

    if (! grep /%chk/, @link) {
        push @link, "%chk=gaussian.chk";
    }
    if (! grep /%rwf/, @link) {
        push @link, "%chk=gaussian.rwf";
    }


    open(my $fh, ">:encoding(UTF-8)", "gaussian.com")
        or die "Couldn't open 'gaussian.com' ($!), stopped at";
    foreach my $ele (@link) {
        print $fh "$ele\n";
    }
    print $fh "#p";
    foreach my $ele (@route) {
        print $fh " $ele ";
    }
    if ($type_dft != 2) {
        print $fh " force ";
    }
    print $fh "\n\n";
    foreach my $ele (@title) {
        print $fh "$ele\n";
    }
    print $fh "\n";
    print $fh "$charge $mult\n";

    foreach my $ele (@geom) {
        print $fh "$ele\n";
    }
    # print $fh "\n";

    foreach my $ele (@other) {
        print $fh "$ele\n";
    }
    print $fh "\n";

    if ($type_dft == 2) {
        print $fh "--link1--\n";
        foreach my $ele (@link) {
            print $fh "$ele\n";
        }
        print $fh "#p ";
        foreach my $ele (@route) {
            next if (($ele =~ m/td/i) or ($ele =~ m/cis/i));
            print $fh "$ele ";
        }
        print $fh "force Geom=AllCheck\n\n";

        foreach my $ele (@other) {
            print $fh "$ele\n";
        }
    }

    close($fh);
}


sub gau_prepare_fake_double {
    # Prepare the input for running the overlap computation.

    mkdir("double_molecule_input")
        or die "Couldn't create directory 'double_molecule_run' ($!), stopped";

    my $new_inp = "./double_molecule_input/sgaussian.com";

    my @parsed = gau_parse_input("gaussian.com");
    my @link = @{$parsed[0]};

    # Memory
    my $mem = '200mw';
    foreach my $ele (@link) {
        if ($ele =~ m/mem/i) {
            $mem = $ele ;
        }
    }

    # Basis set
    my $basis = '';
    my $route = "#p nosymm Geom=NoTest IOp(3/33=1)";
    if (-e "basis") {
        open(my $bas, '<:encoding(UTF-8)', "basis")
            or die "Couldn't open basis ($!), stopped at";
        $basis = <$bas>;
        chomp $basis;
        $route = "$route"." $basis";
	close($bas);
    }
    elsif (-e "basis2") {
        $route = "$route"." GEN";
        copy("basis2", "double_molecule_input/basis2")
            or die "Couldn't copy basis2 ($!), stopped at";
    }


    open(my $fh, ">:encoding(UTF-8)", "$new_inp")
        or die "Couldn't open $new_inp ($!), stopped at";
    print $fh "%kjob l302\n";
    print $fh "%rwf=sgaussian\n";
    print $fh "%chk=sgaussian\n";
    print $fh "$route\n\n";
    print $fh "Overlap run\n\n";
    print $fh "0 1\n";
    close($fh);
}


sub gau_prepare_cioverlap {
    my $folder = "cioverlap";
    my $old_folder = $folder.".old";

    # Files required to run cioverlap
    my @files_to_copy = (
        ['cioverlap.input', ''],
        ['transmomin', ''],
        ['slatergen/slaterfile', 'slaterfile'],
        ['cioverlap.old/eivectors1', 'eivectors2'],
       );

    # The folder should already exist, as this script will
    # ALWAYS be called after run_cis_casida.pl
    # mkdir("$folder");

    foreach my $ff (@files_to_copy) {
        if (! "@{$ff}[1]" eq '') {
            copy("@{$ff}[0]", "$folder/@{$ff}[1]");
        } else {
            copy("@{$ff}[0]", "$folder/@{$ff}[0]");
        }
    }

    # If phases are found, copy them
    if (-e 'cioverlap.old/phases') {
        copy('cioverlap.old/phases', "$folder/phases.old")
            or die "Couldn't copy 'cioverlap.old/phases' ($!), stopped";
    }

    # Rename the outcome from cis_casida
    # move("$folder/casidawf", "$folder/eivectors1")
    #     or die "Couldn't rename 'casidawf' ($!), stopped";

    # Now do some cleanup
    my @files_to_remove = (
        'cis_casida.input',
        'cioverlap.input',
        'cis_slatergen.input',
        'merged_coord',
       );

    foreach my $ff (@files_to_remove) {
        if (-e "$ff") {
            unlink "$ff";
        }
    }
}

return 1;
