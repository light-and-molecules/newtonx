# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tools;

use strict;
use warnings;
use diagnostics;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT_OK      = qw( units prog_name pad_str prog_hash_from_name );
}

sub units {
    my ($key) = @_;
    my $value = 0;
    if ($key eq "BK"      ) {
      $value = 0.3166813639E-5;
    }				# Boltzmann constant Hartree/kelvin
    elsif ($key eq "bk_ev"   ) {
      $value = 8.617343E-5;
    }				# Boltzmann constant eV/kelvin
    elsif ($key eq "proton"  ) {
      $value = 1822.888515;
    }				# (a.m.u.)/(electron mass)
    elsif ($key eq "timeunit") {
      $value = 24.188843265E-3;
    }				# femtoseconds
    elsif ($key eq "au2ev"   ) {
      $value = 27.21138386;
    }				# au to eV
    elsif ($key eq "pi"      ) {
      $value = 3.141592653589793;
    }				# pi
    elsif ($key eq "au2ang"  ) {
      $value = 0.52917720859;
    }				# au to angstrom
    elsif ($key eq "deg2rad" ) {
      $value = 1.745329252E-2;
    }				# degree to radian (pi/180)
    elsif ($key eq "au2cm"   ) {
      $value = 219474.625;
    }				# hartree to cm-1
    elsif ($key eq "cm2au"   ) {
      $value = 4.55633539E-6;
    }				# cm-1 to hartree
    elsif ($key eq "h_planck") {
      $value = 1.5198298508E-16;
    }				# h hartree.s
    elsif ($key eq "h_evs"   ) {
      $value = 4.13566733E-15;
    }				# h eV.s
    elsif ($key eq "light"   ) {
      $value = 299792458;
    }				# speed of light m/s
    elsif ($key eq "lightau" ) {
      $value = 137.035999139;
    }				# speed of light au
    elsif ($key eq "e_charge") {
      $value = -1.602176487E-19;
    }				# electron charge C
    elsif ($key eq "e_mass"  ) {
      $value = 9.10938215E-31;
    }				# electron mass kg
    elsif ($key eq "eps0"    ) {
      $value = 8.854187817e-12;
    }			       # vacuum permitivity C^2*s^2*kg^-1*m^-3
    else {
      die " Constant $key was not found in colib_perl.pm";
    }
    #
    return $value;
  }


sub prog_name {
  my ($prog) = @_;

  my $tostr = sprintf("%.1f", $prog);

  my @tmp = split /./, $tostr;
  my $prog_val = $tmp[0];
  my $method_val = $tmp[1];

  my $pname = '';
  my $mname = '';

  my %prog_hash = (
      '0.1' => ['analytical', 'sbh'], 
      '0.2' => ['analytical', 'recohmodel'],
      '0.3' => ['analytical', 'onedim_model'],
      '1.1' => ['columbus', 'mcscf'],
      '1.2' => ['columbus', 'mrci'],
      '2.1' => ['turbomole', 'tddft'],
      '2.2' => ['turbomole', 'ricc2'],
      '2.3' => ['turbomole', 'adc2'],
      '3.1' => ['gaussian', 'tddft'],
      '10.1' => ['mopac', 'fomo-ci'],
      '11.1' => ['exc_mopac', 'mopac-fomo-ci'],
      '11.2' => ['exc_gaussian', 'gaussian-tddft'],
      '12.1' =>  ['tinker_g16mmp', 'g16mmpol'],
      '12.2' =>  ['tinker_mndo', 'mndo'],
      );

  $pname = @{$prog_hash{$prog}}[0];
  $mname = @{$prog_hash{$prog}}[1];
  return ($pname, $mname);
}


sub prog_hash_from_name {
    my ($progname, $methodname) = @_;

    my ($major, $minor);
    if ($progname eq 'analytical') {
	$major = '0';
	if ($methodname eq 'sbh') { $minor = '1';}
	elsif ($methodname eq 'recohmodel') {$minor = '2';}
	elsif ($methodname eq 'onedim_model') {$minor = '3';}
    }
    elsif ($progname eq 'columbus') {
	$major = '1';
	if ($methodname eq 'mcscf') { $minor = '1';}
	elsif ($methodname eq 'mrci') {$minor = '2';}
    }
    elsif ($progname eq 'turbomole') {
	$major = '2';
	if ($methodname eq 'tddft') { $minor = '1';}
	elsif ($methodname eq 'ricc2') {$minor = '2';}
	elsif ($methodname eq 'adc2') {$minor = '3';}
    }
    elsif ($progname eq 'gaussian') {
	$major = '3';
	if ($methodname eq 'tddft') { $minor = '1';}
    }
    elsif ($progname eq 'mopac') {
	$major = '10';
	if ($methodname eq 'fomo-ci') { $minor = '1';}
    }
    elsif ($progname eq 'exc_mopac') {
	$major = '11';
	if ($methodname eq 'mopac-fomo-ci') { $minor = '1';}
    }
    elsif ($progname eq 'exc_gaussian') {
	$major = '11';
	if ($methodname eq 'gaussian-tddft') { $minor = '2';}
    }
    elsif ($progname eq 'tinker_g16mmp') {
	$major = '12';
	if ($methodname eq 'g16mmpol') { $minor = '1';}
    }
    elsif ($progname eq 'tinker_mndo') {
	$major = '12';
	if ($methodname eq 'mndo') { $minor = '2';}
    }

    return "$major"."."."$minor";

}


sub pad_str {
    my ($str, $total_length) = @_;

    my $len = length($str);

    my $pad = int( $total_length - $len ) / 2;
    my $res = " " x $pad.$str." " x $pad;

    return $res;
}
  
  # if (($prog >= 0.00) and ($prog < 0.95)) {
  #   $value = "analytical";
  # }
  # if (($prog >= 0.95) and ($prog < 1.95)) {
  #   $value = "columbus";
  # }
  # if (($prog >= 1.95) and ($prog < 2.95)) {
  #   $value = "turbomole";
  # }
  # if (($prog >= 2.95) and ($prog < 3.95)) {
  #   $value = "aces2";
  # }
  # if (($prog >= 3.95) and ($prog < 4.45)) {
  #   $value = "mopac";
  # }
  # if (($prog >= 4.45) and ($prog < 4.95)) {
  #   $value = "exc_mopac";
  # }
  # if (($prog >= 4.95) and ($prog < 5.45)) {
  #   $value = "exc_gaussian";
  # }
  # if (($prog >= 5.45) and ($prog < 5.95)) {
  #   $value = "unasigned";
  # }
  # if (($prog >= 5.95) and ($prog < 6.45)) {
  #   $value = "gaussian";
  # }
  # if (($prog >= 6.45) and ($prog < 6.95)) {
  #   $value = "g09";
  # }
  # if (($prog >= 6.95) and ($prog < 7.95)) {
  #   $value = "tinker";
  # }
  # if (($prog >= 7.95) and ($prog < 8.15)) {
  #   $value = "dftb_legacy";
  # }
  # if (($prog >= 8.15) and ($prog < 8.25)) {
  #   $value = "dftb+_legacy";
  # }
  # if (($prog >= 8.45) and ($prog < 8.95)) {
  #   $value = "dftb+";
  # }
  # if (($prog >= 8.95) and ($prog < 9.95)) {
  #   $value = "dftci";
  # }
  # if (($prog >= 9.95) and ($prog < 10.95)) {
  #   $value = "gamess";
  # }
  # if (($prog >= 10.95) and ($prog < 11.05)) {
  #   $value = "misc";
  # }
  # if (($prog >= 11.95) and ($prog < 12.95)) {
  #   $value = "bagel";
  # }
  # if (($prog >= 19.95) and ($prog < 20.95)) {
  #   $value = "hybrid";
  # }
  # if (($prog >= 29.95) and ($prog < 30.95)) {
  #   $value = "tinker_g16mmp"
  # }
  # if (($prog >= 30.95) and ($prog < 31.95)) {
  #   $value = "tinker_mndo"
  # }
  # # New programs must be defined here
  # if ($value eq "") {
  #   return "WARNING: Could not determine 'thres', PLEASE CHECK THE VALUE OF 'prog' !!"

# sub method_name {
#   my ($prog) = @_;
#   my $value = "";
# 
#   my $pname = prog_name($prog);
#   my @tmp = split /./, $prog;
#   my $method_val = $tmp[1];
# 
#   if ($pname eq "analytical") {
#       if ($method_val eq '1') {
# 	  return 'sbh';
#       }
#       elsif ($method_val eq '2') {
# 	  return 'recohmodel';
#       }
#       elsif ($method_val eq '3') {
# 	  return 'onedim_model';
#       }
#   }
#   elsif ($pname eq 'columbus') {
#   }
#   elsif ($pname eq 'turbomole') {
#   }
#   elsif ($pname eq 'mopac') {
#   }
#   elsif ($pname eq 'exciton') {
#   }
# 
#   # Analytical models
#   if (($prog >= 0.05) and ($prog < 0.15)){
#     $value = "sbh";
#   }
#   if (($prog >= 0.15) and ($prog < 0.25)){
#       $value = "recohmodel";
#   }
#   if (($prog >= 0.25) and ($prog < 0.35)){
#       $value = "onedim_model";
#   }
# 
#   # Columbus
#   if (($prog >= 0.95) and ($prog < 1.05)) {
#       $value = "mcscf";
#   }
#   if (($prog >= 1.05) and ($prog < 1.15)) {
#       $value = "mrci";
#   }
# 
#   # Turbomole
#   if (($prog >= 1.95) and ($prog < 2.05)){
#     $value = "ricc2";
#   }
#   if (($prog >= 2.05) and ($prog < 2.15)){
#     $value = "tddft";
#   }
#   if (($prog >= 2.15) and ($prog < 2.25)) {
#     $value = "adc2";
#   }
# 
#   # Exciton model
#   if (($prog >= 3.45) and ($prog < 4.95)) {
#     $value = "exc_mopac";
#   }
#   if (($prog >= 4.95) and ($prog < 5.45)) {
#     $value = "exc_gaussian";
#   }
# 
#   return $value;
# }


1;
