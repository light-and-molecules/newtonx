# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Exc_mopac;

use strict;
use warnings;
use diagnostics;

use Cwd;
# use Async;
use List::Util qw( reduce );
use POSIX qw( floor ); 
use File::Path qw( make_path );
use File::Copy::Recursive qw( rcopy );
use File::Copy qw( copy );
use File::Copy qw( move );

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( calc_excmop run_mopac run_nx2tnk run_exash );
}

#............................................................................
sub calc_excmop {
   my ($nchrom, $nproc, $step) = @_;
   my $etnk = $ENV{"TINKER"};

   #............................................................................
   #if (! -d "cioverlap/") {
   #  make_path("cioverlap/")
   #    or die "Couldn't create cioverlap/ ($!), stopped";
   #}
   # system("mkdir -p cioverlap/");
   if ($step eq 0) { 
      opendir my $dh, "JOB_NAD"
         or die "Couldn't open JOB_NAD/ ($!), stopped";
      my @files = grep {!/^\.\.?$/} readdir $dh;
      foreach my $ff (@files) {
        copy("JOB_NAD/$ff", "$ff")
           or die "Couldn't copy JOB_NAD/$ff ($!), stopped";
      }
      close($dh);
   }
   
   move("fullMM_tnk.xyz_new", "fullMM_tnk.xyz");
   #............................................................................
   
   # => Calculates the "full-MM" energies and gradients.........................
   system("$etnk/testgrad.x fullMM_tnk.xyz -k fullMM_tnk.key Y N N > fullMM_tnk.out 2>&1 &");
   #............................................................................
   
   # => Run the mopac calculations..............................................
   system("rm -rf *_nx.epot");
   system("rm -rf *_nx.grad");
   system("rm -rf *_nx.grad");
   system("rm -rf *_nx.grad.all");
   system("rm -rf *_nx.grad.run_cioverlap.log");

   my $filename = "exc_mop";
   my (@mopac_jobs);
   
   for (my $j = 1; $j <= $nchrom ; $j++){
      push @mopac_jobs , "$filename"."$j";
   }

   my $nBatches = int($nchrom / $nproc);
   my $remainder = $nchrom % $nproc;

   for (my $nn = 0; $nn < $nBatches; $nn++) {
       my %jobs_status = ();
       my @commands = ();
       
       # Each batch has $nproc jobs running
       for (my $i = 0; $i < $nproc; $i++) {
           my $index = $i  + ($nn * $nproc);
           push @commands, sub {run_mopac($mopac_jobs[$index])};
       }
       # print STDOUT "Starting MOPAC job #$nn \n";
       # my $emop = $ENV{MOPAC};
       # system("ls -l > in_perl_before_$nn");
       # system("$emop/mopac2002.x $mopac_jobs[$nn] > mopac_tmp.out ");
       # print STDOUT "Finished MOPAC job #$nn \n";
       # system("ls -l > in_perl_after_$nn");

       # Now we loop over @commands to create one fork per job to execute
       my $i = 1;
       foreach my $cmd (@commands) {
           my $index = $i  + ($nn * $nproc);
           $i++;
           
           my $pid = fork();
           die "Fork failed for $mopac_jobs[$index]" unless defined $pid;

           if ($pid == 0) { # Child process
               eval {
        	   $cmd->();
               };
               $@ and die "Failure in cmd: $@\n"; # If eval has failed
           }

           # Now this is the parent process
           if ($pid <= 0) {
               warn "Job $cmd failed (pid: $pid): $!";
           } else {
               $jobs_status{$pid} = {
        	   cmd => $cmd,
        	   status => 'running'
               };
           }
       }

       # At this point all jobs have been launched.
       while(%jobs_status) {
           my $pid = wait();
           last if ($pid == -1); # No more children to wait for
           next unless exists $jobs_status{$pid}; # We only care about the job we are watching

           my $job = $jobs_status{$pid};
           print STDOUT "$pid ($job->{cmd}) finished with code $?\n";
           delete $jobs_status{$pid};
       }
   }

   if ($remainder != 0) {
       my %jobs_status = ();
       my @commands = ();
       
       # Each batch has $nproc jobs running
       for (my $i = 1; $i <= $remainder; $i++) {
	   my $index = $i  + ($nBatches * $nproc);
	   push @commands, sub {run_mopac($mopac_jobs[$index])};
       }

       # Now we loop over @commands to create one fork per job to execute
       my $i = 1;
       foreach my $cmd (@commands) {
	   my $index = $i  + ($nBatches * $nproc);
	   $i++;
	   
	   my $pid = fork();
	   die "Fork failed for $mopac_jobs[$index]" unless defined $pid;

	   if ($pid == 0) { # Child process
	       eval {
		   $cmd->();
	       };
	       $@ and die "Failure in cmd: $@\n"; # If eval has failed
	   }

	   # Now this is the parent process
	   if ($pid <= 0) {
	       warn "Job $cmd failed (pid: $pid): $!";
	   } else {
	       $jobs_status{$pid} = {
		   cmd => $cmd,
		   status => 'running'
	       };
	   }
       }

       # At this point all jobs have been launched.
       while(%jobs_status) {
	   my $pid = wait();
	   last if ($pid == -1); # No more children to wait for
	   next unless exists $jobs_status{$pid}; # We only care about the job we are watching

	   my $job = $jobs_status{$pid};
	   print STDOUT "$pid ($job->{cmd}) finished with code $?\n";
	   delete $jobs_status{$pid};
       }
   }
   
   # my @status = ();
   # my $limit = 3600;
   
   #Loop over nproc
   # my $ire = $nchrom/$nproc;
   # my $itot = floor($ire);
   
   # my $iloop = 1;
   # my $ib = 0;
   # my $nJobs = 0;
   # while ($iloop le $itot){
   #    $nJobs = $nJobs + $nproc;
   # 
   #    my @jobArray = ();
   #    for (my $i = $ib; $i < $nJobs ; $i++) {
   #        #print $fh "Running job $mopac_jobs[$i]\n";
   #        my $index = 
   #        my $proc = AsyncTimeout->new(sub {run_mopac($mopac_jobs[$i])}, 3600) or die;
   #        push @jobArray, $proc;
   #        push @status, 0;
   #    }
   # 
   #    my $res = 0;
   # 
   #    my $tt = 0;
   #    while ($tt < $limit) {
   #        for (my $i = 0; $i < $nproc ; $i++) {
   #    	   my $proc = $jobArray[$i];
   # 	   if ($proc->ready){ 
   # 	       $status[$i] = 1;
   # 	   }
   #        }
   #        foreach my $aa (@status) {$res = $res + $aa;}
   #        if ($res == $nproc) {
   #           $tt = $limit;
   #        }
   #        sleep 1;
   #        $tt++; 
   #    }
   # 
   #    my $index = 1;
   #    foreach my $proc (@jobArray) {
   #        my $e;
   #        if ($e = $proc->error) {
   #    	   print "Something went wrong in job $index: $e\n";
   #        }
   #        if ($proc->result eq "Timed out\n") {
   #    	   print "Job $index: ", $proc->result;
   #        }
   #        $index++;
   #        undef $proc;
   #    }
   #    $ib = $ib + $nproc;
   #    $iloop = $iloop + 1;
   # }
   # if ($nproc ne $nchrom){
   #    my @jobArray = ();
   #    my $rest = $nchrom - ($nproc * $itot);
   #    $nJobs = $nJobs + $rest;
   #    for (my $i = $ib; $i < $nJobs ; $i++) {
   #        my $proc = AsyncTimeout->new(sub {run_mopac($mopac_jobs[$i])}, 3600) or die;
   #        push @jobArray, $proc;
   #        push @status, 0;
   #    }
   
   #    my $res = 0;
   
   #    my $tt = 0;
   #    while ($tt < $limit) {
   #        for (my $i = $ib; $i < $nJobs ; $i++) {
   #    	      my $proc = $jobArray[$i];
   #            if ($proc->ready){ 
   #                $status[$i] = 1;
   #            }
   #        }
   #        foreach my $aa (@status) {$res = $res + $aa;}
   #        if ($res == $nchrom) {
   #           $tt = $limit;
   #        }
   #        sleep 1;
   #        $tt++; 
   #    }
   
   #    my $index = 1;
   #    foreach my $proc (@jobArray) {
   #        my $e;
   #        if ($e = $proc->error) {
   #    	      print "Something went wrong in job $index: $e\n";
   #        }
   #        if ($proc->result eq "Timed out\n") {
   # 	      print "Job $index: ", $proc->result;
   #        }
   #        $index++;
   #        undef $proc;
   #    }
   # }
   #............................................................................
   
   # => Runs the EXASH program:.................................................
#   my $proc2 = AsyncTimeout->new(sub {run_exash()}, 3600) or die;
#   my $status2 = 0;
#   my $tt2 = 0; 
#   my $limit2 = 3600;
#   while ($tt2 < $limit2){
#       if ($proc2->ready){
#           $status2 = 1;
#        }
#        if ($status2 == 1){
#           $tt2 = $limit2;
#        }
#        #sleep 1;
#        system("sleep 0.1");
#        $tt2 = $tt2 + 0.1; 
#   } # It needs to waint until be finished because we need to copy the cioverlap
     # file in the next ssstep of this script.
   #............................................................................
   
   #............................................................................
#   copy("cioverlap.out", "cioverlap/")
#     or die "Couldn't copy 'cioverlap.out' ($!), stopped";
   # system("cp cioverlap.out cioverlap/");
   #............................................................................
}
   
#............................................................................
sub run_mopac_old {
   my $emop = $ENV{"MOPAC"};
   my ($file2run) = @_;
   my $start = localtime;
   system("$emop/mopac2002.x $file2run > mopac_tmp.out 2>&1 &");
}

sub run_mopac {
    my $emop = $ENV{"MOPAC"};
    my ($file2run) = @_;
    my $start = localtime;
    exec("$emop/mopac2002.x $file2run > mopac_tmp.out 2>&1");
}
#............................................................................

#............................................................................
sub run_nx2tnk{
   my $eexc = $ENV{"EXASH"};
   system("$eexc/nx2tnk &");
}
#............................................................................
 
#............................................................................
#sub run_exash{
#   my $eexc = $ENV{"EXASH"};
#   system("$eexc/exash < excinp &");
#}




return 1;
#............................................................................
