# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::InputGenerator::Item;

use strict;
use warnings;
use diagnostics;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT      = qw();
    @EXPORT_OK   = qw();
    %EXPORT_TAGS = ();
  }

sub new {
  my ($proto, $title, $action) = @_;
  my $class = ref($proto) || $proto;

  if (! defined $action) {
    $action = '';
  }

  if (! defined $title) {
    $title = '';
  }

  my $self = bless {
		    _title => $title,
		    _action => $action,
		   }, $class;

  return $self;
}


# Some accessor methods
sub title {
  my ($self, $title) = @_;
  $self->{_title} = $title if defined $title;
  return $self->{_title};
}

sub action {
  my ($self, $action) = @_;
  $self->{_action} = $action if defined $action;
  return $self->{_action};
}



1;
