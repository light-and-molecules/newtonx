# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Turbomole;

use strict;
use warnings;
use diagnostics;

use File::Copy qw( copy move );
use File::Path qw( remove_tree );
use File::Copy::Recursive qw( fcopy dircopy dirmove );

use Data::Dumper qw( Dumper );


BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( tm_read_control tm_print_control       tm_find_keyword
                     tm_get_value    tm_change_value        tm_delete_keyword
                     tm_get_orbspace tm_make_double_control tm_make_double_mos
                     tm_create_double_molecule_input
                     tm_copy_inputs  tm_update_input_dft        tm_clean_dir
                     tm_prepare_cioverlap tm_update_input_ricc2 tm_update_input_adc2
                  );
}


sub tm_read_control {
    # Read a 'control' file into an array.
    #
    # The 'control' file is read and split using the '$' delimiter. Each
    # element of the array is a line of characters, where the first word is
    # '$key', the rest of the line corresponds to all elements from this
    # "section" in the control file.

    my ($control_file) = @_;
    my @vector;
    my $i;

    open(my $fh, '<:encoding(UTF-8)', $control_file)
        or die "Couldn't open $control_file ($!), stopped\n";
    $i = -1;
    while (<$fh>) {
        if (/\$/) {
            $i++;
            $vector[$i] = "$_";
        }
        else {
            $vector[$i] = $vector[$i].$_;
        }
    }
    close($fh);

    return \@vector;
}


sub tm_print_control {
    # Print a control file from input array.

    my ($array, $filename) = @_;

    open(my $fh, '>:encoding(UTF-8)', "$filename")
        or die "Couldn't open $filename for writing ($!), stopped";
    foreach my $ele (@$array) {
        chomp $ele;
        print $fh "$ele\n";
    }
    close($fh);
}


sub tm_find_keyword {
    # Search a keyword in an array from control file.
    #
    # The array is supposed to have been constructed by the tm_read_control
    # function. The function returns the index of the element if it was found,
    # and -1 else.
    #
    # usage:
    #    my @content = tm_read_control('control');
    #    my $is_found = tm_find_keyword('key', @content);

    my ($key, $array) = @_;

    my $found = -1;
    my $i = 0;
    foreach my $ele (@$array) {
        my @tmp = split /\s+/, $ele;
        if ($tmp[0] =~ /\b$key\b/i) {
            $found = $i;
            last;
        }
        $i++;
    }

    return $found;
}


sub tm_get_value {
    # Find the value of given keyword in the control array.
    #
    # The control file must have been read into an array before hand. The
    # function returns the value corresponding to the keyword if it is found
    # in the control array, and 'not found' otherwise.

    my ($key, $array) = @_;
    my $value = 'not found';

    my $found = tm_find_keyword($key, $array);
    if ($found != -1) {
        my $tmp = @{$array}[$found];
        $tmp =~ s/\$$key//;
        $value = @{$array}[$found];
        chomp($value);
        $value =~ s/^\s+//;
        $value =~ s/\s+$//;
    }

    return $value;
}


sub tm_change_value {
    # Change the value of the given key in the control array.
    #
    my ($key, $value, $array) = @_;

    my $found = tm_find_keyword($key, $array);

    if ($found == -1) {
        my $last = @{$array}[-1];
        pop @$array;
        push @$array, "\$$key $value";
        push @$array, $last;
    }
    else {
        @{$array}[$found] = "\$$key $value";
    }
}


sub tm_delete_keyword {
    # Remove the selected keyword from the control file.
    #
    my ($key, $array);
    my $found = tm_find_keyword($key, $array);
    if ($found != -1) {
        delete @{$array}[$found];
    }
}


sub tm_get_orbspace {
    # Extract orbital information from the control file.
    #
    # The function parses 'control' and returns the number of atoms, the
    # number of occupied orbitals, and the total number of basis functions.
    #
    # usage:
    #    my ($nat, $nocc, $nbas) = tm_get_orbspace('control');

    my ($control) = @_;

    my ($nat, $nocc, $nbas);

    open(my $fh, "<:encoding(UTF-8)", $control)
        or die "Couldn't open $control ($!), stopped ";
    while (<$fh>) {
        if (/natoms/) {
            my @tmp = split /=/;
            $nat = $tmp[1];
        }
        if (/nbf\(AO\)/) {
            my @tmp = split /=/;
            $nbas = $tmp[1];
        }
        if (/closed shells/) {
            $_ = <$fh>;
            my @tmp = split /-/;
            @tmp = split /\s+/, $tmp[1];
            $nocc = $tmp[0];
        }
    }
    close($fh);

    return ($nat, $nocc, $nbas);
}


sub tm_make_double_control {
    # Create a control file for the AO overlap calculation.
    #
    # This function creates a control file corresponding to a molecule that
    # contains twice the system of interest, suitable for the overlap
    # computation between subsequent steps of the dynamics. This is used for
    # both cioverlap and the overlap diagnostics.

    my ($control_file, $new_control, $nat) = @_;

    # Stores the lines of the section of control to be modified
    my %lines;

    my @newat; # Store the list of "created" atoms;
    my @newgroup; # Store the list of "created" group;

    my $ri = "false";
    if (-e 'auxbasis') {
        $ri = "true";
    }

    open(my $fh, "<:encoding(UTF-8)", $control_file)
        or die "Couldn't open $control_file "
               . "for creating double molecule run ($!), stopped ";
    while (<$fh>) {

        my $l1 = $_;

        if ($l1 =~ /\$atoms/) {
            $l1 = <$fh>;
            $lines{"atoms"} = "";
            while ($l1 !~ /\$/) {
                my $l2 = <$fh>;
                if ($ri eq "true") {
                    my $l3 = <$fh>;
                    $l2 = $l2.$l3;
                }
                my @g = split /\s+/, $l1;

                # assuming there is no blanks in atoms ranges
                my @groups = split /,/, $g[1];

                my $j = 0;
                foreach (@groups) {
                    my @ats=split(/-/,$_);
                    my $i=0;
                    foreach (@ats) {
                        $newat[$i] = $_ + $nat;
                        $i++;
                    }
                    if (@ats > 1) {
                        $newgroup[$j]=join("-",@newat);
                    } elsif (@ats == 1) {
                        $newgroup[$j] = $newat[0];
                    }
                    $j++;
                }
                my $newg = join(",", @newgroup);

                $g[1] = $g[1].",".$newg;
                my $newl1 = "$g[0] $g[1]               $g[2]";
                $lines{"atoms"} = $lines{"atoms"}.$newl1."\n".$l2;
                $l1 = <$fh>;
            }
        }
        if ($l1 =~ /\$rundimensions/) {
            $l1 = <$fh>;
            $lines{"rundimensions"}="";
            while ($l1 !~ /\$/) {
                my @g = split /\=/, $l1;
                chomp $g[1];
                $g[1] =~ s/^\s*//;
                $g[1] =~ s/\s*$//;
                if ($g[0] =~ /fock,dens/i) {
                    $g[1] = $g[1]*4;
                }
                if ($g[0] =~ /natoms/i) {
                    $g[1] = $g[1]*2;
                }
                if ($g[0] =~ /nshell/i) {
                    $g[1] = $g[1]*2;
                }
                if ($g[0] =~ /nbf/i) {
                    $g[1] = $g[1]*2;
                }
                if ($g[0] =~ /trafo/i) {
                    $g[1] = $g[1]*2;
                }
                $lines{"rundimensions"} =
                    $lines{"rundimensions"}.$g[0]."=".$g[1]."\n";
                $l1 = <$fh>;
            }
        }
        if ($l1 =~ /\$closed shells/) {
            $l1=<$fh>;
            chomp($l1);
            $l1 =~ s/^\s*//;
            $l1 =~ s/\s*$//;
            my @g = split /\s+/, $l1;
            my @s = split /-/, $g[1];
            my $new_s = $s[1]*2;
            my $new_g = "$s[0]-$new_s";
            $l1 =~ s/$g[1]/$new_g/;
            $lines{"closed shells"} = "\n".$l1;
        }
    }
    close($fh);

    # change control file
    chomp($lines{"atoms"});
    chomp($lines{"rundimensions"});
    $lines{"atoms"}="\n".$lines{"atoms"};

    my $control = tm_read_control($control_file);
    tm_change_value("atoms", $lines{"atoms"}, $control);
    tm_change_value("rundimensions", $lines{"rundimensions"}, $control);
    tm_change_value("closed shells", $lines{"closed shells"}, $control);
    tm_change_value("intsdebug", "  sao", $control);
    tm_change_value("scfiterlimit", "  0", $control);


    tm_print_control($control, $new_control);
}


sub tm_make_double_mos {
    # Create a sample 'mos' file for the overlap computation.
    my ($nao, $nocc, $filename) = @_;

    my $coeff_false = "0.10000000000000D+00";
    my $e_false = 0.0;
    $nao = 2*$nao;
    $nocc = 2*$nocc;

    open(my $fh, '>:encoding(UTF-8)', $filename)
        or die "Couldn't open $filename ($!), stopped";
    print $fh "\$scfmo   expanded   format(4d20.14)\n";
    for (my $i=1; $i<=$nocc; $i++) {
        printf $fh "%6d  a      eigenvalue=%16.14fD-00   nsaos=%d\n", $i, $e_false, $nao;
        my $j = 1;
        my $jaux = 1;
        while ($j <= $nao) {
            print $fh "$coeff_false";
            if ($j == $nao) {
                print $fh "\n";
            }
            if (($jaux == 4) and ($j != $nao)) {
                print $fh "\n";
                $jaux = 0;
            }
            $j++;
            $jaux++;
        }
    }
    print $fh "\$end\n";
    close($fh);
}


sub tm_create_double_molecule_input {
    # Create all input required for the overlap computation.
    #
    my ($nat, $nocc, $nbas) = tm_get_orbspace('control');
    tm_make_double_control('control', 'double_control', $nat);
    tm_make_double_mos($nbas, $nocc, 'double_mos');

    mkdir("double_molecule_input")
        or die "Couldn't create directory 'double_molecule_run' ($!), stopped";
    copy('double_control', 'double_molecule_input/control')
        or die "Couldn't copy 'double_control' ($!), stopped";
    copy('double_mos'    , 'double_molecule_input/mos'    )
        or die "Couldn't copy 'double_mos' ($!), stopped";
    copy('basis'         , 'double_molecule_input/basis'  )
        or die "Couldn't copy 'basis' ($!), stopped";
    if (-e 'auxbasis') {
        copy('auxbasis'     , 'double_molecule_input/auxbasis'  )
            or die "Couldn't copy 'auxbasis' ($!), stopped";
    }

    unlink('double_control');
    unlink('double_mos');
}


sub tm_update_input_dft {
    # Update the input for QM computation.
    #
    my ($nstatdyn, $nstat, $typedft) = @_;

    my @array = tm_read_control('control');

    if ($nstatdyn > 1) {
        tm_change_value("exopt", $nstatdyn - 1, @array);
        my $tmp = $nstat - 1;
        tm_change_value("soes", "all $tmp", @array);
    }

    if (($nstatdyn == 1) and ($nstat > 1)) {
        my $tmp = $nstat - 1;
        tm_change_value("soes", "all $tmp", @array);
    }

    tm_print_control(@array, 'control');
}


sub tm_update_input_ricc2 {
    # Update the TM input for ricc2 computations.
    #
    my ($nstatdyn, $nstat, $mult, $npre, $nstart) = @_;

    my @array = tm_read_control('control');

    my $state_info = '';
    my $nstatdyn_ex = $nstatdyn - 1;
    my $nstat_ex = $nstat - 1;
    my $ia = '';

    if ($mult == 1) {
        $ia = "a";
    }
    elsif ($mult == 3) {
        $ia = "a{3}";
    }

    if ($nstatdyn > 1) {
        $state_info = "\n irrep=a multiplicity=$mult nexc=$nstat_ex npre=$npre";
        $state_info = $state_info." nstart=$nstart \n exgrad states=($ia $nstatdyn_ex)\n";
        $state_info = $state_info."  spectrum states=all operators=diplen";
    } elsif ($nstatdyn == 1) {
        $state_info = "\n irrep=a multiplicity=$mult nexc=$nstat_ex npre=$npre";
        $state_info = $state_info." nstart=$nstart\n  spectrum states=all operators=diplen";
    }

    tm_change_value('excitations', "$state_info", @array);
    my $ricc2 = tm_get_value('ricc2', @array);
    my $test = 'geoopt';
    if ($ricc2 !~ /$test/) {
        my $ricc2_info = "\n cc2\n maxiter=300\n geoopt model=cc2";
        tm_change_value('ricc2', "$ricc2_info", @array);
    }

    tm_print_control(@array, 'control');
}


sub tm_update_input_adc2 {
    # Update the TM input for adc(2) computations.
    #
    my ($nstatdyn, $nstat, $mult, $npre, $nstart) = @_;

    my $state_info = '';
    my $nstatdyn_ex = $nstatdyn - 1;
    my $nstat_ex = $nstat - 1;
    my $ia = '';

    if ($mult == 1) {
        $ia = "a";
    }
    elsif ($mult == 3) {
        $ia = "a{3}";
    }

    if ($nstatdyn > 1) {
        my @array = tm_read_control('control');
        $state_info = "\n irrep=a multiplicity=$mult nexc=$nstat_ex npre=$npre";
        $state_info = $state_info." nstart=$nstart \n exgrad states=($ia $nstatdyn_ex)\n";
        $state_info = $state_info."  spectrum states=all operators=diplen";

        tm_change_value('excitations', "$state_info", @array);

        my $ricc2 = tm_get_value('ricc2', @array);
        my $test = 'geoopt';
        if ($ricc2 !~ /$test/) {
            my $ricc2_info = "\n adc2\n maxiter=300\n geoopt model=adc(2)";
            tm_change_value('ricc2', "$ricc2_info", @array);
        }
        tm_print_control(@array, 'control');
    }
    elsif ($nstatdyn == 1) {
        my @array = tm_read_control('control');
        tm_delete_keyword("excitations", @array);
        my $ricc2_info = "\n mp2\n maxiter=300\n geoopt model=mp2";
        tm_change_value('ricc2', "$ricc2_info", @array);
        tm_print_control(@array, 'control-mp2');

        @array = tm_read_control('control');
        my $state_info = "\n irrep=a multiplicity=$mult nexc=$nstat_ex npre=$npre nstart=$nstart\n";
        $state_info = $state_info."  spectrum states=all operators=diplen";
        tm_change_value('excitations', "$state_info", @array);
        $ricc2_info = "\n adc(2)\n maxiter=300";
        tm_change_value('ricc2', "$ricc2_info", @array);
        tm_print_control(@array, 'control-adc2');
    }
}


sub tm_copy_inputs {
    # Copy the inputs from the given folder into the current directory.
    #
    my ($input_folder) = @_;

    my @list = ('control', 'basis', 'mos');
    foreach my $ff (@list) {
        copy("$input_folder/$ff", "$ff")
            or die "Couldn't copy $ff ($!), stopped";
    }
    if (-e "$input_folder/auxbasis") {
        copy("$input_folder/auxbasis", "auxbasis")
            or die "Couldn't copy auxbasis ($!), stopped";
    }
}


sub tm_clean_dir {
    # Clean the working directory.
    #
    # Optionnally, the function also backs up the deleted files in a debug
    # directory.

    my ($debug, $step) = @_;

    my @backup_list = ('control', 'mos', 'basis', 'coord',
                       'grad.out', 'scf.out', 'gradient'
                      );
    if ($debug >= 2) {
        if (! -e "debug_turbo") {
            mkdir("debug_turbo")
                or die "Couldn't create 'debug_turbo' ($!), stopped";
        }
        my $db_folder = "debug_turbo/step_$step";
        if (-e "$db_folder") {
            # in this case it was probably created by the first run, and we
            # have a surface hopping.
            $db_folder = "$db_folder"."_after_hop";
        }
        mkdir("$db_folder")
            or die "Couldn't create $db_folder ($!), stopped";
        foreach my $ff (@backup_list) {
            if (-e "$ff") {
                copy("$ff", "$db_folder/$ff")
                    or die "Couldn't backup $ff ($!), stopped";
            }
        }
    }

    unlink('scf.out');
    unlink('grad.out');
    unlink('gradient');
    copy('mos', 'mos.old')
        or die "Couldn't copy mos as mos.old ($!), stopped";

    # For CC2 and / or ADC2 computations.
    system("rm -f CC* adc*cao");
    if (-e 'bin2matrix.out') {
        unlink('bin2matrix.out');
    }
}


sub tm_prepare_cioverlap {
    # Prepare the folder architecture for running cioverlap.
    #

    # The first time this is run, there is only cioverlap.old in the working
    # directory, containing only slaterfile and eivectors2.
    # if (-e "cioverlap.old/cioverlap.out") {
    #     remove_tree("cioverlap.old")
    #         or die "Couldn't delete 'cioverlap.old' ($!), stopped";
    # }
    #
    # if (-e "cioverlap") {
    #     dirmove("cioverlap", "cioverlap.old")
    #         or die "Couldn't rename 'cioverlap/' ($!), stopped";
    # }

    my $folder = "cioverlap";
    my $old_folder = $folder.".old";

    # Files required to run cioverlap
    my @files_to_copy = (
        ['cioverlap.input', ''],
        ['transmomin', ''],
        ['slatergen/slaterfile', 'slaterfile'],
        ['cioverlap.old/eivectors1', 'eivectors2'],
       );

    # The folder should already exist, as this script will
    # ALWAYS be called after run_cis_casida.pl
    # mkdir("$folder");

    foreach my $ff (@files_to_copy) {
        if (! "@{$ff}[1]" eq '') {
            copy("@{$ff}[0]", "$folder/@{$ff}[1]");
        } else {
            copy("@{$ff}[0]", "$folder/@{$ff}[0]");
        }
    }

    # If phases are found, copy them
    if (-e 'cioverlap.old/phases') {
        copy('cioverlap.old/phases', "$folder/phases.old")
            or die "Couldn't copy 'cioverlap.old/phases' ($!), stopped";
    }

    # Now do some cleanup
    my @files_to_remove = (
        'cis_casida.input',
        'cioverlap.input',
        'cis_slatergen.input',
        'merged_coord',
       );

    foreach my $ff (@files_to_remove) {
        if (-e "$ff") {
            unlink "$ff";
        }
    }
}


sub tm_finish_cioverlap {
    # Clean the working directory after running cioverlap.





    if (-e "cioverlap.old") {
        remove_tree("cioverlap.old")
            or die "Couldn't delete 'cioverlap.old' ($!), stopped";
    }
    dirmove("cioverlap", "cioverlap.old")
        or die "Couldn't rename 'cioverlap/' ($!), stopped";
}


return 1;
