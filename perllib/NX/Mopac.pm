# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Mopac;

use strict;
use warnings;
use diagnostics;
use File::Copy qw( copy );

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( calc_mop );
}

sub calc_mop {
   my ($step) = @_;

   my $emop = $ENV{"MOPAC"};	# MOPAC environment

   if ($step eq 0) {   
#      system("cp -rf JOB_NAD/* .");
      opendir my $dh, "JOB_NAD"
         or die "Couldn't open JOB_NAD/ ($!), stopped";
      my @files = grep {!/^\.\.?$/} readdir $dh;
      foreach my $ff (@files) {
         copy("JOB_NAD/$ff", "$ff")
            or die "Couldn't copy JOB_NAD/$ff ($!), stopped";
      }
      close($dh);
   }
   
   system("$emop/mopac2002.x mopac ");
}

return 1;


