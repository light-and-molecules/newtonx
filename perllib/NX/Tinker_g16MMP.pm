# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tinker_g16MMP;

use strict;
use warnings;
use diagnostics;

use File::Copy qw( copy move );
use File::Path qw( remove_tree );
use File::Copy::Recursive qw( fcopy dircopy dirmove );

use Data::Dumper qw( Dumper );


BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( tg16mmp_init_copy tg16mmp_clean_dir tg16mmp_update_input
                  );
}

sub tg16mmp_init_copy {
    my ($job_folder, $basename) = @_;
    copy("$job_folder/basis", "basis")
        or die "Could not find $job_folder/basis, stopped";
    if (-e "$job_folder/basis2") {
	copy("$job_folder/basis2", "basis2")
	    or die "Could not find $job_folder/basis, stopped";
    }
    copy("$job_folder/$basename.key", "tg16mmp.key")
        or die "Could not find $job_folder/$basename.key, stopped";
    #TODO check:
    #  * qmmm_in_file gaussian.com
    system("sed -i -e '/^\\s*qmmm_in_file.*/Id' tg16mmp.key");
    system("echo qmmm_in_file gaussian.com >> tg16mmp.key");
    #  * qmmm_out_file gaussian.log
    system("sed -i -e '/^\\s*qmmm_out_file.*/Id' tg16mmp.key");
    system("echo qmmm_out_file gaussian.log >> tg16mmp.key");

    copy("$job_folder/$basename.mmp", "MMPol.mmp")
        or die "Could not find $job_folder/$basename.mmp, stopped";
    copy("$job_folder/header.com", "header.com")
        or die "Could not find $job_folder/header.com, stopped";
    copy("$job_folder/footer.com", "footer.com") if -e "$job_folder/footer.com";
    
    #TODO check 
    #  * %chk=gaussian
    system("sed -i -e '/^\\s*%chk.*/Id' header.com");
    system("sed -i -e '1i\\%chk=gaussian' header.com");
    #  * %rwf=gaussian
    system("sed -i -e '/^\\s*%rwf.*/Id' header.com");
    system("sed -i -e '1i\\%rwf=gaussian' header.com");
}
sub tg16mmp_update_input {
    # Update the input for QM computation.
    #
    my ($job_folder, $basename, $nstatdyn, $nstat, $typedft) = @_;

    # print "Update Input $job_folder, $nstatdyn, $nstat, $typedft\n";
    $nstatdyn -= 1;
    $nstat -= 1;
    system("sed -i -e '/^\\s*qmmm_root.*/Id' tg16mmp.key");
    system("echo qmmm_root $nstatdyn >> tg16mmp.key");
    
    system("sed -i -e '/^\\s*qmmm_nstates.*/Id' tg16mmp.key");
    system("echo qmmm_nstates $nstat >> tg16mmp.key");

    open(my $fout, '>', "tg16mmp.xyz")
        or die "Could not open tg16mmp.xyz ($!), stopped";
    open(my $fin, "$job_folder/$basename.xyz")
        or die "Could not open $job_folder/$basename.xyz ($!), stopped";
    open(my $geom, "geom")
        or die "Could not open geom ($!), stopped";
    my @geom_lines = <$geom>;
    
    my $iline = 0;
    while (my $line = <$fin>){
        # Removes heading white chars
        $line =~  s/^\s+//;
        # ... and tailing newline.
        chomp($line);

        if ($iline == 0){
            print($fout "$line\n");
        }
        else{
            my $newcoord = $geom_lines[$iline-1];
            chomp($newcoord);
            $newcoord =~  s/^\s+//;
            my ($x, $y, $z) = split(/\s+/, $newcoord);
            
            my @tok = split(/\s+/, $line, 6);
            
            printf($fout "%5d %-4s %12.6f %12.6f %12.6f %s\n", 
                   $tok[0], $tok[1], $x*0.52918, $y*0.52918, $z*0.52918, $tok[5]);
        }
        $iline++;
    }

    close($fin);
    close($fout);
    close($geom);
}


sub tg16mmp_clean_dir {
    # Clean the working directory.
    #
    # Optionnally, the function also backs up the deleted/overwritten 
    # files in a debug directory.

    my ($debug, $step) = @_;
    # print "Clean Dir: $debug $step";

    my @backup_list = ('tg16mmp.xyz', 'tg16mmp.key', 'header.com',
                       'MMPol.mmp', 'gaussian.com', 'gaussian.chk', 
                       'gaussian.log', 'MMPol_Tinker', 'interface.dat',
                       'tinker.out'
                      );
    # These files are not usefoul for the next step.
    my @delete_list = ('tg16mmp.xyz', 'gaussian.com', 'gaussian.log', 
                       'interface.dat', 'tinker.out'
                      );
    
    if ($debug >= 2) {
        my $glob_db_folder = "debug_tg16mmp";
        my $db_folder = "debug_tg16mmp/step_$step";
        if (not -e "$glob_db_folder"){
            mkdir("$glob_db_folder") 
                 or die "Could not create $glob_db_folder ($!), stopped";
        }
        mkdir("$db_folder")
            or die "Couldn't created $db_folder ($!), stopped";
        foreach my $ff (@backup_list) {
            if (-e "$ff") {
                copy("$ff", "$db_folder/$ff")
                    or die "Couldn't backup $ff ($!), stopped";
            }
        }
    }
    
    foreach my $ff (@delete_list) {
        if (-e "$ff"){
            unlink("$ff");
        }
    }
}

return 1;
