# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tests::TestItem;

use strict;
use warnings;
use diagnostics;

use File::Path qw( make_path remove_tree );
use File::Copy qw ( copy );
use File::Copy::Recursive qw( dircopy rcopy );
use Cwd qw( getcwd );

use Data::Dumper;

# BEGIN {
#     use Exporter ();
#     use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
#     $VERSION = '0.01';
#     @ISA = qw( Exporter );
#     @EXPORT_OK = qw( );
# }

=head1 NAME

NX::Tests - Testing library for Newton-X

=head1 SYNOPSIS

  use NX::Tests::TestItem;
  my $test = NX::Tests->new( 'name',
			     'description',
			     'origin',
			     'mdexe');
  # Create the test folder, and copy inputs from selected $origin/inputs
  # folder.
  $test->prepare_run();

  # Run the test, compare the results to the outputs from
  # "$self->origin()/outputs""and sets $test->status
  $test->run();

  # Finally print the status of the test
  $test->print();

=head1 DESCRIPTION

This library defines a way to handle individual tests in Newton-X. An individual test
is simply a single trajectory, defined by its ``name`` (usually the name of the directory
it is taken from), a ``description`` (typically the content of a ``README`` file), an
``origin`` (the origin folder where to find the set of inputs and outputs), and the
command to execute the Newton-X ``mdexe``.

=head1 AUTHOR

=head1 COPYRIGHT

=cut

sub new {
    my ($proto, $name, $description, $origin, $mdexe, $eps_pop, $eps_en) = @_;
    my $class = ref($proto) || $proto;

    if (! defined $name) {
	$name = '';
    }

    if (! defined $description) {
	$description = '';
    }

    if (! defined $origin) {
	$origin = '';
    }

    if (! defined $mdexe) {
	$mdexe = '';
    }

    if (! defined $eps_pop) {
	$eps_pop = 0.0000001;
    }

    if (! defined $eps_en) {
	$eps_en = 0.0000001;
    }

    my $self = bless {
	_name => $name,
	_description => $description,
	_origin => $origin,
	_mdexe => $mdexe,
	_diagnostics => '',
	_eps_pop => $eps_pop,
	_eps_en => $eps_en,
	_status => -1,
    }, $class;

    return $self;
}

# Accessors
sub name {
    my ($self, $name) = @_;
    $self->{_name} = $name if defined $name;
    return $self->{_name};
}

sub description {
    my ($self, $description) = @_;
    $self->{_description} = $description if defined $description;
    return $self->{_description};
}

sub origin {
    my ($self, $origin) = @_;
    $self->{_origin} = $origin if defined $origin;
    return $self->{_origin};
}

sub status {
    my ($self, $status) = @_;
    $self->{_status} = $status if defined $status;
    return $self->{_status};
}

sub mdexe {
    my ($self, $mdexe) = @_;
    $self->{_mdexe} = $mdexe if defined $mdexe;
    return $self->{_mdexe};
}

sub diagnostics {
    my ($self, $diagnostics) = @_;

    my $temp = $self->{_diagnostics};
    $self->{_diagnostics} = $temp."$diagnostics"
	if defined $diagnostics;
    return $self->{_diagnostics};
}

sub eps_pop {
    my ($self, $eps_pop) = @_;
    $self->{_eps_pop} = "$self->{_eps_pop}".$eps_pop
	if defined $eps_pop;
    return $self->{_eps_pop};
}

sub eps_en {
    my ($self, $eps_en) = @_;
    $self->{_eps_en} = "$self->{_eps_en}".$eps_en
	if defined $eps_en;
    return $self->{_eps_en};
}


sub prepare_directory {
    my $self = shift;

    my $newdir = $self->name();
    if (-e "$newdir") {
	remove_tree($newdir)
	    or die "Couldn't delete existing directory '$newdir' ($!), stopped";
    }
    make_path($newdir)
	or die "Couldn't create directory '$newdir' ($!), stopped";

    my $input_folder = $self->origin()."/inputs/";

    opendir my $dh, $input_folder
	or die "Couldn't open directory '$input_folder' ($!), stopped";
    my @files = grep {!/^\.\.?$/} readdir $dh;
    close($dh);

    foreach my $f (@files) {
	my $ff = "$input_folder"."$f";
	rcopy("$ff", "$newdir/$f")
	    or die "Couldn't copy file '$ff' ($!), stopped";
	chmod 0755, "$newdir/$f";
    }
}


sub print {
    my $self = shift;

    my $status_str = '';
    if ($self->status() == 0) {
	$status_str = 'PASSED';
    } elsif ($self->status() == 1){
	$status_str = ' FAILED (DATA MISMATCH)';
    } elsif ($self->status() == 2){
	$status_str = ' FAILED (ABNORMAL TERMINATION)';
    } else {
	$status_str = ' PENDING';
    }

    my $name = $self->name();
    my $desc = $self->description();
    my $prtstr = "Test $name ($desc): "."$status_str\n";
    print STDOUT "$prtstr";

    if ($self->status() != -1) {
	my $diag = $self->diagnostics();
	print STDOUT $diag, "\n";
    }
}


sub run {
    my $self = shift;

    my $curdir = getcwd;
    chdir($self->name())
	or die "Couldn't change directory ($!), stopped";
    my $cmd = $self->mdexe();
    system("$cmd");

    my $location = $self->origin();
    my $ref_en = _read_data_file("$location/outputs/energies.dat");
    my $ref_pop = _read_data_file("$location/outputs/populations.dat");

    # Check if job has terminated normally. If not, status is set to 2.
    open(my $fh, '<:encoding(UTF-8)', 'md.out')
	or print STDOUT "Couldn't open md.out ($!) at";
    my $st0 = 1;
    while(<$fh>) {
	if (/Normal termination/) {
	    $st0 = 0;
	}
    }
    close($fh);
    if ($st0 != 0) {
	chdir ("$curdir");
	$self->status(2);
	return 2;
    }

    my $test_en = _read_data_file("RESULTS/energies.dat");
    my $test_pop = _read_data_file("RESULTS/populations.dat");
    my $st1 = $self->_get_and_report_mismatch('energies', $ref_en, $test_en, $self->eps_en());
    my $st2 = $self->_get_and_report_mismatch('populations', $ref_pop, $test_pop, $self->eps_pop());

    if ($st1 + $st2 != 0) {
	$self->status(1);
	chdir ("$curdir");
	return 1;
    }

    $self->status(0);
    chdir ("$curdir");
    return 0;
}


# Private routines
sub _read_data_file {
    # Read a txt file output from NX.
    #
    # The sub takes a file with the structure:
    #
    # time data1 data2 ...
    #
    # and returns an array composed of the elements of the lines (so an array
    # of arrays).

    my ($file) = @_;

    my @res;

    open(my $fh, '<:encoding(UTF-8)', "$file")
	or die "Couldn't open '$file' ($!), stopped";
    while(<$fh>) {
	# Skip the first line
	next if ($_ =~ m/^\s*\#/);
	next if $. == 1;

	chomp; s/^\s+//; s/\s+$//;
	my @line = split /\s+/, $_;
	push @res, \@line;
    }
    close($fh);

    return \@res;
}

sub _compare_by_element {
    # Compare two 2-dimensional Perl arrays.
    #
    # The two Perl tables are substracted, and the absolute values are
    # compared to ``eps``.  The sub returns the list of indices for which the
    # difference is larger than ``eps``.
    my ($arr1, $arr2, $eps) = @_;

    my @res;
    my @diff_line;
    my $iline = 0;

    foreach my $ele (@$arr1) {
	for (my $i=0; $i<@$ele; $i++){
	    my $diff = abs(@$ele[$i] - ${@$arr2[$iline]}[$i]);
	    if ($diff > $eps) {
		push @res, [$iline, $i, $diff];
	    }
	}
	$iline++;
    }

    return \@res;
}

sub _get_and_report_mismatch {
    # Compare the ``ref`` and ``test`` array and report if they don't match.
    #
    # ``value`` correspond to the type of things we are comparing
    # (i.e. 'energies' or 'populations'). The mismatch is reported if any
    # difference higher than ``eps`` is found. If it is the case, the status
    # is > 0. Else 0 is returned.
    my $self = shift;
    my ($value, $ref, $test, $eps) = @_;

    my $diff = _compare_by_element($ref, $test, $eps);
    my $status = 0;

    if (scalar(@$diff) > 0) {
	$self->diagnostics("\nMismatch found in $value:\n");
	$self->diagnostics(sprintf "%10s%10s%20s%20s%20s\n", 'Step', 'Field', 'ref', 'test', 'abs(ref - test)');
	foreach my $ll (@$diff) {
	    my $id = @$ll[1];
	    my $iline = @$ll[0];
	    my $diff = @$ll[2];
	    my $rr = ${@$ref[$iline]}[$id];
	    my $tt = ${@$test[$iline]}[$id];
	    $self->diagnostics(sprintf "%10d%10d%20.12f%20.12f%20.12f\n", $iline, $id, $rr, $tt, $diff);
	}
	$status = 1;
	$self->diagnostics("\n");
    }

    return $status;
}

1;
