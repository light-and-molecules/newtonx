# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tests::Main;

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use YAML::Tiny;

use NX::Tests::TestInterface;

use Pod::Usage;

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( parse_command_line print_test_header);
}

our $NXHOME = $ENV{NXHOME};


sub parse_command_line {

    my %result = ();
    my $program = 'all';
    my $which_tests = '';

    my $mdexe = "$NXHOME/bin/md";
    my $moldyn = undef;
    my $test_dir = './tests/';
    my $make_env = 0;
    
    GetOptions(
	'help' => \my $help,
	'man' => \my $man,
	'program|p=s' => \$program,
	'test_number|t=s' => \$which_tests,
	'moldyn|m=s' => \$moldyn,
	'mdexe=s' => \$mdexe,
	'dir=s' => \$test_dir,
	'make_env=i' => \$make_env,
	);

    pod2usage(-verbose => 1)  if ($help);
    pod2usage(-verbose => 2)  if ($man);

    # print STDOUT "MOLDYN = $result{moldyn}\n";
    if (! defined $result{moldyn}) {
	my $mdexe = $mdexe;
	# if (-f "$NXHOME/utils/moldyn") {
	#     $moldyn = "$NXHOME/utils/moldyn -mdexe $mdexe > md.out 2>&1";
	# } else {
	#     $moldyn = "$NXHOME/bin/moldyn -mdexe $mdexe > md.out 2>&1";
	# }
	$moldyn = "$mdexe > md.out 2>&1";
    }
    $result{moldyn} = $moldyn;
    $result{mdexe} = $mdexe;
    $result{test_dir} = $test_dir;
    $result{make_env} = $make_env;
    
    my @interfaces = ();
    if ($program eq 'all') {
	@interfaces = get_interface_list();
    } else {
	my @tmp = split /,/, $program;
	foreach my $ele (@tmp) {
	    push @interfaces, $ele;
	}
    }
    $result{program} = \@interfaces;

    my @tests_to_run = ();
    if (defined $which_tests) {
	my @tmp = split /\|/, $which_tests;
	foreach my $int (@tmp) {
	    my @tmp2 = split /,/, $int;
	    push @tests_to_run, \@tmp2;
	}
    }

    $result{which_tests} = \@tests_to_run;

    return \%result;
}


sub get_interface_list {
    my $topdir = "$NXHOME/examples/";
    if (! -d "$topdir") {
	$topdir = "$NXHOME/share/newtonx/examples/";
    }
    opendir my $dh, $topdir
	or die "Couldn't open $topdir ($!) , stopped";
    my @interfaces = ();

    while (readdir $dh) {
	next if (m/^\.\.?$/);
	next if (m/analysis/);

	push @interfaces, lc $_;
    }
    closedir $dh;
    return @interfaces;
}


sub get_default_test_config {
    my ($yaml_file) = @_;

    my $test_config = YAML::Tiny->read("$yaml_file");
    return $test_config;
}


sub print_test_header {
    my ($info) = @_;

    print STDOUT "=" x 80;
    print STDOUT "\n";
    print STDOUT "\t\t\tRUNNING NEWTON-X TESTSUITE\n";
    print STDOUT "=" x 80;
    print STDOUT "\n\n";

    print STDOUT "Information:\n";
    print STDOUT "  NXHOME:  ", $NXHOME, "\n";
    print STDOUT "  MDEXE:   ", $info->{mdexe}, "\n";
    print STDOUT "  MOLDYN:  ", $info->{moldyn}, "\n";
    print STDOUT "  DIR:     ", $info->{test_dir}, "\n";
    print STDOUT "  PROGRAMS:\n";
    foreach my $int (@{ $info->{program} }) {
	print STDOUT "    - ", uc $int, "\n";
    }

    print STDOUT "-" x 80;
    print STDOUT "\n";
}

1;
