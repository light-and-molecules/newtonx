# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tests::TestInterface;

use strict;
use warnings;
use diagnostics;

use YAML::Tiny;

use NX::Tests::TestItem;

use Data::Dumper qw ( Dumper );

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( );
}

=head1 NAME

NX::Tests::TestInterface - Test individual interfaces for Newton-X

=head1 SYNOPSIS

  use NX::Tests;
  my $test = NX::Tests::TestInterface->new( 'interface_name' );
  $test->run_all();

  # Another way to create an interface
  use NX::Tests;
  my $test = NX::Tests::TestInterface->read( 'datafile.yml', 'interface_name' );
  my @test_list = ( '01', '02' );
  $test->mdexe( 'path_to_md_executable' );
  $test->origin( 'path_to_examples_folder' );
  $test->populate_tests( @test_list );
  $test->run_tests();

= DESCRIPTION

This module handles testing a new build of Newton-X.

Testing a newly compiled version of Newton-X is done by computing
trajectories for some interfaces, and comparing the results (energies
and electronic populations) to a reference (each test should have its
tolerance for the energy and the population comparisons).
For instance, in Newton-X the tests are gathered in the directory
``$NXHOME/examples``, with the following structure:

  examples/
    |--GAUSSIAN/
    |  |-- 01-GAUSSIAN-TEST-01/
    |  | README
    |  | |--inputs/
    |  | | |-- geom.orig
    |  | | |-- veloc.orig
    |  | | |-- user_config.nml
    |  | | |-- JOB_AD/
    |  | |--oututs/
    |  | | |-- energies.dat
    |  | | |-- populations.dat
    |  |
    |  |-- 02-GAUSSIAN-TEST-02/
    |  |  | ...
    |  |  | ...
    |--COLUMBUS/
       |-- ...

The ``inputs`` folder contains all inputs required to run the dynamics with the ``moldyn``
script, while the ``output`` folder contains the list of energies and populations, as
produced by NX.  The ``use_txt_outputs`` parameter must be set to ``.true.`` for all inputs !

Testing an interface amounts to the following steps:

=over

=item Create the folder corresponding to the interface ;

=item For each selected test, create the corresponding directory and copy input files there ;

=item Loop over the tests, and run each trajectory ;

=item Compare the results obtained to the reference ;

=item Report the result.

=back

=head1 AUTHOR

Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>

=head1 COPYRIGHT

2022, Light and Molecules Group (Aix-Marseille Universite)

=cut

sub new {
    my ($proto, $name, $origin, $mdexe, $pre_run, $post_run) = @_;
    my $class = ref($proto) || $proto;

    if (! defined $name) {
	$name = '';
    }

    if (! defined $origin) {
	$origin = '';
    }

    if (! defined $mdexe) {
	$mdexe = '';
    }

    if (! defined $pre_run) {
	$pre_run = '';
    }

    if (! defined $post_run) {
	$pre_run = '';
    }

    my $self = bless {
	_name => $name,
	_ntests => 0,
	_origin => $origin,
	_mdexe => $mdexe,
	_pre_run => $pre_run,
	_post_run => $post_run,
	_env => [ @_ ],
	_commands => [ @_ ],
	_test_list => [ @_ ],
	_status => 0,
    }, $class;

    return $self;
}


sub read {
    my ($proto, $file_name, $interface) = @_;
    my $class = ref($proto) || $proto;

    my $processed = YAML::Tiny->read($file_name);

    my @env = ();
    my @commands = ();
    my @pre_run = ();
    my @post_run = ();

    if (defined @{ $processed->[0]->{$interface}->{environment} }[0]) {
	@env = @{ $processed->[0]->{$interface}->{environment} };
    }

    if (defined @{ $processed->[0]->{$interface}->{commands} }[0]) {
	@commands = @{ $processed->[0]->{$interface}->{commands} };
    }

    if (defined @{ $processed->[0]->{$interface}->{pre_run} }[0]) {
	@pre_run = @{ $processed->[0]->{$interface}->{pre_run} };
    }

    if (defined @{ $processed->[0]->{$interface}->{post_run} }[0]) {
	@post_run = @{ $processed->[0]->{$interface}->{post_run} };
    }

    my $self = bless {
	_name => $interface,
	_ntests => 0,
	_origin => '',
	_mdexe => '',
	_pre_run => \@pre_run,
	_post_run => \@post_run,
	_env => \@env,
	_commands => \@commands,
	_test_list => [],
	_status => 0,
    }, $class;

    return $self;
}


# Accessors
sub name {
    my ($self, $name) = @_;
    $self->{_name} = $name if defined $name;
    return $self->{_name};
}

sub status {
    my ($self, $status) = @_;
    $self->{_status} = $status if defined $status;
    return $self->{_status};
}

sub ntests {
    my ($self, $ntests) = @_;
    $self->{_ntests} = $ntests if defined $ntests;
    return $self->{_ntests};
}


sub origin {
    my ($self, $origin) = @_;
    $self->{_origin} = $origin if defined $origin;
    return $self->{_origin};
}

sub mdexe {
    my ($self, $mdexe) = @_;
    $self->{_mdexe} = $mdexe if defined $mdexe;
    return $self->{_mdexe};
}

sub set_test_list {
    my ($self, @test_list) = @_;
    $self->{_test_list} = \@test_list;
}

sub get_test_list {
    my ($self) = @_;
    my @tests = @{ $self->{_test_list} };
    wantarray ? @tests : \@tests;
}

sub set_env {
    my ($self, @env) = @_;
    $self->{_env} = \@env;
}

sub get_env {
    my ($self) = @_;
    my @env = @{ $self->{_env} };
    wantarray ? @env : \@env;
}

sub set_commands {
    my ($self, @commands) = @_;
    $self->{_commands} = \@commands;
}

sub get_commands {
    my ($self) = @_;
    my @commands = @{ $self->{_commands} };
    wantarray ? @commands : \@commands;
}

sub set_pre_run {
    my ($self, @commands) = @_;
    $self->{_pre_run} = \@commands;
}

sub get_pre_run {
    my ($self) = @_;
    my @pre_run = @{ $self->{_pre_run} };
    wantarray ? @pre_run : \@pre_run;
}

sub set_post_run {
    my ($self, @commands) = @_;
    $self->{_post_run} = \@commands;
}

sub get_post_run {
    my ($self) = @_;
    my @post_run = @{ $self->{_post_run} };
    wantarray ? @post_run : \@post_run;
}


sub get_full_test_list {
    my ($self) = @_;

    opendir my $dh, $self->origin()
	or die "Couldn't open directory ", $self->origin(), " ($!), stopped";
    my @files = grep {!/^\.\.?$/} readdir $dh;
    close($dh);

    return @files;
}


sub select_tests {
    my ($self, $full_list, $select_tests) = @_;

    my @selected = ();
    foreach my $ff (@$full_list) {
	foreach my $ss (@$select_tests) {
	    if ($ff =~ m/$ss/) {
		push @selected, $ff;
	    }
	}
    }

    return @selected;
}


sub populate_tests {
    my ($self, $selected_list) = @_;

    my @dir_to_test = $self->get_full_test_list();

    if (defined $selected_list) {
	@dir_to_test = $self->select_tests(\@dir_to_test, $selected_list);
    }

    my @test_list = ();
    foreach my $test (sort @dir_to_test) {
	# Increment number of tests
	$self->{_ntests}++;

	# Origin folder of each test: interface_origin/test/
	my $origin = $self->origin()."/$test";

	# The name is simply the name of the folder
	my $name = $test;

	# The description is taken from interface_origin/test/README
	open(my $fh, '<:encoding(UTF-8)', "$origin/README")
	    or die "Couldn't open $origin/README ($!), stopped";
	my $description = '';
	while (<$fh>) {
	    chomp;
	    $description .= "$_";
	}

	# And the mdexe is simply transfered from the interface to the test
	my $mdexe = $self->mdexe();

	my $testitem = NX::Tests::TestItem->new(
	    $name,
	    $description,
	    $origin,
	    $mdexe
	    );
	push @test_list, $testitem;
    }
    $self->set_test_list(@test_list);
}


sub print_header {
    my $self = shift;
    print STDOUT "." x 80;
    print STDOUT "\nStarting tests for interface ", uc $self->name(), "\n";
    print STDOUT "Total number of tests: ", $self->ntests(), "\n";
    print STDOUT "\n";

    print STDOUT "  Executable used: ", $self->mdexe(), "\n";
    print STDOUT "  Inputs from    : ", $self->origin(), "\n";
    if (defined @{$self->get_env}[0]) {
	print STDOUT "  \nEnvironments:\n";
	foreach my $env ($self->get_env) {
	    my $env_str = '';
	    if (defined $ENV{$env}) {
		$env_str = $ENV{$env};
	    }
	    print STDOUT "  $env: ", $env_str, "\n";
	}
    }
    print STDOUT "\n";
}


sub check_env {
    my $self = shift;

    my @undefined = ();
    foreach my $env ( $self->get_env ) {
	if ((! defined $ENV{$env}) or ($ENV{$env} eq '')) {
	    $self->status(77);
	    push @undefined, $env;
	}
    }

    return \@undefined;
}


sub check_commands {
    my $self = shift;

    my $status = 0;
    my @not_found = ();
    foreach my $cmd ( $self->get_commands ) {
	system("$cmd"." > /dev/null 2>&1");
	if ($? >> 8 == 127) {
	    $self->status(77);
	    push @not_found, $cmd;
	}
    }

    if (-e 'foo.inp') {
	unlink 'foo.inp';
    }

    return \@not_found;
}


sub prepare_run {
    my $self = shift;

    foreach my $cmd ($self->get_pre_run ) {
	print STDOUT "  Executing $cmd ... ";
	system("$cmd > /dev/null 2>&1");
	if ($? != 0) {
	    $self->status($?);
	    print STDOUT "\nCommand '$cmd' FAILED !";
	    return;
	}
	print STDOUT " done !\n";
    }
}


sub run_tests {
    my ($self) = @_;

    foreach my $t ($self->get_test_list) {
	$t->prepare_directory();
	$t->run();
	$t->print();
	if ($t->status != 0) {
	    $self->status($t->status);
	}
    }
}


sub finish_run {
    my $self = shift;

    foreach my $cmd ($self->get_post_run ) {
	system("$cmd > /dev/null 2>&1");
	if ($? != 0) {
	    $self->status($?);
	    print STDOUT "Command '$cmd' FAILED !";
	    return;
	}
    }
}


sub check_status {
    my $self = shift;
    my ($message, $list) = @_;

    if ($self->status != 0) {
	print STDOUT "\n$message\n";
	if (defined $list) {
	    foreach my $e (@$list) {
		print STDOUT "  $e\n";
	    }
	}
	exit $self->status;
    }
}

1;
