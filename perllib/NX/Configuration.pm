# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Configuration;

use strict;
use warnings;
use diagnostics;

use YAML::Tiny;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT      = qw();
    @EXPORT_OK   = qw();
    %EXPORT_TAGS = ();
}


sub new {
  my ($proto, $file_name) = @_;
  my $class = ref($proto) || $proto;

  my $self = YAML::Tiny->read($file_name) or
      die "Could not process '$file_name' $!, stopped";

  bless $self, $class;

  return $self;
}

sub section {
  my ($self, $section) = @_;
  die "'$section' is not part of Newton-X configuration, stopped" unless
    grep /$section/, keys(%{$self->[0]});
  return $self->[0]->{$section};
}

sub value {
  my ($self, $section, $keyword, $value) = @_;
  my $subconfig = $self->section($section);
  die "'$keyword' is not part of '$section' in Newton-X configuration, stopped" unless
    grep /$keyword/, keys(%{$subconfig});
  $subconfig->{$keyword}->{value} = $value if defined $value;
  return $subconfig->{$keyword}->{value};
}


sub description {
  my ($self, $section, $keyword, $description) = @_;
  my $subconfig = $self->section($section);
  die "'$keyword' is not part of '$section' in Newton-X configuration, stopped" unless
    grep /$keyword/, keys(%{$subconfig});
  $subconfig->{$keyword}->{description} = $description if defined $description;
  return $subconfig->{$keyword}->{description};
}

sub extended_description {
  my ($self, $section, $keyword, $extended_description) = @_;
  my $subconfig = $self->section($section);
  die "'$keyword' is not part of '$section' in Newton-X configuration, stopped" unless
    grep /$keyword/, keys(%{$subconfig});
  $subconfig->{$keyword}->{extended_description} = $extended_description if defined $extended_description;
  return $subconfig->{$keyword}->{extended_description};
}


sub options {
  my ($self, $section, $keyword, $options) = @_;
  my $subconfig = $self->section($section);
  die "'$keyword' is not part of '$section' in Newton-X configuration, stopped" unless
    grep /$keyword/, keys(%{$subconfig});
  $subconfig->{$keyword}->{options} = $options if defined $options;
  return $subconfig->{$keyword}->{options};
}

sub unit {
  my ($self, $section, $keyword, $unit) = @_;
  my $subconfig = $self->section($section);
  die "'$keyword' is not part of '$section' in Newton-X configuration, stopped" unless
    grep /$keyword/, keys(%{$subconfig});
  $subconfig->{$keyword}->{unit} = $unit if defined $unit;
  return $subconfig->{$keyword}->{unit};
}

# Import user config
sub import_nml {
  my ($self, $file_name) = @_;
  my $section = '';
  my $subconfig = '';

  # This is what gets returned by the function;
  my %read_parameters;

  open(my $fh, '<:encoding(utf-8)', $file_name) or
    die "Could not open '$file_name' $!, stopped";
  while (<$fh>) {
    chomp;

    # Skip blank line, comments and end of section
    next if $_ =~ m/^\s*$/;
    next if $_ =~ m/^\s*!/;
    next if $_ =~ m/^\s*\//;
    next if $_ =~ m/include_pair_size/;
    next if $_ =~ m/never_state_size/;
    next if $_ =~ m/ah_bonds_size/;
    next if $_ =~ m/bc_bonds_size/;

    # Start of a section
    if ($_ =~ m/^\s*\&/) {
      $_ =~ s/\s*\&//g;
      die "'$_' does not exist in Newton-X (line $.), stopped"
	unless grep /$_/, keys(%{$self->[0]});
      $section = $_;
      @read_parameters{$section} = [];
    }

    elsif ($_ =~ m/\w\s*=/) { # Match a line of type 'key = value ! comment'
      die "No section specified when arriving at line $. !"
	unless $section ne '';
      my ($key, $value, $comment) = split /=|!/;
      $key =~ s/^\s+|\s+$//g;
      $key = lc $key;
      $value =~ s/^\s+|\s+$//g;
      die "Could not find '$key' in configuration (line $.), stopped"
	unless grep /$key/, keys(%{$self->section($section)});
      $self->value($section, $key, $value);
      push @{$read_parameters{$section}}, $key;
    }

    elsif ($_ =~ m/\w\([0-9]*[:,]*\)\s*=/) {
	die "No section specified when arriving at line $. !"
	    unless $section ne '';
	my ($key, $value, $comment) = split /=|!/;
	$key =~ s/^\s+|\s+$//g;
	my @tmp = split /\(/, $key;
	$key = lc $tmp[0];

	$value =~ s/^\s+|\s+$//g;
	die "Could not find '$key' in configuration (line $.), stopped"
	    unless grep /$key/, keys(%{$self->section($section)});
	$self->value($section, $key, $value);
	push @{$read_parameters{$section}}, $key;
    }

    else {
      die "I cannot read this line (nr $.),: must be either of the form \'&section\' or \'key = value ! comment\'";
    }

  }
  close $fh;

  return \%read_parameters;
}


# Export the selected sections to the specified file in
# Fortran namelist format.
sub export_nml {
  my ($self, $sections, $file_name) = @_;
  my $max_space = 20; # Used for pretty printing

  open(my $fh, '>:encoding(utf-8)', $file_name)
    or die "Could not open '$file_name' $!, stopped";
  #foreach (@{ $sections }) {

  foreach my $sec (@{ $sections }) {
    #print $fh "&$_\n";
    print $fh "&$sec\n";
    my $subconfig = $self->section($sec);

    foreach my $key (sort(keys(%{$subconfig}))) {

      next if $key =~ m/include_pair_size/;
      next if $key =~ m/never_state_size/;
      next if $key =~ m/ah_bonds_size/;
      next if $key =~ m/bc_bonds_size/;

      my $val = $subconfig->{$key}->{value};
      my $comment = '';
      if (defined $subconfig->{$key}->{description}) {
	$comment = $subconfig->{$key}->{description};
      }
      my $used_space = length($key) + length($val) + 2;
      my $spacer = ' ';
      if ($max_space > $used_space) {
	$spacer = ' ' x ($max_space - $used_space);
      }

      # Add "" to values that are to be read as characters by Fortran
      my $type = '';
      if (defined $subconfig->{$key}->{must_be}) {
	$type = $subconfig->{$key}->{must_be};
	if ($type eq 'character') {
	    $val =~ s/\"(.*?)\"/$1/s;
	    $val =~ s/'(.*?)'/$1/s;
	    $val = "\"$val\"";
	}
      }

      # Some particular cases
      if ("$key" eq "never_state") {
	# Check for the size of the "never_state array"
	my @temp = split /,/, $val;
	my $never_state_size = @temp;
	print $fh "never_state_size = $never_state_size\n";
	# $key = $key."($never_state_size)";
      }
      if ("$key" eq "include_pair") {
	# Check for the size of the "include_pair array"
	my @temp = split /,/, $val;
	my $include_pair_size = @temp;
	print $fh "include_pair_size = $include_pair_size\n";
	# $key = $key."($include_pair_size)";
      }
      if ("$key" eq "ah_bonds") {
	# Check for the size of the "ah_bonds array"
	my @temp = split /,/, $val;
	my $ah_bonds_size = @temp;
	print $fh "ah_bonds_size = $ah_bonds_size\n";
	# $key = $key."($ah_bonds_size)";
      }
      if ("$key" eq "bc_bonds") {
	# Check for the size of the "ah_bonds array"
	my @temp = split /,/, $val;
	my $bc_bonds_size = @temp;
	print $fh "bc_bonds_size = $bc_bonds_size\n";
	# $key = $key."($bc_bonds_size)";
      }
      if ("$key" eq "nat_array") {
	# Check for the size of the "ah_bonds array"
	my @temp = split /,/, $val;
	my $bc_bonds_size = @temp;
	# print $fh "nat_array_size = $bc_bonds_size\n";
	$key = $key."(:)";
	# $key = $key."($bc_bonds_size)";
      }
      if ("$key" eq "nstat") {
	if ("$sec" eq "exc_inp") {
	  # Check for the size of the "ah_bonds array"
	  my @temp = split /,/, $val;
	  my $bc_bonds_size = @temp;
	  # print $fh "nstat_size = $bc_bonds_size\n";
	  $key = $key."(:)";
	  # $key = $key."($bc_bonds_size)";
	}
      }

      print $fh "$key = $val $spacer! $comment\n";
    }

    print $fh "/\n\n";
  }
  close $fh;
  return 0;
}


sub export_subsection_of_config {
  my ($self, $file_name, $section, $keys) = @_;
  my $max_space = 20;

  my $subconfig = $self->section($section);
  open(my $fh, '>>encoding(UTF-8)', $file_name) or die "Could not open '$file_name' $!, stopped";
  print $fh "&$section\n";
  foreach my $k (@$keys) {
      my $val = $self->value($section, $k);
      if (defined $subconfig->{$k}->{must_be}) {
	  my $type = $subconfig->{$k}->{must_be};
	  if ($type eq 'character') {
	      $val =~ s/\"(.*?)\"/$1/s;
	      $val =~ s/'(.*?)'/$1/s;
	      $val = "\"$val\"";
	  }
      }
      my $comment = $subconfig->{$k}->{description};
      my $used_space = length($k) + length($val) + 3;
      my $spacer = ' ';
      if ($max_space > $used_space) {
	$spacer = ' ' x ($max_space - $used_space);
      }
      print $fh "$k = $val $spacer! $comment\n";
  }
  print $fh "/\n";
  close $fh;

  return 0;
}


sub add {
  my ($self, $section, $key, $value) = @_;
  my $subconfig = $self->section($section);
  $subconfig->{$key}->{value} = $value;

  return 0;
}




#################### main pod documentation begin ###################
## Below is the stub of documentation for your module.
## You better edit it!


=head1 NAME

NX::Configuration - Configuration handling for Newton-X

=head1 SYNOPSIS

Two scripts are provided in the distribtion.

``nxinp`` is an interactive input generator for MD simulations. It currently supports
Turbomole and Spin-Boson Hamiltonian model. The script creates a ``user_config.nml`` file,
that contains the parameters that were modified by the user.

``moldyn`` uses the information contained in ``user_config.pl`` to write the
complete configuration for the main ``md`` program. For now, the two programs have to be
called SEPARATELY !

=head1 DESCRIPTION

This module simplifies the configuration file creation for the Newton-X
program.  It is designed to be used by the ``moldyn`` and ``nxinp`` scripts
also provided.

The module NX::Configuration defines the general configuration object, with its
own set of methods, with the aim of being able to convert this object into a
Fortran namelist that the main ``md`` module can read and understand.  The main
configuration file is in the YAML format and can be found under the ``data`` folder.
YAML was chosen for this general configuration file because it makes easy to store,
for each element of the configuration, a set of default values, description, possible
options, and  to specify functions to check the given value, or to assign a default
value based on other configuration components (see below).

For now these sections are used and tested, although other sections are present
as a legacy from older code:

=over

=item * ``nxconfig``: general configuration for the computation, that is intended to be read
in a ``nx_config_t`` object in Fortran;

=item * ``sh``: configuration of the surface hopping routines, intended to be mapped to
a ``nx_sh_t`` object;

=item * ``turbomole``: configuration for Turbomole, intended to be mapped to ``nx_qm_t``;

=item * ``analytical``: kept for legacy reason, it is currently only used for not
overcrowding the ``prog`` section;

=item * ``tinker_g16mmp``: configuration for Tinker-Gaussian QM/MMPol program, intended
to be mapped on nx_qm_t;

=item * ``tinker_mndo``: configuration for Tinker-MNDO program, intended
to be mapped on nx_qm_t;

=item * ``sbh``: configuration for the Spin-Boson Hamiltonian model;

=item * ``cioverlap``: configuration for the cioverlap routines (the presence of this
keyword in the configuration file will trigger the execution of cioverlap in the
main Fortran code).

=back

=head1 INSTALLATION

The following modules are required, and can be installed through CPAN.

=over

=item * ``YAML::Tiny``;

=item * ``Math::VectorReal``.

=back

On Ubuntu the ``cpanminus`` package provides the ``cpanm`` commandm that can be used
like this:

  cpanm YAML::Tiny

to install YAML::Tiny.

It is advised to install NX::Configuration in your home directory, for instance in $HOME/perl5.
To do so you have to set the $PERL5LIB environment variable:

  export PERL5LIB=$PERL5LIB:$HOME/perl5/lib/perl5

and then install NX::Configuration with:

  perl Makefile.PL install_base=~/perl5
  make
  make install

You should then be able to use the ``nxinp`` and ``moldyn`` script provided in the script
folder, for instance by adding this folder to your ``PATH``.

=head1 BUGS

=over

=item * If the given geometry file has atoms in lower case or upper case the internal coordinates
will not be produced (this is due to the implementation in the ``Chemistry`` module). For instance,
atoms 'c', 'na', 'LI' are not understood, but 'C', 'Na' and 'Li' are !

=back


=head1 AUTHOR

    Baptiste Demoulin
    CPAN ID: MODAUTHOR
    Light and Molecules group
    baptiste.demoulin@univ-amu.fr
    http://a.galaxy.far.far.away/modules

=head1 COPYRIGHT

This program is free software licensed under the...

	The General Public License (GPL)
	Version 2, June 1991

The full text of the license can be found in the
LICENSE file included with this module.


=head1 SEE ALSO

nx_main(1).

=cut

#################### main pod documentation end ###################


1;
# The preceding line will help the module return a true value
