# Installation

## Pre-requisites

### Obtaining the code

The code can be obtained by downloading the selected release in the [release
page](https://gitlab.com/light-and-molecules/newtonx/-/releases) section, or by cloning
the this repository:

    git clone https://gitlab.com/light-and-molecules/newtonx
	
Please note that the released tarballs do not contain the sources for compiling the
`cioverlap` programs. Those programs are available as separate downloads on the [release
page](https://gitlab.com/light-and-molecules/newtonx/-/releases).

### Building tools

The preferred way of building Newton-X is by using
[`meson`](https://mesonbuild.com/index.html) and [`ninja`](https://ninja-build.org/).
Both are available as binary release from their respective repositories:
- [Here](https://github.com/ninja-build/ninja/releases) for `ninja` ;
- and [there](https://github.com/mesonbuild/meson/releases) for `meson`.

The install should work by adding the `ninja` and `meson.py` executables to your
`$PATH`. We advise to link `meson.py` as `meson` for easier usage, and for compatibility
with versions installed through a package manager or `pip`:

    ln -s meson.py meson

### Compiler and libraries

- Newton-X can (currently) **only** be compiled with fairly recent versions of GCC
  (versions above 10).

- BLAS and LAPACK libraries are also needed. Currently we support:
  - OpenBLAS
  - Intel MKL
  - Netlib versions (default)

- **Optional**:  [HDF5 libraries](https://portal.hdfgroup.org/display/support/Downloads)

### External programs (time-derivatives)

- The derivative couplings can be obtained through the state overlap matrix if they can't
  be derived from non-adiabatic coupling vectors (for instance in TD-DFT computations).
  We provide a set of static binaries in the [release page](https://gitlab.com/light-and-molecules/newtonx/-/releases).

- Newton-X will look for these program in a directory aliased by the `CIOVERLAP` environment:
  
        export CIOVERLAP=/path/to/cioverlap/binaries/

- The `cioverlap.od` binary provided is compiled without parallelization.  The source code for this program
  can be obtain through request at `baptiste.demoulin [at] univ-amu.fr`.
	
## Building Newton-X

- In this part we assume that Newton-X will be installed in `$HOME/softs/newtonx/`, with the
  source code in `/path/to/nx/source/`.

- Newton-X relies on the `NXHOME` environment variable for running. `NXHOME` should be set
  to the path where it is installed, so in our example:
    
        export =NXHOME=$HOME/softs/newtonx/=

- Building and compiling Newton-X can be done with `meson` and `ninja` with the following
  commands (adjusting the `-j` flag for your needs, the default is to use **all** CPU cores available):
  
        cd /path/to/nx/source/
		export CC=gcc FC=gfortran
		meson setup -Dprefix=$NXHOME build
		meson compile -C build -j 4
		meson install -C build
		
- HDF5 support is added with the `-Dhdf5=true` option on the `setup` command line (see below).

- For compiling with other Linear Algebra libraries, use the corresponding argument to the
  `setup` phase:
  - Intel MKL: `-Dlapack=mkl`
  - OpenBLAS: `-Dlapack=openblas`

- You can review all options available to the build system with `meson configure`, after
  you performed the `setup` step above.  You'll most likely be interested in the last
  section, `Project options`.
  
- The programs `nx_moldyn`, `nx_test` and `nx_restart` will be found in `$(prefix)/bin`, while the 
  other data (`examples` and `data` directories) will be found in `$(prefix)/share/newtonx/`.

### Testing

- A testsuite is available and can be run after build with `meson -C build test`.

- By default, only the tests for the analytical models will be done.  Others will simply be
  skipped (please refer to the manual for information about setting up other interfaces).

- All tests will run under `tests/` in the build directory.

- The test logfile is `meson-logs/testlog.txt` in the build directory.

- In case of failure, you can inspect the content of the dynamics logfile `md.log` in each individual
  test folder.
  
- Individual tests can also be performed with the `nx_test` program. To see how to use the program please
  refer to the help provided:
  
        nx_test --help


## HDF5 integration

- Newton-X can be built with support for HDF5 for producing its main output. You can enable this
  feature by setting `-Dhdf5=true` in the `setup` step.
  
- If the detection of HDF5 libraries fail, make sure that your environment
  `$LIBRARY_PATH` contains the directory to the HDF5 libraries.  You'll also need to add
  this directory to `$LD_LIBRARY_PATH`, as we'll need HDF5 at runtime too. 
  
- The headers (in the `include` folder of your HDF5 installation) will need to be in your
  `$CPATH` and `$C_INCLUDE_PATH`.
  
- If you obtained HDF5 with your package manager, these will be set automatically.

- The libraries can be either installed via the package manager or manually compiled. If you choose
  to install them with the package manager, please make sure to use the same `gfortran` compiler to
  compile Newton-X after, as `mod` files in Fortran are compiler specific !

### Ubuntu

- On Ubuntu you need find the `hdf5-tools` and `libhdf5-dev` packages:
    
        sudo apt-get install hdf5-tools libhdf5-dev
    

### Manual installation

- Newton-X will not make use of the parallel version of HDF5. So you just need to compile the serial
  version (you can use the parallel version as well of course).
    
        export HDF5=/path/to/install/directory
        ./configure --enable-fortran --prefix=$HDF5
        make && make install
		export LIBRARY_PATH="$HDF5/lib/":$LIBRARY_PATH
		export LD_LIBRARY_PATH="$HDF5/lib/":$LD_LIBRARY_PATH
		export CPATH="$HDF5/include/":$CPATH
		export PATH="$HDF5/bin/":$PATH

## Documentation

A manual is available in the `manual` folder. It contains a tutorial as well as a documentation
for the different interfaces and methods available.

The documentation is generated by [FORD](https://github.com/Fortran-FOSS-Programmers/ford), which can be installed using `pip`, and requires at
least `python 3.7`:

    pip install ford

From the top source directory, building the documentation is done with:

    ford -o doc doc.md

You can then open `doc/index.html` with a web browser.
